﻿using LW3.Huffman.Tree;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW3.Huffman.Compressor
{
    public class HuffmanMethod
    {
        public static Dictionary<char, uint> CreateDictionaryByText(string text)
        {
            return text.GroupBy(x => x).OrderBy(x => x.Key).ToDictionary(k => k.Key, e => (uint)e.Count());
        }

        public static Dictionary<char, string> ConvertDictionaryToAlphabetCode(Dictionary<char, uint> dictionary)
        {
            // конвертация в листья дерева
            var leaves = dictionary.Select(x => new TreeItem() { Value = x.Key.ToString(), Weight = x.Value }).OrderBy(x => x.Weight).ToList();
            var copyLeaves = leaves.Select(x => x.Clone()).ToList();

            do
            {
                var firstElem = leaves.ElementAt(0);                    // выдергиваем 2 элемента
                var secondElem = leaves.ElementAt(1);

                for (int i = 1; i >= 0; i--) leaves.RemoveAt(i);        // удаляем их из списка

                var condition = (firstElem.Weight < secondElem.Weight);
                var rigth = (condition) ? firstElem : secondElem;
                var left = (!condition) ? firstElem : secondElem;

                rigth.isRight = true;
                var treeNode = new TreeItem()                       // создаем новый узел на их основе
                {
                    Value = rigth.Value + left.Value,
                    Weight = rigth.Weight + left.Weight,
                    LeftBranch = left,
                    RightBranch = rigth
                };

                leaves.Add(treeNode);                               // вставлем узел обратно и сортируем
                leaves = leaves.OrderBy(x => x.Weight).ToList();

            } while (leaves.Count() != 1);

            var treeRoot = leaves[0];
            treeRoot.isRoot = true;

            // анализируем полученое дерево и формируем код алфавита
            return copyLeaves.ToDictionary(k => k.Value[0], e => treeRoot.GetCode(e.Value[0])); ;
        }

        public static byte[] ConvertAlphabetToBytes(Dictionary<char, string> alphabetCode)
        {
            var result = "";
            var tmp = alphabetCode.Select(x => $"{x.Value}|{x.Key}");
            for (var i = 0; i < tmp.Count(); i++)
            {
                result += tmp.ElementAt(i) + ';';
            }
            return Encoding.GetEncoding(1251).GetBytes($"{result}\n|||");
        }

        public static (Dictionary<string, char>, byte[]) SelectAlphabetAndText(byte[] file)
        {
            var allText = Encoding.GetEncoding(1251).GetString(file);
            var separator = new string[] { "\n|||" };
            var res = allText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            var skipByte = Encoding.GetEncoding(1251).GetBytes(res[0] + "\n|||").Count();
            var alphabet = res[0].Split(';').Where(x => !x.Equals("")).ToDictionary(k => k.Split('|')[0], v => v.Split('|')[1].Equals("") ? ';' : v.Split('|')[1][0]);
            var textBytes = file.Skip(skipByte).ToArray();
            return (alphabet, textBytes);
        }

        public static (byte[], int) ConvertTextToBytesByAlphabet(string text, Dictionary<char, string> alphabet)
        {
            var textSymbolsToCode = text.SelectMany(x => StringToBools(alphabet[x])).ToArray();
            var bits = new BitArray(textSymbolsToCode);
            var bytes = BitArrayToByteArray(bits);
            var diff = (bytes.Length * 8) - bits.Count;
            return (bytes, diff);
        }

        public static string ConvertBytesToTextByAlphabet(byte[] textBytes, Dictionary<string, char> alphabet, ProgressBar progress)
        {
            var diff = textBytes.Last();
            var bits = new BitArray(textBytes.Take(textBytes.Count() - 1).ToArray());
            var boolArr = new bool[bits.Count];
            bits.CopyTo(boolArr, 0);
            boolArr = boolArr.Take(bits.Count - diff).ToArray();

            var result = new string[(int)Math.Ceiling(boolArr.Count() / 10000.0)];
            var tmp1 = "";
            var tmp2 = "";

            progress.Maximum = boolArr.Count();
            progress.Minimum = 0;
            progress.Step = 1;
            progress.Visible = true;

            for (int i = 0, count = 0, index = 0; i < boolArr.Count(); i++, count++)
            {
                tmp1 += boolArr[i] ? "1" : "0";
                if (alphabet.ContainsKey(tmp1))
                {
                    tmp2 += alphabet[tmp1];
                    tmp1 = "";

                    if (count >= 10000 || boolArr.Count() == i + 1)
                    {
                        result[index++] = tmp2;
                        tmp2 = "";
                        count = 0;
                    }
                }
                progress.PerformStep();
            }
            progress.Visible = false;

            var res = "";
            foreach (var str in result) res += str;
            return res;
        }

        private static byte[] BitArrayToByteArray(BitArray bits)
        {
            byte[] ret = new byte[(bits.Length - 1) / 8 + 1];
            bits.CopyTo(ret, 0);
            return ret;
        }

        private static bool[] StringToBools(string code)
        {
            return code.Select(x => x.Equals('1')).ToArray();
        }
    }
}
