﻿using LW1.Methods;
using LW1.PrintMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using C1.WPF.C1Chart3D;
using LW1.Modules;
using Newtonsoft.Json;
using System.IO;

namespace LW1
{
    public partial class MainWindow : Window
    {
        SolidColorBrush[] colors = new SolidColorBrush[] {
                Brushes.Red,
                Brushes.Green,
                Brushes.Blue,
                Brushes.Yellow,
                Brushes.Black
            };


        public MainWindow()
        {
            InitializeComponent();
        }

        // Первая и вторая лабораторные работы
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrintGrapth.ClearPlotter(plotterStaticC);
            PrintGrapth.ClearPlotter(plotterStaticm);
            PrintGrapth.ClearPlotter(plotterStaticT);
            PrintGrapth.ClearPlotter(plotterDynamicCin);
            PrintGrapth.ClearPlotter(plotterDynamicMass);
            PrintGrapth.ClearPlotter(plotterDynamicT);
            PrintGrapth.ClearPlotter(plotterDynamicCout1);
            PrintGrapth.ClearPlotter(plotterDynamicCout2);
            PrintGrapth.ClearPlotter(plotterDynamicCout3);

            // Статика
            var c0_value = 11;
            var c1_value = 16;
            var m0_value = 4.8;
            var m1_value = 9;
            var T0_value = 132;
            var T1_value = 165;

            var step_C = 0.4;
            var step_m = 0.2;
            var step_T = 0.5;

            var const_C = 13;
            var const_m = 6;
            var const_T = 153;

            var staticConcentration = CalcStaticMethods.CalcInputConcentration(c0_value, c1_value, step_C, const_m, const_T);
            var staticMass = CalcStaticMethods.CalcInputMass(m0_value, m1_value, step_m, const_C, const_T);
            var staticTempereture = CalcStaticMethods.CalcInputTemperature(T0_value, T1_value, step_T, const_C, const_m);

            PrintGrapth.PrintLineGrapth(staticConcentration, plotterStaticC, colors[0]);
            PrintGrapth.PrintPointGrapth(new Point() { X = 13, Y = 18.03 }, plotterStaticC);
            PrintGrapth.PrintPointGrapth(new Point() { X = 15, Y = 20.8 }, plotterStaticC);

            PrintGrapth.PrintLineGrapth(staticMass, plotterStaticm, colors[0]);
            PrintGrapth.PrintPointGrapth(new Point() { X = 6, Y = 18.03 }, plotterStaticm);
            PrintGrapth.PrintPointGrapth(new Point() { X = 8.5, Y = 16.185 }, plotterStaticm);

            PrintGrapth.PrintLineGrapth(staticTempereture, plotterStaticT, colors[0]);
            PrintGrapth.PrintPointGrapth(new Point() { X = 153, Y = 18.03 }, plotterStaticT);
            PrintGrapth.PrintPointGrapth(new Point() { X = 162, Y = 19.08 }, plotterStaticT);


            // динамика
            CalcDynamicMethod.dt = Convert.ToDouble(dt.Text);
            var concentrationOutputValue = 18.03;
            var concentrationPoint = staticConcentration.SingleOrDefault(x => x.Y == concentrationOutputValue);
            var massPoint = staticMass.SingleOrDefault(x => x.Y == concentrationOutputValue);
            var temperaturePoint = staticTempereture.SingleOrDefault(x => x.Y == concentrationOutputValue);
            var m_out = CalcStaticMethods.CalcMOut(massPoint.X, concentrationPoint.X, concentrationOutputValue);
            var m_vt = CalcStaticMethods.CalcMvt(massPoint.X, m_out);

            var dynamicConcentration = CalcDynamicMethod.CalcDisturbance(concentrationPoint.X, CalcDynamicMethod.deltaCinput);
            var dynamicMass = CalcDynamicMethod.CalcDisturbance(massPoint.X, CalcDynamicMethod.deltaMass_input);
            var dynamicTempereture = CalcDynamicMethod.CalcDisturbance(temperaturePoint.X, CalcDynamicMethod.deltaT);

            var dynamicConcentrationOut1 = CalcDynamicMethod.CalcDynamicConcentration(massPoint.X, m_out, m_vt, concentrationPoint.X, concentrationOutputValue, temperaturePoint.X, isEuler.IsChecked.Value);
            var dynamicConcentrationOut2 = CalcDynamicMethod.CalcDynamicMass(massPoint.X, m_out, m_vt, concentrationPoint.X, concentrationOutputValue, temperaturePoint.X, isEuler.IsChecked.Value);
            var dynamicConcentrationOut3 = CalcDynamicMethod.CalcDynamicTemperature(massPoint.X, m_out, m_vt, concentrationPoint.X, concentrationOutputValue, temperaturePoint.X, isEuler.IsChecked.Value);

            PrintGrapth.PrintLineGrapth(dynamicConcentrationOut1, plotterDynamicCout1, colors[0]);
            PrintGrapth.PrintLineGrapth(dynamicConcentrationOut2, plotterDynamicCout2, colors[0]);
            PrintGrapth.PrintLineGrapth(dynamicConcentrationOut3, plotterDynamicCout3, colors[0]);

            PrintGrapth.PrintLineGrapth(dynamicConcentration, plotterDynamicCin, colors[0]);
            PrintGrapth.PrintLineGrapth(dynamicMass, plotterDynamicMass, colors[0]);
            PrintGrapth.PrintLineGrapth(dynamicTempereture, plotterDynamicT, colors[0]);
        }

        // третья лабораторная работа
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PrintGrapth.ClearPlotter(plotterStaticC1byZ);
            PrintGrapth.ClearPlotter(plotterStaticC2byZ);

            // статика
            var m = 0.2;
            var T = 1220.0;
            var Cin0 = 20.0;
            var Cin1 = 30.0;
            var L = 80.0;//210.0;

            var (C1, C2) = CalcStaticDistCoordMethod.CalcStatic(Cin0, Cin1, T, m, L);

            for (int i = 0; i < C1.Count(); i++)
            {
                var staticC1 = C1[i];
                var staticC2 = C2[i];

                PrintGrapth.PrintLineGrapth(staticC1, plotterStaticC1byZ, colors[i], 1, 1);
                PrintGrapth.PrintLineGrapth(staticC2, plotterStaticC2byZ, colors[i], 1, 1);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            chart3d_4.Children.Clear();
            
            var calcMethodResult = CalcCharacteristicsMethod.CalcMethod();
            
            int xlen = calcMethodResult.Count();
            int ylen = calcMethodResult[0].Count();
            var zdata = new double[xlen, ylen];

            for (int ix = 0; ix < xlen; ix++)
                for (int iy = 0; iy < ylen; iy++)
                {
                    zdata[ix, iy] = calcMethodResult[ix][iy].z;
                }

            var ds = new GridDataSeries
            {
                Start = new Point(0, 0),
                Step = new Point((1.0 / xlen), (10.0 / ylen)),
                ZData = zdata
            };

            chart3d_4.Children.Add(ds);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            chart3d_5.Children.Clear();
            chart3d_5.Children.Add(new GridDataSeries
            {
                Start = new Point(0, 0),
                Step = new Point(CalcDifferenceMethod.deltaX, CalcDifferenceMethod.deltaTau),
                ZData = CalcDifferenceMethod.CalcMethod()
            });
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            PrintGrapth.ClearPlotter(plotterArr_X);
            PrintGrapth.ClearPlotter(plotterArr_Z);

            var arr_x = RandomProcessGenerator.Random(6010);
            var M_x = RandomProcessGenerator.M(arr_x);
            var disp_x = RandomProcessGenerator.Dispersion(arr_x, M_x);
            var arr_z = RandomProcessGenerator.Filter(arr_x, disp_x);

            PrintGrapth.PrintLineGrapth(PrintGrapth.ConvertToPoints(arr_x), plotterArr_X, colors[0]);
            PrintGrapth.PrintLineGrapth(PrintGrapth.ConvertToPoints(arr_z), plotterArr_Z, colors[0]);

            var arr_M_0 = new List<Point>();
            for (var i = 0; i < arr_z.Count(); i++) { arr_M_0.Add(new Point() { X = i, Y = 42 }); } //14.2
            PrintGrapth.PrintLineGrapth(arr_M_0, plotterArr_Z, colors[1]);

            var json = JsonConvert.SerializeObject(arr_z);
            File.WriteAllText("F:\\Repositories\\LW\\3 - Модели и методы анализа пр. реш. (Литовка)\\CW\\CW\\bin\\Debug\\arr_z.json", json);
        }
    }
}
