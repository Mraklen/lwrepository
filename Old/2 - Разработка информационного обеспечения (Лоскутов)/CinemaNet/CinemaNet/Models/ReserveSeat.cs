﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class ReserveSeat
    {
        public int ReserveSeatID { get; set; }
        public int Row { get; set; }
        public int SeatNum { get; set; }

        public int ReserveID { get; set; }
        public Reserve Reserve { get; set; }
    }
}
