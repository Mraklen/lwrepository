﻿using LW3.Huffman.Compressor;
using LW3.Huffman.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW3.Huffman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PrintTable();
        }

        private void PrintTable(Dictionary<char, string> alphabet = null, Dictionary<char, uint> dictionary = null)
        {
            tableAlphabet.Controls.Clear();

            var controls = new List<Control>
            {
                new Label() { Text = "#" },
                new Label() { Text = "Символ" },
                new Label() { Text = "Код" },
                new Label() { Text = "Количество" }
            };
            if (alphabet != null)
            {
                for (var i = 0; i < alphabet.Count(); i++)
                {
                    var ch = alphabet.Keys.ElementAt(i);
                    controls.Add(new Label() { Text = (i + 1).ToString() });
                    controls.Add(new Label() { Text = ch.ToString() });
                    controls.Add(new Label() { Text = alphabet[ch] });
                    controls.Add(new Label() { Text = (dictionary != null) ? dictionary[ch].ToString() : "-" });
                }
            }
            tableAlphabet.Controls.AddRange(controls.ToArray());
        }

        private void btnCreateArchive_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.SelectFile();                                        // выбрать и читать файл
            var file = FileManager.ReadFile(fileName);

            var text = Encoding.Default.GetString(file);                                    // конвертация в текст
            var dictionary = HuffmanMethod.CreateDictionaryByText(text);                    // получение словаря типа <символ, количество>
            var alphabetCode = HuffmanMethod.ConvertDictionaryToAlphabetCode(dictionary);   // конвертируем в коды алфавита

            var alphabetToBytes = HuffmanMethod.ConvertAlphabetToBytes(alphabetCode);           // алфавит в байты
            var (textToBytes, diff) = HuffmanMethod.ConvertTextToBytesByAlphabet(text, alphabetCode);   // конвертируем текст в байты согласно алфавита

            var archiveFile = alphabetToBytes.Concat(textToBytes.Concat(new byte[] { Convert.ToByte(diff) })).ToArray(); // готовая для сохранения пачка байт /алфавит + сжатый текст/

            lbMainFileSize.Text = $"{file.Count()} кб";
            lbArchiveFileSize.Text = $"{textToBytes.Count()} кб";
            lbSymbolCount.Text = $"{text.Count()}";
            lbAlphabetCount.Text = $"{alphabetCode.Count()}";
            lbPercentCompression.Text = $"{Math.Round(100 - ((textToBytes.Count() / (text.Count() * 1.0)) * 100), 2)} %";
            Task.Delay(100);
            PrintTable(alphabetCode, dictionary);

            FileManager.SaveFile(archiveFile, "CompressedFiles", Path.GetFileNameWithoutExtension(fileName), "huf");  // сохраняем
        }

        private void btnOpenArchive_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.SelectFile(1);                                       // выбрать и читать файл
            var file = FileManager.ReadFile(fileName);

            var (alphabet, textBytes) = HuffmanMethod.SelectAlphabetAndText(file);                  // выдергваем алфавит и текст из файла
            var initialText = HuffmanMethod.ConvertBytesToTextByAlphabet(textBytes, alphabet, pbUnarchive);      // конвертируем байты в текс по алфавиту
            var initialTextBytes = Encoding.UTF8.GetBytes(initialText);
            
            PrintTable(alphabet.ToDictionary(k => k.Value, v => v.Key));

            FileManager.SaveFile(initialTextBytes, "UncompressedFiles", Path.GetFileNameWithoutExtension(fileName), "txt");
        }
    }
}
