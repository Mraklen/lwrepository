﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CinemaNet.Models;
using CinemaNet.Data;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Models.Interfaces;
using CinemaNet.ViewModels;

namespace CinemaNet.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext context;

        public ICinemaList cinemas;
        public IFilmList films;

        public HomeController(ApplicationDbContext context, ICinemaList cinemas, IFilmList films)
        {
            this.context = context;
            this.cinemas = cinemas;
            this.films = films;
        }

        public IActionResult Index() => View(new CinemaFilmModel
        {
            Cinemas = cinemas.CinemaList().ToList(),
            Films = films.FilmList().ToList()
        });

        
        public IActionResult AddData()
        {
            if (!context.Films.Any())
            {
                Film f1 = new Film
                {
                    Image = "/images/коловрат.jpg",
                    Name = "Легенда о Коловрате",
                    Production = "Россия",
                    Premiere = "1 декабря 2017 г.",
                    Genre = "История, Боевик, Фэнтези",
                    Director = "Джаник Файзиев, Иван Шурховецкий",
                    Duration = "1 ч. 57 мин.",
                    Story = "XIII век. Русь раздроблена и вот-вот падет на колени перед ханом Золотой Орды Батыем. Испепеляя города и заливая русские земли кровью, захватчики не встречают серьезного сопротивления, и лишь один воин бросает им вызов. Молодой рязанский витязь Евпатий Коловрат возглавляет отряд смельчаков, чтобы отомстить за свою любовь и за свою родину. Его отвага поразит даже Батыя, а его имя навсегда останется в памяти народа. Воин, ставший легендой. Подвиг, сохранившийся в веках."
                };
                Film f2 = new Film
                {
                    Image = "/images/тайна.jpg",
                    Name = "Тайна Коко",
                    Production = "США",
                    Premiere = "23 ноября 2017 г.",
                    Genre = "Мультфильм, Мюзикл, Фэнтези, Комедия, Семейный",
                    Director = "Ли Анкрич, Эдриан Молина",
                    Duration = "1 ч. 49 мин.",
                    Story = "12-летний Мигель живет в мексиканской деревушке в семье сапожников и тайно мечтает стать музыкантом. Тайно — потому что в его семейном клане музыка считается проклятием. Когда-то его прадед оставил свою жену, прабабку Мигеля, ради мечты, которая теперь не дает спокойно жить Мигелю. С тех пор музыкальная тема в семье стала табу. Мигель обнаруживает, что между ним и его любимым певцом Эрнесто де ла Крусом, ныне покойным, существует некая — пока неназванная — связь."
                };
                Film f3 = new Film
                {
                    Image = "/images/коматозники.jpg",
                    Name = "Коматозники",
                    Production = "США, Канада",
                    Premiere = "23 ноября 2017 г.",
                    Genre = "Ужасы, Фантастика, Триллер, Драма",
                    Director = "Нильс Арден Оплев",
                    Duration = "1 ч. 50 мин.",
                    Story = "Пятеро студентов-медиков, одержимых желанием узнать, что происходит с человеком после смерти, решаются на рискованный эксперимент: на время они останавливают друг другу сердце, чтобы погрузиться в состояние клинической смерти и на собственном опыте понять, что происходит с человеком по ту сторону. Но они забывают, что, переступив черту, нужно быть готовым к последствиям."
                };
                Film f4 = new Film
                {
                    Image = "/images/снеговик.jpg",
                    Name = "Снеговик",
                    Production = "Великобритания, США, Швеция",
                    Premiere = "23 ноября 2017 г.",
                    Genre = "Ужасы, Триллер, Драма, Криминал, Детектив",
                    Director = "Томас Альфредсон",
                    Duration = "1 ч. 59 мин.",
                    Story = "В течение многих лет в день, когда выпадает первый снег, бесследно исчезают замужние женщины. Сложить все части загадочного пазла под силу только знаменитому детективу. Он потерял покой и сон, ведь время следующего снегопада неумолимо приближается."
                };
                Film f5 = new Film
                {
                    Image = "/images/атлантида.jpg",
                    Name = "Атлантида",
                    Production = "Испания, Франция",
                    Premiere = "30 ноября 2017 г.",
                    Genre = "Ужасы, Фантастика, Триллер, Приключения",
                    Director = "Ксавье Жанс",
                    Duration = "1 ч. 56 мин.",
                    Story = "После длительного плавания молодой метеоролог Френд прибывает на затерянный в океане остров принять годичную вахту на метеостанции. На острове оказывается лишь суровый и немногословный смотритель маяка Грюнер. Френд довольно быстро понимает, что Грюнер что-то скрывает, старый метеоролог попал в какую-то передрягу, а остров кажется необитаемым только на первый взгляд."
                };
                Film f6 = new Film
                {
                    Image = "/images/счастливого.jpg",
                    Name = "Счастливого дня смерти",
                    Production = "США",
                    Premiere = "7 декабря 2017 г.",
                    Genre = "Ужасы, Триллер, Детектив, Комедия",
                    Director = "Кристофер Лэндон",
                    Duration = "1 ч. 36 мин.",
                    Story = "Каждый в универе мечтал попасть на её день рождения, но праздник был безнадежно испорчен незнакомцем в маске, убившим виновницу торжества. Однако судьба преподнесла имениннице леденящий душу подарок — бесконечный запас жизней. И теперь у девушки появился шанс вычислить своего убийцу, ведь этот день будет повторяться снова и снова…"
                };

                context.Films.AddRange(new List<Film> { f1, f2, f3, f4, f5, f6 });

                Cinema c1 = new Cinema
                {
                    Name = "Кинотеарт \"Галактика\"",
                    Address = "Тамбов, ул. Советская, 99А"
                };
                Cinema c2 = new Cinema
                {
                    Name = "Кинотеатр \"Меркурий\"",
                    Address = "Тамбов, Студенецкая наб. ул., 20А"
                };
                Cinema c3 = new Cinema
                {
                    Name = "Кинтеатр \"Мир Люксор\"",
                    Address = "Тамбов, ул. Степана Разина, 5"
                };

                context.Cinemas.AddRange(new List<Cinema> { c1, c2, c3 });

                context.SaveChanges();

                c1.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c1.CinemaID,
                    FilmID = f1.FilmID
                });
                c1.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c1.CinemaID,
                    FilmID = f3.FilmID
                });
                c1.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c1.CinemaID,
                    FilmID = f4.FilmID
                });
                c1.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c1.CinemaID,
                    FilmID = f6.FilmID
                });
                c2.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c2.CinemaID,
                    FilmID = f2.FilmID
                });
                c2.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c2.CinemaID,
                    FilmID = f3.FilmID
                });
                c2.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c2.CinemaID,
                    FilmID = f5.FilmID
                });
                c2.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c2.CinemaID,
                    FilmID = f6.FilmID
                });
                c3.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c3.CinemaID,
                    FilmID = f1.FilmID
                });
                c3.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c3.CinemaID,
                    FilmID = f2.FilmID
                });
                c3.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c3.CinemaID,
                    FilmID = f3.FilmID
                });
                c3.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c3.CinemaID,
                    FilmID = f4.FilmID
                });
                c3.CinemaFilms.Add(new CinemaFilms
                {
                    CinemaID = c3.CinemaID,
                    FilmID = f6.FilmID
                });

                context.SaveChanges();
            }

            if (!context.CinemaRooms.Any())
            {
                context.CinemaRooms.AddRange(
                    new CinemaRoom { Name = "Зал №1", Row = 9, SeatInRow = 12 },
                    new CinemaRoom { Name = "Зал №2", Row = 10, SeatInRow = 13 },
                    new CinemaRoom { Name = "Зал №3", Row = 8, SeatInRow = 12 }
                );
                context.SaveChanges();
            }

            if (!context.Seats.Any())
            {
                int rooms = context.CinemaRooms.Count();

                for (int i = 1; i <= rooms; i++)
                {
                    int rows = context.CinemaRooms
                        .Where(cr => cr.CinemaRoomID == i).ToList().ElementAt(0).Row;
                    for (int r = 1; r <= rows; r++)
                    {
                        int seats = context.CinemaRooms
                        .Where(cr => cr.CinemaRoomID == i).ToList().ElementAt(0).SeatInRow;
                        for (int s = 1; s <= seats; s++)
                        {
                            context.Seats.Add(new Seat
                            {
                                Row = r,
                                SeatNum = s,
                                CinemaRoomID = i
                            });
                        }
                    }
                }
                context.SaveChanges();
            }

            if (!context.FilmSessions.Any())
            {
                var cinemas = context.Cinemas.Include(cf => cf.CinemaFilms)
                    .ThenInclude(f => f.Film).ToList();
                int countRoom = context.CinemaRooms.Count();
                var rnd = new Random();
                string[] date = { "10.12.2017, ВС", "11.12.2017, ПН", "12.12.2017, ВТ" };

                foreach (var c in cinemas)
                {
                    var films = c.CinemaFilms.Select(f => f.Film).ToList();
                    foreach (var f in films)
                    {
                        for (int d = 0; d < date.Length; d++)
                        {
                            int hour = 10;
                            int sessionsCount = rnd.Next(3, 5);
                            for (int s = 0; s < sessionsCount; s++)
                            {
                                context.FilmSessions.Add(new FilmSession
                                {
                                    Date = date[d],
                                    Time = $"{hour} : {rnd.Next(10, 60)}",
                                    CinemaID = c.CinemaID,
                                    FilmID = f.FilmID,
                                    CinemaRoomID = rnd.Next(1, countRoom)
                                });
                                hour += rnd.Next(1, 2);
                            }
                        }
                    }
                }
                context.SaveChanges();
            }

            if (!context.Reserves.Any())
            {
                int maxReserveID = 1;
                var rnd = new Random();
                var sessions = context.FilmSessions
                    .Where(fs => fs.Date == "10.12.2017, ВС" || fs.Date == "11.12.2017, ПН")
                    .Include(fs => fs.CinemaRoom)
                    .Include(fs => fs.CinemaRoom.Seats).ToList();

                foreach (var session in sessions)
                {
                    int reserveID = maxReserveID;
                    int maxReserveCount = rnd.Next(20, 40);

                    for (int i = 1; i < maxReserveCount; i++)
                    {
                        context.Reserves.Add(new Reserve
                        {
                            Email = "<tel-number>",
                            FilmSessionID = session.FilmSessionID
                        });
                        maxReserveID++;
                    }
                    context.SaveChanges();

                    var seats = session.CinemaRoom.Seats.ToList();
                    foreach (var seat in seats)
                    {
                        if (Convert.ToBoolean(rnd.Next(0, 2)) && reserveID < maxReserveID)
                        {
                            context.ReserveSeats.Add(new ReserveSeat
                            {
                                Row = seat.Row,
                                SeatNum = seat.SeatNum,
                                ReserveID = reserveID++
                            });
                        }
                    }
                    context.SaveChanges();
                }
            }
        
            return Ok("Нормас");
        }
        
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
