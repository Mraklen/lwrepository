﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW5.RSA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btEncrypt_Click(object sender, EventArgs e)
        {
            long p = Convert.ToInt64(txt_p.Text);
            long q = Convert.ToInt64(txt_q.Text);

            var s = "";
            using (var sr = new StreamReader("text.txt"))
            {
                s += sr.ReadLine();
            }
            
            long n = p * q;
            long m = (p - 1) * (q - 1);
            long d = EncryptionModule.calc_d(m);
            long e_ = EncryptionModule.calc_e(d, m);

            var res = EncryptionModule.StartCoder(s, e_, n);

            using (var sw = new StreamWriter("encrypt.txt"))
            {
                foreach (string item in res)
                    sw.WriteLine(item);
            }

            txt_d.Text = d.ToString();
            txt_n.Text = n.ToString();

            Process.Start("encrypt.txt");
        }

        private void btDecrypt_Click(object sender, EventArgs e)
        {
            long d = Convert.ToInt64(txt_d.Text);
            long n = Convert.ToInt64(txt_n.Text);

            var input = new List<string>();
            using (var sr = new StreamReader("encrypt.txt"))
            {
                while (!sr.EndOfStream)
                {
                    input.Add(sr.ReadLine());
                }
            }

            string result = EncryptionModule.StartDecoder(input, d, n);

            StreamWriter sw = new StreamWriter("decoding.txt");
            sw.WriteLine(result);
            sw.Close();

            Process.Start("decoding.txt");
        }
    }
}
