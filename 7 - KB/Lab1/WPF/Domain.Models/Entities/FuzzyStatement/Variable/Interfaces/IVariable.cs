﻿
namespace Domain.Models.Entities.FuzzyStatement.Variable.Interfaces
{
    public enum VarType
    {
        None = -1,
        Small,
        Average,
        Large
    }

    public enum ModType
    {
        None = -1,
        Con,
        Dil
    }

    public interface IVariable
    {
        string CodeName { get; }
        string Name { get; }
        double Value { get; set; }
        double CalcTermSet(double value);
    }
}
