﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Unity;
using WPF.Helpers;
using WPF.ViewModels.Base;

namespace WPF.Modules.Base
{
    public enum ModuleType
    {
        Main,
        ExpertSystem,
        FuzzyStatement,
        NeuronNet
    }

    abstract class BaseModule : IModule
    {
        public abstract string Name { get; }
        protected abstract UserControl View { get; set; }
        protected abstract BaseViewModel ViewModel { get; set; }

        protected abstract void CreateViewAndViewModel();

        protected readonly IUnityContainer container;

        public UserControl UserInterface => View;

        public WindowSetting WindowSetting { get; private set; }

        public BaseModule(IUnityContainer container, SettingType type)
        {
            this.container = container;
            WindowSetting = new WindowSetting(type);

            var winCheck = SystemParameters.PrimaryScreenHeight > 800;
            WindowSetting.Scale = 1;//winCheck ? 1.5 : 1.2;

            CreateViewAndViewModel();

            View.DataContext = ViewModel;
        }

        public void Deactivate()
        {
            if (View != null)
            {
                var d = View.DataContext as IDisposable;
                d?.Dispose();
                View = null;
            }
        }
    }
}
