﻿using Domain.Models.Entities.KnowledgeBase;
using QueryAbstractions.ReadModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries.ReadModels.KnowledgeBase
{
    public class VariableReadModel : BaseReadModel<int>
    {
        public string Name { get; set; }
        public string CodeName { get; set; }

        public VarType VarType { get; set; }

        public IEnumerable<ParameterReadModel> Parameters { get; set; }
    }
}
