﻿using LW1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    public class DefaultEntityRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity<int>
    {
        protected readonly ApplicationDbContext context;

        public DefaultEntityRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        protected DbSet<TEntity> Set => context.Set<TEntity>();

        public TEntity GetById(int id)
        {
            var set = context.Set<TEntity>();
            return set.FirstOrDefault(x => x.Id == id);
        }

        public void UpdateOrInsert(TEntity entity)
        {
            var set = context.Set<TEntity>();
            if (entity.Id == 0)
                set.Add(entity);
            else
                set.Attach(entity);
            context.SaveChanges();
        }
    }
}
