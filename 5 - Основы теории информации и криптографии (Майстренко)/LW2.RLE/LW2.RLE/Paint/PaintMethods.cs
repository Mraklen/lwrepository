﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW2.RLE.Paint
{
    public enum PenType
    {
        Rect,
        Circle
    }

    public class PaintMethods
    {
        public int PenSize { get; set; }
        public PenType PenType { get; set; }
        public Color PenColor { get; set; }
        public bool ContinuousDraw { get; set; }

        public PaintMethods()
        {
            PenColor = Color.Black;
            PenType = PenType.Rect;
        }

        public void CanvasClear(PictureBox picture)
        {
            var g = Graphics.FromImage(picture.Image);
            g.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, picture.Width, picture.Height));
            g.Dispose();
            picture.Invalidate();
        }

        public void DrawPen(PictureBox picture, int x, int y)
        {
            var g = Graphics.FromImage(picture.Image);

            var shift = Convert.ToInt32(Math.Ceiling(PenSize / 2.0));
            var penBrush = new SolidBrush(PenColor);
            var pen = new Rectangle(x - shift, y - shift, PenSize, PenSize);

            switch (PenType)
            {
                case PenType.Circle:
                    g.FillEllipse(penBrush, pen);
                    break;
                case PenType.Rect:
                    g.FillRectangle(penBrush, pen);
                    break;
            }

            g.Dispose();
            picture.Invalidate();
        }


        public static void DrawNoize(PictureBox picture)
        {
            //var g = Graphics.FromImage(picture.Image);
            var bmp = new Bitmap(picture.Image);

            var i = 0;
            for (var y = 0; y < picture.Height; y++)
            {
                for (var x = 0; x < picture.Width; x++)
                {
                    if (i++ % 2.0 == 0/* && (new Random(DateTime.Now.Millisecond)).Next(2) == 1*/)
                        bmp.SetPixel(x, y, Color.Black);
                }
            }

            picture.Image = bmp;
        }
    }
}
