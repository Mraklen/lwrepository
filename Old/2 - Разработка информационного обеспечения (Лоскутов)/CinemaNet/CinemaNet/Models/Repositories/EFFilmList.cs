﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFFilmList : IFilmList
    {
        public ApplicationDbContext context;
        public EFFilmList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public void AddFilm(Film film)
        {
            context.Films.Add(film);
            context.SaveChanges();
        }

        public IEnumerable<Film> FilmList(int cinemaID)
        {
            return from f in context.Films
                   join cf in context.CinemaFilms on f.FilmID equals cf.FilmID
                   where cf.CinemaID == cinemaID
                   select f;
        }

        public IEnumerable<Film> FilmList() => context.Films
            .Include(f => f.FilmSessions);

        public void RemoveFilmById(int id)
        {
            context.Films.Remove(context.Films.Where(x => x.FilmID == id).Single());
            context.SaveChanges();
        }
    }
}
