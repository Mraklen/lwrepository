﻿using EntityAbstractions.Entities;
using System.ComponentModel.DataAnnotations;

namespace EntityAbstractions.Implementations
{
    public abstract class BaseEntity : IEntity<int>
    {
        [Key]
        public int Id { get; protected set; }
    }
}
