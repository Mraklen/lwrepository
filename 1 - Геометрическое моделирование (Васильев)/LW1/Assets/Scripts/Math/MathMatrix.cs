﻿using Assets.Scripts.Math;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathMatrix : MonoBehaviour {
    
    public static Matrix Multiplication(Matrix a, Matrix b)
    {
        if (a.ColumnCount != b.RowCount) throw new Exception("Матрицы нельзя перемножить");
        float[,] r = new float[a.RowCount, b.ColumnCount];
        for (int i = 0; i < a.RowCount; i++)
        {
            for (int j = 0; j < b.ColumnCount; j++)
            {
                for (int k = 0; k < b.RowCount; k++)
                {
                    r[i, j] += a.Content[i, k] * b.Content[k, j];
                }
            }
        }
        return new Matrix()
        {
            Content = r,
            ColumnCount = b.ColumnCount,
            RowCount = a.RowCount
        };
    }

    public static Matrix Transpose(Matrix a)
    {
        var trans = new float[a.ColumnCount, a.RowCount];
        for (int i = 0; i < a.RowCount; i++)
        {
            for (int j = 0; j < a.ColumnCount; j++)
            {
                trans[j, i] = a.Content[i, j];
            }
        }
        return new Matrix()
        {
            RowCount = a.ColumnCount,
            ColumnCount = a.RowCount,
            Content = trans
        };
    }
}
