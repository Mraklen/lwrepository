﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Windows.Controls;
using System.Threading.Tasks;
using Microsoft.Research.DynamicDataDisplay.DataSources.MultiDimensional;
using Microsoft.Research.DynamicDataDisplay.Common.Auxiliary;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System.Reflection;
using System.Windows.Media;
using LW3.Models;

namespace LW3.Services
{
    public class Methods
    {
        public FunctionParam Function { get; set; }
        int maxSleepValue = 1;

        List<Color> Colors = new List<Color>() {
            Color.FromRgb(255, 0, 0),
            Color.FromRgb(0, 255, 0),
            Color.FromRgb(0, 0, 255),
            Color.FromRgb(255, 255, 0),
            Color.FromRgb(255, 0, 255),
            Color.FromRgb(0, 255, 255)
        };
        
        public void SetFunction(double x1Value, double x2Value, double eValue)
        {
            Function = new FunctionParam(x1Value, x2Value, eValue);
        }

        /*
        public async void CalcMethod(PlotterParam plotterParam, Label label_stepValue, Label label_extremum)
        {
            ClearPlotter(plotterParam.Plotter);
            // вывод основного графика
            PrintGraph(plotterParam, Function.GetIsolineData());
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetFineData(FineType.First).Points);
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetFineData(FineType.Second).Points);
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetIsolineData().ExtremPoint);
            await Task.Delay(maxSleepValue);

            var stepCount = 0;
            double dx, dx1, dx2, dx11, dx22, x1, x2, e, s, f;

            x1 = FunctionParam.X1Value;
            x2 = FunctionParam.X2Value;
            e = FunctionParam.EValue;

            PrintPoint(plotterParam, new PointXY() { X1 = x1, X2 = x2 });
            await Task.Delay(maxSleepValue);

            dx1 = 5;
            dx2 = 5;

            do
            {
                dx = dx1;
                dx1 = dx1 / 2;

                dx11 = (Function.CalcFunction(x1 + dx, x2) - Function.CalcFunction(x1, x2)) / dx;
                dx22 = (Function.CalcFunction(x1 + dx1, x2) - Function.CalcFunction(x1, x2)) / dx1;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx1 = dx;

            do
            {
                dx = dx2;
                dx2 = dx2 / 2;

                dx11 = (Function.CalcFunction(x1, x2 + dx) - Function.CalcFunction(x1, x2)) / dx;
                dx22 = (Function.CalcFunction(x1, x2 + dx2) - Function.CalcFunction(x1, x2)) / dx2;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx2 = dx;

            var tmpPoint = new PointXY() { X1 = x1, X2 = x2, Y = Function.CalcFunction(x1, x2) };
            var point = new PointXY();
            var res = false;
            do
            {
                dx11 = (Function.CalcFunction(x1 + dx1, x2) - Function.CalcFunction(x1, x2)) / dx1;
                dx22 = (Function.CalcFunction(x1, x2 + dx2) - Function.CalcFunction(x1, x2)) / dx2;

                s = HalfDivMethod(x1, x2, dx11, dx22, e);

                x1 = x1 - dx11 * s;
                x2 = x2 - dx22 * s;

                f = Function.CalcFunction(x1, x2);
                point = new PointXY() { X1 = x1, X2 = x2, Y = f };

                stepCount++;
                // вывод значения на график
                PrintPoint(plotterParam, point);

                // обновить лейблы
                label_stepValue.Content = stepCount;
                label_extremum.Content = point.PrintPointXY();

                await Task.Delay(maxSleepValue * 2);

                res = Math.Abs(tmpPoint.Y - point.Y) > e;
                tmpPoint = point;
            }
            while (res);
        }
        */

        public async void CalcInternalMethod(PlotterParam plotterParam, Label label_stepValue, Label label_extremum)
        {
            ClearPlotter(plotterParam.Plotter);
            // вывод основного графика
            PrintGraph(plotterParam, Function.GetIsolineData());
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetFineData(FineType.First).Points, true);
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetFineData(FineType.Second).Points, true);
            await Task.Delay(maxSleepValue);
            PrintPoint(plotterParam, Function.GetIsolineData().ExtremPoint, Brushes.Red, 6);
            await Task.Delay(maxSleepValue);
            
            var stepCount = 0;
            double dx, dx1, dx2, dx11, dx22, x1, x2, e, s, b, d1x1, d2x2, f, k = 0.1, c = 1;

            x1 = FunctionParam.X1Value;
            x2 = FunctionParam.X2Value;
            e = FunctionParam.EValue;

            var firstPoint = new PointXY() { X1 = x1, X2 = x2 };
            PrintPoint(plotterParam, firstPoint, Brushes.Blue, 10);
            await Task.Delay(maxSleepValue);

            //проверить условие попадаения точки, если выбран внутренний способ штрафа
            if(FunctionParam.CurrenFineType == FineType.Internal)
            {
                if (!Function.Condition(x1, x2))
                {
                    x1 = 4;
                    x2 = -2;
                }
            }
            else
            {
                k = 100;
            }

            dx1 = 5;
            dx2 = 5;

            do
            {
                dx = dx1;
                dx1 = dx1 / 2;

                dx11 = (Function.CalcFunction(x1 + dx, x2, k) - Function.CalcFunction(x1, x2, k)) / dx;
                dx22 = (Function.CalcFunction(x1 + dx1, x2, k) - Function.CalcFunction(x1, x2, k)) / dx1;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx1 = dx;

            do
            {
                dx = dx2;
                dx2 = dx2 / 2;

                dx11 = (Function.CalcFunction(x1, x2 + dx, k) - Function.CalcFunction(x1, x2, k)) / dx;
                dx22 = (Function.CalcFunction(x1, x2 + dx2, k) - Function.CalcFunction(x1, x2, k)) / dx2;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx2 = dx;

            var tmpPoint = new PointXY() { X1 = x1, X2 = x2, Y = Function.CalcFunction(x1, x2, k) };
            var points = new List<PointXY>()
            {
                firstPoint
            };
            var res = false;
            do
            {
                s = HalfDivMethod(x1, x2, dx11, dx22, e, k);

                x1 -= dx11 * s;
                x2 -= dx22 * s;

                d1x1 = (Function.CalcFunction(x1 + dx1, x2, k) - Function.CalcFunction(x1, x2, k)) / dx1;
                d2x2 = (Function.CalcFunction(x1, x2 + dx2, k) - Function.CalcFunction(x1, x2, k)) / dx2;

                b = (Math.Pow(d1x1, 2) + Math.Pow(d2x2, 2)) / 
                       (Math.Pow(dx11, 2) + Math.Pow(dx22, 2));

                dx11 = d1x1 + dx11 * b;
                dx22 = d2x2 + dx22 * b;

                var tf = Function.CalcFunction(x1, x2, 1);
                f = Function.CalcFunction(x1, x2, k);
                var point = new PointXY() { X1 = x1, X2 = x2, Y = f };
                points.Add(point);

                if (FunctionParam.CurrenFineType == FineType.Internal)
                    k *= c;
                else
                    k = 100;
                
                stepCount++;
                // вывод значения на график
                PrintPoint(plotterParam, points, false, 4);

                // обновить лейблы
                label_stepValue.Content = stepCount;
                label_extremum.Content = point.PrintPointXY();

                await Task.Delay(maxSleepValue * 200);

                res = Math.Abs(tmpPoint.Y - point.Y) > e;
                tmpPoint = point;
            }
            while (res);
        }
        

        public double HalfDivMethod(double x1, double x2, double dx1, double dx2, double e, double k)
        {
            double a0, b0, a1, b1, x01, x02, x11, x12, f0, f1;

            a0 = x1;
            a1 = x1 - dx1 * 100;
            b0 = x2;
            b1 = x2 - dx2 * 100;

            do
            {
                x01 = a0 + (a1 - a0) / 4;
                x02 = b0 + (b1 - b0) / 4;
                x11 = a1 - (a1 - a0) / 4;
                x12 = b1 - (b1 - b0) / 4;

                f0 = Function.CalcFunction(x01, x02, k);
                f1 = Function.CalcFunction(x11, x12, k);

                if (f0 > f1)
                {
                    a0 = x01;
                    b0 = x02;
                }
                else
                {
                    a1 = x11;
                    b1 = x12;
                }
            }
            while (Math.Sqrt((a1 - a0) * (a1 - a0) + (b1 - b0) * (b1 - b0)) > e);

            if((dx1 == 0) || (dx2 == 0))
            {
                return 0;
            }
            else
            {
                return -(((a1 + a0) / 2 - x1) / dx1 + ((b1 + b0) / 2 - x2) / dx2) / 2;
            }
        }

        void PrintGraph(PlotterParam plotterParam, IsolineData isolineData)
        {
            WarpedDataSource2D<double> dataSource = new WarpedDataSource2D<double>(isolineData.Data, isolineData.Grid);
            plotterParam.IsolineGraph.DataSource = dataSource;
            plotterParam.IsolineTracking.DataSource = dataSource;

            Rect visible = dataSource.GetGridBounds();
            plotterParam.Plotter.Viewport.Visible = visible;
        }
        void PrintPoint(PlotterParam plotterParam, PointXY point, Brush brush = null, int pointRadius = 3)
        {
            plotterParam.Plotter.Legend.Visibility = Visibility.Collapsed;
            var marker = new CirclePointMarker
            {
                Pen = new Pen((brush ?? Brushes.Black), pointRadius)
            };
            plotterParam.Plotter.AddLineGraph(
                CreateDataSource(point),
                new Pen(),
                marker,
                new PenDescription(" "));
        }
        void PrintPoint(PlotterParam plotterParam, List<PointXY> points, bool point = false, int pointRadius = 3)
        {
            if (!point)
            {
                plotterParam.Plotter.Legend.Visibility = Visibility.Collapsed;

                Random rnd = new Random();
                plotterParam.Plotter.AddLineGraph(
                    CreateDataSource(points), Colors[rnd.Next(Colors.Count)], pointRadius);

                plotterParam.Plotter.LegendVisible = false;
                plotterParam.Plotter.FitToView();
            }
            else
            {
                foreach(var item in points)
                {
                    PrintPoint(plotterParam, item);
                }
            }
        }
        private void ClearPlotter(ChartPlotter plotter)
        {
            plotter.Children.RemoveAll(typeof(LineGraph));
            plotter.Children.RemoveAll(typeof(MarkerPointsGraph));
        }

        private EnumerableDataSource<PointXY> CreateDataSource(List<PointXY> rates)
        {
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(rates);
            ds.SetXMapping(x => x.X1);
            ds.SetYMapping(y => y.X2);
            return ds;
        }
        private EnumerableDataSource<PointXY> CreateDataSource(PointXY rates)
        {
            List<PointXY> points = new List<PointXY>() { rates };
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(points);
            ds.SetXMapping(x => x.X1);
            ds.SetYMapping(y => y.X2);
            return ds;
        }
    }
}
