﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Unity;
using WPF.Controls.ExpertSystem;
using WPF.Helpers;
using WPF.Modules.Base;
using WPF.ViewModels.Base;
using WPF.ViewModels.ExpertSystem;

namespace WPF.Modules.ExpertSystem
{
    class ExpertSystemModule : BaseModule, IDisposable
    {
        public ExpertSystemModule(IUnityContainer container, SettingType type) : base(container, type) { }

        public override string Name => "Экспертная система";

        protected override UserControl View { get; set; }
        protected override BaseViewModel ViewModel { get; set; }

        protected override void CreateViewAndViewModel()
        {
            View = new ExpertSystemPage();
            ViewModel = container.Resolve<ExpertSystemViewModel>();
        }

        public void Dispose() { }
    }
}
