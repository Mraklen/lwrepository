﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.ViewModels
{
    public class KeyWordViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
