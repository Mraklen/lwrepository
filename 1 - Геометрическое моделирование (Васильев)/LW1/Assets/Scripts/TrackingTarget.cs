﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingTarget : MonoBehaviour {

    [SerializeField] GameObject Target;
    public GameObject End;

    public float magnitude = 0;

    private void Update()
    {
        transform.LookAt(Target.transform);
    }

    public Vector3 GetVector()
    {
        return transform.forward;
    }

    public float GetMagnitude()
    {
        magnitude = (Target.transform.position - transform.position).magnitude;
        return magnitude;
    }
}
