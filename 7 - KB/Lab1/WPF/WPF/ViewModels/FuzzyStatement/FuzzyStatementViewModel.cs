﻿using Domain.Models.Core;
using Domain.Models.Entities.FuzzyStatement;
using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity;
using WPF.State;
using WPF.ViewModels.Base;
using LiveCharts;
using LiveCharts.Wpf;
using System.Windows.Media;

namespace WPF.ViewModels.FuzzyStatement
{
    class FuzzyStatementViewModel : BaseViewModel, IDisposable
    {
        private readonly IVariableFactory variableFactory;

        List<IVariable> Variables { get; set; }

        public double TemperatureValue
        {
            get => Math.Round(Variables.FirstOrDefault(x => x.CodeName == "T").Value, 3);
            set
            {
                if (Variables.Any(x => x.CodeName == "T"))
                {
                    var oldVal = Variables.FirstOrDefault(x => x.CodeName == "T").Value;

                    Variables.FirstOrDefault(x => x.CodeName == "T").Value = value;
                    OnPropertyChanged("TemperatureValue");

                    ValueChanged();

                    if (temperatureCollection != null)
                    {
                        temperatureCollection[0].Values[(int)oldVal - 100] = 0.0;
                        temperatureCollection[0].Values[(int)value - 100] = 1.0;
                    }
                }
            }
        }

        public double PressureValue
        {
            get => Math.Round(Variables.FirstOrDefault(x => x.CodeName == "P").Value, 3);
            set
            {
                if (Variables.Any(x => x.CodeName == "P"))
                {
                    var oldVal = Variables.FirstOrDefault(x => x.CodeName == "P").Value;
                    Variables.FirstOrDefault(x => x.CodeName == "P").Value = value;
                    OnPropertyChanged("PressureValue");

                    ValueChanged();

                    if (pressureCollection != null)
                    {
                        pressureCollection[0].Values[(int)oldVal] = 0.0;
                        pressureCollection[0].Values[(int)value] = 1.0;
                    }
                }
            }
        }

        string result;
        public string Result { get => $"Результат: {result}"; set { result = value; OnPropertyChanged("Result"); } }

        SeriesCollection temperatureCollection;
        public SeriesCollection TemperatureCollection { get => temperatureCollection; set { temperatureCollection = value; OnPropertyChanged("TemperatureCollection"); } }
        string[] temperatureLabels;
        public string[] TemperatureLabels { get => temperatureLabels; set { temperatureLabels = value; OnPropertyChanged("TemperatureLabels"); } }

        SeriesCollection pressureCollection;
        public SeriesCollection PressureCollection { get => pressureCollection; set { pressureCollection = value; OnPropertyChanged("PressureCollection"); } }
        string[] pressureLabels;
        public string[] PressureLabels { get => pressureLabels; set { pressureLabels = value; OnPropertyChanged("PressureLabels"); } }

        public FuzzyStatementViewModel(AppStateStore stateStore, IUnityContainer container, IVariableFactory variableFactory) : base(stateStore, container)
        {
            this.variableFactory = variableFactory;

            var var1 = variableFactory.GetProcessTemperature();
            var var2 = variableFactory.GetPressureInMachine();

            Variables = new List<IVariable> { var1, var2 };

            CreateStatements();

            TemperatureValue = 110;
            PressureValue = 25;

            ValueChanged();

            TemperatureCalc(100, 150);
            PressureCalc(0, 50);
        }

        private void TemperatureCalc(int start, int end)
        {
            var var1 = variableFactory.GetProcessTemperature(VarType.Small);
            var var2 = variableFactory.GetProcessTemperature(VarType.Average);
            var var3 = variableFactory.GetProcessTemperature(VarType.Large);
            var var4 = variableFactory.GetProcessTemperature(VarType.Small, ModType.Con);

            temperatureCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "текущее значение",
                    Values = new ChartValues<double>(),
                    Stroke = Brushes.Black,
                    PointForeground = Brushes.Black
                },
                new LineSeries
                {
                    Title = "малое",
                    Values = new ChartValues<double>()
                },
                new LineSeries
                {
                    Title = "среднее",
                    Values = new ChartValues<double>()
                },
                new LineSeries
                {
                    Title = "большое",
                    Values = new ChartValues<double>()
                },
                new LineSeries
                {
                    Title = "очень малое",
                    Values = new ChartValues<double>()
                }
            };

            var tmpTemperatureLabels = new List<string>();
            var tmpTemperatureVal = Math.Round(TemperatureValue);

            for (var i = start; i <= end; i++)
            {
                var res1 = var1.CalcTermSet(i);
                var res2 = var2.CalcTermSet(i);
                var res3 = var3.CalcTermSet(i);
                var res4 = var4.CalcTermSet(i);

                temperatureCollection[0].Values.Add(i == tmpTemperatureVal ? 1.0 : 0.0);
                temperatureCollection[1].Values.Add(res1);
                temperatureCollection[2].Values.Add(res2);
                temperatureCollection[3].Values.Add(res3);
                temperatureCollection[4].Values.Add(res4);
                tmpTemperatureLabels.Add(i.ToString());
            }

            temperatureLabels = tmpTemperatureLabels.ToArray();

            OnPropertyChanged("TemperatureCollection");
            OnPropertyChanged("TemperatureLabels");
        }

        private void PressureCalc(int start, int end)
        {
            var var1 = variableFactory.GetPressureInMachine(VarType.Small);
            var var2 = variableFactory.GetPressureInMachine(VarType.Average);
            var var3 = variableFactory.GetPressureInMachine(VarType.Large);

            pressureCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "текущее значение",
                    Values = new ChartValues<double>(),
                    Stroke = Brushes.Black,
                    PointForeground = Brushes.Black
                },
                new LineSeries
                {
                    Title = "малое",
                    Values = new ChartValues<double>()
                },
                new LineSeries
                {
                    Title = "среднее",
                    Values = new ChartValues<double>()
                },
                new LineSeries
                {
                    Title = "большое",
                    Values = new ChartValues<double>()
                }
            };

            var tmpPressureLabels = new List<string>();
            var tmpPressureVal = Math.Round(PressureValue);

            for (var i = start; i <= end; i++)
            {
                var res1 = var1.CalcTermSet(i);
                var res2 = var2.CalcTermSet(i);
                var res3 = var3.CalcTermSet(i);

                pressureCollection[0].Values.Add(i == tmpPressureVal ? 1.0 : 0.0);
                pressureCollection[1].Values.Add(res1);
                pressureCollection[2].Values.Add(res2);
                pressureCollection[3].Values.Add(res3);
                tmpPressureLabels.Add(i.ToString());
            }

            pressureLabels = tmpPressureLabels.ToArray();

            OnPropertyChanged("PressureCollection");
            OnPropertyChanged("PressureLabels");
        }
        
        // вычисление результата
        private void ValueChanged()
        {
            var statement = stateStore.State.Statements.OrderBy(x => x.CheckCondition(Variables)).LastOrDefault();
            Result = statement.Result;
        }

        private void CreateStatements()
        {
            stateStore.State.Statements = new List<Statement>
            {
                new Statement
                {
                    Result = "Процесс = 1",
                    Condition = new Condition
                    {
                         Type = ConditionType.OR,
                         Conditions = new List<Condition>
                         {
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Small) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Small) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Small) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Average) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Average) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Small) }
                                  }
                             }
                         }
                    }
                },
                new Statement
                {
                    Result = "Процесс = 2",
                    Condition = new Condition
                    {
                         Type = ConditionType.OR,
                         Conditions = new List<Condition>
                         {
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Small) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Large) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Large) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Small) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Average) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Average) }
                                  }
                             }
                         }
                    }
                },
                new Statement
                {
                    Result = "Процесс = 3",
                    Condition = new Condition
                    {
                         Type = ConditionType.OR,
                         Conditions = new List<Condition>
                         {
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Average) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Large) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Large) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Average) }
                                  }
                             },
                             new Condition
                             {
                                  Type = ConditionType.AND,
                                  Conditions = new List<Condition>
                                  {
                                      new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Large) },
                                      new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Large) }
                                  }
                             }
                         }
                    }
                },
                new Statement
                {
                    Result = "Процесс = 4",
                    Condition = new Condition
                    {
                        Type = ConditionType.AND,
                        Conditions = new List<Condition>
                        {
                            new Condition { Variable = variableFactory.GetProcessTemperature(VarType.Small, ModType.Con) },
                            new Condition
                            {
                                Type = ConditionType.OR,
                                Conditions = new List<Condition>
                                {
                                    new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Small) },
                                    new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Average) },
                                    new Condition { Variable = variableFactory.GetPressureInMachine(VarType.Large) }
                                }
                            }
                        }
                    }
                }
            };
        }

        public void Dispose() { }
    }
}
