﻿using LW2.RLE.Compressor;
using LW2.RLE.Managers;
using LW2.RLE.Paint;
using LW2.RLE.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW2.RLE
{
    public partial class Form1 : Form
    {
        PaintMethods Paint = new PaintMethods();

        public Form1()
        {
            InitializeComponent();

            Paint.PenSize = tbPenSize.Value * 5;
            lbPenSize.Text = Paint.PenSize.ToString();

            pbCodeImg.Image = Resources.defImg;
        }

        #region настройка кисти
        private void tbPenSize_ValueChanged(object sender, EventArgs e)
        {
            Paint.PenSize = ((TrackBar)sender).Value * 5;
            lbPenSize.Text = Paint.PenSize.ToString();
        }

        private void btnRect_Click(object sender, EventArgs e)
        {
            Paint.PenType = PenType.Rect;
        }

        private void btnCircle_Click(object sender, EventArgs e)
        {
            Paint.PenType = PenType.Circle;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Paint.CanvasClear(pbCodeImg);
        }

        private void btnBlackColor_Click(object sender, EventArgs e)
        {
            Paint.PenColor = Color.Black;
        }

        private void btnWhiteColor_Click(object sender, EventArgs e)
        {
            Paint.PenColor = Color.White;
        }
        #endregion

        #region рисование
        private void pbCodeImg_MouseDown(object sender, MouseEventArgs e)
        {
            Paint.DrawPen(pbCodeImg, e.X, e.Y);
            Paint.ContinuousDraw = true;
        }

        private void pbCodeImg_MouseUp(object sender, MouseEventArgs e)
        {
            Paint.ContinuousDraw = false;
        }

        private void pbCodeImg_MouseMove(object sender, MouseEventArgs e)
        {
            if (Paint.ContinuousDraw)
            {
                Paint.DrawPen(pbCodeImg, e.X, e.Y);
            }
        }

        private void drowNoize_Click(object sender, EventArgs e)
        {
            PaintMethods.DrawNoize(pbCodeImg);
        }
        #endregion

        #region сохранение и загрузка
        private void btnSaveAndCode_Click(object sender, EventArgs e)
        {
            // перевод изображения в массив 
            var bmp = new Bitmap(pbCodeImg.Image);
            var imageData = ImageManager.CreateImageDataByBitmap(bmp, pbCodeImg.Width, pbCodeImg.Height);
            // кодирование
            var compressResult = CompressorMethods.Compressing(imageData);
            // сохранение в файл
            var fileName = DateTime.Now.Ticks.ToString();
            FileManager.SaveFile(compressResult, fileName);
            var bmpFileName = $"{Application.StartupPath}\\UncompressedFiles\\{fileName}.bmp";
            bmp.Save(bmpFileName, ImageFormat.Bmp);
            var fileInfo = new FileInfo(bmpFileName);

            // обновление информации в окне
            var bmpFileSize = fileInfo.Length;
            var compressFileSize = compressResult.Length;

            lbMainFileSize.Text = $"{bmpFileSize / 1024} KB";
            lbArchiveFileSize.Text = $"{compressFileSize / 1024} KB";
            lbPercentCompression.Text = $"{Math.Round(100 - ((compressFileSize / (bmpFileSize * 1.0)) * 100), 2)} %";
        }

        private void btnLoadAndDecode_Click(object sender, EventArgs e)
        {
            // чтение файла 
            var fileContent = FileManager.LoadFile();
            // декодирование 
            var imageData = CompressorMethods.Decompressing(fileContent);
            // вывод на форму
            var bitmap = ImageManager.CreateBitmapByImageData(imageData, pbDecodeImg.Width, pbDecodeImg.Height);
            pbDecodeImg.Image = bitmap;
        }
        #endregion
    }
}
