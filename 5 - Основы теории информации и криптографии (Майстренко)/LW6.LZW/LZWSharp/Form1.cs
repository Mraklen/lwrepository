﻿using LZWLib;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace LZWSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //PrintTable();
        }

        private void btnPack_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();

            var fileName = "";
            if (opf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileName = opf.FileName;
            }

            var text = File.ReadAllText(fileName);
            var encoder = new LZWLib.Encoder();

            var buffer = encoder.EncodeToByteList(text);
            
            var initSize = Encoding.Unicode.GetBytes(text).Length;
            lbInitial.Text = $"{initSize} б";
            lbCompress.Text = $"{buffer.Length} б";
            lbPercent.Text = $"{Math.Round(100 - ((buffer.Length / (initSize * 1.0)) * 100), 2)} %";
            File.WriteAllBytes(fileName + ".lzw", buffer);

            MessageBox.Show("Кодирование завершено.");

            //PrintTable(encoder.dict);
            File.WriteAllLines(fileName + ".txt", encoder.dict.Select(x => $"{x.Key}: {x.Value}").ToArray());
        }

        private void btnUnpack_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "*.lzw|*.lzw";
            opf.InitialDirectory = $"{Application.StartupPath}";
            if (opf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var buffer = File.ReadAllBytes(opf.FileName);
                var decoder = new LZWLib.Decoder();

                var text = decoder.DecodeFromCodes(buffer);
                var fileInfo = new FileInfo(opf.FileName);
                File.WriteAllText($"{fileInfo.DirectoryName}\\dec_{fileInfo.Name.Replace(".lzw", "")}", text, System.Text.Encoding.Default);

                MessageBox.Show("Декодирование завершено.");
            }

        }

        //private void PrintTable(Dictionary<string, int> dict = null)
        //{
        //   // tableAlphabet.Controls.Clear();

        //    var controls = new List<Control>
        //    {
        //        new Label() { Text = "Символ" },
        //        new Label() { Text = "Номер" }
        //    };
        //    if (dict != null)
        //    {
        //        for (var i = 0; i < dict.Count(); i++)
        //        {
        //            var ch = dict.Keys.ElementAt(i);
        //            controls.Add(new Label() { Text = ch });
        //            controls.Add(new Label() { Text = dict[ch].ToString() });
        //        }
        //    }
        //    tableAlphabet.Controls.AddRange(controls.ToArray());
        //}
    }
}
