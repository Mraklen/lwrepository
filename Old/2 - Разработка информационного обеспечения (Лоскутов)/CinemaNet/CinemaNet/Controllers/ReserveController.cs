﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Models;
using CinemaNet.ViewModels;
using CinemaNet.Models.Interfaces;


namespace CinemaNet.Controllers
{
    public class ReserveController : Controller
    {
        public IReserveList reserves;
        public IReserveSeatList reserveSeats;
        public IFilmSessionList filmSessions;
        public ReserveController(IReserveList reserves, IReserveSeatList reserveSeats, IFilmSessionList filmSessions)
        {
            this.reserves = reserves;
            this.reserveSeats = reserveSeats;
            this.filmSessions = filmSessions;
        }

        // GET: /<controller>/
        public IActionResult Page(string reserve, string email, int sessionID)
        {
            Reserve newReserve = new Reserve {
                FilmSessionID = sessionID,
                Email = email
            };
            reserves.AddReserve(newReserve);
            int reserveID = reserves.ReserveList.Where(r => r.Email == email).ToList().ElementAt(0).ReserveID;
            
            string[][] reserveElem = parserStringToArray(reserve);
            for(int i = 0; i < reserveElem.Length; i++)
            {
                int row = Convert.ToInt32(reserveElem[i][0]);
                int seat = Convert.ToInt32(reserveElem[i][1]);
                ReserveSeat newReserveSeat = new ReserveSeat
                {
                    ReserveID = reserveID,
                    Row = row,
                    SeatNum = seat 
                };
                reserveSeats.AddReserveSeats(newReserveSeat);
            }

            var session = filmSessions.FilmSessionLists()
                    .Where(fs => fs.FilmSessionID == sessionID)
                    .ToList().ElementAt(0);
            return View(new ReserveCompleteInfo
            {
                CinemaName = session.Cinema.Name,
                FilmName = session.Film.Name,
                SessionDate = session.Date,
                SessionTime = session.Time,
                CinemaRoomName = session.CinemaRoom.Name,
                ReserveSeats = parserArrayToFormString(reserveElem),
                ReserveID = reserveID.ToString(),
                ReserveStatus = "Билеты забронированы 9 ноября 2017 г."
            });
        }

        private string parserArrayToFormString(string[][] array)
        {
            string str = "";
            for (int i = 0; i < array.Length; i++)
            {
                str += "Ряд: " + array[i][0] + " / Место: " + array[i][1] + "; ";
            }
            return str;
        }
        private string[][] parserStringToArray(string str)
        {
            var reserveArr = str.Split(new string[] { "; " }, StringSplitOptions.None);
            int lenght = reserveArr.Length - 1;
            string[][] array = new string[lenght][];

            for (int i = 0; i < lenght; i++)
            {
                array[i] = reserveArr[i].Split(new string[] { " / " }, StringSplitOptions.None);
            }

            return array;
        }
    }
}
