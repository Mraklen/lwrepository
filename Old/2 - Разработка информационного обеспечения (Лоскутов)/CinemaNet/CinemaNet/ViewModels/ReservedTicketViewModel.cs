﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.ViewModels
{
    public class ReservedTicketViewModel
    {
        public string FilmName { get; set; }
        public string Date { get; set; }
        public string Seats { get; set; }
        public string CinemaName { get; set; }
        public string CinemaAddress { get; set; }
    }
}
