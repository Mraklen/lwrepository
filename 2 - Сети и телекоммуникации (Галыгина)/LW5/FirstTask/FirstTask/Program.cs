﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("\nПерейдите в браузере по ссылке: http://127.0.0.1:80/");
            new Server(80);
        }
    }
    class Server
    {
        TcpListener listener;
        public Server(int port)
        {
            listener = new TcpListener(IPAddress.Any, port);
            listener.Start();
            while (true)
                new Client(listener.AcceptTcpClient());
        }
        ~Server()
        {
            if (listener != null)
                listener.Stop();
        }
    }

    class Client
    {
        public Client(TcpClient client)
        {
            var fileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);
            var fileName = $"{fileInfo.Directory}\\HtmlPage\\TestPage.html";
            using (StreamReader sr = new StreamReader(fileName))
            {
                string html = sr.ReadToEnd();
                string str = $"{html}";
                byte[] buffer = Encoding.UTF8.GetBytes(str);
                client.GetStream().Write(buffer, 0, buffer.Length);
                client.Close();
            }
        }
    }
}
