﻿using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.FuzzyStatement
{
    public class Statement
    {
        public string Result { get; set; }
        public Condition Condition { get; set; }

        public double CheckCondition(IEnumerable<IVariable> variables)
        {
            var result = Condition.CheckCondition(variables);
            return result;
        }
    }
}
