﻿using LW3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CW.Methods
{
    public class GradientMethod
    {
        public FunctionParam Function;

        public async void Calc(Label label)
        {
            var model = new MathModel();

            //double x1 = new Random((int)DateTime.Now.Ticks / 5).Next((int)model.Tmin, (int)model.Tmax);
            //double x2 = new Random((int)DateTime.Now.Ticks / 10).Next((int)model.Vmin, (int)model.Vmax);
            double x1 = 620;
            double x2 = 37;

            Function = new FunctionParam(x1, x2, 0.00001);

            double dx, dx1, dx2, dx11, dx22, e, s, b, d1x1, d2x2, f, k = 0.1, c = 1;

            x1 = FunctionParam.X1Value;
            x2 = FunctionParam.X2Value;
            e = FunctionParam.EValue;
            
            //проверить условие попадаения точки, если выбран внутренний способ штрафа
            if (FunctionParam.CurrenFineType != FineType.Internal)
            {
                k = 100;
            }

            dx1 = 5;
            dx2 = 1;

            do
            {
                dx = dx1;
                dx1 = dx1 / 2;

                dx11 = (Function.CalcFunction(x1 + dx, x2, k) - Function.CalcFunction(x1, x2, k)) / dx;
                dx22 = (Function.CalcFunction(x1 + dx1, x2, k) - Function.CalcFunction(x1, x2, k)) / dx1;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx1 = dx;

            do
            {
                dx = dx2;
                dx2 = dx2 / 2;

                dx11 = (Function.CalcFunction(x1, x2 + dx, k) - Function.CalcFunction(x1, x2, k)) / dx;
                dx22 = (Function.CalcFunction(x1, x2 + dx2, k) - Function.CalcFunction(x1, x2, k)) / dx2;
            }
            while (Math.Abs(dx11 - dx22) > e);

            dx2 = dx;

            var tmpPoint = new PointXY() { X1 = x1, X2 = x2, Y = Function.CalcFunction(x1, x2, k) };
            var point = new PointXY();
            var res = false;
            var count = 0;
            do
            {
                s = HalfDivMethod(x1, x2, dx11, dx22, e, k);

                x1 = x1 - dx11 * s;
                x2 = x2 - dx22 * s;

                d1x1 = (Function.CalcFunction(x1 + dx1, x2, k) - Function.CalcFunction(x1, x2, k)) / dx1;
                d2x2 = (Function.CalcFunction(x1, x2 + dx2, k) - Function.CalcFunction(x1, x2, k)) / dx2;

                b = (Math.Pow(d1x1, 2) + Math.Pow(d2x2, 2)) / (Math.Pow(dx11, 2) + Math.Pow(dx22, 2));

                dx11 = d1x1 + dx11 * b;
                dx22 = d2x2 + dx22 * b;

                //var tf = Function.CalcFunction(x1, x2, 1);
                f = Function.CalcFunction(x1, x2, k);
                point = new PointXY() { X1 = x1, X2 = x2, Y = f };

                if (FunctionParam.CurrenFineType == FineType.Internal)
                    k *= c;
                else
                    k = 100;

                label.Content = ++count + " : ";
                label.Content += point.PrintPointXY();

                res = Math.Abs(tmpPoint.Y - point.Y) > e;
                tmpPoint = point;
                
                await Task.Delay(20);
            }
            while (res);


            var end = res;
        }


        public double HalfDivMethod(double x1, double x2, double dx1, double dx2, double e, double k)
        {
            double a0, b0, a1, b1, x01, x02, x11, x12, f0, f1;

            a0 = x1;
            a1 = x1 - dx1 * 100;
            b0 = x2;
            b1 = x2 - dx2 * 100;

            do
            {
                x01 = a0 + (a1 - a0) / 4;
                x02 = b0 + (b1 - b0) / 4;
                x11 = a1 - (a1 - a0) / 4;
                x12 = b1 - (b1 - b0) / 4;

                f0 = Function.CalcFunction(x01, x02, k);
                f1 = Function.CalcFunction(x11, x12, k);

                if (f0 > f1)
                {
                    a0 = x01;
                    b0 = x02;
                }
                else
                {
                    a1 = x11;
                    b1 = x12;
                }
            }
            while (Math.Sqrt((a1 - a0) * (a1 - a0) + (b1 - b0) * (b1 - b0)) > e);

            if ((dx1 == 0) || (dx2 == 0))
            {
                return 0;
            }
            else
            {
                return -(((a1 + a0) / 2 - x1) / dx1 + ((b1 + b0) / 2 - x2) / dx2) / 2;
            }
        }

    }
}
