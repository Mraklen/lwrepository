﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LW1.CreateModels;
using LW1.Data.Repositories;
using LW1.Models.Project;
using LW1.Providers;
using Microsoft.AspNetCore.Mvc;

namespace LW1.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IDomainRepositoryFactory repositoryFactory;
        private readonly IUserProvider userProvider;

        public ProjectController(IDomainRepositoryFactory repositoryFactory, IUserProvider userProvider)
        {
            this.repositoryFactory = repositoryFactory;
            this.userProvider = userProvider;
        }

        public IActionResult CreateProject()
        {
            return View();
        }

        public RedirectResult NewProject(ProjectCreateModel project)
        {
            var projectRepos = repositoryFactory.GetProjectRepository();
            var newProject = Project.CreateProject(userProvider.CurrentUserId, project.Name, project.MainUrl, project.KeyWords);
            projectRepos.Create(newProject);

            return Redirect("/Home/Index");
        }

        public IActionResult ViewProject(Guid id)
        {
            var repos = repositoryFactory.GetProjectRepository();
            var project = repos.GetProjectViewModelById(id);
            return View(project);
        }
    }
}
