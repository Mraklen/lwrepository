﻿namespace LW4.ArithmeticCoding
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateArchive = new System.Windows.Forms.Button();
            this.btnOpenArchive = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbAlphabetCount = new System.Windows.Forms.Label();
            this.lbPercentCompression = new System.Windows.Forms.Label();
            this.lbSymbolCount = new System.Windows.Forms.Label();
            this.lbArchiveFileSize = new System.Windows.Forms.Label();
            this.lbMainFileSize = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableAlphabet = new System.Windows.Forms.TableLayoutPanel();
            this.pbUnarchive = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreateArchive
            // 
            this.btnCreateArchive.Location = new System.Drawing.Point(13, 13);
            this.btnCreateArchive.Name = "btnCreateArchive";
            this.btnCreateArchive.Size = new System.Drawing.Size(218, 23);
            this.btnCreateArchive.TabIndex = 0;
            this.btnCreateArchive.Text = "Создать архив";
            this.btnCreateArchive.UseVisualStyleBackColor = true;
            this.btnCreateArchive.Click += new System.EventHandler(this.btnCreateArchive_Click);
            // 
            // btnOpenArchive
            // 
            this.btnOpenArchive.Location = new System.Drawing.Point(233, 13);
            this.btnOpenArchive.Name = "btnOpenArchive";
            this.btnOpenArchive.Size = new System.Drawing.Size(218, 23);
            this.btnOpenArchive.TabIndex = 1;
            this.btnOpenArchive.Text = "Открыть архив";
            this.btnOpenArchive.UseVisualStyleBackColor = true;
            this.btnOpenArchive.Click += new System.EventHandler(this.btnOpenArchive_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbAlphabetCount);
            this.groupBox1.Controls.Add(this.lbPercentCompression);
            this.groupBox1.Controls.Add(this.lbSymbolCount);
            this.groupBox1.Controls.Add(this.lbArchiveFileSize);
            this.groupBox1.Controls.Add(this.lbMainFileSize);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 93);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация";
            // 
            // lbAlphabetCount
            // 
            this.lbAlphabetCount.AutoSize = true;
            this.lbAlphabetCount.Location = new System.Drawing.Point(324, 42);
            this.lbAlphabetCount.Name = "lbAlphabetCount";
            this.lbAlphabetCount.Size = new System.Drawing.Size(13, 13);
            this.lbAlphabetCount.TabIndex = 9;
            this.lbAlphabetCount.Text = "0";
            // 
            // lbPercentCompression
            // 
            this.lbPercentCompression.AutoSize = true;
            this.lbPercentCompression.Location = new System.Drawing.Point(324, 20);
            this.lbPercentCompression.Name = "lbPercentCompression";
            this.lbPercentCompression.Size = new System.Drawing.Size(13, 13);
            this.lbPercentCompression.TabIndex = 8;
            this.lbPercentCompression.Text = "0";
            // 
            // lbSymbolCount
            // 
            this.lbSymbolCount.AutoSize = true;
            this.lbSymbolCount.Location = new System.Drawing.Point(345, 64);
            this.lbSymbolCount.Name = "lbSymbolCount";
            this.lbSymbolCount.Size = new System.Drawing.Size(13, 13);
            this.lbSymbolCount.TabIndex = 7;
            this.lbSymbolCount.Text = "0";
            // 
            // lbArchiveFileSize
            // 
            this.lbArchiveFileSize.AutoSize = true;
            this.lbArchiveFileSize.Location = new System.Drawing.Point(116, 42);
            this.lbArchiveFileSize.Name = "lbArchiveFileSize";
            this.lbArchiveFileSize.Size = new System.Drawing.Size(13, 13);
            this.lbArchiveFileSize.TabIndex = 6;
            this.lbArchiveFileSize.Text = "0";
            // 
            // lbMainFileSize
            // 
            this.lbMainFileSize.AutoSize = true;
            this.lbMainFileSize.Location = new System.Drawing.Point(116, 20);
            this.lbMainFileSize.Name = "lbMainFileSize";
            this.lbMainFileSize.Size = new System.Drawing.Size(13, 13);
            this.lbMainFileSize.TabIndex = 5;
            this.lbMainFileSize.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Размер алфавита:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(217, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Процент сжатия:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество символов:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Размер архива:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Исходный размер:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableAlphabet);
            this.panel1.Location = new System.Drawing.Point(13, 143);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(438, 349);
            this.panel1.TabIndex = 3;
            // 
            // tableAlphabet
            // 
            this.tableAlphabet.AutoScroll = true;
            this.tableAlphabet.BackColor = System.Drawing.Color.White;
            this.tableAlphabet.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableAlphabet.ColumnCount = 3;
            this.tableAlphabet.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableAlphabet.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableAlphabet.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableAlphabet.Location = new System.Drawing.Point(0, 0);
            this.tableAlphabet.Name = "tableAlphabet";
            this.tableAlphabet.RowCount = 1;
            this.tableAlphabet.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableAlphabet.Size = new System.Drawing.Size(438, 349);
            this.tableAlphabet.TabIndex = 0;
            // 
            // pbUnarchive
            // 
            this.pbUnarchive.Location = new System.Drawing.Point(13, 13);
            this.pbUnarchive.Maximum = 1;
            this.pbUnarchive.Name = "pbUnarchive";
            this.pbUnarchive.Size = new System.Drawing.Size(438, 23);
            this.pbUnarchive.Step = 1;
            this.pbUnarchive.TabIndex = 4;
            this.pbUnarchive.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 504);
            this.Controls.Add(this.pbUnarchive);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOpenArchive);
            this.Controls.Add(this.btnCreateArchive);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "ЛР4";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateArchive;
        private System.Windows.Forms.Button btnOpenArchive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbAlphabetCount;
        private System.Windows.Forms.Label lbPercentCompression;
        private System.Windows.Forms.Label lbSymbolCount;
        private System.Windows.Forms.Label lbArchiveFileSize;
        private System.Windows.Forms.Label lbMainFileSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableAlphabet;
        private System.Windows.Forms.ProgressBar pbUnarchive;
    }
}

