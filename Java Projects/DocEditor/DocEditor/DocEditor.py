import docx
import random
from DocContent import DocContent
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Inches

def generate_doc():
    document = docx.Document();

    print('Размер шрифта:')
    fontSize = input()
    
    print('Отступ:')
    indent = input()
    
    print('Номер стиля таблиц [0, 3]:')
    borderStyle = input()
    
    print('Сколько страниц добавить в конце документа?:')
    pageCount = input()

    for i in range(random_value(20, 40)):
        add_block(document, i, indent, fontSize, borderStyle)

    for i in range(int(pageCount)):
        page = document.add_page_break()

    document.save('Result Document.docx');

def add_block(document, i, indent, fontSize, borderStyle):
    content = DocContent()
    blockType = random_value(1, 3)


    if (blockType == 1):
        # блок текста        
        headerCheck = random_boolean()
        if (headerCheck):
            document.add_page_break()
            document.add_heading(content.get_header(i), 0)

        paragraph = document.add_paragraph()

        for i in range(random_value(1, 10)):
            pf = paragraph.paragraph_format
            pf.space_before = docx.shared.Pt(int(indent))
            pf.space_after = docx.shared.Pt(int(indent))

            run = paragraph.add_run()
            text = content.get_paragraph_run()

            run.add_text(text)

            run.italic = random_boolean()
            run.underline = random_boolean()
            run.bold = random_boolean()

            run.font.size = docx.shared.Pt(int(fontSize))

    if (blockType == 2):
        # блок с картинкой

        # добавить параграф и отступ
        paragraph = document.add_paragraph()
        pf = paragraph.paragraph_format
        pf.space_before = docx.shared.Pt(int(indent))
        pf.space_after = docx.shared.Pt(int(indent))

        # добавить run и картинку, центрировать
        run = paragraph.add_run()
        run.add_picture(content.get_picture_path(), width = docx.shared.Cm(10))
        paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

    if (blockType == 3):
        tableData = content.get_table_data()
        table = document.add_table(rows = len(tableData), cols = len(tableData[0]))
        table.style = str(content.get_table_style(int(borderStyle)))

        for i in range(len(tableData)):
            for j in range(len(tableData[0])):
                cell = table.cell(i, j)
                if (i == 0):
                    cell.text = 'Колонка ' + str(j)
                else:
                    cell.text = str(tableData[i][j])


def random_value(start, end):
    return random.randint(start, end)

def random_boolean():
    return bool(random.getrandbits(1))

generate_doc()