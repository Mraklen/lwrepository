﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models;

namespace CinemaNet.ViewModels
{
    public class CinemaFilmModel
    {
        public IEnumerable<Cinema> Cinemas { get; set; }
        public IEnumerable<Film> Films { get; set; }
    }
}
