﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LW1.Model;

namespace LW1.Servises
{
    public class AlphabetItem : Alphabet
    {
        public AlphabetItem() { }
        public AlphabetItem(string name, string symbols)
        {
            Name = name;
            Symbols = symbols;
            PowerAlphabet = CalcPowerAlphabet(symbols);
            DimensionSymbol = CalcDimensionSymbol(PowerAlphabet);
        }
        
        public static int CalcPowerAlphabet(string symbols)
        {
            return symbols.Length;
        }
        public static int CalcDimensionSymbol(int powerAlphabet)
        {
            return (int) Math.Floor(Math.Log(powerAlphabet, 2));
        }
    }
}
