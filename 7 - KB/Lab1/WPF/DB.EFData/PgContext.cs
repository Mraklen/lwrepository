﻿using Domain.Models.Entities.KnowledgeBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.EFData
{
    class PgContext : DbContext
    {
        //public PgContext(): base("PgConnection") { }
        public PgContext(): base("PgConnectionHome") { }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            mb.HasDefaultSchema("public");

            // таблицы
            mb.Entity<Parameter>().HasKey(x => x.Id);

            mb.Entity<Variable>().HasKey(x => x.Id);

            mb.Entity<Rule>().HasKey(x => x.Id);
            mb.Entity<RuleParameter>().HasKey(x => x.Id);

            // связи
            mb.Entity<Variable>().HasMany(x => x.Parameters).WithRequired(x => x.Variable).HasForeignKey(x => x.VariableId);
            mb.Entity<Parameter>().HasRequired(x => x.Variable).WithMany(x => x.Parameters).HasForeignKey(x => x.VariableId);

            mb.Entity<Rule>().HasMany(x => x.RuleParameters).WithRequired(x => x.Rule).HasForeignKey(x => x.RuleId);
            mb.Entity<Parameter>().HasMany(x => x.RuleParameters).WithRequired(x => x.Parameter).HasForeignKey(x => x.ParameterId);
            
            mb.Entity<RuleParameter>().HasRequired(x => x.Parameter).WithMany(x => x.RuleParameters).HasForeignKey(x => x.ParameterId);
            mb.Entity<RuleParameter>().HasRequired(x => x.Rule).WithMany(x => x.RuleParameters).HasForeignKey(x => x.RuleId);
            
            base.OnModelCreating(mb);
        }

        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<RuleParameter> RuleParameters { get; set; }
    }
}
