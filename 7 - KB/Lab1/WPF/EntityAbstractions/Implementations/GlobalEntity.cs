﻿using EntityAbstractions.Entities;
using System;

namespace EntityAbstractions.Implementations
{
    public abstract class GlobalEntity : ITrackedEntity, IEntity<Guid>, IAggregate
    {
        public Guid Id { get; protected set; }

        public DateTime CreationDate { get; protected set; }
        
        public GlobalEntity()
        {
            CreationDate = DateTime.UtcNow;
        }
    }
}
