﻿using EntityAbstractions.Entities;
using EntityAbstractions.Implementations;

namespace Domain.Models.Repositories.Interfaces
{
    public interface IDomainRepositoryFactory
    {
        IBaseAggregateRepository<TAggregate> GetAggregateRepository<TAggregate>() where TAggregate : GlobalEntity, IAggregate;
        IBaseRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : BaseEntity;

        IRuleRepository GetRuleRepository();
        IVariableRepository GetVariableRepository();
    }
}
