﻿using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.ViewModels.KnowledgeBase
{
    public class ParameterViewModel : BaseViewModel<int>
    {
        public string Name { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
    }
}
