﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.RLE.Compressor
{
    public class CompressorMethods
    {
        public static byte[] Compressing(List<int> imageData)
        {
            var bytes = new List<byte>();

            var oldValue = imageData[0];
            var countRepeats = 0;

            for (var i = 1; i <= imageData.Count(); i++)
            {
                var check = (i < imageData.Count()) ? (oldValue == imageData[i]) : false;
                if (check && (i + 1 != imageData.Count()) && countRepeats < 16383)
                {
                    countRepeats++;
                }
                else
                {
                    var b = BitConverter.GetBytes(countRepeats).Take(countRepeats > 63 ? 2 : 1).ToArray();
                    var bits = new BitArray(b);

                    bits[bits.Length - 1] = (countRepeats > 63);
                    bits[bits.Length - 2] = (oldValue == 1);

                    var bytes2 = BitArrayToByteArray(bits);
                    bytes.AddRange(bytes2.Reverse());

                    countRepeats = 0;
                    if (i < imageData.Count())
                        oldValue = imageData[i];
                }
            }

            var falseValue = bytes.Where(x => x == 0).Count();
            var trueValue = bytes.Where(x => x == 64).Count();

            return bytes.ToArray();
        }

        public static List<int> Decompressing(byte[] fileData)
        {
            var imageData = new List<int>();

            for (var i = 0; i < fileData.Length; i++)
            {
                var firstByte = fileData[i];
                var bs = new BitArray(new byte[] { firstByte });

                var color = Convert.ToInt32(bs[bs.Length - 2]);
                var over63 = bs[bs.Length - 1];

                if (over63)
                {
                    var secondByte = fileData[++i];
                    bs = new BitArray(new byte[] { secondByte, firstByte });
                }

                bs[bs.Length - 1] = false;
                bs[bs.Length - 2] = false;

                var count = BitConverter.ToInt16(BitArrayToByteArray16(bs), 0);

                imageData.AddRange((new int[count + 1]).Select(x => color));
            }

            return imageData;
        }

        private static byte[] BitArrayToByteArray(BitArray bits)
        {
            byte[] ret = new byte[(bits.Length - 1) / 8 + 1];
            bits.CopyTo(ret, 0);
            return ret;
        }

        private static byte[] BitArrayToByteArray16(BitArray bits)
        {
            byte[] ret = new byte[2];
            bits.CopyTo(ret, 0);
            return ret;
        }
    }
}
