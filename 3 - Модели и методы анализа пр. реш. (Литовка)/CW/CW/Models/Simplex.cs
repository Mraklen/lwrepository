﻿using LW3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.Models
{
    public class Simplex
    {
        public static double[] x10 = new double[3];
        public static double[] x20 = new double[3];
        public static double[] f1 = new double[3];

        public static int IndexMinValue()
        {
            return Array.FindIndex(f1, val => val == f1.Min());
        }

        public static List<PointXY> PrintPointXY()
        {
            return new List<PointXY>()
            {
                new PointXY() { X1 = x10[0], X2 = x20[0] },
                new PointXY() { X1 = x10[1], X2 = x20[1] },
                new PointXY() { X1 = x10[2], X2 = x20[2] },
                new PointXY() { X1 = x10[0], X2 = x20[0] }
            };
        }

        public static string PrintMinValue()
        {
            var index = IndexMinValue();
            
            return $"(T: {Math.Round(x10[index], 2)}; V: {Math.Round(x20[index], 2)}) => {Math.Round(f1[index], 2) * -1}";
        }

        public static PointXY GetMinValue()
        {
            var index = IndexMinValue();
            return new PointXY { X1 = x10[index], X2 = x20[index], Y = f1[index] };
        }
    }
}
