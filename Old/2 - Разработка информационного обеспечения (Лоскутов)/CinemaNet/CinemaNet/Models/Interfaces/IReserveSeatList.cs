﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface IReserveSeatList
    {
        IEnumerable<ReserveSeat> ReserveSeatList { get; }
        void AddReserveSeats(ReserveSeat reserve);
    }
}
