﻿using DB.EFData;
using Domain.Models.Repositories.Interfaces;
using EntityAbstractions.Entities;
using EntityAbstractions.Implementations;
using System;
using System.Threading.Tasks;

namespace DB.EF.Data.Repositories.Implementations
{
    class DefaultAggregateRepository<TAggreate> : IBaseAggregateRepository<TAggreate> where TAggreate : GlobalEntity, IAggregate
    {
        protected readonly PgContext context;

        public DefaultAggregateRepository(PgContext context) { this.context = context; }
        
        public TAggreate GetById(Guid id)
        {
            return context.Set<TAggreate>().Find(id);
        }

        public async Task<TAggreate> GetByIdAsync(Guid id)
        {
            return await context.Set<TAggreate>().FindAsync(id);
        }

        public void Update(TAggreate aggregate)
        {
            context.Set<TAggreate>().Attach(aggregate);
            context.SaveChanges();
        }

        public async Task UpdateAsync(TAggreate aggregate)
        {
            context.Set<TAggreate>().Attach(aggregate);
            await context.SaveChangesAsync();
        }

        public void Create(TAggreate aggregate)
        {
            context.Set<TAggreate>().Add(aggregate);
            context.SaveChanges();
        }

        public async Task CreateAsync(TAggreate aggregate)
        {
            context.Set<TAggreate>().Add(aggregate);
            await context.SaveChangesAsync();
        }
        
        public async Task Delete(TAggreate aggregate)
        {
            context.Set<TAggreate>().Remove(aggregate);
            await context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var existing = await GetByIdAsync(id);
            if (existing == null) return;

            await Delete(existing);
        }
    }
}
