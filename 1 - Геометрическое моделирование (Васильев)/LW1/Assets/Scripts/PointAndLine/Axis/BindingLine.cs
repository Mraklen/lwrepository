﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BindingLine : MonoBehaviour {
    
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Point"))
        {
            var point = other.gameObject.GetComponent<Point>();
            if (!point.isMove)
            {
                point.transform.position = new Vector3(transform.position.x, point.transform.position.y, transform.position.z);
            }
        }
    }
}
