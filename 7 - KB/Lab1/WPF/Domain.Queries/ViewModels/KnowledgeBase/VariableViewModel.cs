﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Models.Entities.KnowledgeBase;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.ViewModels.KnowledgeBase
{
    public class VariableViewModel : BaseViewModel<int>
    {
        public string Name { get; set; }
        public string CodeName { get; set; }
        public VarType VarType { get; set; }

        public IEnumerable<ParameterViewModel> Parameters { get; set; }
    }
}
