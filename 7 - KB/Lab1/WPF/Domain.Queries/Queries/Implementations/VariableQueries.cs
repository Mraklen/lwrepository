﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Queries.Queries.Interfaces;
using Domain.Queries.ViewModels.KnowledgeBase;
using QueryAbstractions.Queries;
using QueryAbstractions.Queries.Implementations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.Queries.Implementations
{
    public class VariableQueries : AbstractQueries<VariableReadModel, VariableViewModel, int>, IVariableQueries
    {
        public VariableQueries(IQueryDatabase db) : base(db) { }

        protected override Expression<Func<VariableReadModel, VariableViewModel>> Projection => x => new VariableViewModel
        {
            Id = x.Id,
            CodeName = x.CodeName,
            Name = x.Name,
            VarType = x.VarType,
            Parameters = x.Parameters.Select(p => new ParameterViewModel
            {
                Id = p.Id,
                Description = p.Description,
                Name = p.Name,
                Question = p.Question
            }).ToList()
        };

        public async Task<IEnumerable<VariableViewModel>> GetListAsync()
        {
            var quer = Queryable;
            var sel = quer.Select(Projection);
            var list = await sel.ToListAsync();
            return null;
        }
    }
}
