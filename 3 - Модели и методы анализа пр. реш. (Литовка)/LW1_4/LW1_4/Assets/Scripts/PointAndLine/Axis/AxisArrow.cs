﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AxisType
{
    X, Y, Z, None
}

public class AxisArrow : MonoBehaviour
{
    [SerializeField]
    public AxisType arrowType;
    [Space(10)]
    [SerializeField]
    Material DefaultMaterial;
    [SerializeField]
    Material TargetMaterial;
    [Space(10)]
    [SerializeField]
    MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public void SetMaterial(bool selected)
    {
        meshRenderer.material = selected ? TargetMaterial : DefaultMaterial;
    }
}
