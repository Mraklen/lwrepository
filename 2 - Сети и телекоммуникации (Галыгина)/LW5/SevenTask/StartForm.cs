﻿using Newtonsoft.Json;
using SeatBattle.CSharp.EventManager;
using SeatBattle.CSharp.Net;
using SeatBattle.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeatBattle.CSharp
{
    public partial class StartForm : Form
    {
        string host = "localhost";
        int port = 5000;
        string Ip { get { return $"{host}:{port}"; } }

        private NetworkManager NetworkManager;

        public StartForm()
        {
            InitializeComponent();
            NewRandomPort();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            NewRandomPort();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            var ipParam = serverHost.Text.Split(':');
            var enemPort = Convert.ToInt32(ipParam[1]);

            Server.myPort = port;
            Server.enemyPort = enemPort;

            NetworkManager.PostMessageToServer(new PostPackage()
            {
                Type = PackageType.ConnectToServer,
                Content = JsonConvert.SerializeObject(Server.myPort)
            });

            GlobalEventManager.Instance.PostEventMessage(EVENT_TYPE.StartGame);
        }

        public void NetworkStop()
        {
            if (NetworkManager != null)
                NetworkManager.StopServer();
        }

        private void NewRandomPort()
        {
            port = (new Random()).Next(5000, 5500);
            Server.myPort = port;
            myHost.Text = Ip;
        }

        private void startServerBtn_Click(object sender, EventArgs e)
        {
            Server.isMainServer = true;
            NetworkManager = new NetworkManager();
            NetworkManager.StartNewServer();
        }
    }
}
