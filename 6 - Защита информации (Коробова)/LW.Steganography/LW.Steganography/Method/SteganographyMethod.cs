﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace LW.Steganography.Method
{
    public class SteganographyMethod
    {
        public static string Encrypt(string filePath, string message, Color color)
        {
            string resultPath = null;

            message = message.ToLower();
            var maxIndex = message.Count();
            var chIndex = 0;
            var index = 0;

            var word = new Application { Visible = false };
            var doc = new Document();
            doc = word.Documents.Open(filePath, ReadOnly: false);
            doc.Activate();

            foreach(Range symbol in doc.Characters)
            {
                var text = symbol.Text.ToLower();
                if (message[index].Equals(text[0]))
                {
                    object begin = chIndex, end = chIndex + 1;
                    var range = doc.Range(ref begin, ref end);
                    range.Select();
                    
                    range.Font.Shading.BackgroundPatternColor = (WdColor)ColorTranslator.ToOle(color);

                    index++;
                    if (index == maxIndex) break;
                }
                chIndex++;
            }
            
            resultPath = $"{new FileInfo(filePath).DirectoryName}/encrypt.docx";
            doc.SaveAs2(resultPath);
            doc.Close();
            word.Quit();
            
            return resultPath;
        }
        
        public static string Decrypt(string filePath, Color color)
        {
            var result = "";

            var word = new Application { Visible = false };
            var doc = new Document();
            doc = word.Documents.Open(filePath, ReadOnly: false);
            doc.Activate();

            var wdColor = (WdColor)ColorTranslator.ToOle(color);
            var chIndex = 0;

            foreach (Range symbol in doc.Characters)
            {
                var text = symbol.Text.ToLower();
                object begin = chIndex, end = chIndex + 1;
                var range = doc.Range(ref begin, ref end);
                range.Select();
                
                if(range.Font.Shading.BackgroundPatternColor == wdColor)
                {
                    result += text;
                }

                chIndex++;
            }
            
            doc.Close();
            word.Quit();

            return result;
        }
    }
}
