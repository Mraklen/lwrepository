﻿using CW.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CW
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SolidColorBrush[] colors = new SolidColorBrush[] {
                Brushes.Red,
                Brushes.Green,
                Brushes.Blue,
                Brushes.Yellow,
                Brushes.Black
            };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Calc(object sender, RoutedEventArgs e)
        {
            new LW2.Services.Methods().Calc(plotterArr_Result_z, plotterArr_Result, Result, Result);
        }
    }
}
