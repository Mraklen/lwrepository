﻿using QueryAbstractions.ReadModels;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace QueryAbstractions.Queries.Implementations
{
    public class BaseQueries<TEntity, TView, TID> : AbstractQueries<TEntity, TView, TID>
        where TEntity : BaseReadModel<TID>
        where TView : BaseViewModel<TID>
        where TID : struct, IEquatable<TID>
    {
        public BaseQueries(IQueryDatabase db) : base(db) { }

        protected override Expression<Func<TEntity, TView>> Projection => x => null;
    }
}
