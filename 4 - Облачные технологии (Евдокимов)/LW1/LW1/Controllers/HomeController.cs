﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LW1.Models;
using LW1.Providers;
using LW1.Data;
using LW1.Data.Repositories;

namespace LW1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserProvider userProvider;
        private readonly IDomainRepositoryFactory repositoryFactory;

        public HomeController(IUserProvider userProvider, IDomainRepositoryFactory repositoryFactory)
        {
            this.userProvider = userProvider;
            this.repositoryFactory = repositoryFactory;
        }

        public IActionResult Index()
        {
            var projects = repositoryFactory.GetProjectRepository().GetProjectsByUserId(userProvider.CurrentUserId).ToList();
            return View(projects);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
