﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Extentions;
using System;

public class MainPoint : Point
{
    [Space(10)]
    [SerializeField]
    Point FirstVector;
    [SerializeField]
    Point SecondVector;
    [Space(10)]
    [SerializeField]
    LineRenderer LineRenderer;
    [Space(10)]
    [SerializeField]
    Material MainMaterial;
    [SerializeField]
    Material TransparentMaterial;

    float time = 0;
    bool showVector = false;

    public Vector3 GetFirstPointPosition { get { return FirstVector.transform.position; } }
    public Vector3 GetSecondPointPosition { get { return transform.position - (SecondVector.transform.position - transform.position); } }

    void Update()
    {
        if (showVector)
        {
            time += Time.deltaTime;
            if (time > 0.04f)
            {
                VectorRotation();

                LineRenderer.LineRenderByPoints(new List<Vector3>() {
                    FirstVector.transform.localPosition,
                    Vector3.zero,
                    SecondVector.transform.localPosition
                });
                time = 0;
            }
        }
        else
        {
            LineRenderer.Clear();
        }
    }

    public void Invisible(bool showOnlyCalcLine)
    {
        CurrentMaterial = (showOnlyCalcLine) ? TransparentMaterial : lastMaterial;
    }

    private void VectorRotation()
    {
        if (FirstVector.isSelect)
        {
            transform.LookAt(GetFirstPointPosition);
        }
        if (SecondVector.isSelect)
        {
            transform.LookAt(GetSecondPointPosition);
        }
    }

    public void VectorShow(bool isSwow)
    {
        showVector = isSwow;
        FirstVector.gameObject.SetActive(isSwow);
        SecondVector.gameObject.SetActive(isSwow);
    }

    public JsonMainPoint GetJsonMainPoint()
    {
        return new JsonMainPoint
        {
            Main = GetJsonPoint(),
            FirstVector = FirstVector.GetJsonPoint(),
            SecondVector = SecondVector.GetJsonPoint()
        };
    }

    internal void VectorUpdate(Vector3? fvPos, Vector3? svPos)
    {
        FirstVector.transform.position = fvPos.Value;
        SecondVector.transform.position = svPos.Value;
    }
}
