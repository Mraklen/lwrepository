﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Unity;
using WPF.Controls.NeuronNet;
using WPF.Helpers;
using WPF.Modules.Base;
using WPF.ViewModels.Base;
using WPF.ViewModels.NeuronNet;

namespace WPF.Modules.NeuronNet
{
    class NeuronNetModule : BaseModule, IDisposable
    {
        public override string Name => "Нейронная сеть";

        protected override UserControl View { get; set; }
        protected override BaseViewModel ViewModel { get; set; }

        public NeuronNetModule(IUnityContainer container, SettingType type) : base(container, type) { }

        protected override void CreateViewAndViewModel()
        {
            View = new NeuronNetPage();
            ViewModel = container.Resolve<NeuronNetViewModel>();
        }

        public void Dispose() { }
    }
}
