﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPF.Helpers
{
    public enum SettingType
    {
        Default = 1
    }

    public class WindowSetting
    {
        public double Scale { get; set; }
        public SizeToContent SizeToContent { get; set; }
        public WindowState WindowState { get; set; }
        public bool ShowInTaskbar { get; set; }
        public WindowStyle WindowStyle { get; set; }

        public WindowSetting(SettingType type)
        {
            switch (type)
            {
                case SettingType.Default: SetDefaultSetup(); break;
            }
        }
        
        public void SetDefaultSetup()
        {
            SizeToContent = SizeToContent.WidthAndHeight;
            WindowState = WindowState.Normal;
            ShowInTaskbar = true;
            WindowStyle = WindowStyle.SingleBorderWindow;
        }
    }
}
