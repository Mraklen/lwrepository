﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Models.Project
{
    public class KeyWord : IEntity<int>
    {
        public int Id { get; private set; }

        public Guid ProjectId { get; private set; }
        public Project Project { get; private set; }

        public string Value { get; private set; }

        public static KeyWord CreateNew(string value)
        {
            return new KeyWord()
            {
                Value = value
            };
        }
    }
}
