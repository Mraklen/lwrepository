﻿using LW1.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Configs
{
    public class DIConfig
    {
        public static Dictionary<Type, Type> GetMap()
        {
            return new Dictionary<Type, Type>
            {
                {typeof(IDomainRepositoryFactory), typeof(PgSqlRepositoryFactory) },
            };
        }
    }
}
