﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.EditModels
{
    public class KeyWordResultEditModel
    {
        public int Id { get; set; }

        public int RegionId { get; set; }
        public object Engine { get; set; }

        public int ServerKeywordId { get; set; }
        public string UrlValue { get; set; }
        public int Order { get; set; }

        public DateTime OnDate { get; set; }
    }
}
