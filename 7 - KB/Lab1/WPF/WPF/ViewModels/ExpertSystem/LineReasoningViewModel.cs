﻿using Domain.Models.Entities.KnowledgeBase;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using WPF.Events;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels.ExpertSystem
{
    class LineReasoningViewModel : BaseViewModel, IDisposable
    {
        string searchCodeName = "Д";
        int currentReasoningItemId = 0;

        Visibility lineViw;
        public Visibility LineVis { get => lineViw; set { lineViw = value; OnPropertyChanged("LineVis"); } }

        public ObservableCollection<ReasoningItem> ReasoningItems { get; }

        public DelegateCommand PositiveAnswerCommand { get; }
        public DelegateCommand NegativeAnswerCommand { get; }

        public LineReasoningViewModel(AppStateStore stateStore, IUnityContainer container) : base(stateStore, container)
        {
            LineVis = Visibility.Hidden;

            ReasoningItems = new ObservableCollection<ReasoningItem>();

            PositiveAnswerCommand = new DelegateCommand(action1: (param) => PositiveAnswerEvent((int)param));
            NegativeAnswerCommand = new DelegateCommand(action1: (param) => NegativeAnswerEvent((int)param));

            stateStore.EventAggregator.GetEvent<PubSubEvent<AcceptComplaintsEvent>>().Subscribe(AcceptComplaints);
        }

        private void PositiveAnswerEvent(int itemId)
        {
            var item = ReasoningItems.FirstOrDefault(x => x.Id == itemId);

            item.RuleParameter.Parameter.State = item.State = true;

            NextReasoningItem();
        }

        private void NegativeAnswerEvent(int itemId)
        {
            var item = ReasoningItems.FirstOrDefault(x => x.Id == itemId);

            item.RuleParameter.Parameter.State = item.State = false;

            NextReasoningItem();
        }

        private void AcceptComplaints(AcceptComplaintsEvent obj)
        {
            LineVis = Visibility.Visible;

            var variableParams = stateStore.State.Variables.SelectMany(x => x.Parameters);
            var rules = stateStore.State.Rules.Where(x => !x.State.HasValue || (x.State.HasValue && !x.State.Value)).ToList();

            // копируем переменные в правила для реактивности
            foreach (var rule in rules)
            {
                foreach (var ruleParam in rule.RuleParameters)
                {
                    var parameter = variableParams.FirstOrDefault(x => x.Id == ruleParam.ParameterId);

                    ruleParam.SetParameter(parameter);
                }
            }

            NextReasoningItem();
        }

        private void NextReasoningItem()
        {
            // определяем выбывшие правила
            CheckLeaveRule();

            // определяем факты на основе простых правил
            var newFacts = CheckSimpleRules();
            
            // выводим результаты обнаружения простых правил
            foreach (var fact in newFacts)
            {
                var sItem = new ReasoningItem { RuleParameter = fact };
                sItem.State = true;
                sItem.SetSimpleState();
                AddNewItem(sItem);
            }

            // определяем новый шаг
            var rParam = SearchNewParam();

            if (rParam.Any(x => x.ParamType == ParamType.Result))
            {
                // выводим все возможные ответы
                foreach (var res in rParam)
                {
                    var sItem = new ReasoningItem
                    {
                        RuleParameter = res,
                        State = true
                    };
                    AddNewItem(sItem);
                }
            }
            else if (rParam.Count() == 0)
            {
                var sItem = new ReasoningItem
                {
                    State = true
                };
                AddNewItem(sItem);
            }
            else
            {
                AddNewItem(new ReasoningItem { RuleParameter = rParam.FirstOrDefault() });
            }
        }

        private IEnumerable<RuleParameter> SearchNewParam()
        {
            // поиск параметров в правилах где есть как определенные части условия, так и не определенные
            var rules1 = stateStore.State.Rules.Where(x => !x.State.HasValue && 
                x.RuleParameters.Any(rp => rp.ParamType == ParamType.Condition && !rp.Parameter.State.HasValue) && 
                x.RuleParameters.Any(rp => rp.ParamType == ParamType.Condition && rp.Parameter.State.HasValue && rp.Parameter.State.Value)).ToList();
            
            if (rules1.Any())
            {
                return rules1.FirstOrDefault().RuleParameters.Where(x => x.ParamType == ParamType.Condition && !x.Parameter.State.HasValue);
            }

            // поиск правил где все части условия определены но правило не отмечено
            var rules2 = stateStore.State.Rules.Where(x => !x.State.HasValue &&
                x.RuleParameters.Count(rp => rp.ParamType == ParamType.Condition && rp.Parameter.State.HasValue && rp.Parameter.State.Value) == x.RuleParameters.Count(rp => rp.ParamType == ParamType.Condition)).ToList();

            foreach (var rule in rules2)
            {
                rule.SetRuleState(true);
            }

            // поиск правил без единого ответа
            var rules3 = stateStore.State.Rules.Where(x => !x.State.HasValue &&
                x.RuleParameters.Count(rp => rp.ParamType == ParamType.Condition && !rp.Parameter.State.HasValue) == x.RuleParameters.Count(rp => rp.ParamType == ParamType.Condition)).ToList();

            if (rules3.Any())
            {
                return rules3.FirstOrDefault().RuleParameters.Where(x => x.ParamType == ParamType.Condition && !x.Parameter.State.HasValue);
            }

            // если все определено, то вытаскиваем ответы по интересующему результату
            var rules4 = stateStore.State.Rules.Where(x => x.State.HasValue && x.State.Value &&
                x.RuleParameters.Any(rp => rp.Parameter.Variable.CodeName == searchCodeName)).ToList();

            return rules4.Select(x => x.RuleParameters.FirstOrDefault(rp => rp.ParamType == ParamType.Result));
        }

        private void AddNewItem(ReasoningItem item)
        {
            item.SetId(++currentReasoningItemId);

            ReasoningItems.Insert(0, item);
        }

        private IEnumerable<RuleParameter> CheckSimpleRules()
        {
            var rules = stateStore.State.Rules.Where(x => !x.State.HasValue && 
                x.RuleParameters.Count(rp => rp.ParamType == ParamType.Condition) == 1 && 
                x.RuleParameters.Any(rp => rp.ParamType == ParamType.Condition && rp.Parameter.State.HasValue && rp.Parameter.State.Value)).ToList();

            foreach (var rule in rules)
            {
                rule.SetRuleState(true);
            }

            return rules.Select(x => x.RuleParameters.FirstOrDefault(rp => rp.ParamType == ParamType.Result));
        }

        private void CheckLeaveRule()
        {
            var rules = stateStore.State.Rules.Where(x => !x.State.HasValue || (x.State.HasValue && x.State.Value)).ToList();

            foreach (var rule in rules)
            {
                foreach (var ruleParam in rule.RuleParameters.GroupBy(x => x.Parameter.Variable.CodeName))
                {
                    // если все параметры неопределены, то переходим к следующему параметру
                    if (!ruleParam.Any(x => x.Parameter.State.HasValue)) continue;

                    // если параметров несколько, есть определенный и неопределенные параметры
                    if (ruleParam.Count() > 1)
                    {
                        // берем определенный параметр и проставляем его значение остальным
                        var param = ruleParam.FirstOrDefault(x => x.Parameter.State.HasValue);
                        foreach (var rp in ruleParam)
                        {
                            rp.Parameter.State = param.Parameter.State;
                        }
                    }

                    // если нет ни одного положительного результата, выкидываем правило из расмотрения
                    if (!ruleParam.Any(x => x.Parameter.State.Value))
                    {
                        rule.SetRuleState(false);
                        break;
                    }
                }
            }
        }

        public void Dispose() { }
    }
}
