﻿using QueryAbstractions.ReadModels;
using QueryAbstractions.ViewModels;
using System.Linq;

namespace QueryAbstractions.Queries
{
    public interface IQueryDatabase
    {
        IQueryable<TEntity> GetQueryableForEntities<TEntity, TView, TID>(bool noTracking = true, bool onlyExisting = true)
            where TEntity : BaseReadModel<TID>
            where TView : BaseViewModel<TID>
            where TID : struct;
    }
}
