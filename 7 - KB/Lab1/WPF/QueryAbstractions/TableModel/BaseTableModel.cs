﻿using QueryAbstractions.Filter;
using System.Collections.Generic;

namespace QueryAbstractions.TableModel
{
    public class BaseTableModel<TRead, TView> : AbstractTableModel<TRead, TView>
        where TRead : class
        where TView : class
    {
        public override IEnumerable<TView> Data { get; set; }
        public override FilterModel<TRead, TView> FilterModel { get; set; }
    }
}
