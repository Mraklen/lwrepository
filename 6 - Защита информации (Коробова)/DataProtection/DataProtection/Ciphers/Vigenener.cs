﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProtection.Ciphers
{
    public class VigenenerAndGamma
    {
        static char[] Alphabet = new char[] { 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
                                              'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
                                              'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ',
                                              'Э', 'Ю', 'Я',
                                              'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и',
                                              'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с',
                                              'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'ъ',
                                              'э', 'ю', 'я',
                                              ' ', '.', ',', ':', ';', '!', '?', '\n', '\r', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
        static int N { get => Alphabet.Length; }

        public static string Encrypt(string input, string keyword)
        {
            if (keyword.Equals("")) return "";

            var result = "";
            var keyword_index = 0;

            foreach (var symbol in input)
            {
                var c = (Array.IndexOf(Alphabet, symbol) + Array.IndexOf(Alphabet, keyword[keyword_index])) % N;
                result += Alphabet[c];

                keyword_index++;

                if ((keyword_index + 1) == keyword.Length) keyword_index = 0;
            }

            return result;
        }

        internal static string GetRandomKey(int length, int seed)
        {
            var rand = new Random(seed);
            var result = "";

            for (var i = 0; i < length; i++) result += Alphabet[rand.Next(0, Alphabet.Length)];

            return result;
        }

        public static string Decrypt(string input, string keyword)
        {
            if (keyword.Equals("")) return "";

            var result = "";
            var keyword_index = 0;

            foreach (var symbol in input)
            {
                var p = (Array.IndexOf(Alphabet, symbol) + N - Array.IndexOf(Alphabet, keyword[keyword_index])) % N;
                result += Alphabet[p];

                keyword_index++;

                if ((keyword_index + 1) == keyword.Length) keyword_index = 0;
            }

            return result;
        }
    }
}
