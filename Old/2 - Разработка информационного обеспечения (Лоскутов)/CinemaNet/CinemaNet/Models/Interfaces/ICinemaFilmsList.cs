﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface ICinemaFilmsList
    {
        IEnumerable<CinemaFilms> CinemaFilmsList { get; }
    }
}
