﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TaskClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var firstMessageFile = "F:\\Repositories\\LabWork\\2 - Сети и телекоммуникации (Галыгина)\\LW5\\SixTask\\TaskClient\\bin\\Debug\\Messages\\1 message.txt";
            var secondMessageFile = "F:\\Repositories\\LabWork\\2 - Сети и телекоммуникации (Галыгина)\\LW5\\SixTask\\TaskClient\\bin\\Debug\\Messages\\2 message.txt";
            var thirdMessageFile = "F:\\Repositories\\LabWork\\2 - Сети и телекоммуникации (Галыгина)\\LW5\\SixTask\\TaskClient\\bin\\Debug\\Messages\\3 message.txt";

            var messages = new List<string>()
            {
                FileManager.ReadFile(firstMessageFile),
                FileManager.ReadFile(secondMessageFile),
                FileManager.ReadFile(thirdMessageFile)
            };

            Console.WriteLine("Результаты отправки на сервер (http://localhost:5500) сообщейний разной длины:\n");
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:5500");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                for (var i = 0; i < messages.Count(); i++)
                {
                    var firstDate = DateTime.Now;
                    var resp = client.PostAsync("Home/sendMessage", new StringContent(messages[i], Encoding.UTF8)).Result;
                    var secondDate = DateTime.Now;

                    Console.WriteLine($"{i + 1}: Размер: {messages[i].Count() / 1024} кб | Затраченое время: {(secondDate.Ticks - firstDate.Ticks) / TimeSpan.TicksPerMillisecond} мс");
                }
            }

            Console.WriteLine("\nНажмите любую клавишу для продолжения...");
            Console.ReadKey();
        }
    }

    public class FileManager
    {
        public static string ReadFile(string filePath)
        {
            using (var stream = File.OpenRead(filePath))
            {
                byte[] arr = new byte[stream.Length];
                stream.Read(arr, 0, arr.Length);
                return Encoding.Default.GetString(arr);
            }
        }
    }
}
