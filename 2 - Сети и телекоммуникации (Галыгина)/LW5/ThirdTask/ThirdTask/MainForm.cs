﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp.WinForms;
using CefSharp;
using HtmlAgilityPack;
using System.IO;

namespace ThirdTask
{
    public partial class MainForm : Form
    {
        ChromiumWebBrowser browser;
        private bool firstLoaded = true;

        IEnumerable<string> urls = null;
        int currentIndex = -1;

        int rndNum;

        public MainForm()
        {
            rndNum = (new Random()).Next(0, 16000);
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            browser.Load(urlLable.Text);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CefSettings settings = new CefSettings();
            if (!Cef.IsInitialized) Cef.Initialize(settings);

            browser = new ChromiumWebBrowser("https://yandex.ru/");

            Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
            browser.BrowserSettings.ImageLoading = CefState.Disabled;
            browser.FrameLoadEnd += ChromeBrowser_FrameLoadAsync;
        }

        private async void ChromeBrowser_FrameLoadAsync(object sender, FrameLoadEndEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                var Frame = e.Browser.FocusedFrame;

                if(firstLoaded)
                {
                    firstLoaded = false;
                    return;
                }

                var res = await Frame.EvaluateScriptAsync(@"document.getElementsByTagName('html')[0].outerHTML");

                if (urls == null)
                {
                    FileManager.SaveTextToFileAsync(res.Result.ToString(), $"{rndNum}_MainPage");
                    urls = Parser.ParsingHtml(res.Result.ToString());

                    if (urls.Count() > 0)
                        browser.Load(urls.ElementAt(++currentIndex));
                    else
                        Application.Exit();
                    return;
                }

                FileManager.SaveTextToFileAsync(res.Result.ToString(), $"{rndNum}_{currentIndex}");

                if (currentIndex < urls.Count() - 1)
                {
                    browser.Load(urls.ElementAt(++currentIndex));

                    await Task.Delay(1000);
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }
    }

    class Parser
    {
        public static IEnumerable<string> ParsingHtml(string html)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            var divs = doc.DocumentNode.Descendants().Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value.StartsWith("organic ")).ToList();
            var res = divs.Select(div =>
            {
                var yaClickUrl = div.Descendants().FirstOrDefault(x => x.Name.ToLower() == "a" && x.Attributes["class"].Value.StartsWith("link") && x.Attributes["class"].Value.Contains("organic__url")).Attributes["href"].Value;
                if (yaClickUrl.Contains("yandex"))
                    return null;
                else
                    return yaClickUrl;
            }).Where(x => x != null).ToList();

            return res;
        }
    }

    class FileManager
    {
        public static async void SaveTextToFileAsync(string text, string fileName)
        {
            var fullname = $"{Application.StartupPath}\\Pages\\{fileName}.html";
            using (FileStream fstream = new FileStream(fullname, FileMode.OpenOrCreate))
            {
                byte[] array = Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
            }

            await Task.Delay(1000);
        }
    }
}
