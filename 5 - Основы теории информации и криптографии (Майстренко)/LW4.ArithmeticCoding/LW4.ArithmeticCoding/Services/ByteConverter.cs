﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW4.ArithmeticCoding.Services
{
    public class ByteConverter
    {
        public static byte[] DoubleArrayToBytes(double[] array)
        {
            return array.SelectMany(value => BitConverter.GetBytes(value)).ToArray();
        }

        public static double[] BytesToDoubleArray(byte[] bytes)
        {
            return Enumerable.Range(0, bytes.Length / sizeof(double))
                .Select(offset => BitConverter.ToDouble(bytes, offset * sizeof(double)))
                .ToArray();
        }

        public static (double, int, char[], double[]) ConvertBytesToFileContent(byte[] file)
        {
            var separatorBytes = new byte[] { 0, 255, 255, 0 };
            var contentBytes = new List<byte[]>();
            var buffer = new List<byte>();

            var fileCount = (uint)file.Count();
            for (var i = 0; i < fileCount; i++)
            {
                if (fileCount - 5 != i)
                {
                    if (separatorBytes[0] == file[i] && separatorBytes[1] == file[i + 1] && separatorBytes[2] == file[i + 2] && separatorBytes[3] == file[i + 3])
                    {
                        i += 4;
                        contentBytes.Add(buffer.ToArray());
                        buffer = new List<byte>();
                    }
                }
                buffer.Add(file[i]);
            }
            contentBytes.Add(buffer.ToArray());
            
            var code = BitConverter.ToDouble(contentBytes.ElementAt(0), 0);
            var length = BitConverter.ToInt32(contentBytes.ElementAt(1), 0);
            var letters = Encoding.UTF8.GetString(contentBytes.ElementAt(2)).Select(x => x).ToArray();
            var probability = BytesToDoubleArray(contentBytes.ElementAt(3));

            return (code, length, letters, probability);
        }
    }
}
