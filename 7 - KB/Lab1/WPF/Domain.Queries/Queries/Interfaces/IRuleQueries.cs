﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Queries.ViewModels.KnowledgeBase;
using QueryAbstractions.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.Queries.Interfaces
{
    public interface IRuleQueries : IBaseQueries<RuleReadModel, RuleViewModel, int>
    {
        Task<IEnumerable<RuleViewModel>> GetListAsync();
    }
}
