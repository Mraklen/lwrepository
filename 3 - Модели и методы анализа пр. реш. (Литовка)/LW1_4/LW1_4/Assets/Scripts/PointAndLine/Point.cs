﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour {
    
    [Space(10)]
    [SerializeField] protected GameObject ModelPoint;
    [Space(10)]
    [SerializeField] protected Material currentMaterial;
    [SerializeField] protected Material lastMaterial;
    
    [Space(10)]
    [SerializeField] public bool isSelect = false;
    [SerializeField] public bool isMove = false;

    protected MeshRenderer ThisMesh;
    
    public Material CurrentMaterial
    {
        get { return currentMaterial; }
        set
        {
            lastMaterial = currentMaterial;
            currentMaterial = value;
            if (ThisMesh == null) ThisMesh = ModelPoint.GetComponent<MeshRenderer>();
            ThisMesh.material = value;
        }
    }

    private void Start()
    {
        if (!ModelPoint) ModelPoint = gameObject; 
        ThisMesh = ModelPoint.GetComponent<MeshRenderer>();
    }

}
