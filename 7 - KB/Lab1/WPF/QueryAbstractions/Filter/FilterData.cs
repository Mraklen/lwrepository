﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueryAbstractions.Filter
{
    public class FilterData<T> where T : class
    {
        public string SortBy { get; set; }

        public bool? Descending { get; set; }

        public int? Page { get; set; } = 1;

        public int? RowsPerPage { get; set; } = 10;

        public int? TotalItems { get; set; } = 0;

        public T Filter { get; set; }
    }
}
