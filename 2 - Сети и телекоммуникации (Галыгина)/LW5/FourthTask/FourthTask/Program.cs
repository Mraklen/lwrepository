﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FourthTask
{
    class Program
    {
        // ftp://press.tretyakov.ru/Korzhev/_K6A2708.jpg

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Введите ftp ссылку на файл: ");
            var link = Console.ReadLine();
            Console.Write("Введите путь для сохранения файла: ");
            var savePath = Console.ReadLine();

            FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(link);
            ftp.Credentials = new NetworkCredential("", "");
            ftp.KeepAlive = true;
            ftp.UsePassive = true;
            ftp.UseBinary = true;
            ftp.Method = WebRequestMethods.Ftp.DownloadFile;

            FtpWebResponse response = (FtpWebResponse) ftp.GetResponse();

            Stream stream = response.GetResponseStream();
            List<byte> list = new List<byte>();
            int b;
            while ((b = stream.ReadByte()) != -1)
                list.Add((byte)b);
            var fileSaveName = $"{savePath}\\{link.Split('/').Last()}";
            File.WriteAllBytes(fileSaveName, list.ToArray());

            Console.WriteLine($"\nФайл: {fileSaveName} - сохранен! \n\n...для продолжения нажмите любую кнопку");
            Console.ReadKey();
        }
    }
}
