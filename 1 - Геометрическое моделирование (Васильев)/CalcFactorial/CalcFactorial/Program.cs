﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CalcFactorial
{
    class Program
    {
        static void Main(string[] args)
        {
            int m = 20;
            Dictionary<int, List<long>> dictionary = new Dictionary<int, List<long>>();

            for (int i = 0; i <= m; i++)
            {
                Console.WriteLine($"New list: {i}");

                var list = new List<long>();
                for (int j = 0; j <= i; j++)
                {
                    list.Add(CalcFunc(j, i));
                }

                Console.WriteLine($"List write: {i}");
                dictionary.Add(i, list.ToList());
            }

            var json = JsonConvert.SerializeObject(dictionary);
            using (FileStream fstream = new FileStream($"F:\\Repositories\\LabWork\\1 - Геометрическое моделирование (Васильев)\\CalcFactorial\\CalcFactorial\\Result\\CmResult.json", FileMode.OpenOrCreate))
            {
                byte[] array = Encoding.Default.GetBytes(json);
                fstream.Write(array, 0, array.Length);
            }
        }

        private static long CalcFunc(int currentValue, int totalCount)
        {
            var tmp1 = Factorial(totalCount);
            var tmp2 = Factorial(currentValue);
            var tmp3 = Factorial(totalCount - currentValue);
            return tmp1 / (tmp2 * tmp3);
        }


        private static long Factorial(long x)
        {
            return (x == 0) ? 1 : x * Factorial(x - 1);
        }
    }
}
