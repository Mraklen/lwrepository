﻿namespace LZWSharp
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPack = new System.Windows.Forms.Button();
            this.btnUnpack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbInitial = new System.Windows.Forms.Label();
            this.lbCompress = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbPercent = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnPack
            // 
            this.btnPack.Location = new System.Drawing.Point(16, 15);
            this.btnPack.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPack.Name = "btnPack";
            this.btnPack.Size = new System.Drawing.Size(208, 60);
            this.btnPack.TabIndex = 0;
            this.btnPack.Text = "Упаковать";
            this.btnPack.UseVisualStyleBackColor = true;
            this.btnPack.Click += new System.EventHandler(this.btnPack_Click);
            // 
            // btnUnpack
            // 
            this.btnUnpack.Location = new System.Drawing.Point(232, 15);
            this.btnUnpack.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUnpack.Name = "btnUnpack";
            this.btnUnpack.Size = new System.Drawing.Size(216, 60);
            this.btnUnpack.TabIndex = 2;
            this.btnUnpack.Text = "Распаковать";
            this.btnUnpack.UseVisualStyleBackColor = true;
            this.btnUnpack.Click += new System.EventHandler(this.btnUnpack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 95);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Исходный файл";
            // 
            // lbInitial
            // 
            this.lbInitial.AutoSize = true;
            this.lbInitial.Location = new System.Drawing.Point(228, 95);
            this.lbInitial.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbInitial.Name = "lbInitial";
            this.lbInitial.Size = new System.Drawing.Size(16, 17);
            this.lbInitial.TabIndex = 4;
            this.lbInitial.Text = "0";
            // 
            // lbCompress
            // 
            this.lbCompress.AutoSize = true;
            this.lbCompress.Location = new System.Drawing.Point(228, 134);
            this.lbCompress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCompress.Name = "lbCompress";
            this.lbCompress.Size = new System.Drawing.Size(16, 17);
            this.lbCompress.TabIndex = 6;
            this.lbCompress.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 134);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Сжатый файл";
            // 
            // lbPercent
            // 
            this.lbPercent.AutoSize = true;
            this.lbPercent.Location = new System.Drawing.Point(228, 176);
            this.lbPercent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPercent.Name = "lbPercent";
            this.lbPercent.Size = new System.Drawing.Size(16, 17);
            this.lbPercent.TabIndex = 8;
            this.lbPercent.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 176);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Процент сжатия";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 207);
            this.Controls.Add(this.lbPercent);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbCompress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbInitial);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUnpack);
            this.Controls.Add(this.btnPack);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Лабораторная работа №6 LZW";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPack;
        private System.Windows.Forms.Button btnUnpack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbInitial;
        private System.Windows.Forms.Label lbCompress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbPercent;
        private System.Windows.Forms.Label label6;
    }
}

