﻿using LW1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    class DefaultAggregateRepository<TAggreate> : IBaseAggregateRepository<TAggreate> where TAggreate : class, IAggregate
    {
        protected readonly ApplicationDbContext context;

        public DefaultAggregateRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        
        public TAggreate GetById(Guid id)
        {
            return context.Set<TAggreate>().Find(id);
        }

        public void Update(TAggreate aggregate)
        {
            context.Attach(aggregate);
            context.SaveChanges();
        }

        public void Create(TAggreate aggregate)
        {
            context.Add(aggregate);
            context.SaveChanges();
        }

        //public void LoadCollection(TAggreate entity, Expression<Func<TAggreate, IEnumerable<object>>> expression)
        //{
        //    context.Entry(entity).Collection(expression).Load();
        //}

        //public void LoadReference(TAggreate entity, Expression<Func<TAggreate, object>> expression)
        //{
        //    context.Entry(entity).Reference(expression);
        //}
    }
}
