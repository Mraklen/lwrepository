﻿using Assets.Scripts.Methods;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class ParametricLines : MethodItem
{
    protected override void Init()
    {
        qualityStep = 0.02f;
        step = 0.1f;
    }

    protected override List<Vector3> CalcMethod(List<MainPoint> mainPoints)
    {
        var points = new List<Vector3>();

        for (int i = 0; i < mainPoints.Count() - 1; i++)
        {
            var tmp1 = mainPoints.ElementAt(i);
            var mainPoint1 = tmp1.transform.position;
            var firstPoint1 = tmp1.GetFirstPointPosition;

            var tmp2 = mainPoints.ElementAt(i + 1);
            var mainPoint2 = tmp2.transform.position;
            var secondPoint2 = tmp2.GetSecondPointPosition;

            var p1x = mainPoint1.x;
            var p2x = mainPoint2.x;
            var r1x = firstPoint1.x - p1x;
            var r2x = secondPoint2.x - p2x;

            var p1y = mainPoint1.y;
            var p2y = mainPoint2.y;
            var r1y = firstPoint1.y - p1y;
            var r2y = secondPoint2.y - p2y;

            var p1z = mainPoint1.z;
            var p2z = mainPoint2.z;
            var r1z = firstPoint1.z - p1z;
            var r2z = secondPoint2.z - p2z;

            for (var t = 0f; t <= 1f; t += Step)
            {
                points.Add(new Vector3()
                {
                    x = ParametricFunc(p1x, p2x, r1x, r2x, t),
                    y = ParametricFunc(p1y, p2y, r1y, r2y, t),
                    z = ParametricFunc(p1z, p2z, r1z, r2z, t)
                });
            }
        }

        return points;
    }

    private float ParametricFunc(float p1, float p2, float r1, float r2, float t)
    {
        var t2 = (float)Math.Pow(t, 2);
        var t3 = (float)Math.Pow(t, 3);
        return ((p1 * (2 * t3 - 3 * t2 + 1)) + (p2 * (-2 * t3 + 3 * t2)) + (r1 * (t3 - 2 * t2 + t)) + (r2 * (t3 - t2)));
    }
}
