﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace LW5.RSA
{
    public class EncryptionModule
    {
        public static List<string> StartCoder(string s, long e, long n)
        {
            List<string> result = new List<string>();

            BigInteger bi;

            var bytes = Encoding.Unicode.GetBytes(s);
            var ints = bytes.Select(x => Convert.ToInt32(x)).ToArray();

            for (int i = 0; i < ints.Count(); i++)
            {
                bi = new BigInteger(ints[i]);
                bi = BigInteger.Pow(bi, (int)e);
                BigInteger n_ = new BigInteger((int)n);

                bi = bi % n_;

                result.Add(bi.ToString());
            }

            return result;
        }

        public static string StartDecoder(List<string> input, long d, long n)
        {
            var bytes = new List<byte>();
            BigInteger bi;
            foreach (string item in input)
            {
                bi = new BigInteger(Convert.ToDouble(item));
                bi = BigInteger.Pow(bi, (int)d);

                BigInteger n_ = new BigInteger((int)n);

                bi = bi % n_;

                int index = Convert.ToInt32(bi.ToString());

                bytes.Add(Convert.ToByte(index));
            }

            return Encoding.Unicode.GetString(bytes.ToArray());
        }

        //вычисление параметра d. d должно быть взаимно простым с m
        public static long calc_d(long m)
        {
            long d = m - 1;
            for (long i = 2; i <= m; i++)
                if ((m % i == 0) && (d % i == 0)) //если имеют общие делители
                {
                    d--;
                    i = 1;
                }
            return d;
        }

        //вычисление параметра e
        public static long calc_e(long d, long m)
        {
            long e = 10;
            while (true)
            {
                if ((e * d) % m == 1)
                    break;
                else
                    e++;
            }
            return e;
        }
    }
}
