﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SavePointPanel : MonoBehaviour
{
    [SerializeField] GameObject SaveItemPrefab;
    [Space(10)]
    [SerializeField] RectTransform SaveItemContent;
    [SerializeField] List<SaveItem> SaveItemList;
    [Space(10)]
    [SerializeField] GameObject InputDialog;
    [SerializeField] InputField NewFileName;
    [Space(10)]
    [SerializeField] LineRendering LinePoints;
    [SerializeField] SurfaceRendering SurfacePoints;

    private void Start()
    {
        LoadFiles();
    }

    internal void SetJsonPoints(JsonPoints points)
    {
        LinePoints.SetJsonMainPoints(points.MainPoints);

        SurfacePoints.SetJsonPoints(points.SurfacePoints);
    }

    public void LoadFiles()
    {
        // очистить текущий список
        SaveItemList.ForEach(x => Destroy(x));
        SaveItemList = new List<SaveItem>();

        // получить названия файлов в папке
        if (!new DirectoryInfo($"{Application.dataPath}/SaveFiles").Exists) Directory.CreateDirectory($"{Application.dataPath}/SaveFiles");
        var fileNames = new DirectoryInfo($"{Application.dataPath}/SaveFiles").GetFiles().Select(x => x.FullName).Where(x => !x.Contains(".meta"));

        // создать и вывести панельки в список
        for (var i = 0; i < fileNames.Count(); i++)
        {
            var item = Instantiate(SaveItemPrefab);

            var saveItem = item.GetComponent<SaveItem>();
            saveItem.Init(fileNames.ElementAt(i));

            var rectTransform = item.GetComponent<RectTransform>();
            rectTransform.parent = SaveItemContent;
            
            rectTransform.anchoredPosition = new Vector2(118, -25 - (i * 42));
            rectTransform.transform.localScale = Vector3.one;
        }
    }

    public void SaveNewPoints()
    {
        NewFileName.text = "";
        InputDialog.SetActive(true);
    }

    public void SaveEvent()
    {
        var newFile = NewFileName.text;

        // получить точки
        var jsonPoints = new JsonPoints
        {
            MainPoints = LinePoints.GetJsonMainPoints(),
            SurfacePoints = SurfacePoints.GetJsonPoints()
        };

        // записать в файл
        if (!new DirectoryInfo($"{Application.dataPath}/SaveFiles").Exists) Directory.CreateDirectory($"{Application.dataPath}/SaveFiles");
        FileManager.WriteJsonFile($"{Application.dataPath}/SaveFiles/{newFile}.json", jsonPoints);

        // обновить список файлов
        LoadFiles();

        CancelEvent();
    }

    public void CancelEvent()
    {
        NewFileName.text = "";
        InputDialog.SetActive(false);
    }
}
