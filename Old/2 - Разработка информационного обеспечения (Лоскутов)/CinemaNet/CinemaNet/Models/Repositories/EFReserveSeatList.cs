﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFReserveSeatList : IReserveSeatList
    {
        public ApplicationDbContext context;
        public EFReserveSeatList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<ReserveSeat> ReserveSeatList => context.ReserveSeats;

        public void AddReserveSeats(ReserveSeat reserve)
        {
            context.ReserveSeats.Add(reserve);
            context.SaveChanges();
        }
    }
}
