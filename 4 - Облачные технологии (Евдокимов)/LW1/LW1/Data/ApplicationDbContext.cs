﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using LW1.Models;
using Microsoft.Extensions.Configuration;
using LW1.Models.Project;

namespace LW1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        DbContextOptions options;
        private readonly string connectionString;

        public ApplicationDbContext(IConfiguration config)
        {
            connectionString = config["Database:WriteDbConnectionString"];
        }


        public ApplicationDbContext(string connectionString) : base()
        {
            this.connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Project>().ToTable("Projects");

            builder.Entity<KeyWord>().
                ToTable("KeyWords").
                HasOne(x => x.Project).
                WithMany(x => x.KeyWords).
                OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Selection>().
                ToTable("Selections").
                HasOne(x => x.Project).
                WithMany(x => x.Selections).
                OnDelete(DeleteBehavior.Cascade);

            builder.Entity<SelectionKeyWord>().
                ToTable("SelectionKeyWord").
                HasOne(x => x.Selection).
                WithMany(x => x.SelectionKeyWords).
                OnDelete(DeleteBehavior.Cascade);

            builder.Entity<SelectionUrl>().
                ToTable("SelectionUrls").
                HasOne(x => x.SelectionKeyWord).
                WithMany(x => x.SelectionUrls).
                OnDelete(DeleteBehavior.Cascade);
        }

        public DbSet<Project> Projects { get; protected set; }
        public DbSet<Selection> Selections { get; protected set; }
        public DbSet<SelectionUrl> SelectionUrls { get; protected set; }
        public DbSet<KeyWord> KeyWords { get; protected set; }
        public DbSet<SelectionKeyWord> SelectionKeyWords { get; protected set; }
    }
}
