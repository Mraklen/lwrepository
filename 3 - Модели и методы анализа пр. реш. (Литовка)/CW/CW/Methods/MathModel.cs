﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CW.Methods
{
    public class MathModel
    {
        public double dt = 0.05;
        public double t_max = 300;

        private double A1 = 8000.0;
        private double A2 = 48000.0;
        private double A3 = 17600.0;
        private double A4 = 5400000.0;

        private double E1 = 58000.0;
        private double E2 = 71000.0;
        private double E3 = 64000.0;
        private double E4 = 95000.0;

        private double R = 8.31;

        private double v = 0.5;

        public double Tmin = 600;
        public double Tmax = 800;
        public double Tmid = 700;

        public double Vmin = 30;
        public double Vmax = 40;
        public double Vmid = 35;

        public static List<Point> Z_Result { get; private set; }

        private double e = 0.00001;

        public double StartCalc(double T, double V, double[] arr_z = null)
        {
            var result = 0.0;

            Z_Result = arr_z != null ? new List<Point>() : null;

            var result1 = new List<Point>();
            var result2 = new List<Point>();
            var result3 = new List<Point>();
            var result4 = new List<Point>();
            var result6 = new List<Point>();

            var C1_in = arr_z != null ? arr_z[0] : 42.5;
            var C6_in = 15.6;
            double C2_in = 0.0, C3_in = 0.0, C4_in = 0.0;

            var pC1 = new Point { X = 0.0, Y = C1_in };
            var pC2 = new Point { X = 0.0, Y = C2_in };
            var pC3 = new Point { X = 0.0, Y = C3_in };
            var pC4 = new Point { X = 0.0, Y = C4_in };
            var pC6 = new Point { X = 0.0, Y = C6_in };

            var k1 = K(A1, E1, T);
            var k2 = K(A2, E2, T);
            var k3 = K(A3, E3, T);
            var k4 = K(A4, E4, T);

            result1.Add(pC1);
            result2.Add(pC2);
            result3.Add(pC3);
            result4.Add(pC4);
            result6.Add(pC6);

            var i = 0;
            for (var t = 0.0; t <= t_max; t += dt, i++)
            {
                var tmp_pC1 = new Point
                {
                    X = t,
                    Y = pC1.Y + dt * (CalcPerfectMix(V, (arr_z != null ? arr_z[i] : 42.5), pC1.Y) - SpeedReact(k1, pC1.Y, pC6.Y))
                };
                var tmp_pC2 = new Point
                {
                    X = t,
                    Y = pC2.Y + dt * (CalcPerfectMix(V, C2_in, pC2.Y) - SpeedReact(k2, pC2.Y, pC6.Y) + SpeedReact(k1, pC1.Y, pC6.Y))
                };
                var tmp_pC3 = new Point
                {
                    X = t,
                    Y = pC3.Y + dt * (CalcPerfectMix(V, C3_in, pC3.Y) - SpeedReact(k3, pC3.Y, pC6.Y) + SpeedReact(k2, pC2.Y, pC6.Y))
                };
                var tmp_pC4 = new Point
                {
                    X = t,
                    Y = pC4.Y + dt * (CalcPerfectMix(V, C4_in, pC4.Y) - SpeedReact(k4, pC4.Y, pC6.Y) + SpeedReact(k3, pC3.Y, pC6.Y))
                };

                var tmp_pC6 = new Point
                {
                    X = t,
                    Y = pC6.Y + dt * (CalcPerfectMix(V, C6_in, pC6.Y) - SpeedReact(k1, pC1.Y, pC6.Y) - SpeedReact(k2, pC2.Y, pC6.Y) - SpeedReact(k3, pC3.Y, pC6.Y) - SpeedReact(k4, pC4.Y, pC6.Y))
                };
                
                pC1 = tmp_pC1;
                pC2 = tmp_pC2;
                pC3 = tmp_pC3;
                pC4 = tmp_pC4;
                pC6 = tmp_pC6;

                if (arr_z != null) Z_Result.Add(new Point { X = t, Y = (pC4.Y * 11.8) / 1.7 });

                result1.Add(tmp_pC1);
                result2.Add(tmp_pC2);
                result3.Add(tmp_pC3);
                result4.Add(tmp_pC4);
                result6.Add(tmp_pC6);
            }
            
            result = result4.Max(x => x.Y);

            result = (result * 11.8) / 1.7;

            return result;
        }

        private bool ValueCheck(double C_old, double C_new)
        {
            return Math.Abs(C_new - C_old) < e;
        }

        private double SpeedReact(double k, double C_f, double C_s)
        {
            return k * C_f * C_s;
        }

        private double CalcPerfectMix(double V, double C_in, double C_out)
        {
            return (v / V) * (C_in - C_out);
        }

        private double K(double A, double E, double T)
        {
            return (A * Math.Exp((-E) / (R * T)));
        }
    }
}
