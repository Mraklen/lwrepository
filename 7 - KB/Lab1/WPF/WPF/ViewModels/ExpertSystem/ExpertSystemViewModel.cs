﻿using Domain.Models.Repositories.Interfaces;
using Prism.Events;
using System;
using System.Linq;
using System.Threading.Tasks;
using Unity;
using WPF.Events;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels.ExpertSystem
{
    class ExpertSystemViewModel : BaseViewModel, IDisposable
    {
        private readonly IDomainRepositoryFactory repositoryFactory;
        
        public VariablesPageViewModel Variables { get; }
        public LineReasoningViewModel LineReasoning { get; }

        public ExpertSystemViewModel(AppStateStore stateStore, IUnityContainer container, IDomainRepositoryFactory repositoryFactory) : base(stateStore, container)
        {
            Variables = container.Resolve<VariablesPageViewModel>();
            LineReasoning = container.Resolve<LineReasoningViewModel>();

            this.repositoryFactory = repositoryFactory;

            Task.Factory.StartNew(LoadData);
        }

        private async Task LoadData()
        {
            stateStore.State.Variables = (await repositoryFactory.GetVariableRepository().GetList()).ToList();
            stateStore.State.Rules = (await repositoryFactory.GetRuleRepository().GetList()).ToList();

            stateStore.EventAggregator.GetEvent<PubSubEvent<UpdateDataEvent>>().Publish(new UpdateDataEvent { type = Modules.Base.ModuleType.ExpertSystem });
        }

        public void Dispose() { }
    }
}
