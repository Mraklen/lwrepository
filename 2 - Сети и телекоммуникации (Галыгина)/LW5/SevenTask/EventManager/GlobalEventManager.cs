﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SeatBattle.CSharp.EventManager
{
    public enum EVENT_TYPE
    {
        StartGame,
        PostMessage,
        ReadyPlay,
        EnemyReadyPlay,
        SetFirstPlayer,
        EnemyPlayerShot
    };

    public class GlobalEventManager
    {
        private static GlobalEventManager instance;
        private GlobalEventManager() { }
        public static GlobalEventManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new GlobalEventManager();
                return instance;
            }
        }

        public delegate void OnEvent(EVENT_TYPE Event_Type, object Param = null);
        private Dictionary<EVENT_TYPE, List<OnEvent>> Listeners = new Dictionary<EVENT_TYPE, List<OnEvent>>();

        public void AddEventListener(EVENT_TYPE Event_Type, OnEvent Listener)
        {
            if (Listeners.TryGetValue(Event_Type, out List<OnEvent> ListenList))
            {
                ListenList.Add(Listener);
                return;
            }
            ListenList = new List<OnEvent> { Listener };
            Listeners.Add(Event_Type, ListenList);
        }

        public void PostEventMessage(EVENT_TYPE Event_Type, object Param = null)
        {
            if (!Listeners.TryGetValue(Event_Type, out List<OnEvent> ListenList)) { return; }
            for (int i = 0; i < ListenList.Count; i++)
            {
                if (!ListenList[i].Equals(null))
                {
                    ListenList[i](Event_Type, Param);
                }
            }
        }

        public void RemoveEvent(EVENT_TYPE Event_Type)
        {
            Listeners.Remove(Event_Type);
        }

        public void RemoveRedundancies()
        {
            Dictionary<EVENT_TYPE, List<OnEvent>> TmpListeners = new Dictionary<EVENT_TYPE, List<OnEvent>>();
            foreach (KeyValuePair<EVENT_TYPE, List<OnEvent>> Item in Listeners)
            {
                for (int i = Item.Value.Count - 1; i >= 0; i--)
                {
                    if (Item.Value[i].Equals(null))
                        Item.Value.RemoveAt(i);
                }
                if (Item.Value.Count > 0)
                {
                    TmpListeners.Add(Item.Key, Item.Value);
                }
            }
            Listeners = TmpListeners;
        }
    }
}
