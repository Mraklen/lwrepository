﻿using EntityAbstractions.Entities;
using EntityAbstractions.Implementations;
using System;
using System.Threading.Tasks;

namespace Domain.Models.Repositories.Interfaces
{
    public interface IBaseAggregateRepository<TAggregate> where TAggregate : GlobalEntity, IAggregate
    {
        Task<TAggregate> GetByIdAsync(Guid id);

        Task CreateAsync(TAggregate aggregate);
        Task UpdateAsync(TAggregate aggregate);

        Task Delete(TAggregate aggregate);
        Task Delete(Guid id);
    }
}
