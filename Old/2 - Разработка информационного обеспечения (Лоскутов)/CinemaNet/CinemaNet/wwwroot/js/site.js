﻿var reserves = [];
$("[data-ui-elem=place]").on("click", function () {
    var val = $(this).children(".rowNum").html() + " / " + $(this).children(".seatNum").html();
    if ($(this).hasClass("noSelected")) {
        $(this).removeClass("noSelected");
        $(this).addClass("selected");
        reserves.push(val);
        printResult();
    } else {
        $(this).removeClass("selected");
        $(this).addClass("noSelected");
        var id = -1;
        reserves.forEach(function (value, index, array) {
            if (value === val) {
                id = index;
            }
        });
        if (id !== -1) {
            reserves.splice(id, 1);
        }
        printResult();
    }
    if (reserves.length > 0) {
        $('#buttonReserve').prop('disabled', false);
    }
    else 
    {
        $('#buttonReserve').prop('disabled', true);
    }
});

function printResult() {
    var text = arrayToText(reserves);
    $("[data-ui-item=result]").html(text);
}

function arrayToText(array) {
    var text = "";
    array.forEach(function (value, index, array) {
        text += value + "; ";
    });
    return text;
}

$("#buttonReserve").on("click", function acceptReservePlace() {
    $(this).hide();
    $("#form").show();
    $("[data-ui-elem=reserveArr]").val(arrayToText(reserves));
});

$("#buttonCancel").on("click", function acceptReservePlace() {
    $("#form").hide();
    $("#buttonReserve").show();
});

$("[name=removeFilmBtn]").on("click", function removeFilmBtnClick(data) {
    debugger;
    var value = data.target.value;
    $.ajax({
        type: "POST",
        url: "/Manage/RemoveFilm",
        data: {
            value: value
        },
        success: function () {
            debugger;
            $(location).attr('href', "/Manage/FilmList");
        }
    });
});

/*$("[data-ui-elem=cinema]").on("click", "li", function () {
    var cinemaID = $(this).val();
    $.post("/FilmList/List", { cinemaID: cinemaID }, function () { });
});
$("[data-ui-elem=films]").on("click", "li", function () {
    var cinemaID = $(this).val();
    $.post("/CinemaList/List", { filmID: filmID }, function () { });
});*/
