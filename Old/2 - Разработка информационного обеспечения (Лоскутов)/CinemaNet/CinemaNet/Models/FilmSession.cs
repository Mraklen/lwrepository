﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class FilmSession
    {
        public int FilmSessionID { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

        public int CinemaID { get; set; }
        public Cinema Cinema { get; set; }

        public int FilmID { get; set; }
        public Film Film { get; set; }

        public int CinemaRoomID { get; set; }
        public CinemaRoom CinemaRoom { get; set; }

        public List<Reserve> Reserves { get; set; } 
    }
}
