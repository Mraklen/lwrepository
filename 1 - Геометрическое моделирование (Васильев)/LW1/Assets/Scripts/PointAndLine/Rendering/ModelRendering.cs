﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ModelRendering : EntityRender
{

    [SerializeField] BindingLine BindingLine;
    [SerializeField] List<GameObject> SupportPoints;
    [Space(10)]
    [SerializeField] MeshCreator MeshCreator;

    [SerializeField]
    [Range(5, 120)] float rotateAngle = 120;

    protected override void Init()
    {
        GlobalEventSystem.Instance.AddEventListener(EventType.RenderEvent, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.RotateAngleChange, OnEvent);
    }

    private void OnEvent(EventType eventType, Component component, object param)
    {
        switch (eventType)
        {
            case EventType.RenderEvent:
                RenderStart();
                break;
            case EventType.RotateAngleChange:
                RotateAngleChange((float)param);
                break;
        }
    }

    float time = 0;
    private void Update()
    {
        //if (LW1.Collision.Collision.Instance.gameObject.activeInHierarchy)
        //{
        //    time += Time.deltaTime;

        //    if (time > 0.25f)
        //    {
        //        RenderStart();
        //        time = 0;
        //    }
        //}
    }

    public void RenderStart()
    {
        var mainPoints = MethodsManager.Instance.GetActiveMethod.GetRenderPoints();
        var vertex = CalcVertexMatrix(mainPoints);
        MeshCreator.Render(vertex);
    }

    private Vector3[,] CalcVertexMatrix(List<Vector3> mainPoints)
    {
        for (var i = 0; i < mainPoints.Count; i++)
        {
            var go = new GameObject("Support Point " + i);
            go.transform.parent = transform;
            go.transform.position = mainPoints[i];
            SupportPoints.Add(go);
        }

        var stepCount = (int)Mathf.Round(360.0f / rotateAngle);
        var rows = mainPoints.Count;
        var matrix = new Vector3[rows, stepCount];

        for (var i = 0; i < stepCount; i++)
        {
            for (var j = 0; j < rows; j++)
            {
                matrix[j, i] = SupportPoints[j].transform.position;
                SupportPoints[j].transform.RotateAround(BindingLine.transform.position, Vector3.up, rotateAngle);
            }
        }

        StartCoroutine(RemoveOldPoints());

        return matrix;
    }

    private IEnumerator RemoveOldPoints()
    {
        SupportPoints.ForEach(x => Destroy(x));
        SupportPoints.Clear();
        yield return null;
    }

    public void RotateAngleChange(float value)
    {
        rotateAngle = value;
    }

    public void ClearModel()
    {
        MeshCreator.ClearModel();
    }
}
