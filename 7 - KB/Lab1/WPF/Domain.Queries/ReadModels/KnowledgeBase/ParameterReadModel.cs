﻿using QueryAbstractions.ReadModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries.ReadModels.KnowledgeBase
{
    public class ParameterReadModel : BaseReadModel<int>
    {
        public string Name { get; set; }

        public string Question { get; set; }
        public string Description { get; set; }

        public int VariableId { get; set; }
        public VariableReadModel Variable { get; set; }

        public IEnumerable<RuleParameterReadModel> RuleParameters { get; set; }
    }
}
