﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Unity;
using WPF.Helpers;
using WPF.Modules.Base;
using WPF.ViewModels.Base;
using WPF.Controls.Main;
using WPF.ViewModels.Main;

namespace WPF.Modules.Main
{
    class MainPageModule : BaseModule, IDisposable
    {
        public override string Name => "Главная страница";

        protected override UserControl View { get ; set; }
        protected override BaseViewModel ViewModel { get; set; }

        public MainPageModule(IUnityContainer container, SettingType type) : base(container, type) { }

        protected override void CreateViewAndViewModel()
        {
            View = new MainPage();
            ViewModel = container.Resolve<MainPageViewModel>();
        }

        public void Dispose() { }
    }
}
