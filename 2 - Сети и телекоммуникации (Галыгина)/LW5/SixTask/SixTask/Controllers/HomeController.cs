﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SixTask.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        [Route("Index")]
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        [Route("sendMessage")]
        public IActionResult Send([FromBody] string message)
        {
            return Ok();
        }
    }
}
