﻿using Assets.Scripts.Math;
using Assets.Scripts.Methods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BeziersSurface : MethodItem
{
    Matrix MultTransMbAndMb;
    Matrix Mb = new Matrix()
    {
        RowCount = 4,
        ColumnCount = 4,
        Content = new float[,]
        {
            { -1, 3, -3, 1 },
            { 3, -6, 3, 0 },
            { -3, 3, 0, 0 },
            { 1, 0, 0, 0 }
        }
    };
    Matrix TranspMb = new Matrix()
    {
        RowCount = 4,
        ColumnCount = 4,
        Content = new float[,]
        {
            { -1, 3, -3, 1 },
            { 3, -6, 3, 0 },
            { -3, 3, 0, 0 },
            { 1, 0, 0, 0 }
        }
    };

    protected override void Init()
    {
        qualityStep = 0.05f;
        step = 0.25f;
        
        MultTransMbAndMb = MathMatrix.Multiplication(Mb, TranspMb);
        EntityType = EntityType.surface;
    }

    protected override List<Vector3> CalcMethod(List<MainPoint> mainPoints)
    {
        var result = new List<Vector3>();
        var pointInSurfase = 16;
        var surfaseCount = mainPoints.Count() / pointInSurfase;
        var surfases = Enumerable.Range(0, surfaseCount).Select(x => mainPoints.Skip(x * pointInSurfase).Take(pointInSurfase).ToArray());

        for (var id = 0; id < surfaseCount; id++)
        {
            var rows = 4;
            var columns = 4;
            var points = surfases.ElementAt(id);
            var contentX = new float[rows, columns];
            var contentY = new float[rows, columns];
            var contentZ = new float[rows, columns];

            for (int i = 0, index = 0; i < rows; i++)
            {
                for (var j = 0; j < columns; j++, index++)
                {
                    var point = points.ElementAt(index).transform.position;
                    contentX[i, j] = point.x;
                    contentY[i, j] = point.y;
                    contentZ[i, j] = point.z;
                }
            }

            var Qbx = new Matrix()
            {
                ColumnCount = rows,
                RowCount = columns,
                Content = contentX
            };
            var Qby = new Matrix()
            {
                ColumnCount = rows,
                RowCount = columns,
                Content = contentY
            };
            var Qbz = new Matrix()
            {
                ColumnCount = rows,
                RowCount = columns,
                Content = contentZ
            };
            
            for (var s = 0f; s <= 1f; s += Step)
            {
                for (var t = 0f; t <= 1f; t += Step)
                {
                    var S = new Matrix() { RowCount = 1, ColumnCount = 4, Content = new float[,] { { (s * s * s), (s * s), s, 1f } } };
                    var T = new Matrix() { RowCount = 1, ColumnCount = 4, Content = new float[,] { { (t * t * t), (t * t), t, 1f } } };
                    var transpT = MathMatrix.Transpose(T);

                    var MultSMb = MathMatrix.Multiplication(S, Mb);
                    
                    var tmpXSMb = MathMatrix.Multiplication(MultSMb, Qbx);
                    var tmpYSMb = MathMatrix.Multiplication(MultSMb, Qby);
                    var tmpZSMb = MathMatrix.Multiplication(MultSMb, Qbz);

                    var tmpXSMbQ = MathMatrix.Multiplication(tmpXSMb, TranspMb);
                    var tmpYSMbQ = MathMatrix.Multiplication(tmpYSMb, TranspMb);
                    var tmpZSMbQ = MathMatrix.Multiplication(tmpZSMb, TranspMb);

                    var X = MathMatrix.Multiplication(tmpXSMbQ, transpT).Content[0, 0];
                    var Y = MathMatrix.Multiplication(tmpYSMbQ, transpT).Content[0, 0];
                    var Z = MathMatrix.Multiplication(tmpZSMbQ, transpT).Content[0, 0];
                    
                    result.Add(new Vector3(X, Y, Z));
                }
            }
        }
        return result;
    }
}
