﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.ViewModels
{
    public class SelectionViewModel
    {
        public int Id { get; set; }
        public DateTime DateSelection { get; set; }
        public ICollection<SelectionKeyWordViewModel> SelectionKeyWords { get; set; }
    }
}
