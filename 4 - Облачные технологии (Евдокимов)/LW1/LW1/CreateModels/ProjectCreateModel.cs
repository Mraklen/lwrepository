﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.CreateModels
{
    public class ProjectCreateModel
    {
        public string Name { get; set; }
        public string MainUrl { get; set; }
        public string KeyWords { get; set; }
    }
}
