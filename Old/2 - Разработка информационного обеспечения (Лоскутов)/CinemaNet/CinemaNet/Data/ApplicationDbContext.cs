﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Models;

namespace CinemaNet.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Reserve> Reserves { get; set; }
        public DbSet<CinemaRoom> CinemaRooms { get; set; }
        public DbSet<FilmSession> FilmSessions { get; set; }
        public DbSet<CinemaFilms> CinemaFilms { get; set; }
        public DbSet<ReserveSeat> ReserveSeats { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CinemaFilms>()
                .HasKey(t => new { t.CinemaID, t.FilmID });

            modelBuilder.Entity<CinemaFilms>()
                .HasOne(cf => cf.Cinema)
                .WithMany(c => c.CinemaFilms)
                .HasForeignKey(cf => cf.CinemaID);

            modelBuilder.Entity<CinemaFilms>()
                .HasOne(cf => cf.Film)
                .WithMany(f => f.CinemaFilms)
                .HasForeignKey(cf => cf.FilmID);
        }
    }
}
