﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityRender : MonoBehaviour {

    protected int minEntityCount = 2;

    protected bool showOnlyCalcLine = false;
    protected bool showMainLine = false;

    private void Start()
    {
        Init();
    }

    protected virtual void Init() { }
    public virtual int CountEntity() { return 0; }
    public virtual List<MainPoint> GetPositionMainPoints() { return new List<MainPoint>(); }
    public virtual void LineRenderByPoints(List<Vector3> points) { }
    public virtual IEnumerator CoroutineLineRenderByPoints(List<Vector3> points) { yield return new WaitForSeconds(0); }
    public virtual void LineRenderByPoint(Vector3 point, int index) { }
    public virtual void ClearLine() { }
}
