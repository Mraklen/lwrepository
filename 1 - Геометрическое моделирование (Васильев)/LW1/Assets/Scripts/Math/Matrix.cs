﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Math
{
    public class Matrix
    {
        public int ColumnCount { get; set; }
        public int RowCount { get; set; }
        public float[,] Content { get; set; }

        public void CreateContent(List<float> list, int rows, int column)
        {
            Content = new float[rows, column];
            for(int i = 0, index = 0; i < rows; i++)
            {
                for(var j = 0; j < column; j++, index++)
                {
                    Content[i, j] = list[index];
                }
            }
        }
    }
}
