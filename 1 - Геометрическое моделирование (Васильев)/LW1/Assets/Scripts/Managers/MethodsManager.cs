﻿using Assets.Scripts.Methods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Interpolation))]
[RequireComponent(typeof(Beziers))]
[RequireComponent(typeof(ParametricLines))]
[RequireComponent(typeof(BSpline))]
[RequireComponent(typeof(BeziersSurface))]
public class MethodsManager : Singleton<MethodsManager>
{
    [SerializeField] List<MethodItem> MethodList;
    [Space(10)]
    [SerializeField] LineRendering Line;
    [SerializeField] SurfaceRendering Surface;
    [SerializeField] ModelRendering Model;

    public MethodItem GetActiveMethod { get { return MethodList.SingleOrDefault(x => x.enabled); } }

    public override void StartInit()
    {
        GlobalEventSystem.Instance.AddEventListener(EventType.ChangeMethod, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.CreateModelMod, OnEvent);

        var Interpolation = GetComponent<Interpolation>();
        var Beziers = GetComponent<Beziers>();
        var ParametricLines = GetComponent<ParametricLines>();
        var BSpline = GetComponent<BSpline>();
        var BeziersSurface = GetComponent<BeziersSurface>();

        Interpolation.SetEntity(Line);
        Beziers.SetEntity(Line);
        ParametricLines.SetEntity(Line);
        BSpline.SetEntity(Line);
        BeziersSurface.SetEntity(Surface);

        Interpolation.enabled = false;
        Beziers.enabled = false;
        ParametricLines.enabled = false;
        BSpline.enabled = false;
        BeziersSurface.enabled = false;

        MethodList = new List<MethodItem>
        {
            Interpolation,
            Beziers,
            ParametricLines,
            BSpline,
            BeziersSurface
        };
    }

    private void PlayMethod(object method)
    {
        MethodList.ForEach(x =>
        {
            x.enabled = method.GetType() == x.GetType();
            ShowLine(!(method.GetType() == typeof(BeziersSurface)));
        });
    }

    private void ShowLine(bool value)
    {
        Line.gameObject.SetActive(value);
        Surface.gameObject.SetActive(!value);
        ShowModel(false);

        var count = value ? Line.CountEntity() : Surface.CountEntity();
        GlobalEventSystem.Instance.PostEventMessage(EventType.PointCountChange, this, count);
    }

    private void ShowModel(bool? value = null)
    {
        if (!Line.gameObject.activeInHierarchy) return;

        var res = value ?? !Model.gameObject.activeInHierarchy;
        Model.gameObject.SetActive(res);

        if (res) Model.ClearModel();

        GlobalEventSystem.Instance.PostEventMessage(EventType.ModelModChange, this, value);
    }

    public void OnEvent(EventType eventType, Component component, object Param = null)
    {
        switch (eventType)
        {
            case EventType.ChangeMethod:
                PlayMethod(Param);
                break;
            case EventType.CreateModelMod:
                ShowModel();
                break;
        }
    }

    public override void AwakeInit() { }
    public override void UpdateInit() { }
}
