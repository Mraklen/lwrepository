﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Helpers
{
    public class PathBuilder
    {
        public static string BasePath => $"{new FileInfo(Assembly.GetExecutingAssembly().Location).Directory}\\";
                
        private static string Date => DateTime.Now.ToString("ddMMyyyy");

        public static string GetBasePathJson(string fileName) => $"{BasePath}{fileName}.json";
    }
}
