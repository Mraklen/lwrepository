﻿using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.FuzzyStatement
{
    public enum ConditionType
    {
        AND,
        OR
    }

    public class Condition
    {
        public ConditionType Type { get; set; }

        public IVariable Variable { get; set; }

        public List<Condition> Conditions { get; set; }

        internal double CheckCondition(IEnumerable<IVariable> variables)
        {
            if (Variable != null)
            {
                var result = Variable.CalcTermSet(variables.FirstOrDefault(x => x.CodeName == Variable.CodeName).Value);
                return result;
            }
            else
            {
                var result = CheckByType(variables);
                return result;
            }
        }

        private double CheckByType(IEnumerable<IVariable> variables)
        {
            switch (Type)
            {
                case ConditionType.AND:
                    var res = Conditions.Min(x => x.CheckCondition(variables));
                    return res;
                case ConditionType.OR:
                    var res2 = Conditions.Max(x => x.CheckCondition(variables));
                    return res2;
                default: return 0;
            }
        }
    }
}
