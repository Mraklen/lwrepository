﻿using LW1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    public interface IBaseAggregateRepository<TAggregate> where TAggregate : class, IAggregate
    {
        TAggregate GetById(Guid id);

        void Update(TAggregate aggregate);

        void Create(TAggregate aggregate);

        //void LoadCollection(TAggregate entity, Expression<Func<TAggregate, IEnumerable<object>>> expression);

        //void LoadReference(TAggregate entity, Expression<Func<TAggregate, object>> expression);
    }
}
