﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WPF.Helpers;

namespace WPF.Modules.Base
{
    public interface IModule
    {
        string Name { get; }
        UserControl UserInterface { get; }
        WindowSetting WindowSetting { get; }
        void Deactivate();
    }
}
