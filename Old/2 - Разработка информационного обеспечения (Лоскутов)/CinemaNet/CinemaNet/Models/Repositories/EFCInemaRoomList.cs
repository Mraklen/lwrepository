﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class _EFCinemaRoomList : ICinemaRoomList
    {
        private ApplicationDbContext context;
        public _EFCinemaRoomList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public CinemaRoom GetCinemaRoom(int sessionID) =>
            context.CinemaRooms
            .Include(cr => cr.Seats)
            .Include(cr => cr.FilmSessions)
            .Where(cr => cr.FilmSessions.Any(fs => fs.FilmSessionID == sessionID))
            .ToList().ElementAt(0);

        public IEnumerable<CinemaRoom> CinemaRoomList() => context.CinemaRooms
            .Include(cr => cr.Seats)
            .Include(cr => cr.FilmSessions);
    }
}
