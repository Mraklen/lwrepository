﻿using Newtonsoft.Json;
using SeatBattle.CSharp.EventManager;
using SeatBattle.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SeatBattle.CSharp.Net
{
    public class NetworkManager
    {
        public NetworkManager()
        { }
        
        private Server Server;

        public void StartNewServer()
        {
            StartServer();
        }

        public void RestartServer()
        {
            StopServer();
            StartServer();
        }

        private void StartServer()
        {
            Server = new Server();
            Server.Start();
        }

        public static void PostMessageToServer(PostPackage package)
        {
            var ipHost = Dns.GetHostEntry("localhost");
            var ipAddress = ipHost.AddressList[0];
            var ipEndPoint = new IPEndPoint(ipAddress, Server.enemyPort);
            var sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            sender.Connect(ipEndPoint);
            
            byte[] msg = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(package));
            int bytesSent = sender.Send(msg);
            
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
        
        public void StopServer()
        {
            if (Server != null) Server.Stop();
        }
    }
}
