﻿using DB.EFData;
using DB.EFData.Repositories;
using Domain.Models.Repositories;
using Domain.Models.Repositories.Interfaces;
using EntityAbstractions.Entities;
using EntityAbstractions.Implementations;

namespace DB.EF.Data.Repositories.Implementations
{
    public class PgSqlRepositoryFactory : IDomainRepositoryFactory
    {
        PgContext context;

        public PgSqlRepositoryFactory()
        {
            context = new PgContext();
        }
        
        public IBaseRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : BaseEntity => new DefaultEntityRepository<TEntity>(context);
        public IBaseAggregateRepository<TAggregate> GetAggregateRepository<TAggregate>() where TAggregate : GlobalEntity, IAggregate => new DefaultAggregateRepository<TAggregate>(context);

        public IRuleRepository GetRuleRepository() => new RuleRepository(context);

        public IVariableRepository GetVariableRepository() => new VariableRepository(context);
    }
}
