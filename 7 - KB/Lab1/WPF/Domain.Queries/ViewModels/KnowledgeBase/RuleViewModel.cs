﻿using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.ViewModels.KnowledgeBase
{
    public class RuleViewModel : BaseViewModel<int>
    {
        public bool? State { get; set; }

        public ICollection<RuleParameterViewModel> RuleParameters { get; set; }
    }
}
