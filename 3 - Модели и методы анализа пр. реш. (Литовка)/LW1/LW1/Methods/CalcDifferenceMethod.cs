﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Methods
{
    public class CalcDifferenceMethod
    {
        public static double deltaX = 0.1;
        public  static double deltaTau = 0.05;

        static double a = 0.013;
        static double X = 1;
        static double tauMax = 100;
        
        public static double[,] CalcMethod()
        {
            var tauCount = (int)(tauMax / deltaTau) + 1;
            var xCount = (int)(X / deltaX) + 1;

            double[,] res = new double[xCount, tauCount];

            // вычисление краевых условий
            var x = 0.0;
            for (var xi = 0; xi < xCount; xi++, x += deltaX)
            {
                res[xi, 0] = fi(x);
            }
            var tau = 0.0;
            for (var taui = 1; taui < tauCount; taui++, tau += deltaTau)
            {
                res[0, taui] = f1(tau);
                res[xCount - 1, taui] = f2(tau);
            }

            //вычисление результата
            for (var taui = 0; taui < tauCount - 1; taui++)
            {
                for (var xi = 1; xi < xCount - 1; xi++)
                {
                    res[xi, taui + 1] = res[xi, taui] + A() * (res[xi + 1, taui] - 2 * res[xi, taui] + res[xi - 1, taui]);
                }
            }

            return res;
        }
        
        private static double fi(double x)
        {
            return 34 - Math.Pow((x + 2), 2);
        }

        private static double f1(double tau)
        {
            return 20 + (20 / (Math.Exp(0.1 * tau) + 1));
        }

        private static double f2(double tau)
        {
            return 25 + (4 * Math.Sin(tau));
        }

        private static bool DeltaConditionCheck(double deltaTau, double deltaX)
        {
            return deltaTau <= CalcDeltaTauByDeltaX();
        }

        private static double CalcDeltaTauByDeltaX()
        {
            return 0.5 * Math.Pow(deltaX, 2);
        }

        private static double A()
        {
            return (a * deltaTau) / Math.Pow(deltaX, 2);
        }
    }
}
