﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Methods
{
    public class MethodItem : Method
    {
        protected EntityRender Rendering;
        protected bool SlowMotionRender = false;
        private bool isSlowMotionActive = false;
        private float time = 0;

        protected bool isHighQuality = true;
        protected float qualityStep = 0;
        protected float step = 0;
        public float Step { get { return isHighQuality ? qualityStep : step; } }

        private void Start()
        {
            GlobalEventSystem.Instance.AddEventListener(EventType.SlowMotionRender, OnEvent);
            GlobalEventSystem.Instance.AddEventListener(EventType.ChangeQuality, OnEvent);
            Init();
        }

        private void OnEvent(EventType eventType, Component component, object param)
        {
            switch (eventType)
            {
                case EventType.SlowMotionRender:
                    SlowMotionRender = !SlowMotionRender;
                    ((GuiManager)component).ShowMessage((SlowMotionRender) ? "Медленный рендер" : "Быстрый рендер");
                    break;
                case EventType.ChangeQuality:
                    isHighQuality = !isHighQuality;
                    GlobalEventSystem.Instance.PostEventMessage(EventType.QualityChanged, this, isHighQuality);
                    break;
            }
        }

        private void Update()
        {
            time += Time.deltaTime;
            if (time > 0.025f)
            {
                var mainPoints = Rendering.GetPositionMainPoints();
                if (mainPoints.Count() > 1)
                {
                    if (!SlowMotionRender)
                    {
                        StartNewRender(CalcMethod(mainPoints));
                    }
                    else
                    {
                        StartNewSlowMotionRender(CalcMethod(mainPoints));
                    }
                }

                time = 0;
            }
        }

        protected virtual void Init() { }
        
        protected virtual List<Vector3> CalcMethod(List<MainPoint> mainPoints) { return new List<Vector3>(); }

        public List<Vector3> GetRenderPoints()
        {
            var mainPoints = Rendering.GetPositionMainPoints();
            return mainPoints.Count > 1 ? CalcMethod(mainPoints) : null;
        }

        public void SetEntity(EntityRender rendering)
        {
            Rendering = rendering;
        }

        protected void StartNewRender(List<Vector3> points)
        {
            StopAllCoroutines();
            isSlowMotionActive = false;
            Rendering.LineRenderByPoints(points);
        }

        protected void StartNewSlowMotionRender(List<Vector3> points)
        {
            if (!isSlowMotionActive)
            {
                isSlowMotionActive = true;
                StopAllCoroutines();
                StartCoroutine(Render(points));
            }
        }

        protected virtual IEnumerator Render(List<Vector3> points)
        {
            Rendering.ClearLine();
            yield return StartCoroutine(Rendering.CoroutineLineRenderByPoints(points));
            isSlowMotionActive = false;
        }
    }
}
