﻿using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Domain.Models.Entities.KnowledgeBase
{
    [Serializable]
    public class Rule : BaseEntity
    {
        [NotMapped] public bool? State { get; set; }

        public virtual ICollection<RuleParameter> RuleParameters { get; private set; }

        public static Rule CreateNew(ICollection<RuleParameter> ruleParameters)
        {
            return new Rule { RuleParameters = ruleParameters };
        }

        public void SetRuleState(bool state)
        {
            State = state;
            RuleParameters.FirstOrDefault(x => x.ParamType == ParamType.Result).Parameter.State = state;
        }
    }
}
