﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFCinemaList : ICinemaList
    {
        private ApplicationDbContext context;
        public EFCinemaList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<Cinema> CinemaList(int filmID)
        {
            return from f in context.Cinemas
                   join cf in context.CinemaFilms on f.CinemaID equals cf.CinemaID
                   where cf.FilmID == filmID
                   select f;
        }

        public IEnumerable<Cinema> CinemaList() => context.Cinemas
            .Include(c => c.CinemaFilms)
            .Include(c => c.FilmSessions);
    }
}
