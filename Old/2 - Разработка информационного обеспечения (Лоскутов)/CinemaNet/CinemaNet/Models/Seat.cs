﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class Seat
    {
        public int SeatID { get; set; }
        public int Row { get; set; }
        public int SeatNum { get; set; }

        public int CinemaRoomID { get; set; }
        public CinemaRoom CinemaRoom { get; set; }
    }
}
