﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.ViewModels
{
    public class ProjectViewModel
    {
        public Guid Id { get; set; }

        public string ApplicationUserId { get; set; }

        public string Name { get; set; }
        public string MainUrl { get; set; }
        public DateTime? LastSelection { get; set; }

        public ICollection<KeyWordViewModel> KeyWords { get; set; }
        public ICollection<SelectionViewModel> Selections { get; set; }
    }
}
