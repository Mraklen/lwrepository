﻿using Domain.Models.Entities.KnowledgeBase;
using QueryAbstractions.ReadModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries.ReadModels.KnowledgeBase
{
    public class RuleParameterReadModel : BaseReadModel<int>
    {
        public ParamType ParamType { get; set; }
        
        public int RuleId { get; set; }
        public RuleReadModel Rule { get; set; }

        public int ParameterId { get; set; }
        public ParameterReadModel Parameter { get; set; }
    }
}
