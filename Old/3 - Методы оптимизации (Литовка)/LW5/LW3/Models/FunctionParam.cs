﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW3.Models
{
    public class FunctionParam
    {
        public static double X1Value { get; private set; }
        public static double X2Value { get; private set; }
        public static double EValue { get; private set; }

        const int width = 20;
        const int height = 20;
        const int x1Value = -10;
        const int x2Value = -10;

        public static double a = 3;
        public static double b = -2;
        public static double c = 1;
        public static double d = 3;
        public static double alpha = 75;
        public static double step = 1f;

        public static IsolineType CurrenIsolineType { get; set; } = IsolineType.Ellipse;
        public static FineType CurrenFineType { get; set; } = FineType.Internal;

        private Dictionary<IsolineType, IsolineData> IsolinesData = new Dictionary<IsolineType, IsolineData>()
        {
            { IsolineType.Ellipse, new IsolineData() },
            { IsolineType.Rosenbroke, new IsolineData() }
        };

        private Dictionary<FineType, FineData> FinesData = new Dictionary<FineType, FineData>()
        {
            { FineType.First, new FineData() },
            { FineType.Second, new FineData() }
        };

        public FunctionParam(double x1, double x2, double e)
        {
            X1Value = x1;
            X2Value = x2;
            EValue = 0.0000001;
            CalcEllipseData();
            CalcRosenbrokeData();
            CalcFirstFineData();
            CalcSecondFineData();
        }

        public double CalcFunction(double x1, double x2, double k)
        {
            switch (CurrenIsolineType)
            {
                case IsolineType.Ellipse: return CalcNoFakeEllipseFuntion(x1, x2) + Fine(x1, x2, k);
                case IsolineType.Rosenbroke: return CalcRosenbrokeFunction(x1, x2) + Fine(x1, x2, k);
            }
            return 0;
        }

        public double CalcFineFunction(double x1, double x2, FineType fineType)
        {
            switch (fineType)
            {
                case FineType.First: return CalcFirstFineFunction(x1, x2);
                case FineType.Second: return CalcSecondFineFunction(x1, x2);
            }
            return 0;
        }

        private double Fine(double x1, double x2, double k)
        {
            k = 1000;
            double penalty = 0;
            double fz1 = CalcFineFunction(x1, x2, FineType.First);
            double fz2 = CalcFineFunction(x1, x2, FineType.Second);

            double tmp = 1;
            if (fz1 > 0)
            {
                tmp = Math.Log(fz1);
                penalty -= 1 / k * tmp;
            }
            else
                penalty += k * Math.Pow(fz2, 2);

            return penalty;
        }

        public bool Condition(double x1, double x2)
        {
            double f1, f2;

            f1 = CalcFineFunction(x1, x2, FineType.First);
            f2 = -CalcFineFunction(x1, x2, FineType.Second);

            if (f1 <= 0 && f2 <= 0)
                return true;
            else
                return false;
        }

        public IsolineData GetIsolineData()
        {
            return IsolinesData[CurrenIsolineType];
        }

        public FineData GetFineData(FineType fineType)
        {
            return FinesData[fineType];
        }

        private void CalcEllipseData()
        {
            var data = IsolinesData[IsolineType.Ellipse];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcEllipseFuntion(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);

            data.ExtremPoint = new PointXY() { X1 = a, X2 = b };
        }

        private void CalcRosenbrokeData()
        {
            var data = IsolinesData[IsolineType.Rosenbroke];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcRosenbrokeFunction(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);

            data.ExtremPoint = new PointXY() { X1 = 1, X2 = 1 };
        }

        private void CalcFirstFineData()
        {
            var data = FinesData[FineType.First];
            data.Points = new List<PointXY>();

            var minStep = step / 10;
            for (double x1 = x1Value; x1 < -x1Value; x1 += minStep)
                for (double x2 = x2Value; x2 < -x2Value; x2 += minStep)
                {
                    var y = CalcFirstFineFunction(x1, x2);
                    if (y < 0.1 && y > -0.1) //y == 0 || 
                    {
                        data.Points.Add(new PointXY() { X1 = x1, X2 = x2 });
                    }
                }
        }

        private void CalcSecondFineData()
        {
            var data = FinesData[FineType.Second];
            data.Points = new List<PointXY>();
            for (double x1 = x1Value; x1 < -x1Value; x1 += step)
                for (double x2 = x2Value; x2 < -x2Value; x2 += step)
                {
                    var y = CalcSecondFineFunction(x1, x2);
                    if (y >= 0)
                    {
                        data.Points.Add(new PointXY() { X1 = x1, X2 = x2 });
                    }
                }
        }

        private double CalcEllipseFuntion(double x1, double x2)
        {
            return (Math.Pow(((x1 - a) * Math.Cos(alpha) + (x2 - b) * Math.Sin(alpha)), 2) / (Math.Pow(c, 2))) +
                   (Math.Pow(((x2 - b) * Math.Cos(alpha) + (x1 - a) * Math.Sin(alpha)), 2) / (Math.Pow(d, 2)));
        }

        private double CalcNoFakeEllipseFuntion(double x1, double x2)
        {
            double af = 1.5;
            double bf = -0.3;
            double cf = 1;
            double df = 3;
            double alphaf = 1.42;
            return (Math.Pow(((x1 - af) * Math.Cos(alphaf) + (x2 - bf) * Math.Sin(alphaf)), 2) / (Math.Pow(cf, 2))) +
                   (Math.Pow(((x2 - bf) * Math.Cos(alphaf) + (x1 - af) * Math.Sin(alphaf)), 2) / (Math.Pow(df, 2)));
        }

        private double CalcRosenbrokeFunction(double x1, double x2)
        {
            return 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2) + Math.Pow((1 - x1), 2);
        }

        private double CalcFirstFineFunction(double x1, double x2)
        {
            return x1 + 5 * x2;
        }

        private double CalcSecondFineFunction(double x1, double x2)
        {
            return Math.Pow(x2, 2) + 3 * x1 - 2 * x2 - 5;
        }
    }
}
