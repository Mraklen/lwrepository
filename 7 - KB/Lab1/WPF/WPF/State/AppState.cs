﻿using Domain.Models.Entities.FuzzyStatement;
using Domain.Models.Entities.KnowledgeBase;
using System.Collections.Generic;

namespace WPF.State
{
    public class AppState
    {
        public List<Variable> Variables { get; set; }
        public List<Rule> Rules { get; set; }

        public List<Statement> Statements { get; set; }

        public AppState()
        {

        }
    }
}
