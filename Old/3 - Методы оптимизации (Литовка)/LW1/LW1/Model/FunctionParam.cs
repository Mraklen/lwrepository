﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicExpresso;

namespace LW1.Model
{
    public class FunctionParam
    {
        public static string FunctionText { get; private set; }
        public static double AValue { get; private set; }
        public static double BValue { get; private set; }
        public static double SValue { get; private set; }
        public List<PointXY> Points { get; private set; }

        public double[] GetAxisData(char axisName)
        {
            return (axisName == 'X') ?
                Points.ToDictionary(k => k.X).Keys.ToArray() :
                Points.ToDictionary(k => k.Y).Keys.ToArray();
        }

        public FunctionParam(string function, double a, double b, double s)
        {
            FunctionText = function;
            AValue = a;
            BValue = b;
            SValue = s;
            InitPoints();
        }

        public static double CalcFunc(double value)
        {
            var interpreter = new Interpreter();
            Parameter x = new Parameter("x", typeof(double), value);
            return (double) interpreter.Eval(FunctionText, x);
        }

        private void InitPoints()
        {
            Points = new List<PointXY>();
            var interpreter = new Interpreter();

            double xLastStep = BValue;
            double xStep = SValue;

            for (Parameter xCurrent = new Parameter("x", typeof(double), AValue);
                 (double) xCurrent.Value < xLastStep;
                 xCurrent.Value = (double) xCurrent.Value + (xStep / 10))
            {
                double yTmp = (double) interpreter.Eval(FunctionText, xCurrent);

                if (!Double.IsNaN(yTmp))
                {
                    Points.Add(new PointXY() { X = (double) xCurrent.Value, Y = yTmp });
                }
            }
        }
    }
}
