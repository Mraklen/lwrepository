﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Models.Project
{
    public class SelectionUrl : IEntity<int>
    {
        public int Id { get; private set; }

        public int SelectionKeyWordId { get; private set; }
        public SelectionKeyWord SelectionKeyWord { get; private set; } 

        public int KeyWordId { get; private set; }
        public KeyWord KeyWord { get; private set; }

        public int Order { get; private set; }
        public string Value { get; private set; }

        public int? Offset { get; private set; }
        public bool? IsUp { get; private set; }
        public bool Marked { get; private set; }

        public static SelectionUrl CreateNew(string value, int order, int keyWordId)
        {
            return new SelectionUrl()
            {
                Value = value,
                Order = order,
                KeyWordId = keyWordId,
                Marked = false
            };
        }

        public void SetMarked(bool value)
        {
            Marked = value;
        }
    }
}
