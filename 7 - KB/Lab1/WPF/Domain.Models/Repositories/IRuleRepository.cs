﻿using Domain.Models.Entities.KnowledgeBase;
using Domain.Models.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Models.Repositories
{
    public interface IRuleRepository : IBaseRepository<Rule>
    {
        Task<IEnumerable<Rule>> GetList();
    }
}
