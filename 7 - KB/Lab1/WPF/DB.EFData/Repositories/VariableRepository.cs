﻿using DB.EF.Data.Repositories.Implementations;
using Domain.Models.Entities.KnowledgeBase;
using Domain.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.EFData.Repositories
{
    class VariableRepository : DefaultEntityRepository<Variable>, IVariableRepository
    {
        public VariableRepository(PgContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Variable>> GetList()
        {
            return await Set.Include(x => x.Parameters).ToListAsync();
        }
    }
}
