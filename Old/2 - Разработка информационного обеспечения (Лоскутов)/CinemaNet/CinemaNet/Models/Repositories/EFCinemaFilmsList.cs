﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFCinemaFilmsList : ICinemaFilmsList
    {
        public ApplicationDbContext context;
        public EFCinemaFilmsList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<CinemaFilms> CinemaFilmsList => context.CinemaFilms;
    }
}
