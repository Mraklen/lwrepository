﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface IFilmSessionList
    {
        IEnumerable<FilmSession> GetFilmSession(int cinemaID, int filmID);
        IEnumerable<FilmSession> FilmSessionLists();
        IEnumerable<FilmSession> GetFilmSessionByIds(List<int> filmSessionId);
    }
}
