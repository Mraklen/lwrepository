﻿using QueryAbstractions.Filter;
using QueryAbstractions.ReadModels;
using QueryAbstractions.TableModel;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QueryAbstractions.Queries
{
    public interface IBaseQueries<TEntity, TView, TID>
        where TEntity : BaseReadModel<TID>
        where TView : BaseViewModel<TID>
        where TID : struct, IEquatable<TID>
    {
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetById(TID id);

        Task<IEnumerable<TEntity>> GetList(FilterModel<TEntity, TView> filter, bool withPaging = true);
        IQueryable<TEntity> GetListQuery(FilterModel<TEntity, TView> filter, bool withPaging = true);

        Task<TableAnswer<TView>> GetTableList(FilterModel<TEntity, TView> filter, Expression<Func<TEntity, TView>> projection = null);

        Task<int> GetCount(FilterModel<TEntity, TView> filter);
    }
}
