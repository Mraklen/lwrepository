﻿using Domain.Models.Helpers;
using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Domain.Models.Entities.KnowledgeBase
{
    [Serializable]
    public class ReasoningItem : BasePropertyChanged
    {
        public string Question => ruleParameter.Parameter.Question;

        public string Result => State.HasValue && State.Value && ruleParameter == null ? $"Мы не знаем что с Вами :(" : $"{(isSimple ? "Из простого правила: " : (ruleParameter.ParamType == ParamType.Result ? "Ответ: " : ""))}Факт - {Id} | Правило - {ruleParameter.RuleId} | {(!State.HasValue ? null : State.Value ? $"{ruleParameter.Parameter.Variable.CodeName}{(string.IsNullOrEmpty(ruleParameter.Parameter.Name) ? "" : $" = {ruleParameter.Parameter.Name}")}" : $"{ruleParameter.Parameter.Variable.CodeName} = /---/")}";

        bool? state = null;
        public bool? State
        {
            get => state;
            set
            {
                state = value;
                OnPropertyChanged("State");
                OnPropertyChanged("Result");
            }
        }

        bool isSimple { get; set; }

        RuleParameter ruleParameter;
        public RuleParameter RuleParameter
        {
            get => ruleParameter;
            set
            {
                ruleParameter = value;

                OnPropertyChanged("RuleParameter");
                OnPropertyChanged("Question");
            }
        }

        public void SetId(int v)
        {
            Id = v;
        }

        public void SetSimpleState()
        {
            isSimple = true;
        }
    }
}
