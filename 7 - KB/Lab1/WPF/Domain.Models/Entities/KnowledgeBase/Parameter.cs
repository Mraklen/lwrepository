﻿using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models.Entities.KnowledgeBase
{
    [Serializable]
    public class Parameter : BaseEntity
    {
        public string Name { get; private set; }
        
        public string Question { get; private set; }
        public string Description { get; private set; }
        
        [NotMapped] public bool? State { get; set; }

        public int VariableId { get; private set; }
        public Variable Variable { get; private set; }

        public virtual ICollection<RuleParameter> RuleParameters { get; private set; }
        
        public static Parameter CreteNew(string name, string question, string description = null)
        {
            return new Parameter
            {
                Name = name,
                Question = question,
                Description = description,
                RuleParameters = new List<RuleParameter>()
            };
        }
    }
}
