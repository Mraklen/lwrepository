﻿namespace LW2.RLE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.drowNoize = new System.Windows.Forms.Button();
            this.btnWhiteColor = new System.Windows.Forms.Button();
            this.btnBlackColor = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCircle = new System.Windows.Forms.Button();
            this.btnRect = new System.Windows.Forms.Button();
            this.tbPenSize = new System.Windows.Forms.TrackBar();
            this.lbPenSize = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSaveAndCode = new System.Windows.Forms.Button();
            this.pbCodeImg = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLoadAndDecode = new System.Windows.Forms.Button();
            this.pbDecodeImg = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbPercentCompression = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbArchiveFileSize = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbMainFileSize = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPenSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCodeImg)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDecodeImg)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.drowNoize);
            this.groupBox1.Controls.Add(this.btnWhiteColor);
            this.groupBox1.Controls.Add(this.btnBlackColor);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.btnCircle);
            this.groupBox1.Controls.Add(this.btnRect);
            this.groupBox1.Controls.Add(this.tbPenSize);
            this.groupBox1.Controls.Add(this.lbPenSize);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSaveAndCode);
            this.groupBox1.Controls.Add(this.pbCodeImg);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(628, 466);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Рисование и кодирование";
            // 
            // drowNoize
            // 
            this.drowNoize.Location = new System.Drawing.Point(439, 360);
            this.drowNoize.Name = "drowNoize";
            this.drowNoize.Size = new System.Drawing.Size(146, 31);
            this.drowNoize.TabIndex = 10;
            this.drowNoize.Text = "Шум";
            this.drowNoize.UseVisualStyleBackColor = true;
            this.drowNoize.Click += new System.EventHandler(this.drowNoize_Click);
            // 
            // btnWhiteColor
            // 
            this.btnWhiteColor.Location = new System.Drawing.Point(524, 166);
            this.btnWhiteColor.Margin = new System.Windows.Forms.Padding(4);
            this.btnWhiteColor.Name = "btnWhiteColor";
            this.btnWhiteColor.Size = new System.Drawing.Size(93, 49);
            this.btnWhiteColor.TabIndex = 9;
            this.btnWhiteColor.Text = "Белый";
            this.btnWhiteColor.UseVisualStyleBackColor = true;
            this.btnWhiteColor.Click += new System.EventHandler(this.btnWhiteColor_Click);
            // 
            // btnBlackColor
            // 
            this.btnBlackColor.Location = new System.Drawing.Point(423, 166);
            this.btnBlackColor.Margin = new System.Windows.Forms.Padding(4);
            this.btnBlackColor.Name = "btnBlackColor";
            this.btnBlackColor.Size = new System.Drawing.Size(93, 49);
            this.btnBlackColor.TabIndex = 8;
            this.btnBlackColor.Text = "Черный";
            this.btnBlackColor.UseVisualStyleBackColor = true;
            this.btnBlackColor.Click += new System.EventHandler(this.btnBlackColor_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(423, 223);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(195, 42);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnCircle
            // 
            this.btnCircle.Location = new System.Drawing.Point(524, 110);
            this.btnCircle.Margin = new System.Windows.Forms.Padding(4);
            this.btnCircle.Name = "btnCircle";
            this.btnCircle.Size = new System.Drawing.Size(93, 49);
            this.btnCircle.TabIndex = 6;
            this.btnCircle.Text = "Круг";
            this.btnCircle.UseVisualStyleBackColor = true;
            this.btnCircle.Click += new System.EventHandler(this.btnCircle_Click);
            // 
            // btnRect
            // 
            this.btnRect.Location = new System.Drawing.Point(423, 110);
            this.btnRect.Margin = new System.Windows.Forms.Padding(4);
            this.btnRect.Name = "btnRect";
            this.btnRect.Size = new System.Drawing.Size(93, 49);
            this.btnRect.TabIndex = 5;
            this.btnRect.Text = "Квадрат";
            this.btnRect.UseVisualStyleBackColor = true;
            this.btnRect.Click += new System.EventHandler(this.btnRect_Click);
            // 
            // tbPenSize
            // 
            this.tbPenSize.Location = new System.Drawing.Point(423, 46);
            this.tbPenSize.Margin = new System.Windows.Forms.Padding(4);
            this.tbPenSize.Maximum = 30;
            this.tbPenSize.Minimum = 1;
            this.tbPenSize.Name = "tbPenSize";
            this.tbPenSize.Size = new System.Drawing.Size(197, 56);
            this.tbPenSize.TabIndex = 4;
            this.tbPenSize.Value = 6;
            this.tbPenSize.ValueChanged += new System.EventHandler(this.tbPenSize_ValueChanged);
            // 
            // lbPenSize
            // 
            this.lbPenSize.AutoSize = true;
            this.lbPenSize.Location = new System.Drawing.Point(535, 25);
            this.lbPenSize.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPenSize.Name = "lbPenSize";
            this.lbPenSize.Size = new System.Drawing.Size(16, 17);
            this.lbPenSize.TabIndex = 3;
            this.lbPenSize.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Размер кисти:";
            // 
            // btnSaveAndCode
            // 
            this.btnSaveAndCode.Location = new System.Drawing.Point(9, 402);
            this.btnSaveAndCode.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveAndCode.Name = "btnSaveAndCode";
            this.btnSaveAndCode.Size = new System.Drawing.Size(611, 54);
            this.btnSaveAndCode.TabIndex = 1;
            this.btnSaveAndCode.Text = "Завершить рисование и сохранить изображение";
            this.btnSaveAndCode.UseVisualStyleBackColor = true;
            this.btnSaveAndCode.Click += new System.EventHandler(this.btnSaveAndCode_Click);
            // 
            // pbCodeImg
            // 
            this.pbCodeImg.BackColor = System.Drawing.SystemColors.Control;
            this.pbCodeImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbCodeImg.Location = new System.Drawing.Point(9, 25);
            this.pbCodeImg.Margin = new System.Windows.Forms.Padding(4);
            this.pbCodeImg.Name = "pbCodeImg";
            this.pbCodeImg.Size = new System.Drawing.Size(399, 369);
            this.pbCodeImg.TabIndex = 0;
            this.pbCodeImg.TabStop = false;
            this.pbCodeImg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbCodeImg_MouseDown);
            this.pbCodeImg.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbCodeImg_MouseMove);
            this.pbCodeImg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbCodeImg_MouseUp);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLoadAndDecode);
            this.groupBox2.Controls.Add(this.pbDecodeImg);
            this.groupBox2.Location = new System.Drawing.Point(864, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(417, 469);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Загрузка и декодирование";
            // 
            // btnLoadAndDecode
            // 
            this.btnLoadAndDecode.Location = new System.Drawing.Point(9, 402);
            this.btnLoadAndDecode.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadAndDecode.Name = "btnLoadAndDecode";
            this.btnLoadAndDecode.Size = new System.Drawing.Size(397, 54);
            this.btnLoadAndDecode.TabIndex = 9;
            this.btnLoadAndDecode.Text = "Загрузить изображение";
            this.btnLoadAndDecode.UseVisualStyleBackColor = true;
            this.btnLoadAndDecode.Click += new System.EventHandler(this.btnLoadAndDecode_Click);
            // 
            // pbDecodeImg
            // 
            this.pbDecodeImg.BackColor = System.Drawing.SystemColors.Control;
            this.pbDecodeImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDecodeImg.Location = new System.Drawing.Point(8, 25);
            this.pbDecodeImg.Margin = new System.Windows.Forms.Padding(4);
            this.pbDecodeImg.Name = "pbDecodeImg";
            this.pbDecodeImg.Size = new System.Drawing.Size(399, 369);
            this.pbDecodeImg.TabIndex = 8;
            this.pbDecodeImg.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbPercentCompression);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.lbArchiveFileSize);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lbMainFileSize);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(651, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(207, 466);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Информация";
            // 
            // lbPercentCompression
            // 
            this.lbPercentCompression.AutoSize = true;
            this.lbPercentCompression.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPercentCompression.Location = new System.Drawing.Point(23, 222);
            this.lbPercentCompression.Name = "lbPercentCompression";
            this.lbPercentCompression.Size = new System.Drawing.Size(55, 39);
            this.lbPercentCompression.TabIndex = 7;
            this.lbPercentCompression.Text = "00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Процент сжатия";
            // 
            // lbArchiveFileSize
            // 
            this.lbArchiveFileSize.AutoSize = true;
            this.lbArchiveFileSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbArchiveFileSize.Location = new System.Drawing.Point(22, 133);
            this.lbArchiveFileSize.Name = "lbArchiveFileSize";
            this.lbArchiveFileSize.Size = new System.Drawing.Size(55, 39);
            this.lbArchiveFileSize.TabIndex = 5;
            this.lbArchiveFileSize.Text = "00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Размер сжатого файла";
            // 
            // lbMainFileSize
            // 
            this.lbMainFileSize.AutoSize = true;
            this.lbMainFileSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbMainFileSize.Location = new System.Drawing.Point(23, 50);
            this.lbMainFileSize.Name = "lbMainFileSize";
            this.lbMainFileSize.Size = new System.Drawing.Size(55, 39);
            this.lbMainFileSize.TabIndex = 3;
            this.lbMainFileSize.Text = "00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Исходный размер файла";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 490);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Лабораторная работа №2. RLE";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbPenSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCodeImg)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbDecodeImg)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pbCodeImg;
        private System.Windows.Forms.Label lbPenSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSaveAndCode;
        private System.Windows.Forms.TrackBar tbPenSize;
        private System.Windows.Forms.Button btnCircle;
        private System.Windows.Forms.Button btnRect;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnLoadAndDecode;
        private System.Windows.Forms.PictureBox pbDecodeImg;
        private System.Windows.Forms.Button btnWhiteColor;
        private System.Windows.Forms.Button btnBlackColor;
        private System.Windows.Forms.Button drowNoize;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbMainFileSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbPercentCompression;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbArchiveFileSize;
        private System.Windows.Forms.Label label4;
    }
}

