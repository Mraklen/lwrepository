﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GuiManager : Singleton<GuiManager>
{
    [SerializeField] Text TextInfo;

    public override void AwakeInit() { }
    public override void StartInit() { }

    public override void UpdateInit()
    {
        if (Input.GetKeyDown(KeyCode.M)) ShowAxis();
    }
    
    private void ShowAxis()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.AxisMoveShow, this);
    }
    
    public void ShowMessage(string message)
    {
        StopAllCoroutines();
        StartCoroutine(SendInfoMessage(message));
    }

    private IEnumerator SendInfoMessage(string text)
    {
        TextInfo.text = text;
        yield return new WaitForSeconds(3);
        //TextInfo.text = "";
    }
}
