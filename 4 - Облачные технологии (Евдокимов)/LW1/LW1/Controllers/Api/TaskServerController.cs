﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LW1.Data.Repositories;
using LW1.EditModels;
using LW1.Models.Project;
using Microsoft.AspNetCore.Mvc;

namespace LW1.Controllers.Api
{
    [Route("api/[controller]")]
    public class TaskServerController : Controller
    {
        private readonly IDomainRepositoryFactory repositoryFactory;

        public TaskServerController(IDomainRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        [HttpPost]
        [Route("SendResult")]
        public IActionResult SendResult([FromBody]IEnumerable<KeyWordResultEditModel> keyWordResult)
        {
            var keyWordRepos = repositoryFactory.GetEntityRepository<KeyWord>();
            var projRepos = repositoryFactory.GetProjectRepository();
            var project = projRepos.GetById(keyWordRepos.GetById(keyWordResult.FirstOrDefault().ServerKeywordId).ProjectId);

            project.AddNewSelection(keyWordResult);

            projRepos.Update(project);
            return Ok();
        }
    }
}
