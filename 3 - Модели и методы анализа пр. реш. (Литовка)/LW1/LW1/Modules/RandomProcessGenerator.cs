﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Modules
{
    public class RandomProcessGenerator
    {
        private static double CongruentialMethod(double x)
        {
            double m = Math.Pow(3, 18);
            double a = Math.Pow(5, 11);

            x = (a * x) % m;
            return (x / m);
        }

        public static double[] Random(int count = 1)
        {
            var result = new double[count];

            result[0] = CongruentialMethod(DateTime.Now.Ticks);
            for (var i = 1; i < count; i++)
            {
                result[i] = CongruentialMethod(DateTime.Now.Ticks * result[i - 1]);
            }
            for (var i = 0; i < count; i++) { result[i] -= 0.5; }

            return result;
        }

        public static double M(double[] arr)
        {
            var summ = arr.Sum();
            return (summ / arr.Count());
        }

        public static double Dispersion(double[] arr, double M)
        {
            var summ = 0.0;
            for (var i = 0; i < arr.Count(); i++) { summ += Math.Pow((arr[i] - M), 2); }
            return (summ / arr.Count());
        }

        public static double[] Filter(double[] arr, double disp_x)
        {
            var Ns = 10;
            //var M_0 = 14.4;
            //var disp_0 = 6;
            //var alpha_0 = 0.14;
            var M_0 = 42;
            var disp_0 = 26;
            var alpha_0 = 0.12;
            var result = new double[arr.Count() - Ns];

            for (var k = 0; k < arr.Count() - Ns; k++)
            {
                var tmp = 0.0;
                for (var i = k; i < k + Ns; i++)
                {
                    var arrVal = arr[i];
                    var tmp1 = Math.Sqrt(disp_0 / (alpha_0 * disp_x));
                    var tmp2 = Math.Pow(Math.E, -alpha_0 * (i - k));

                    tmp += arrVal * tmp1 * tmp2;
                }
                result[k] = (tmp / Ns) + M_0;
            }

            return result;
        }
    }
}
