﻿using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using EntityAbstractions.Implementations;
using System;

namespace Domain.Models.Entities.FuzzyStatement.Variable
{
    public abstract class BaseVariable : BaseEntity, IVariable
    {
        public abstract string CodeName { get; }
        public abstract string Name { get; }
        public abstract VarType Type { get; set; }
        public abstract ModType Mod { get; set; }

        public double Value { get; set; }

        /// <summary> Малое терм-множество </summary>
        public abstract double CalcSmallTermSet(double value);   
        /// <summary> Среднее терм-множество </summary>  
        public abstract double CalcAverageTermSet(double value);
        /// <summary> Большое терм-множество </summary>
        public abstract double CalcLargeTermSet(double value);
        
        public double CalcTermSet(double value)
        {
            switch (Type)
            {
                case VarType.Small: return CalcSmallTermSet(value);
                case VarType.Average: return CalcAverageTermSet(value);
                case VarType.Large: return CalcLargeTermSet(value);
                default: return 0;
            }
        }

        protected double SetMode(double value)
        {
            switch (Mod)
            {
                case ModType.Con: return Math.Pow(value, 2.0);
                case ModType.Dil: return Math.Pow(value, 0.5);
                default: return value;
            }
        }
    }
}
