﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSize : MonoBehaviour
{
    private static Transform cameraTransform;

    private static float xPos => cameraTransform.position.x;
    private static float yPos => cameraTransform.position.y;

    private static float screenHeight => Camera.main.pixelHeight;
    private static float screenWidht => Camera.main.pixelWidth;
    private static float size => Camera.main.orthographicSize;

    public static float yMax => Camera.main.orthographicSize;
    public static float xMax => (screenWidht / unitSize) / 2;

    public static float yMaxWorld => yMax + yPos;
    public static float xMaxWorld => xMax + xPos;
    public static float yMinWorld => yPos - yMax;
    public static float xMinWorld => xPos - xMax;

    private static float unitSize => screenHeight / (size * 2);

    private void Awake()
    {
        cameraTransform = Camera.main.transform;
    }

    private void Update()
    {
        Camera.main.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * 2;
    }

    public static Vector3 GetMousePosition()
    {
        var shiftX = Input.mousePosition.x / unitSize;
        var shiftY = Input.mousePosition.y / unitSize;
        return new Vector3(shiftX + xMinWorld, shiftY + yMinWorld);
    }
}
