﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.NeuronNet.Layers
{
    public class OutputLayer : Layer
    {
        public OutputLayer(int numNeuron, int numPrevNeuron, NeuronType type, string memoryName, bool addShift = false) : base(numNeuron, numPrevNeuron, type, memoryName, addShift) { }

        public override void Recognize(NNet net, Layer nextLayer)
        {
            net.Fact = Neurons.Select(x => x.Output).ToList();
        }

        public override List<double> BackwardPass(List<double> errors)
        {
            var gradientSum = new List<double>();

            // находим суммы градиентов
            for (var j = 0; j < NumPrevNeuron; j++)
            {
                var sum = 0.0;

                for (var k = 0; k < Neurons.Count; k++) sum += Neurons[k].Weights[j] * Neurons[k].Gradientor(errors[k], Neurons[k].OutputDerivative(Neurons[k].Output), 0);

                gradientSum.Add(sum);
            }

            // корректируем весовые коеффициенты
            for (var i = 0; i < NumNeuron; i++)
            {
                for (var n = 0; n < NumPrevNeuron; n++)
                {
                    var inputValue = AddShift && n == NumPrevNeuron - 1 ? 1 : Neurons[i].Inputs[n];

                    // коррекция весовых коэффициентов
                    Neurons[i].Weights[n] += LearningRate * inputValue * Neurons[i].Gradientor(errors[i], Neurons[i].OutputDerivative(Neurons[i].Output), 0);
                }
            }

            return gradientSum;
        }
    }
}
