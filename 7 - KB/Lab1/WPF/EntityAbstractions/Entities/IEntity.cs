﻿
namespace EntityAbstractions.Entities
{
    public interface IEntity<TId>
    {
        TId Id { get; }
    }
}
