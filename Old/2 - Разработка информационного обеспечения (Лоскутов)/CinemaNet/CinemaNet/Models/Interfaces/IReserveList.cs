﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface IReserveList
    {
        IEnumerable<Reserve> ReserveList { get; }
        void AddReserve(Reserve reserve);
        IEnumerable<Reserve> GetReserveByEmail(string email);
    }
}
