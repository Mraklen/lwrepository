﻿using QueryAbstractions.Filter;
using System.Collections.Generic;

namespace QueryAbstractions.TableModel
{
    public abstract class AbstractTableModel<TRead, TView>
        where TRead : class
        where TView : class
    {
        public abstract IEnumerable<TView> Data { get; set; }
        public abstract FilterModel<TRead, TView> FilterModel { get; set; }
    }
}
