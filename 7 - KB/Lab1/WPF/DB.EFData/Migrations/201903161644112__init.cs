namespace DB.EFData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.Parameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Question = c.String(),
                        Description = c.String(),
                        VariableId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Variables", t => t.VariableId, cascadeDelete: true)
                .Index(t => t.VariableId);
            
            CreateTable(
                "public.RuleParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParamType = c.Int(nullable: false),
                        State = c.Boolean(),
                        RuleId = c.Int(nullable: false),
                        ParameterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Rules", t => t.RuleId, cascadeDelete: true)
                .ForeignKey("public.Parameters", t => t.ParameterId, cascadeDelete: true)
                .Index(t => t.RuleId)
                .Index(t => t.ParameterId);
            
            CreateTable(
                "public.Rules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.Variables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CodeName = c.String(),
                        VarType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.Parameters", "VariableId", "public.Variables");
            DropForeignKey("public.RuleParameters", "ParameterId", "public.Parameters");
            DropForeignKey("public.RuleParameters", "RuleId", "public.Rules");
            DropIndex("public.RuleParameters", new[] { "ParameterId" });
            DropIndex("public.RuleParameters", new[] { "RuleId" });
            DropIndex("public.Parameters", new[] { "VariableId" });
            DropTable("public.Variables");
            DropTable("public.Rules");
            DropTable("public.RuleParameters");
            DropTable("public.Parameters");
        }
    }
}
