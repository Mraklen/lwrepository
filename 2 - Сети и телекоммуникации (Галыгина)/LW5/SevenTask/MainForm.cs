﻿using Newtonsoft.Json;
using SeatBattle.CSharp.EventManager;
using SeatBattle.CSharp.Net;
using SeatBattle.CSharp.SeaBattleGame;
using SeatBattle.Model;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeatBattle.CSharp
{
    public class MainForm : Form
    {
        NetworkManager NetworkManager;

        private readonly Player _thisPlayer;
        //private readonly Player _computerPlayer;
        private readonly Player _netEnemyPlayer;

        private readonly Board _thisPlayerBoard;
        private readonly Board _newEnemyBoard;

        private readonly GameController _controller;

        private readonly ScoreBoard _scoreboard;

        private readonly Button _shuffleButton;
        private readonly Button _startGameButton;
        private readonly Button _newGameButton;

        private static readonly Color ButtonBackColor = Color.FromArgb(65, 133, 243);
        private const char ShuffleCharacter = (char)0x60;
        private const char StartGameCharacter = (char)0x55;
        private const char NewGameCharacter = (char)0x6C;

        public MainForm()
        {
            GlobalEventManager.Instance.AddEventListener(EVENT_TYPE.EnemyReadyPlay, OnEvent);

            NetworkManager = new NetworkManager();
            NetworkManager.StartNewServer();

            SuspendLayout();

            _thisPlayerBoard = new Board();
            _newEnemyBoard = new Board(false);

            _thisPlayer = new HumanPlayer($"{Server.myPort}", _newEnemyBoard);
            _netEnemyPlayer = new HumanPlayer($"{Server.enemyPort}", _thisPlayerBoard);
            //_computerPlayer = new ComputerPlayer($"{Server.enemyPort}");
            //_netEnemyPlayer = new NetPlayer($"{Server.enemyPort}");

            _scoreboard = new ScoreBoard(_thisPlayer, _netEnemyPlayer, 10, 100);
            _controller = new GameController(_thisPlayer, _netEnemyPlayer, _thisPlayerBoard, _newEnemyBoard, _scoreboard);

            _shuffleButton = CreateButton(ShuffleCharacter.ToString(), ButtonBackColor);
            _newGameButton = CreateButton(NewGameCharacter.ToString(), ButtonBackColor);
            _startGameButton = CreateButton(StartGameCharacter.ToString(), ButtonBackColor);

            SetupWindow();
            LayoutControls();

            _scoreboard.GameEnded += OnGameEnded;

            _shuffleButton.Click += OnShuffleButtonClick;
            _startGameButton.Click += OnStartGameButtonClick;
            _newGameButton.Click += OnNewGameButtonClick;

            ResumeLayout();

            StartNewGame();
        }

        private void OnNewGameButtonClick(object sender, System.EventArgs e)
        {
            StartNewGame();
        }


        private void StartNewGame()
        {
            _shuffleButton.Visible = true;
            _startGameButton.Visible = true;
            _newGameButton.Visible = false;
            _controller.NewGame();
        }


        private void OnStartGameButtonClick(object sender, System.EventArgs e)
        {
            _shuffleButton.Visible = false;
            _newGameButton.Visible = false;
            _startGameButton.Visible = false;

            Task.Run(() =>
            {
                _thisPlayer.SetReady(true);
                NetworkManager.PostMessageToServer(new PostPackage() {
                    Type = PackageType.ReadyPlay,
                    Content = JsonConvert.SerializeObject(_thisPlayerBoard.GetShips())
                });
                do
                {
                    Task.Delay(5000);
                }
                while (!_netEnemyPlayer.isReady);

                _controller.StartGame();
            });
        }

        private void OnShuffleButtonClick(object sender, System.EventArgs e)
        {
            _thisPlayerBoard.AddRandomShips();
        }

        private void OnGameEnded(object sender, System.EventArgs e)
        {
            _shuffleButton.Visible = false;
            _startGameButton.Visible = false;
            _newGameButton.Visible = true;
            _newEnemyBoard.ShowShips();
        }

        private void SetupWindow()
        {
            AutoScaleDimensions = new SizeF(8, 19);
            AutoScaleMode = AutoScaleMode.Font;
            Font = new Font("Calibri", 10, FontStyle.Regular, GraphicsUnit.Point, 186);
            Margin = Padding.Empty;
            Text = "SeaBattle.CSharp";
            BackColor = Color.FromArgb(235, 235, 235);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            StartPosition = FormStartPosition.CenterScreen;
            MaximizeBox = false;
        }

        private static Button CreateButton(string text, Color backColor)
        {
            var button = new Button
            {
                FlatStyle = FlatStyle.Flat,
                ForeColor = Color.White,
                BackColor = backColor,
                UseVisualStyleBackColor = false,
                Size = new Size(40, 40),
                Text = text,
                Font = new Font("Webdings", 24, FontStyle.Regular, GraphicsUnit.Point),
                TextAlign = ContentAlignment.TopCenter,
            };
            button.FlatAppearance.BorderSize = 0;

            return button;
        }

        private void LayoutControls()
        {
            _thisPlayerBoard.Location = new Point(0, 0);
            _newEnemyBoard.Location = new Point(_thisPlayerBoard.Right, 0);
            _scoreboard.Location = new Point(25, _thisPlayerBoard.Bottom);
            _scoreboard.Width = _newEnemyBoard.Right - 25;
            _newGameButton.Location = new Point(_newEnemyBoard.Right - _newGameButton.Width, _scoreboard.Bottom);
            _startGameButton.Location = _newGameButton.Location;
            _shuffleButton.Location = new Point(_newGameButton.Location.X - _shuffleButton.Width - 25, _newGameButton.Location.Y);

            Controls.AddRange(new Control[]
                                  {
                                      _thisPlayerBoard,
                                      _newEnemyBoard,
                                      _scoreboard,
                                      _newGameButton,
                                      _startGameButton,
                                      _shuffleButton
                                  });

            ClientSize = new Size(_newEnemyBoard.Right + 25, _startGameButton.Bottom + 25);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(475, 302);
            this.Name = "MainForm";
            this.ResumeLayout(false);

        }


        private void OnEvent(EVENT_TYPE Event_Type, object Param)
        {
            switch (Event_Type)
            {
                case EVENT_TYPE.EnemyReadyPlay:
                    _newEnemyBoard.SetShips((List<Ship>)Param);
                    _netEnemyPlayer.SetReady(true);
                    break;
            }
        }
    }
}
