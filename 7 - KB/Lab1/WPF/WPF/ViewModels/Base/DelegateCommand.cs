﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WPF.ViewModels.Base
{
    class DelegateCommand : ICommand
    {
        private readonly Func<Task> task;
        private readonly Action action;
        private readonly Action<object> action1;
        private bool isEnabled;

        public DelegateCommand(Func<Task> task)
        {
            this.task = task;
            isEnabled = true;
        }

        public DelegateCommand(Action action)
        {
            this.action = action;
            isEnabled = true;
        }

        public DelegateCommand(Action<object> action1)
        {
            this.action1 = action1;
            isEnabled = true;
        }

        public void Execute(object parameter)
        {
            if (task != null) Task.Factory.StartNew(task);
            action?.Invoke();
            action1?.Invoke(parameter);
        }

        public bool CanExecute(object parameter)
        {
            return isEnabled;
        }

        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnCanExecuteChanged();
                }
            }
        }

        public event EventHandler CanExecuteChanged;

        protected virtual void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
