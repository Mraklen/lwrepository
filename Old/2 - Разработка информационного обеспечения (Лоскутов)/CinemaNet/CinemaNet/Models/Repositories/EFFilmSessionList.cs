﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFFilmSessionList : IFilmSessionList
    {
        private ApplicationDbContext context;
        public EFFilmSessionList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<FilmSession> GetFilmSession(int cinemaID, int filmID)
        {
            return context.FilmSessions
                .Where(cf => cf.CinemaID == cinemaID && cf.FilmID == filmID)
                .Include(c => c.Cinema).Include(f => f.Film)
                .ToList();
        }

        public IEnumerable<FilmSession> FilmSessionLists() => context.FilmSessions
            .Include(fs => fs.CinemaRoom)
            .Include(fs => fs.Film)
            .Include(fs => fs.Cinema);

        public IEnumerable<FilmSession> GetFilmSessionByIds(List<int> filmSessionIds) => context.FilmSessions
            .Where(x => filmSessionIds.Contains(x.FilmSessionID))
            .Include(fs => fs.CinemaRoom)
            .Include(fs => fs.Film)
            .Include(fs => fs.Cinema);
    }
}
