﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.ViewModels
{
    public class SelectionUrlViewModel
    {
        public int Id { get; set; }
        public int KeyWordId { get; set; }
        public int Order { get; set; }
        public string Value { get; set; }
        public int? Offset { get; set; }
        public bool? IsUp { get; set; }
        public bool Marked { get; set; }
    }
}
