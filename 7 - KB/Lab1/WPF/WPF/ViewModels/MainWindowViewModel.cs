﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Unity;
using WPF.Events;
using WPF.Modules.Base;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels
{
    class MainWindowViewModel : BaseViewModel
    {
        Dictionary<ModuleType, IModule> Modules { get; set; }

        private ModuleType currentModule;
        public ModuleType CurrentModule
        {
            get => currentModule;
            set
            {
                if (value != currentModule)
                {
                    SelectedModule?.Deactivate();
                    currentModule = value;
                    OnPropertyChanged("SelectedModule");
                    OnPropertyChanged("UserInterface");

                    OnPropertyChanged("Title");
                    OnPropertyChanged("Scale");
                    OnPropertyChanged("SizeToContent");
                    OnPropertyChanged("WindowState");
                    OnPropertyChanged("ShowInTaskbar");
                    OnPropertyChanged("WindowStyle");
                }
            }
        }
        
        public IModule SelectedModule => Modules[currentModule];
        
        public UserControl UserInterface => SelectedModule.UserInterface;
        
        public string Title => SelectedModule.Name;
        public double Scale => SelectedModule.WindowSetting.Scale;
        public SizeToContent SizeToContent => SelectedModule.WindowSetting.SizeToContent;
        public WindowState WindowState => SelectedModule.WindowSetting.WindowState;
        public bool ShowInTaskbar => SelectedModule.WindowSetting.ShowInTaskbar;
        public WindowStyle WindowStyle => SelectedModule.WindowSetting.WindowStyle;

        public MainWindowViewModel(AppStateStore stateStore, IUnityContainer container) : base(stateStore, container)
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<ChangePageEvent>>().Subscribe(ChangePage);
        }

        private void ChangePage(ChangePageEvent arg)
        {
            CurrentModule = arg.ModuleType;
        }

        public void UpdateModules(Dictionary<ModuleType, IModule> modules, ModuleType firstModule)
        {
            Modules = modules;
            CurrentModule = firstModule;
        }
    }
}
