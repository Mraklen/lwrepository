﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class Film
    {
        public int FilmID { get; set; }
        public string Name { get; set; }
        public string Production { get; set; }
        public string Premiere { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Duration { get; set; }
        public string Story { get; set; }
        public string Image { get; set; }
        public List<CinemaFilms> CinemaFilms { get; set; }

        public Film()
        {
            CinemaFilms = new List<CinemaFilms>();
        }

        public List<FilmSession> FilmSessions { get; set; }
    }
}
