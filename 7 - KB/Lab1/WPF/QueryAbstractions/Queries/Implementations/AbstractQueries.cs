﻿using QueryAbstractions.Filter;
using QueryAbstractions.ReadModels;
using QueryAbstractions.TableModel;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace QueryAbstractions.Queries.Implementations
{
    public abstract class AbstractQueries<TEntity, TView, TID> : IBaseQueries<TEntity, TView, TID>
            where TEntity : BaseReadModel<TID>
            where TView : BaseViewModel<TID>
            where TID : struct, IEquatable<TID>
    {
        protected readonly IQueryDatabase db;

        public AbstractQueries(IQueryDatabase db) { this.db = db; }

        protected abstract Expression<Func<TEntity, TView>> Projection { get; }

        protected virtual IQueryable<TEntity> Queryable => db.GetQueryableForEntities<TEntity, TView, TID>();

        public async virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await Queryable.ToListAsync();
        }

        public async Task<TEntity> GetById(TID id)
        {
            return await Queryable.FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<int> GetCount(FilterModel<TEntity, TView> filter)
        {
            return await Queryable.ApplyFilter(filter).CountAsync();
        }

        public virtual async Task<IEnumerable<TEntity>> GetList(FilterModel<TEntity, TView> filter, bool withPaging = true)
        {
            return await GetListQuery(filter, withPaging).ToListAsync();
        }

        public virtual IQueryable<TEntity> GetListQuery(FilterModel<TEntity, TView> filter, bool withPaging = true)
        {
            var query = Queryable.ApplyFilter(filter).ApplySorting(filter);

            if (withPaging) query = query.ApplyPaging(filter);

            return query;
        }

        protected virtual IQueryable<TEntity> ApplyFilterToQuery(IQueryable<TEntity> queryable, FilterModel<TEntity, TView> filter, bool withPaging = true)
        {
            var query = queryable.ApplyFilter(filter).ApplySorting(filter);

            if (withPaging) query = query.ApplyPaging(filter);

            return query;
        }

        public virtual async Task<TableAnswer<TView>> GetTableList(FilterModel<TEntity, TView> filter, Expression<Func<TEntity, TView>> newProjection = null)
        {
            var result = new BaseTableModel<TEntity, TView> { FilterModel = filter };

            if (filter != null) result.FilterModel.TotalItems = await GetCount(filter);

            var query = GetListQuery(filter);

            if (newProjection != null)
                result.Data = await query.Select(newProjection).ToListAsync();
            else if (Projection != null)
                result.Data = await query.Select(Projection).ToListAsync();

            return new TableAnswer<TView> { Data = result.Data, FilterModel = new FilterModel { TotalItems = result.FilterModel.TotalItems } };
        }
    }
}
