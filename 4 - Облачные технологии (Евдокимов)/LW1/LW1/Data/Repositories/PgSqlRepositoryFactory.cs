﻿using LW1.Data.Repositories.Implements;
using LW1.Data.Repositories.Interfaces;
using LW1.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    public class PgSqlRepositoryFactory : IDomainRepositoryFactory
    {
        ApplicationDbContext context;

        public PgSqlRepositoryFactory(IConfiguration configuration)
        {
            var connectionString = configuration["Database:WriteDbConnectionString"];
            context = new ApplicationDbContext(connectionString);
        }
        
        public IBaseAggregateRepository<TAggregate> GetAggregateRepository<TAggregate>() where TAggregate : class, IAggregate 
            => new DefaultAggregateRepository<TAggregate>(context);

        public IBaseRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class, IEntity<int>
            => new DefaultEntityRepository<TEntity>(context);

        public IProjectRepository GetProjectRepository()
            => new ProjectRepository(context);
    }
}
