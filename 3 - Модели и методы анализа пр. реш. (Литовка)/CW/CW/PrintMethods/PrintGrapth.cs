﻿using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace LW1.PrintMethods
{
    public class PrintGrapth
    {
        public static void PrintLineGrapth(IEnumerable<Point> points, ChartPlotter chartPlotter, SolidColorBrush color, double penSize = 4, double pointSize = 4) 
        {
            if (points == null) return;
            chartPlotter.AddLineGraph(CreateDataSource(points), new Pen(color, penSize), new CirclePointMarker() { Size = pointSize }, new PenDescription(" "));

            chartPlotter.LegendVisible = false;
            chartPlotter.FitToView();
        }

        public static void PrintPointGrapth(Point point, ChartPlotter chartPlotter)
        {
            chartPlotter.Legend.Visibility = Visibility.Collapsed;
            if (point == null) return;
            var marker = new CirclePointMarker
            {
                Pen = new Pen(Brushes.Black, 8)
            };
            chartPlotter.AddLineGraph(
                CreateDataSource(point),
                new Pen(),
                marker,
                new PenDescription(" "));
        }

        public static void ClearPlotter(ChartPlotter plotter)
        {
            plotter.Children.RemoveAll(typeof(LineGraph));
            plotter.Children.RemoveAll(typeof(MarkerPointsGraph));
        }

        private static EnumerableDataSource<Point> CreateDataSource(IEnumerable<Point> rates)
        {
            EnumerableDataSource<Point> ds = new EnumerableDataSource<Point>(rates);
            ds.SetXMapping(x => x.X);
            ds.SetYMapping(y => y.Y);
            return ds;
        }

        private static EnumerableDataSource<Point> CreateDataSource(Point rates)
        {
            List<Point> points = new List<Point>() { rates };
            EnumerableDataSource<Point> ds = new EnumerableDataSource<Point>(points);
            ds.SetXMapping(x => x.X);
            ds.SetYMapping(y => y.Y);
            return ds;
        }

        public static List<Point> ConvertToPoints(double[] arr)
        {
            var result = new List<Point>();
            for(var i = 0; i < arr.Count(); i++)
            {
                result.Add(new Point() {
                    X = i,
                    Y = arr[i]
                });
            }
            return result;
        }
    }
}
