﻿/*
 * Система событий
 * 
 */
using System.Collections.Generic;
using UnityEngine;

#region Список позможных событий
/// <summary>
/// Список событий.
/// </summary>
public enum EventType
{
    AddEntity,
    RemoveEntity,
    PointCountChange,
    ChangeMethod,
    SupportLineShowChange,
    ShowOnlyCalcLine,
    LastPointChange,
    ChangeMethodType,
    SlowMotionRender,
    CameraProjectionChange,
    AxisMoveShow,
    ChangeQuality,
    CreateModelMod,
    RenderEvent,
    RotateAngleChange,
    ModelModChange,
    QualityChanged
};
#endregion

/// <summary>
/// Глабальная система событий
/// </summary>
public class GlobalEventSystem : Singleton<GlobalEventSystem>
{
    public delegate void OnEvent(EventType eventType, Component component, object param = null);
    private Dictionary<EventType, List<OnEvent>> Listeners = new Dictionary<EventType, List<OnEvent>>();
    
    #region Методы
    /// <summary>
    /// Регистрация нового получателя
    /// </summary>
    /// <param name="eventType">Тип события.</param>
    /// <param name="Listener">Новый слушатель.</param>
    public void AddEventListener(EventType eventType, OnEvent Listener)
    {
        List<OnEvent> ListenList = null;

        if (Listeners.TryGetValue(eventType, out ListenList))
        {
            ListenList.Add(Listener);
            return;
        }

        ListenList = new List<OnEvent>
        {
            Listener
        };
        Listeners.Add(eventType, ListenList);
    }

    /// <summary>
    /// Рассылка сообщения подписчикам события.
    /// </summary>
    /// <param name="eventType">Тип рассылаемого события.</param>
    /// <param name="component">Компонент - инициатор события.</param>
    /// <param name="param">Необязательный аргумент (отсылаемое значение).</param>
    public void PostEventMessage(EventType eventType, Component component, object param = null)
    {
        List<OnEvent> ListenList = null;

        if (!Listeners.TryGetValue(eventType, out ListenList)) return;

        ListenList.ForEach(listen =>
        {
            if (!listen.Equals(null))
                listen(eventType, component, param);
        });
    }

    /// <summary>
    /// Удаление события из словаря и всех его подписчиков.
    /// </summary>
    /// <param name="eventType">Тип удаляемого события.</param>
    public void RemoveEvent(EventType eventType)
    {
        Listeners.Remove(eventType);
    }

    /// <summary>
    /// Очистка словаря от всех пустых ссылок.
    /// </summary>
    public void RemoveRedundancies()
    {
        Dictionary<EventType, List<OnEvent>> TmpListeners = new Dictionary<EventType, List<OnEvent>>();
        
        foreach (KeyValuePair<EventType, List<OnEvent>> Item in Listeners)
        {
            for (int i = Item.Value.Count - 1; i >= 0; i--)
            {
                if (Item.Value[i].Equals(null))
                    Item.Value.RemoveAt(i);
            }
            
            if (Item.Value.Count > 0)
            {
                TmpListeners.Add(Item.Key, Item.Value);
            }
        }
        
        Listeners = TmpListeners;
    }

    /// <summary>
    /// Вызывается при перезагрузке сцены.
    /// </summary>
    void OnLevelWasLoaded()
    {
        RemoveRedundancies();
    }

    public override void AwakeInit() { }
    public override void StartInit() { }
    public override void UpdateInit() { }
    #endregion
}