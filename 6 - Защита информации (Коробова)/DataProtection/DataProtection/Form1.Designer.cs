﻿namespace DataProtection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCombined = new System.Windows.Forms.TabPage();
            this.tabVigenener = new System.Windows.Forms.TabPage();
            this.gammaNRadio = new System.Windows.Forms.RadioButton();
            this.transparentRadio = new System.Windows.Forms.RadioButton();
            this.vigenenerRadio = new System.Windows.Forms.RadioButton();
            this.decryptBtn_Vigenener = new System.Windows.Forms.Button();
            this.encryptBtn_Vigenener = new System.Windows.Forms.Button();
            this.keyText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.seedText = new System.Windows.Forms.TextBox();
            this.seedLb = new System.Windows.Forms.Label();
            this.ADFGVXRadio = new System.Windows.Forms.RadioButton();
            this.tabVigenener.SuspendLayout();
            this.tabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCombined
            // 
            this.tabCombined.Location = new System.Drawing.Point(4, 22);
            this.tabCombined.Name = "tabCombined";
            this.tabCombined.Padding = new System.Windows.Forms.Padding(3);
            this.tabCombined.Size = new System.Drawing.Size(458, 259);
            this.tabCombined.TabIndex = 4;
            this.tabCombined.Text = "Шифр ADFGVX";
            this.tabCombined.UseVisualStyleBackColor = true;
            // 
            // tabVigenener
            // 
            this.tabVigenener.Controls.Add(this.ADFGVXRadio);
            this.tabVigenener.Controls.Add(this.seedText);
            this.tabVigenener.Controls.Add(this.seedLb);
            this.tabVigenener.Controls.Add(this.gammaNRadio);
            this.tabVigenener.Controls.Add(this.transparentRadio);
            this.tabVigenener.Controls.Add(this.vigenenerRadio);
            this.tabVigenener.Controls.Add(this.decryptBtn_Vigenener);
            this.tabVigenener.Controls.Add(this.encryptBtn_Vigenener);
            this.tabVigenener.Controls.Add(this.keyText);
            this.tabVigenener.Controls.Add(this.label1);
            this.tabVigenener.Location = new System.Drawing.Point(4, 22);
            this.tabVigenener.Margin = new System.Windows.Forms.Padding(0);
            this.tabVigenener.Name = "tabVigenener";
            this.tabVigenener.Size = new System.Drawing.Size(458, 289);
            this.tabVigenener.TabIndex = 1;
            this.tabVigenener.Text = "Шифр Виженера";
            this.tabVigenener.UseVisualStyleBackColor = true;
            // 
            // gammaNRadio
            // 
            this.gammaNRadio.AutoSize = true;
            this.gammaNRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gammaNRadio.Location = new System.Drawing.Point(22, 48);
            this.gammaNRadio.Name = "gammaNRadio";
            this.gammaNRadio.Size = new System.Drawing.Size(282, 28);
            this.gammaNRadio.TabIndex = 6;
            this.gammaNRadio.Text = "Гаммирование по модулю N";
            this.gammaNRadio.UseVisualStyleBackColor = true;
            this.gammaNRadio.CheckedChanged += new System.EventHandler(this.gammaNRadio_CheckedChanged);
            // 
            // transparentRadio
            // 
            this.transparentRadio.AutoSize = true;
            this.transparentRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.transparentRadio.Location = new System.Drawing.Point(202, 14);
            this.transparentRadio.Name = "transparentRadio";
            this.transparentRadio.Size = new System.Drawing.Size(218, 28);
            this.transparentRadio.TabIndex = 5;
            this.transparentRadio.Text = "Поворотная решетка";
            this.transparentRadio.UseVisualStyleBackColor = true;
            // 
            // vigenenerRadio
            // 
            this.vigenenerRadio.AutoSize = true;
            this.vigenenerRadio.Checked = true;
            this.vigenenerRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vigenenerRadio.Location = new System.Drawing.Point(22, 14);
            this.vigenenerRadio.Name = "vigenenerRadio";
            this.vigenenerRadio.Size = new System.Drawing.Size(174, 28);
            this.vigenenerRadio.TabIndex = 4;
            this.vigenenerRadio.TabStop = true;
            this.vigenenerRadio.Text = "Шифр Виженера";
            this.vigenenerRadio.UseVisualStyleBackColor = true;
            // 
            // decryptBtn_Vigenener
            // 
            this.decryptBtn_Vigenener.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.decryptBtn_Vigenener.Location = new System.Drawing.Point(228, 238);
            this.decryptBtn_Vigenener.Name = "decryptBtn_Vigenener";
            this.decryptBtn_Vigenener.Size = new System.Drawing.Size(192, 43);
            this.decryptBtn_Vigenener.TabIndex = 3;
            this.decryptBtn_Vigenener.Text = "Расшифровать";
            this.decryptBtn_Vigenener.UseVisualStyleBackColor = true;
            this.decryptBtn_Vigenener.Click += new System.EventHandler(this.decryptBtn_Click);
            // 
            // encryptBtn_Vigenener
            // 
            this.encryptBtn_Vigenener.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.encryptBtn_Vigenener.Location = new System.Drawing.Point(22, 238);
            this.encryptBtn_Vigenener.Name = "encryptBtn_Vigenener";
            this.encryptBtn_Vigenener.Size = new System.Drawing.Size(192, 43);
            this.encryptBtn_Vigenener.TabIndex = 2;
            this.encryptBtn_Vigenener.Text = "Зашифровать";
            this.encryptBtn_Vigenener.UseVisualStyleBackColor = true;
            this.encryptBtn_Vigenener.Click += new System.EventHandler(this.encryptBtn_Click);
            // 
            // vigenenerKey
            // 
            this.keyText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.keyText.Location = new System.Drawing.Point(165, 116);
            this.keyText.Name = "vigenenerKey";
            this.keyText.Size = new System.Drawing.Size(198, 29);
            this.keyText.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(98, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ключ:";
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabVigenener);
            this.tabs.Controls.Add(this.tabCombined);
            this.tabs.Location = new System.Drawing.Point(-3, -1);
            this.tabs.Name = "tabs";
            this.tabs.Padding = new System.Drawing.Point(0, 0);
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(466, 315);
            this.tabs.TabIndex = 0;
            // 
            // seedText
            // 
            this.seedText.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.seedText.Location = new System.Drawing.Point(165, 151);
            this.seedText.Name = "seedText";
            this.seedText.Size = new System.Drawing.Size(198, 29);
            this.seedText.TabIndex = 8;
            this.seedText.Visible = false;
            // 
            // seedLb
            // 
            this.seedLb.AutoSize = true;
            this.seedLb.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.seedLb.Location = new System.Drawing.Point(98, 154);
            this.seedLb.Name = "seedLb";
            this.seedLb.Size = new System.Drawing.Size(60, 24);
            this.seedLb.TabIndex = 7;
            this.seedLb.Text = "Seed:";
            this.seedLb.Visible = false;
            // 
            // ADFGVXRadio
            // 
            this.ADFGVXRadio.AutoSize = true;
            this.ADFGVXRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ADFGVXRadio.Location = new System.Drawing.Point(310, 48);
            this.ADFGVXRadio.Name = "ADFGVXRadio";
            this.ADFGVXRadio.Size = new System.Drawing.Size(107, 28);
            this.ADFGVXRadio.TabIndex = 9;
            this.ADFGVXRadio.Text = "ADFGVX";
            this.ADFGVXRadio.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 314);
            this.Controls.Add(this.tabs);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabVigenener.ResumeLayout(false);
            this.tabVigenener.PerformLayout();
            this.tabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabCombined;
        private System.Windows.Forms.TabPage tabVigenener;
        private System.Windows.Forms.RadioButton transparentRadio;
        private System.Windows.Forms.RadioButton vigenenerRadio;
        private System.Windows.Forms.Button decryptBtn_Vigenener;
        private System.Windows.Forms.Button encryptBtn_Vigenener;
        private System.Windows.Forms.TextBox keyText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.RadioButton gammaNRadio;
        private System.Windows.Forms.TextBox seedText;
        private System.Windows.Forms.Label seedLb;
        private System.Windows.Forms.RadioButton ADFGVXRadio;
    }
}

