﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Windows.Controls;
using System.Threading.Tasks;
using Microsoft.Research.DynamicDataDisplay.DataSources.MultiDimensional;
using Microsoft.Research.DynamicDataDisplay.Common.Auxiliary;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System.Reflection;
using System.Windows.Media;
using LW2.Models;

namespace LW2.Services
{
    public class Methods
    {
        public FunctionParam Function { get; set; }
        int maxSleepValue = 500;
        int minSleepValue = 1;

        public void SetFunction(double x1Value, double x2Value, double hValue, double sValue)
        {
            Function = new FunctionParam(x1Value, x2Value, hValue, sValue);
        }

        public async void CalcPowellMethod(PlotterParam plotterParam, Label label_stepValue, Label label_extremum)
        {
            ClearPlotter(plotterParam.Plotter);
            // вывод основного графика
            PrintGraph(plotterParam, Function.GetIsolineData());
            await Task.Delay(maxSleepValue);

            double x1, x2, x,
                 dx, //Шаг поиска для локализации отрезка
                 x1t, x2t, //Временные переменные для предыдущей точки
                 x1n, x2n;  //Координаты нововй точки
            int i = 1, j;

            x1 = FunctionParam.X1Value;
            x2 = FunctionParam.X2Value;
            dx = FunctionParam.HValue;

            PrintPoint(plotterParam, new PointXY() { X1 = x1, X2 = x2 });
            await Task.Delay(maxSleepValue);

            do
            {
                x1t = x1;
                x2t = x2;

                if (Function.CalcFunction(x1, x2) > Function.CalcFunction(x1 + dx, x2))
                {
                    for (x = x1 + dx; Function.CalcFunction(x, x2) > Function.CalcFunction(x + dx, x2); x += dx, i++) ;
                }
                else
                {
                    for (x = x1 - dx; Function.CalcFunction(x, x2) > Function.CalcFunction(x - dx, x2); x -= dx, i++) ;
                }
                if (x1 > x)
                    x = f_opt_x1(x, x1, x2);
                else
                    x = f_opt_x1(x1, x, x2);
                x1 = x;

                var point = new PointXY() { X1 = x1, X2 = x2 };
                PrintPoint(plotterParam, point);
                await Task.Delay(maxSleepValue);

                if (Function.CalcFunction(x1, x2) > Function.CalcFunction(x1, x2 + dx))
                {
                    for (x = x2 + dx; Function.CalcFunction(x1, x) > Function.CalcFunction(x1, x + dx); x += dx, i++) ;
                }
                else
                {
                    for (x = x2 - dx; Function.CalcFunction(x1, x) > Function.CalcFunction(x1, x - dx); x -= dx, i++) ;
                }
                if (x2 > x)
                    x = f_opt_x2(x, x2, x1);
                else
                    x = f_opt_x2(x2, x, x1);
                x2 = x;

                point = new PointXY() { X1 = x1, X2 = x2 };
                PrintPoint(plotterParam, point);
                await Task.Delay(maxSleepValue);

                do
                {
                    x1n = x1 + (x1 - x1t);
                    x2n = x2 + (x2 - x2t);
                    if (Function.CalcFunction(x1n, x2n) < Function.CalcFunction(x1, x2))
                    {
                        x1t = x1;
                        x2t = x2;
                        x1 = x1n;
                        x2 = x2n;
                    }

                    i++;

                    if (label_stepValue != null)
                        label_stepValue.Content = i;
                    point = new PointXY() { X1 = x1n, X2 = x2n };

                    PrintPoint(plotterParam, point);
                    await Task.Delay(maxSleepValue);

                } while (Function.CalcFunction(x1n, x2n) < Function.CalcFunction(x1, x2));
            } while (Math.Sqrt(Math.Pow(x1 - x1t, 2) + Math.Pow(x2 - x2t, 2)) >= FunctionParam.SValue);

            label_extremum.Content = (new PointXY() { X1 = x1, X2 = x2 }).PrintPointXY();
        }

        double f_opt_x1(double a0, double b0, double x)
        {
            double x0, x1, x2, a, b, s, e;

            e = FunctionParam.SValue;
            a = a0;
            b = b0;

            do
            {
                x0 = (a + b) / 2;
                x1 = a + 0.25 * (b - a);
                x2 = b - 0.25 * (b - a);
                if (Function.CalcFunction(x1, x) > Function.CalcFunction(x2, x))
                    a = x1;
                else
                    b = x2;
            }
            while (Math.Abs(a - b) >= e);

            return x0;
        }
        double f_opt_x2(double a0, double b0, double x)
        {
            double x0, x1, x2, a, b, s, e;
            e = FunctionParam.SValue;
            a = a0;
            b = b0;

            do
            {
                x0 = (a + b) / 2;
                x1 = a + 0.25 * (b - a);
                x2 = b - 0.25 * (b - a);

                if (Function.CalcFunction(x, x1) > Function.CalcFunction(x, x2))
                    a = x1;
                else
                    b = x2;
            }
            while (Math.Abs(a - b) >= e);
            return x0;
        }

        public async void CalcSimplexMethod(PlotterParam plotterParam, Label label_stepValue, Label label_extremum)
        {
            ClearPlotter(plotterParam.Plotter);
            // вывод основного графика
            PrintGraph(plotterParam, Function.GetIsolineData());
            await Task.Delay(maxSleepValue);

            PrintPoint(plotterParam, Simplex.PrintPointXY());
            await Task.Delay(maxSleepValue);

            double t, x1t, x2t, dx1, dx2;
            int i = 3, j, k;
            Simplex.f1[0] = Function.CalcFunction(Simplex.x10[0], Simplex.x20[0]);
            Simplex.f1[1] = Function.CalcFunction(Simplex.x10[1], Simplex.x20[1]);
            Simplex.f1[2] = Function.CalcFunction(Simplex.x10[2], Simplex.x20[2]);

            do
            {
                for (j = 1; j <= 2; j++)
                {
                    k = j;
                    while (Simplex.f1.ElementAt(k) > Simplex.f1.ElementAt(k - 1) && k >= 1)
                    {
                        t = Simplex.f1[k - 1];
                        Simplex.f1[k - 1] = Simplex.f1[k];
                        Simplex.f1[k] = t;
                        t = Simplex.x10[k - 1];
                        Simplex.x10[k - 1] = Simplex.x10[k];
                        Simplex.x10[k] = t;
                        t = Simplex.x20[k - 1];
                        Simplex.x20[k - 1] = Simplex.x20[k];
                        Simplex.x20[k] = t;
                        k--;

                        if (!(k >= 1)) break;
                    }
                }

                x1t = Simplex.x10[2] - (Simplex.x10[2] - Simplex.x10[1]) / 2;
                x2t = Simplex.x20[2] - (Simplex.x20[2] - Simplex.x20[1]) / 2;
                dx1 = x1t - Simplex.x10[0];
                dx2 = x2t - Simplex.x20[0];

                if (Function.CalcFunction(x1t + dx1, x2t + dx2) < Simplex.f1[0])
                {
                    if (Function.CalcFunction(x1t + 2 * dx1, x2t + 2 * dx2) < Simplex.f1[0])
                    {
                        Simplex.x10[0] = x1t + 2 * dx1;
                        Simplex.x20[0] = x2t + 2 * dx2;
                    }
                    else
                    {
                        Simplex.x10[0] = x1t + dx1;
                        Simplex.x20[0] = x2t + dx2;
                    }
                }
                else
                {
                    Simplex.x10[0] = x1t - dx1 / 2;
                    Simplex.x20[0] = x2t - dx2 / 2;
                }
                Simplex.f1[0] = Function.CalcFunction(Simplex.x10[0], Simplex.x20[0]);
                i++;

                PrintPoint(plotterParam, Simplex.PrintPointXY());
                await Task.Delay(maxSleepValue);

                if (label_stepValue != null)
                    label_stepValue.Content = i;
                label_extremum.Content = Simplex.PrintMinValue();
            } while (Condition(Simplex.x10[0], Simplex.x20[0], Simplex.x10[1], Simplex.x20[1], Simplex.x10[2], Simplex.x20[2], FunctionParam.SValue));
        }

        bool Condition(double x10, double x20, double x11, double x21, double x12, double x22, double e)
        {
            if (Math.Sqrt((x10 - x11) * (x10 - x11) + (x20 - x21) * (x20 - x21)) +
               Math.Sqrt((x11 - x12) * (x11 - x12) + (x21 - x22) * (x21 - x22)) +
               Math.Sqrt((x10 - x12) * (x10 - x12) + (x20 - x22) * (x20 - x22)) > e)
                return true;
            else
                return false;
        }

        void PrintGraph(PlotterParam plotterParam, IsolineData isolineData)
        {
            WarpedDataSource2D<double> dataSource = new WarpedDataSource2D<double>(isolineData.Data, isolineData.Grid);
            plotterParam.IsolineGraph.DataSource = dataSource;
            plotterParam.IsolineTracking.DataSource = dataSource;

            Rect visible = dataSource.GetGridBounds();
            plotterParam.Plotter.Viewport.Visible = visible;
        }
        void PrintPoint(PlotterParam plotterParam, PointXY point)
        {
            plotterParam.Plotter.Legend.Visibility = Visibility.Collapsed;
            var marker = new CirclePointMarker
            {
                Pen = new Pen(Brushes.Black, 3)
            };
            plotterParam.Plotter.AddLineGraph(
                CreateDataSource(point),
                new Pen(),
                marker,
                new PenDescription(" "));
        }
        void PrintPoint(PlotterParam plotterParam, List<PointXY> points)
        {
            plotterParam.Plotter.Legend.Visibility = Visibility.Collapsed;

            plotterParam.Plotter.AddLineGraph(
                CreateDataSource(points), Colors.Blue, 3);

            plotterParam.Plotter.LegendVisible = false;
            plotterParam.Plotter.FitToView();
        }
        private void ClearPlotter(ChartPlotter plotter)
        {
            plotter.Children.RemoveAll(typeof(LineGraph));
            plotter.Children.RemoveAll(typeof(MarkerPointsGraph));
        }

        private EnumerableDataSource<PointXY> CreateDataSource(List<PointXY> rates)
        {
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(rates);
            ds.SetXMapping(x => x.X1);
            ds.SetYMapping(y => y.X2);
            return ds;
        }
        private EnumerableDataSource<PointXY> CreateDataSource(PointXY rates)
        {
            List<PointXY> points = new List<PointXY>() { rates };
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(points);
            ds.SetXMapping(x => x.X1);
            ds.SetYMapping(y => y.X2);
            return ds;
        }
    }
}
