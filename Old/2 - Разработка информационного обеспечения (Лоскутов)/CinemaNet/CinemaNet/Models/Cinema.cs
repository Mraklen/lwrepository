﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class Cinema
    {
        public int CinemaID { get; set; }
        public string Name { get; set; } // Название кинотеарта
        public string Address { get; set; }
        public List<CinemaFilms> CinemaFilms { get; set; }
        
        public Cinema()
        {
            CinemaFilms = new List<CinemaFilms>();
        }

        public List<FilmSession> FilmSessions { get; set; }
    }
}
