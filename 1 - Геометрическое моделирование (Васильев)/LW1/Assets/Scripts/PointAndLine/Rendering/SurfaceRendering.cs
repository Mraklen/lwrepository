﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.Scripts.Extentions;

public class SurfaceRendering : EntityRender
{
    [SerializeField] List<Points> Surfaces = null;
    [Space(10)]
    [SerializeField] LineRenderer HorizontalLineRenderer;
    [SerializeField] LineRenderer VerticalLineRenderer;
    [SerializeField] LineRenderer MainLineRenderer;

    GameObject PointsPrefab;

    protected override void Init()
    {
        minEntityCount = 0;
        PointsPrefab = Resources.Load<GameObject>("Prefabs/Points");

        GlobalEventSystem.Instance.AddEventListener(EventType.AddEntity, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.RemoveEntity, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.SupportLineShowChange, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.ShowOnlyCalcLine, OnEvent);
    }

    float time = 0;
    private void Update()
    {
        time += Time.deltaTime;
        if (time > 0.04f)
        {
            if (showMainLine && !showOnlyCalcLine)
            {
                MainLineRenderer.LineRenderByPoints(Surfaces.SelectMany(x => x.GetPositions().Select(p => p.transform.position)));
            }
            else
            {
                MainLineRenderer.Clear();
            }
            time = 0;
        }
    }

    private void OnEvent(EventType eventType, Component component, object param)
    {
        if (gameObject.activeInHierarchy)
            switch (eventType)
            {
                case EventType.AddEntity:
                    if (Surfaces == null) Surfaces = new List<Points>();
                    if (Surfaces.Count() > 0) return;

                    var newPoints = Instantiate(PointsPrefab);
                    newPoints.transform.position = transform.position + (new Vector3(195, 200));
                    newPoints.transform.parent = transform;

                    var points = newPoints.GetComponent<Points>();
                    points.GeneratePoints(4, 4, new Vector3(3, 0, 3));

                    Surfaces.Add(points);

                    GlobalEventSystem.Instance.PostEventMessage(EventType.PointCountChange, this, Surfaces.Count());
                    GlobalEventSystem.Instance.PostEventMessage(EventType.LastPointChange, this, Surfaces.Last().GetLastPointGameObject());
                    break;
                case EventType.RemoveEntity:
                    if (Surfaces.Count() > minEntityCount)
                    {
                        DestroyObject(Surfaces.Last().gameObject);
                        Surfaces.Clear();
                        ClearLine();

                        GlobalEventSystem.Instance.PostEventMessage(EventType.PointCountChange, this, Surfaces.Count());
                        GlobalEventSystem.Instance.PostEventMessage(EventType.LastPointChange, this, null);
                    }
                    break;
                case EventType.SupportLineShowChange:
                    showMainLine = !showMainLine;
                    break;
                case EventType.ShowOnlyCalcLine:
                    showOnlyCalcLine = !showOnlyCalcLine;
                    Surfaces.ForEach(x => x.Invisible(showOnlyCalcLine));
                    break;
            }
    }

    public override void LineRenderByPoints(List<Vector3> points)
    {
        var count = points.Count();
        var ttmp = Math.Sqrt(count);
        var rows = (int)ttmp;
        var columns = rows;
        var matrix = new Vector3[rows, columns];

        for (int i = 0, index = 0; i < rows; i++)
            for (var j = 0; j < columns; j++, index++)
                matrix[i, j] = points.ElementAt(index);

        var horizontalPoints = new List<Vector3>();
        var verticalPoints = new List<Vector3>();

        for (var i = 0; i < rows;)
        {
            for (var j = 0; j < columns; j++)
            {
                horizontalPoints.Add(matrix[i, j]);
                verticalPoints.Add(matrix[j, i]);
            }
            i++;
            if (i == rows) break;
            for (var j = columns - 1; j >= 0; j--)
            {
                horizontalPoints.Add(matrix[i, j]);
                verticalPoints.Add(matrix[j, i]);
            }
            i++;
        }

        HorizontalLineRenderer.LineRenderByPoints(horizontalPoints);
        VerticalLineRenderer.LineRenderByPoints(verticalPoints);
    }

    public override IEnumerator CoroutineLineRenderByPoints(List<Vector3> points)
    {
        var count = points.Count();
        var rows = (int)Math.Sqrt(count);
        var columns = rows;

        var matrix = new Vector3[rows, columns];
        for (int i = 0, index = 0; i < rows; i++)
            for (var j = 0; j < columns; j++, index++)
                matrix[i, j] = points.ElementAt(index);

        var horizontalPoints = new List<Vector3>();
        var verticalPoints = new List<Vector3>();

        for (var i = 0; i < rows;)
        {
            for (var j = 0; j < columns; j++)
            {
                horizontalPoints.Add(matrix[i, j]);
                verticalPoints.Add(matrix[j, i]);
            }
            i++;
            if (i == rows) break;
            for (var j = columns - 1; j >= 0; j--)
            {
                horizontalPoints.Add(matrix[i, j]);
                verticalPoints.Add(matrix[j, i]);
            }
            i++;
        }

        for (var i = 0; i < horizontalPoints.Count(); i++)
        {
            HorizontalLineRenderer.RenderPoint(horizontalPoints[i], i);
            VerticalLineRenderer.RenderPoint(verticalPoints[i], i);
            yield return new WaitForSeconds(0.02f);
        }
    }

    public override void ClearLine()
    {
        HorizontalLineRenderer.Clear();
        VerticalLineRenderer.Clear();
    }

    public override List<MainPoint> GetPositionMainPoints()
    {
        return Surfaces.SelectMany(x => x.GetPositions()).ToList();
    }

    public override int CountEntity()
    {
        return Surfaces.Count();
    }

    public List<JsonPoint> GetJsonPoints()
    {
        return Surfaces.SelectMany(x => x.GetJsonPoints()).ToList();
    }

    public void SetJsonPoints(List<JsonPoint> jsonPoints)
    {
        if (Surfaces.Any())
        {
            Surfaces.ForEach(x => 
            {
                x.ClearPoints();
                Destroy(x.gameObject);
            });
            Surfaces.Clear();
            ClearLine();
        }

        if (!jsonPoints.Any())
        {

            return;
        }

        if (!PointsPrefab) PointsPrefab = Resources.Load<GameObject>("Prefabs/Points");

        var newPoints = Instantiate(PointsPrefab);
        newPoints.transform.position = transform.position + (new Vector3(195, 200));
        newPoints.transform.parent = transform;
        
        var points = newPoints.GetComponent<Points>();
        points.SetJsonPoints(jsonPoints);

        Surfaces.Add(points);
    }
}
