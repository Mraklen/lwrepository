﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectItem : MonoBehaviour {
    
    [SerializeField] ObjectType Type;
    [Space(10)]
    [SerializeField] [Range(1, 5)] float scale = 1; 
    
    private void Update()
    {
        transform.localScale = new Vector3(scale, scale, scale);
    }
    
}

public enum ObjectType
{
    Cube, Sphere, Cylinder
}
