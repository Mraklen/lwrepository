﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LW1.Collision
{
    public class Collision : Singleton<Collision>
    {
        [SerializeField] List<float> Collisions;
        [SerializeField] List<GameObject> ViewPoints;

        int countPoints = 0;

        GameObject PointPrefab;

        public TrackingTarget Targeting;

        // Алгоритм Моллера — Трумбора
        public float TriangleIntersection(Vector3 orig, Vector3 dir, Vector3 point_0, Vector3 point_1, Vector3 point_2)
        {
            var e1 = point_1 - point_0;
            var e2 = point_2 - point_0;
            var pVec = Vector3.Cross(dir, e2);
            var det = Vector3.Dot(e1, pVec);

            if (det < 1e-8 && det > -1e-8) return 0;

            var inv_det = 1.0f / det;
            var tVec = orig - point_0;
            var u = Vector3.Dot(tVec, pVec) * inv_det;

            if (u < 0 || u > 1) return 0;

            var qVec = Vector3.Cross(tVec, e1);
            var v = Vector3.Dot(dir, qVec) * inv_det;

            if (v < 0 || u + v > 1) return 0;

            var result = Vector3.Dot(e2, qVec) * inv_det;

            AddNewCollision(result);
            return result;
        }

        private void AddNewCollision(float distantion)
        {
            if (distantion < 0) return;

            Collisions.Add(distantion);

            var newPoint = Instantiate(PointPrefab);
            newPoint.transform.position = Vector3.MoveTowards(transform.position, Targeting.End.transform.position, distantion);
            newPoint.transform.parent = transform;
            ViewPoints.Add(newPoint);
        }

        public void ClearCollisions()
        {
            countPoints = 0;
            Collisions?.Clear();
            ViewPoints.ForEach(x => Destroy(x));
            ViewPoints?.Clear();
        }

        public void CalcIntersectionCount()
        {
            var targetDistantion = Targeting.GetMagnitude();
            countPoints = Collisions.Count(x => targetDistantion > x);
            GlobalEventSystem.Instance.PostEventMessage(EventType.CollisionChange, this, $"Точек перед таргетом: {countPoints} ({Collisions.Count}) => таргет {(countPoints % 2 == 0 ? "снаружи области" : "внутри области")}");
        }

        public override void AwakeInit()
        {
            Collisions = new List<float>();
            ViewPoints = new List<GameObject>();
        }
        public override void StartInit()
        {
            PointPrefab = Resources.Load<GameObject>("Prefabs/ViewPoint");
            gameObject.SetActive(false);
        }
        public override void UpdateInit() { }
    }
}

