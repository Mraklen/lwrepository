﻿using EntityAbstractions.Implementations;
using System;

namespace Domain.Models.Entities.KnowledgeBase
{
    public enum ParamType
    {
        Condition,
        Result
    }

    [Serializable]
    public class RuleParameter : BaseEntity
    {
        public ParamType ParamType { get; private set; }
        
        public int RuleId { get; private set; }
        public virtual Rule Rule { get; private set; }

        public int ParameterId { get; private set; }
        public virtual Parameter Parameter { get; private set; }

        public static RuleParameter CreateNew(ParamType type, int parameterId)
        {
            return new RuleParameter
            {
                ParamType = type,
                ParameterId = parameterId
            };
        }

        public void SetParameter(Parameter parameter)
        {
            Parameter = parameter;
        }
    }
}
