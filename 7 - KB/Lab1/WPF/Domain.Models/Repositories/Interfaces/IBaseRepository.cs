﻿using EntityAbstractions.Entities;
using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Models.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity, IEntity<int>
    {
        Task<TEntity> GetByIdAsync(int id);

        Task UpdateOrInsertAsync(TEntity entity);

        Task Remove(int id);
        Task Remove(TEntity entity);
        Task RemoveRange(IEnumerable<int> ids);
        Task RemoveRange(IEnumerable<TEntity> existings);

        void SetDetached(Object some);
        void SetModified(Object some);

        IQueryable<TEntity> GetRaw();
        Task<IEnumerable<TEntity>> GetListAsync();

        bool AnyCheck();
    }
}
