﻿using Prism.Events;
using System.Collections.Generic;
using System.Windows;
using Unity;
using WPF.State;
using WPF.Modules.Base;
using WPF.ViewModels;
using WPF.Modules.Main;
using DB.EF.Data.Repositories.Implementations;
using WPF.Modules.ExpertSystem;
using Domain.Models.Repositories.Interfaces;
using WPF.Modules.FuzzyStatement;
using Domain.Models.Core;
using WPF.Modules.NeuronNet;

namespace WPF
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IUnityContainer container = new UnityContainer();

            container.RegisterType<IEventAggregator, EventAggregator>();

            container.RegisterType<IDomainRepositoryFactory, PgSqlRepositoryFactory>();
            container.RegisterType<IVariableFactory, VariableFactory>();
            
            container.RegisterInstance(new AppStateStore(container));

            var mainWindow = container.Resolve<MainWindow>();

            var modules = new Dictionary<ModuleType, IModule>
            {
                { ModuleType.Main, new MainPageModule(container, Helpers.SettingType.Default) },
                { ModuleType.ExpertSystem, new ExpertSystemModule(container, Helpers.SettingType.Default) },
                { ModuleType.FuzzyStatement, new FuzzyStatementModule(container, Helpers.SettingType.Default) },
                { ModuleType.NeuronNet, new NeuronNetModule(container, Helpers.SettingType.Default) }
            };

            var vm = container.Resolve<MainWindowViewModel>();

            vm.UpdateModules(modules, ModuleType.Main);

            mainWindow.DataContext = vm;
            mainWindow.Closing += (s, args) => vm.SelectedModule.Deactivate();
            mainWindow.Show();
        }
    }
}
