﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Windows.Controls;
using System.Threading.Tasks;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System.Reflection;
using LW1.Model;
using System.Windows.Media;


namespace LW1.Services
{
    public class Methods
    {
        FunctionParam Function { get; set; }
        List<int> FibonaciRow = new List<int>()
        {
            1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946
        };
        int maxSleepValue = 1000;
        int minSleepValue = 1;

        public void SetFunction(string functionText, double aValue, double bValue, double sValue)
        {
            Function = new FunctionParam(functionText, aValue, bValue, sValue);
        }
        
        public async void CalcEnumMethod(Label label_stepValue, Label label_extremum, ChartPlotter plotter)
        {
            ClearPlotter(plotter);

            int amountSteps = 0;
            bool checkChange = false;

            double xFirstStep = FunctionParam.AValue;
            double xLastStep = FunctionParam.BValue;
            double xStep = FunctionParam.SValue;
            PointXY pointExtrem = new PointXY();

            var amountStep = Math.Sqrt(Math.Pow((xLastStep - xFirstStep), 2)) / xStep;
            
            for (double xCurrent = xFirstStep; 
                xCurrent < xLastStep; 
                xCurrent += xStep)
            {
                double yTmp = FunctionParam.CalcFunc(xCurrent);

                if (!Double.IsNaN(yTmp))
                {
                    if (xCurrent == FunctionParam.AValue)
                    {
                        pointExtrem.SetValues(xCurrent, yTmp);
                        checkChange = true;
                    }
                    else if (pointExtrem.Y > yTmp)
                    {
                        pointExtrem.SetValues(xCurrent, yTmp);
                        checkChange = true;
                    }
                    else
                        checkChange = false;
                    
                    // render graph + point extrem
                    if(checkChange) PrintGraph(plotter, pointExtrem);

                    label_extremum.Content = pointExtrem.PrintPointXY();
                }

                label_stepValue.Content = ++amountSteps;
                
                await Task.Delay((checkChange) ? 
                    maxSleepValue / 15 : minSleepValue);
            }

            label_stepValue.Content += " (!!!)";
        }

        public async void CalcHalfDivMethod(Label label_stepValue, Label label_extremum, ChartPlotter plotter)
        {
            ClearPlotter(plotter);
            // первое вычисление сегмента
            var segment = CalcSegmentHalfMethod(FunctionParam.AValue, FunctionParam.BValue);
            var pointExtrem = SearchMinPointInSegment(segment);

            int amountSteps = 0;
            do
            {
                label_stepValue.Content = ++amountSteps;
                label_extremum.Content = pointExtrem.PrintPointXY();

                // render graph + segment
                ClearPlotter(plotter);
                PrintGraph(plotter, segment.Points.Values.ToList());

                // calc next step
                segment = CalceNextSegmentForHalfMethod(segment);
                pointExtrem = SearchMinPointInSegment(segment);

                await Task.Delay(maxSleepValue);
            }
            while (!EpsilonCheck(segment));

            // render graph + point extrem
            ClearPlotter(plotter);
            PrintGraph(plotter, pointExtrem);

            label_stepValue.Content += " (!!!)";
        }

        public async void CalcGoldenSecMethod(Label label_stepValue, Label label_extremum, ChartPlotter plotter)
        {
            ClearPlotter(plotter);
            // первое вычисление сегмента
            var segment = CalcSegmentGoldenSecMethod(FunctionParam.AValue, FunctionParam.BValue);
            var pointExtrem = SearchMinPointInSegment(segment);
            
            int amountSteps = 0;
            do
            {
                label_stepValue.Content = ++amountSteps;
                label_extremum.Content = pointExtrem.PrintPointXY();

                // render graph + segment
                ClearPlotter(plotter);
                PrintGraph(plotter, segment.Points.Values.ToList());

                // calc next step
                segment = CalcNextSegmentGoldenSecMethod(segment);
                pointExtrem = SearchMinPointInSegment(segment);

                await Task.Delay(maxSleepValue);
            }
            while (!EpsilonCheck(segment));

            // render graph + point extrem
            ClearPlotter(plotter);
            PrintGraph(plotter, pointExtrem);

            label_stepValue.Content += " (!!!)";
        }
        
        public async void CalcFibonaciMethod(Label label_stepValue, Label label_extremum, ChartPlotter plotter)
        {
            ClearPlotter(plotter);
            var indexFibonaciRow = CalcIndexFibonaciRow();

            // первое вычисление сегмента
            var segment = CalcSegmentFibonaciMethod(FunctionParam.AValue, FunctionParam.BValue, indexFibonaciRow);
            var pointExtrem = SearchMinPointInSegment(segment);
            var h = CalcStepSearch(segment.Points["A"].X, segment.Points["B"].X, indexFibonaciRow);

            int amountSteps = 1;
            for (int n = indexFibonaciRow - 1; n >= 3; n--)
            {
                label_stepValue.Content = ++amountSteps;
                label_extremum.Content = pointExtrem.PrintPointXY();

                // render graph + segment
                ClearPlotter(plotter);
                PrintGraph(plotter, segment.Points.Values.ToList());

                // calc next step
                segment = CalcNextSegmentFibonaciMethod(segment, n, h);

                pointExtrem = SearchMinPointInSegment(segment);

                await Task.Delay(maxSleepValue);
            }

            pointExtrem = SearchMinPointInSegment(segment);
            label_extremum.Content = pointExtrem.PrintPointXY();

            // render graph + point extrem
            ClearPlotter(plotter);
            PrintGraph(plotter, pointExtrem);

            label_stepValue.Content += " (!!!)";
        }

        // Наборы функций для вычисления методов
        private bool EpsilonCheck(SegmentAB segment)
        {
            return ((segment.Points["B"].X - segment.Points["A"].X) < FunctionParam.SValue);
        }
        private PointXY SearchMinPointInSegment(SegmentAB segment)
        {
            return segment.Points.Min(c => c.Value);
        }

        // числа фибоначчи
        private SegmentAB CalcSegmentFibonaciMethod(double Xa, double Xb, int fibonaciIndex)
        {
            var h = CalcStepSearch(Xa, Xb, fibonaciIndex);

            var A = new PointXY()
            {
                X = Xa, Y = FunctionParam.CalcFunc(Xa)
            };
            var x1 = new PointXY()
            {
                X = A.X + h * FibonaciRow[fibonaciIndex - 2]
            };
            var x2 = new PointXY()
            {
                X = A.X + h * FibonaciRow[fibonaciIndex - 1]
            };
            var B = new PointXY()
            {
                X = Xb, Y = FunctionParam.CalcFunc(Xb)
            };

            x1.Y = FunctionParam.CalcFunc(x1.X);
            x2.Y = FunctionParam.CalcFunc(x2.X);

            return new SegmentAB()
            {
                Points = new Dictionary<string, PointXY>()
                {
                    { "A", A },
                    { "X1", x1 },
                    { "X2", x2 },
                    { "B", B }
                }
            };
        }
        private SegmentAB CalcNextSegmentFibonaciMethod(SegmentAB segment, int fibonaciIndex, double h)
        {
            if(segment.Points["X1"].Y <= segment.Points["X2"].Y)
            {
                segment.Points["B"] = segment.Points["X2"].Clone();
                segment.Points["X2"] = segment.Points["X1"].Clone();
                
                segment.Points["X1"].X = 
                    segment.Points["A"].X + h * FibonaciRow[fibonaciIndex - 2];
                segment.Points["X1"].Y = 
                    FunctionParam.CalcFunc(segment.Points["X1"].X);
            }
            else if(segment.Points["X1"].Y > segment.Points["X2"].Y)
            {
                segment.Points["A"] = segment.Points["X1"].Clone();
                segment.Points["X1"] = segment.Points["X2"].Clone();
                
                segment.Points["X2"].X = 
                    segment.Points["A"].X + h * FibonaciRow[fibonaciIndex - 1];
                segment.Points["X2"].Y =
                    FunctionParam.CalcFunc(segment.Points["X2"].X);
            }

            return segment;
        }
        private double CalcStepSearch(double a, double b, int fibonaciIndex)
        {
            return (b - a) / FibonaciRow[fibonaciIndex];
        }
        private int CalcIndexFibonaciRow()
        {
            var N = (FunctionParam.BValue - FunctionParam.AValue) / FunctionParam.SValue;
            return FibonaciRow.IndexOf(FibonaciRow.Where(x => (N <= x)).Min(x => x));
        }

        // золотое сечение
        private SegmentAB CalcNextSegmentGoldenSecMethod(SegmentAB segment)
        {
            var points = segment.Points;
            var minValue = SearchMinPointInSegment(segment);
            var pointName = points.Where(x => x.Value == minValue).SingleOrDefault().Key;

            switch (pointName)
            {
                case "A":
                    return CalcSegmentGoldenSecMethod(points["A"], points["X2"], points["X1"], false);
                case "X1":
                    return CalcSegmentGoldenSecMethod(points["A"], points["X2"], points["X1"], false);
                case "X2":
                    return CalcSegmentGoldenSecMethod(points["X1"], points["B"], points["X2"], true);
                case "B":
                    return CalcSegmentGoldenSecMethod(points["X1"], points["B"], points["X2"], true);
            }
            return null;
        }
        private SegmentAB CalcSegmentGoldenSecMethod(double Xa, double Xb)
        {
            var A = new PointXY() { X = Xa, Y = FunctionParam.CalcFunc(Xa) };
            var B = new PointXY() { X = Xb, Y = FunctionParam.CalcFunc(Xb) };
            var x1 = new PointXY() { X = CalcGoldenSecX(A.X, B.X, '+') };
            var x2 = new PointXY() { X = CalcGoldenSecX(A.X, B.X, '-') };
            x1.Y = FunctionParam.CalcFunc(x1.X);
            x2.Y = FunctionParam.CalcFunc(x2.X);

            return new SegmentAB()
            {
                Points = new Dictionary<string, PointXY>()
                {
                    { "A", A }, { "X1", x1 }, { "X2", x2 }, { "B", B }
                }
            };
        }
        private SegmentAB CalcSegmentGoldenSecMethod(PointXY A, PointXY B, PointXY X, bool mod)
        {
            var x1 = (mod) ? X.Clone() : new PointXY() { X = CalcGoldenSecX(A.X, B.X, '+') };
            var x2 = (mod) ? new PointXY() { X = CalcGoldenSecX(A.X, B.X, '-') } : X.Clone();

            x1.Y = (mod) ? x1.Y : FunctionParam.CalcFunc(x1.X);
            x2.Y = (mod) ? FunctionParam.CalcFunc(x2.X) : x2.Y;

            return new SegmentAB()
            {
                Points = new Dictionary<string, PointXY>()
                {
                    { "A", A.Clone() }, { "X1", x1.Clone() }, { "X2", x2.Clone() }, { "B", B.Clone() }
                }
            };
        }
        private double CalcGoldenSecX(double Xa, double Xb, char mod)
        {
            return (mod == '+') ? Xa + 0.38f * (Xb - Xa) : Xb - 0.38f * (Xb - Xa);
        }

        // половинное деление
        private SegmentAB CalceNextSegmentForHalfMethod(SegmentAB segment)
        {
            var points = segment.Points;
            var minValue = SearchMinPointInSegment(segment);
            var pointName = points.Where(x => x.Value == minValue).SingleOrDefault().Key;

            switch (pointName)
            {
                case "A":
                    return CalcSegmentHalfMethod(points["A"], points["X2"], points["X1"]);
                case "X1":
                    return CalcSegmentHalfMethod(points["A"], points["X2"], points["X1"]);
                case "X2":
                    return CalcSegmentHalfMethod(points["X1"], points["X3"], points["X2"]);
                case "X3":
                    return CalcSegmentHalfMethod(points["X2"], points["B"], points["X3"]);
                case "B":
                    return CalcSegmentHalfMethod(points["X2"], points["B"], points["X3"]);
            }
            return null;
        }
        private SegmentAB CalcSegmentHalfMethod(double Xa, double Xb)
        {
            var A = new PointXY() { X = Xa, Y = FunctionParam.CalcFunc(Xa) };
            var B = new PointXY() { X = Xb, Y = FunctionParam.CalcFunc(Xb) };
            var x2 = new PointXY() { X = CalcHalfLine(A.X, B.X) };
            var x1 = new PointXY() { X = CalcHalfLine(A.X, x2.X) };
            var x3 = new PointXY() { X = CalcHalfLine(x2.X, B.X) };
            x1.Y = FunctionParam.CalcFunc(x1.X);
            x2.Y = FunctionParam.CalcFunc(x2.X);
            x3.Y = FunctionParam.CalcFunc(x3.X);

            return new SegmentAB()
            {
                Points = new Dictionary<string, PointXY>()
                {
                    { "A", A }, { "X1", x1 }, { "X2", x2 }, { "X3", x3 }, { "B", B }
                }
            };
        }
        private SegmentAB CalcSegmentHalfMethod(PointXY A, PointXY B, PointXY X2)
        {
            var x1 = new PointXY() { X = CalcHalfLine(A.X, X2.X) };
            var x3 = new PointXY() { X = CalcHalfLine(X2.X, B.X) };
            x1.Y = FunctionParam.CalcFunc(x1.X);
            x3.Y = FunctionParam.CalcFunc(x3.X);

            return new SegmentAB()
            {
                Points = new Dictionary<string, PointXY>()
                {
                    { "A", A }, { "X1", x1 }, { "X2", X2 }, { "X3", x3 }, { "B", B }
                }
            };
        }
        private double CalcHalfLine(double a, double b)
        {
            return (a + b) / 2;
        }

        // Отрисовка графика
        void PrintGraph(ChartPlotter plotter, List<PointXY> segment)
        {
            //ClearPlotter(plotter);

            // adding graph to plotter
            plotter.AddLineGraph(
                CreateDataSource(Function.Points), Colors.Blue, 3);
            if(segment != null) plotter.AddLineGraph(
                CreateDataSource(segment),
                new Pen(Brushes.Red, 4), new CirclePointMarker(), new PenDescription(" "));
            
            plotter.LegendVisible = false;
            plotter.FitToView();
        }
        void PrintGraph(ChartPlotter plotter, PointXY point)
        {
           // ClearPlotter(plotter);

            // adding graph to plotter
            plotter.AddLineGraph(
                CreateDataSource(Function.Points), Colors.Blue, 3);
            if(point != null) plotter.AddLineGraph(
                CreateDataSource(point),
                new Pen(Brushes.Red, 4), new CirclePointMarker(), new PenDescription(" "));
            
            plotter.LegendVisible = false;
            plotter.FitToView();
        }
        private void ClearPlotter(ChartPlotter plotter)
        {
            plotter.Children.RemoveAll(typeof(LineGraph));
            plotter.Children.RemoveAll(typeof(MarkerPointsGraph));
        }
        private EnumerableDataSource<PointXY> CreateDataSource(List<PointXY> rates)
        {
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(rates);
            ds.SetXMapping(x => x.X);
            ds.SetYMapping(y => y.Y);
            return ds;
        }
        private EnumerableDataSource<PointXY> CreateDataSource(PointXY rates)
        {
            List<PointXY> points = new List<PointXY>() { rates };
            EnumerableDataSource<PointXY> ds = new EnumerableDataSource<PointXY>(points);
            ds.SetXMapping(x => x.X);
            ds.SetYMapping(y => y.Y);
            return ds;
        }
    }
}
