﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Extentions
{
    public static class LineRendererExtentions
    {
        public static void RenderPoint(this LineRenderer renderer, Vector3 point, int index)
        {
            renderer.SetVertexCount(index + 1);
            renderer.SetPosition(index, point);
        }

        public static void LineRenderByPoints(this LineRenderer renderer, IEnumerable<Vector3> points)
        {
            var pointCount = points.Count();
            renderer.SetVertexCount(pointCount);
            renderer.SetPositions(points.ToArray());
        }
        
        public static void Clear(this LineRenderer renderer)
        {
            renderer.SetVertexCount(0);
        }
    }
}
