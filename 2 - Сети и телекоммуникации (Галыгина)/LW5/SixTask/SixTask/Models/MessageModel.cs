﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SixTask.Models
{
    public class MessageModel
    {
        public DateTime Date { get; set; }
        public string Message { get; set; }
    }
}
