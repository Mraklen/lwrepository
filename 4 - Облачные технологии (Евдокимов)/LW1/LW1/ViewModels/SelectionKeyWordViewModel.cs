﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.ViewModels
{
    public class SelectionKeyWordViewModel
    {
        public int Id { get; set; }
        public int KeyWordId { get; set; }
        public KeyWordViewModel KeyWord { get; set; }
        public int? BestPosition { get; set; }
        public int? Offset { get; set; }
        public bool? IsUp { get; set; }
        public ICollection<SelectionUrlViewModel> SelectionUrls { get; set; }
    }
}
