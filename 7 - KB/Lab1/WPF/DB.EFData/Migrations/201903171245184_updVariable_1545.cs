namespace DB.EFData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updVariable_1545 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.Variables", "IsVisible", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("public.Variables", "IsVisible");
        }
    }
}
