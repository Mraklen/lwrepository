﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Model
{
    public class Alphabet
    {
        public string Name { get; set; }
        public string Symbols { get; set; }
        public int PowerAlphabet { get; set; }
        public int DimensionSymbol { get; set; }
    }
}
