﻿using LW.Steganography.Method;
using Project.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW.Steganography
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void encryptBtn_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.GetFileName();
            var message = messageBox.Text;
            var color = Color.FromArgb(255, Convert.ToInt32(rValueLb.Text), Convert.ToInt32(gValueLb.Text), Convert.ToInt32(bValueLb.Text));
            var resultPath = SteganographyMethod.Encrypt(fileName, message, color);

            if (resultPath != null) Process.Start(resultPath);
        }

        private void decryptBtn_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.GetFileName();

            var color = Color.FromArgb(255, Convert.ToInt32(rValueLb.Text), Convert.ToInt32(gValueLb.Text), Convert.ToInt32(bValueLb.Text));

            var result = SteganographyMethod.Decrypt(fileName, color);

            MessageBox.Show(result, "Зашифрованное сообщение:", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
    }
}
