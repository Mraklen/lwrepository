﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LW2.Services;
using LW2.Models;

namespace LW2
{
    public partial class MainWindow : Window
    {
        Methods CalcFuncMethods { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            CalcFuncMethods = new Methods();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var x1Value = Convert.ToSingle(Text_x1PowellValue.Text);
            var x2Value = Convert.ToSingle(Text_x2PowellValue.Text);
            var hValue = Convert.ToSingle(Text_hValue.Text);
            var sValue = Convert.ToSingle(Text_sValue.Text);
            Simplex.x10[0] = Convert.ToSingle(Text_x11Value.Text);
            Simplex.x20[0] = Convert.ToSingle(Text_x12Value.Text);
            Simplex.x10[1] = Convert.ToSingle(Text_x21Value.Text);
            Simplex.x20[1] = Convert.ToSingle(Text_x22Value.Text);
            Simplex.x10[2] = Convert.ToSingle(Text_x31Value.Text);
            Simplex.x20[2] = Convert.ToSingle(Text_x32Value.Text);

            CalcFuncMethods.SetFunction(x1Value, x2Value, hValue, sValue);

            CalcFuncMethods.CalcPowellMethod(new PlotterParam()
            {   Plotter = PowellPlotter,
                IsolineGraph = PowellIsoline,
                IsolineTracking = PowellTracking
            }, lbPowellSteps, lbPowellExtrem);
            CalcFuncMethods.CalcSimplexMethod(new PlotterParam()
            {
                Plotter = SimplexPlotter,
                IsolineGraph = SimplexIsoline,
                IsolineTracking = SimplexTracking
            }, lbSimplexSteps, lbSimplexExtrem);
        }
        
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbox_CurrenFunction.SelectedIndex)
            {
                case 0:
                    FunctionParam.CurrenIsolineType =
                        IsolineType.Ellipse;
                    break;
                case 1:
                    FunctionParam.CurrenIsolineType = 
                        IsolineType.Rosenbroke;
                    break;
            }
        }
    }
}
