﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace WPF.State
{
    [Serializable]
    public class AppStateStore
    {
        public AppStateStore(IUnityContainer container)
        {
            State = container.Resolve<AppState>();
            EventAggregator = container.Resolve<IEventAggregator>();
        }

        public AppState State { get; set; }
        public IEventAggregator EventAggregator { get; private set; }
    }
}
