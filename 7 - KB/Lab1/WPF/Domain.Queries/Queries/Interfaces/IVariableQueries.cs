﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Queries.ViewModels.KnowledgeBase;
using QueryAbstractions.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.Queries.Interfaces
{
    public interface IVariableQueries : IBaseQueries<VariableReadModel, VariableViewModel, int>
    {
        Task<IEnumerable<VariableViewModel>> GetListAsync();
    }
}
