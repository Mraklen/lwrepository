﻿using System.IO;
using System.Windows.Forms;

namespace LW4.ArithmeticCoding.Managers
{
    public class FileManager
    {
        public static void SaveFile(byte[] fileContent, string folder, string fileName, string ext)
        {
            using (var stream = new FileStream($"{Application.StartupPath}\\{folder}\\{fileName}.{ext}", FileMode.OpenOrCreate))
            {
                stream.Write(fileContent, 0, fileContent.Length);
            }
        }

        public static byte[] ReadFile(string fileName)
        {
            if (fileName == null || fileName.Equals("")) return new byte[0];
            using (var stream = File.OpenRead(fileName))
            {
                var bytes = new byte[stream.Length];
                stream.Read(bytes, 0, bytes.Length);
                return bytes;
            }
        }

        public static string SelectFile(int filterIndex = 2)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = $"{Application.StartupPath}\\CompressedFiles",
                Filter = "rle files (*.ath)|*.ath|All files (*.*)|*.*",
                FilterIndex = filterIndex,
                RestoreDirectory = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }
            return "";
        }
    }
}
