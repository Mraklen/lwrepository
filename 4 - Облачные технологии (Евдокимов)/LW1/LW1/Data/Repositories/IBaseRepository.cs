﻿using LW1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : IEntity<int>
    {
        TEntity GetById(int id);
        void UpdateOrInsert(TEntity aggregate);
    }
}
