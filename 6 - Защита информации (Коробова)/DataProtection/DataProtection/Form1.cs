﻿using DataProtection.Ciphers;
using DataProtection.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataProtection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void encryptBtn_Click(object sender, EventArgs e)
        {
            var fileContent = FileManager.LoadFile();
            var fileName = "";
            var filePath = "";
            var codingResult = "";

            if (vigenenerRadio.Checked)
            {
                fileName = "VigenenerEncrypt.vgn";
                codingResult = VigenenerAndGamma.Encrypt(Encoding.UTF8.GetString(fileContent), keyText.Text);
            }
            if (transparentRadio.Checked)
            {
                fileName = "TranspositionEncrypt.trn";
                Transposition.SetKey(keyText.Text);
                codingResult = Transposition.Encrypt(Encoding.UTF8.GetString(fileContent));
            }
            if (gammaNRadio.Checked)
            {
                fileName = "GammaEncrypt.rnd";
                codingResult = VigenenerAndGamma.Encrypt(Encoding.UTF8.GetString(fileContent), 
                    VigenenerAndGamma.GetRandomKey(keyText.Text.Length, Convert.ToInt32(seedText.Text)));
            }
            if (ADFGVXRadio.Checked)
            {
                fileName = "ADFGVXEncrypt.adf";
                codingResult = ADFGVX.Encrypt(Encoding.UTF8.GetString(fileContent), keyText.Text);
            }

            filePath = FileManager.SaveFile(Encoding.UTF8.GetBytes(codingResult), fileName);
            Process.Start(filePath);
        }

        private void decryptBtn_Click(object sender, EventArgs e)
        {
            var fileContent = FileManager.LoadFile();
            var encodingResult = "";
            var fileName = "";
            var filePath = "";

            if (vigenenerRadio.Checked)
            {
                fileName = "VigenenerDecrypt.txt";
                encodingResult = VigenenerAndGamma.Decrypt(Encoding.UTF8.GetString(fileContent), keyText.Text);
            }
            if (transparentRadio.Checked)
            {
                fileName = "TranspositionDecrypt.txt";
                Transposition.SetKey(keyText.Text);
                encodingResult = Transposition.Decrypt(Encoding.UTF8.GetString(fileContent));
            }
            if (gammaNRadio.Checked)
            {
                fileName = "GammaEncrypt.txt";
                encodingResult = VigenenerAndGamma.Decrypt(Encoding.UTF8.GetString(fileContent), 
                    VigenenerAndGamma.GetRandomKey(keyText.Text.Length, Convert.ToInt32(seedText.Text)));
            }
            if (ADFGVXRadio.Checked)
            {
                fileName = "ADFGVXEncrypt.txt";
                encodingResult = ADFGVX.Decrypt(Encoding.UTF8.GetString(fileContent), keyText.Text);
            }

            filePath = FileManager.SaveFile(Encoding.UTF8.GetBytes(encodingResult), fileName);
            Process.Start(filePath);
        }

        private void gammaNRadio_CheckedChanged(object sender, EventArgs e)
        {
            seedText.Visible = gammaNRadio.Checked;
            seedLb.Visible = gammaNRadio.Checked;
        }
    }
}
