﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.NeuronNet.Layers
{
    public class InputItem
    {
        /// <summary> Предполагаемые входы </summary>
        public List<double> Inputs { get; private set; }
        /// <summary> Предполагаемые выходы </summary>
        public List<double> Outputs { get; private set; }

        public static InputItem CreateNew(List<double> inputs, List<double> outputs)
        {
            return new InputItem { Inputs = inputs, Outputs = outputs };
        }
    }

    public class InputLayer
    {
        /// <summary> Тренировочное множество </summary>
        public List<InputItem> TrainSet { get; private set; }

        public InputLayer()
        {
            TrainSet = new List<InputItem>
            {
                InputItem.CreateNew(new List<double> { 0, 0, 0 }, new List<double> { 0 }),
                InputItem.CreateNew(new List<double> { 0, 0, 1 }, new List<double> { 1 }),
                InputItem.CreateNew(new List<double> { 0, 1, 0 }, new List<double> { 0 }),
                InputItem.CreateNew(new List<double> { 0, 1, 1 }, new List<double> { 1 }),
                InputItem.CreateNew(new List<double> { 1, 0, 0 }, new List<double> { 0 }),
                InputItem.CreateNew(new List<double> { 1, 0, 1 }, new List<double> { 1 }),
                InputItem.CreateNew(new List<double> { 1, 1, 0 }, new List<double> { 0 }),
                InputItem.CreateNew(new List<double> { 1, 1, 1 }, new List<double> { 1 })
            };
        }
    }
}
