﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LW1.Services;
using LW1.Model;


namespace LW1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Methods CalcFuncMethods { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            CalcFuncMethods = new Methods();
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var functionText = Text_Function.Text;
            var aValue = Convert.ToSingle(Text_aValue.Text);
            var bValue = Convert.ToSingle(Text_bValue.Text);
            var sValue = Convert.ToSingle(Text_sValue.Text);

            CalcFuncMethods.SetFunction(functionText, aValue, bValue, sValue);

            // Запуск 4 методов
            CalcFuncMethods.CalcEnumMethod(Lable_StepValue_Enum, Lable_Extremum_Enum, EnumPlotter);
            CalcFuncMethods.CalcHalfDivMethod(Lable_StepValue_HalfDiv, Lable_Extremum_HalfDiv, HalfDivPlotter);
            CalcFuncMethods.CalcGoldenSecMethod(Lable_StepValue_GoldenSec, Lable_Extremum_GoldenSec, GoldenSecPlotter);
            CalcFuncMethods.CalcFibonaciMethod(Lable_StepValue_Fibonaci, Lable_Extremum_Fibonaci, FibonaciPlotter);
        }

    }
}
