﻿using LW1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Providers
{
    public interface IUserProvider
    {
        string CurrentUserId { get; }
        ApplicationUser CurrentUserAccount { get; }
    }
}
