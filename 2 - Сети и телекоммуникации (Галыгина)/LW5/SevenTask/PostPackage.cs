﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeatBattle.Model
{
    public enum PackageType
    {
        ConnectToServer,
        ConnectToServerResult,
        EnemyReady,
        ReadyPlay,
        FirstPlayer,
        PlayerShot
    }

    public class PostPackage
    {
        public PackageType Type { get; set; }
        public string Content { get; set; }
    }
}
