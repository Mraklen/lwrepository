﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.ViewModels
{
    public class ReserveCompleteInfo
    {
        public string CinemaName { get; set; }
        public string FilmName { get; set; }
        public string SessionDate { get; set; }
        public string SessionTime { get; set; }
        public string CinemaRoomName { get; set; }
        public string ReserveSeats { get; set; }
        public string ReserveID { get; set; }
        public string ReserveStatus { get; set; }
    }
}
