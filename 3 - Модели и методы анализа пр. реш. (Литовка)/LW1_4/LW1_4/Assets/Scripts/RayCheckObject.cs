﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCheckObject : Singleton<RayCheckObject>
{
    [SerializeField] static List<ObjectItem> objects;
    public static List<ObjectItem> Objects => objects;

    public override void AwakeInit() { }
    public override void StartInit() { }
    public override void UpdateInit() { }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("CheckObject"))
        {
            objects.Add(collision.gameObject.GetComponentInParent<ObjectItem>());
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag.Equals("CheckObject"))
        {
            objects.Remove(collision.gameObject.GetComponentInParent<ObjectItem>());
        }
    }

}
