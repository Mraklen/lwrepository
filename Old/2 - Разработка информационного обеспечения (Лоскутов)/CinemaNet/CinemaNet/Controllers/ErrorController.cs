﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;


namespace CinemaNet.Controllers
{
    public class ErrorController : Controller
    {
       
        public ErrorController()
        {
            
        }

        public IActionResult Index()
        {
            var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerFeature>();

            var exception = exceptionHandler?.Error;
            if (exception != null)
            {
                ViewBag.Exception = exception;
                //_logger.LogError(1, exception, exception.Message);
            }

            return View();
        }

        public IActionResult Error401()
        {
            return View();
        }

        public IActionResult Error403()
        {
            return View();
        }

        public IActionResult Error404()
        {
            return View();
        }

        public IActionResult Error500()
        {
            return View();
        }
    }
}
