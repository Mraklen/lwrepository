--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 10.6

-- Started on 2019-03-16 22:32:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 367390)
-- Name: Parameters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Parameters" (
    "Id" integer NOT NULL,
    "Name" text,
    "Question" text,
    "Description" text,
    "VariableId" integer NOT NULL
);


ALTER TABLE public."Parameters" OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 367388)
-- Name: Parameters_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Parameters_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Parameters_Id_seq" OWNER TO postgres;

--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 185
-- Name: Parameters_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Parameters_Id_seq" OWNED BY public."Parameters"."Id";


--
-- TOC entry 188 (class 1259 OID 367402)
-- Name: RuleParameters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RuleParameters" (
    "Id" integer NOT NULL,
    "ParamType" integer NOT NULL,
    "RuleId" integer NOT NULL,
    "ParameterId" integer NOT NULL
);


ALTER TABLE public."RuleParameters" OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 367400)
-- Name: RuleParameters_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RuleParameters_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RuleParameters_Id_seq" OWNER TO postgres;

--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 187
-- Name: RuleParameters_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RuleParameters_Id_seq" OWNED BY public."RuleParameters"."Id";


--
-- TOC entry 190 (class 1259 OID 367412)
-- Name: Rules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Rules" (
    "Id" integer NOT NULL
);


ALTER TABLE public."Rules" OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 367410)
-- Name: Rules_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Rules_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Rules_Id_seq" OWNER TO postgres;

--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 189
-- Name: Rules_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Rules_Id_seq" OWNED BY public."Rules"."Id";


--
-- TOC entry 192 (class 1259 OID 367420)
-- Name: Variables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Variables" (
    "Id" integer NOT NULL,
    "Name" text,
    "CodeName" text,
    "VarType" integer NOT NULL
);


ALTER TABLE public."Variables" OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 367418)
-- Name: Variables_Id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Variables_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Variables_Id_seq" OWNER TO postgres;

--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 191
-- Name: Variables_Id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Variables_Id_seq" OWNED BY public."Variables"."Id";


--
-- TOC entry 193 (class 1259 OID 367444)
-- Name: __MigrationHistory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."__MigrationHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ContextKey" character varying(300) NOT NULL,
    "Model" bytea NOT NULL,
    "ProductVersion" character varying(32) NOT NULL
);


ALTER TABLE public."__MigrationHistory" OWNER TO postgres;

--
-- TOC entry 2026 (class 2604 OID 367393)
-- Name: Parameters Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Parameters" ALTER COLUMN "Id" SET DEFAULT nextval('public."Parameters_Id_seq"'::regclass);


--
-- TOC entry 2027 (class 2604 OID 367405)
-- Name: RuleParameters Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RuleParameters" ALTER COLUMN "Id" SET DEFAULT nextval('public."RuleParameters_Id_seq"'::regclass);


--
-- TOC entry 2028 (class 2604 OID 367415)
-- Name: Rules Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Rules" ALTER COLUMN "Id" SET DEFAULT nextval('public."Rules_Id_seq"'::regclass);


--
-- TOC entry 2029 (class 2604 OID 367423)
-- Name: Variables Id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Variables" ALTER COLUMN "Id" SET DEFAULT nextval('public."Variables_Id_seq"'::regclass);


--
-- TOC entry 2164 (class 0 OID 367390)
-- Dependencies: 186
-- Data for Name: Parameters; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Parameters" VALUES (1, 'сильная', 'Больевые ощущения характерно сильные?', NULL, 1);
INSERT INTO public."Parameters" VALUES (2, 'умеренная', 'Болевые ощущения умеренные?', NULL, 1);
INSERT INTO public."Parameters" VALUES (3, 'тупая', 'Имеются тупые болевые ощущения?', NULL, 1);
INSERT INTO public."Parameters" VALUES (4, 'периодическая', 'Боль появляется периодически?', NULL, 2);
INSERT INTO public."Parameters" VALUES (5, 'постоянная', 'Боль присутствует постоянно?', NULL, 2);
INSERT INTO public."Parameters" VALUES (6, 'спонтанная', 'Боль возникает спонтанно, независимо от вермени?', NULL, 2);
INSERT INTO public."Parameters" VALUES (7, 'движение', 'Уменьшается ли сила болезненных ощущений при движении?', NULL, 3);
INSERT INTO public."Parameters" VALUES (8, 'движение', 'Есть ли усиление боли в движении?', NULL, 4);
INSERT INTO public."Parameters" VALUES (9, 'сырая погода', 'Боль усиливается во время сырой погоды?', NULL, 4);
INSERT INTO public."Parameters" VALUES (10, 'неудобная поза', 'Боль усиливается при неудобных позах?', NULL, 4);
INSERT INTO public."Parameters" VALUES (11, 'мягкие ткани', 'Присутствует ли деформация мягких тканией?', NULL, 5);
INSERT INTO public."Parameters" VALUES (12, 'увеличение', 'Увеличены ли суставы в размерах?', NULL, 5);
INSERT INTO public."Parameters" VALUES (13, 'деформация', 'Заметны ли деформации в суставах?', NULL, 5);
INSERT INTO public."Parameters" VALUES (14, 'коленные суставы', 'Локализация проблем в коленных суставах?', NULL, 6);
INSERT INTO public."Parameters" VALUES (15, 'плечевые суставы', 'Локализация проблем в плечевых суставах?', NULL, 6);
INSERT INTO public."Parameters" VALUES (16, 'локтевые суставы', 'Локализация проблем в локтевых суставах?', NULL, 6);
INSERT INTO public."Parameters" VALUES (17, 'кистевые суставы', 'Локализация проблем в кистевых суставах?', NULL, 6);
INSERT INTO public."Parameters" VALUES (18, 'позвоночник', 'Локализация проблем в позвоночном отделе?', NULL, 6);
INSERT INTO public."Parameters" VALUES (19, 'суставы', 'Локализация проблем в суставах?', NULL, 6);
INSERT INTO public."Parameters" VALUES (20, NULL, 'Есть ли хруст в суставах при движении?', NULL, 7);
INSERT INTO public."Parameters" VALUES (21, NULL, 'Чувтвуется ли тугоподвижность суставов?', NULL, 8);
INSERT INTO public."Parameters" VALUES (22, NULL, 'Характерно ли прогрессирующее ограничение подвижности?', NULL, 9);
INSERT INTO public."Parameters" VALUES (23, NULL, 'Отмечается ли быстрое утомленеие во время активных движений?', NULL, 10);
INSERT INTO public."Parameters" VALUES (24, NULL, 'Возникали ли неприятные ощущения в области сердца?', NULL, 11);
INSERT INTO public."Parameters" VALUES (25, NULL, 'Обнаружено ли поражение суставов в виде припухлости?', NULL, 12);
INSERT INTO public."Parameters" VALUES (26, NULL, 'Замечено ли покраснение кожи в области суставов?', NULL, 13);
INSERT INTO public."Parameters" VALUES (27, 'артрит', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (28, 'острый артрит', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (29, 'хронический артрит', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (30, 'ревматоидный артрит', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (31, 'артроз', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (32, 'остеохандроз', NULL, NULL, 14);
INSERT INTO public."Parameters" VALUES (33, 'ревматизм', NULL, NULL, 14);


--
-- TOC entry 2166 (class 0 OID 367402)
-- Dependencies: 188
-- Data for Name: RuleParameters; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."RuleParameters" VALUES (1, 0, 1, 4);
INSERT INTO public."RuleParameters" VALUES (2, 0, 1, 6);
INSERT INTO public."RuleParameters" VALUES (3, 0, 1, 7);
INSERT INTO public."RuleParameters" VALUES (4, 0, 1, 11);
INSERT INTO public."RuleParameters" VALUES (5, 0, 1, 19);
INSERT INTO public."RuleParameters" VALUES (6, 1, 1, 27);
INSERT INTO public."RuleParameters" VALUES (7, 0, 2, 1);
INSERT INTO public."RuleParameters" VALUES (8, 0, 2, 5);
INSERT INTO public."RuleParameters" VALUES (9, 0, 2, 26);
INSERT INTO public."RuleParameters" VALUES (10, 0, 2, 12);
INSERT INTO public."RuleParameters" VALUES (11, 0, 2, 19);
INSERT INTO public."RuleParameters" VALUES (12, 1, 2, 28);
INSERT INTO public."RuleParameters" VALUES (13, 0, 3, 8);
INSERT INTO public."RuleParameters" VALUES (14, 0, 3, 22);
INSERT INTO public."RuleParameters" VALUES (15, 1, 3, 29);
INSERT INTO public."RuleParameters" VALUES (16, 0, 4, 2);
INSERT INTO public."RuleParameters" VALUES (17, 0, 4, 8);
INSERT INTO public."RuleParameters" VALUES (18, 0, 4, 17);
INSERT INTO public."RuleParameters" VALUES (19, 0, 4, 16);
INSERT INTO public."RuleParameters" VALUES (20, 1, 4, 30);
INSERT INTO public."RuleParameters" VALUES (21, 0, 5, 3);
INSERT INTO public."RuleParameters" VALUES (22, 0, 5, 9);
INSERT INTO public."RuleParameters" VALUES (23, 0, 5, 23);
INSERT INTO public."RuleParameters" VALUES (24, 0, 5, 21);
INSERT INTO public."RuleParameters" VALUES (25, 0, 5, 13);
INSERT INTO public."RuleParameters" VALUES (26, 0, 5, 20);
INSERT INTO public."RuleParameters" VALUES (27, 1, 5, 31);
INSERT INTO public."RuleParameters" VALUES (28, 0, 6, 5);
INSERT INTO public."RuleParameters" VALUES (29, 0, 6, 10);
INSERT INTO public."RuleParameters" VALUES (30, 0, 6, 24);
INSERT INTO public."RuleParameters" VALUES (31, 0, 6, 21);
INSERT INTO public."RuleParameters" VALUES (32, 0, 6, 18);
INSERT INTO public."RuleParameters" VALUES (33, 1, 6, 32);
INSERT INTO public."RuleParameters" VALUES (34, 0, 7, 8);
INSERT INTO public."RuleParameters" VALUES (35, 0, 7, 24);
INSERT INTO public."RuleParameters" VALUES (36, 0, 7, 14);
INSERT INTO public."RuleParameters" VALUES (37, 0, 7, 15);
INSERT INTO public."RuleParameters" VALUES (38, 0, 7, 16);
INSERT INTO public."RuleParameters" VALUES (39, 1, 7, 33);
INSERT INTO public."RuleParameters" VALUES (40, 0, 8, 22);
INSERT INTO public."RuleParameters" VALUES (41, 1, 8, 13);
INSERT INTO public."RuleParameters" VALUES (42, 0, 9, 12);
INSERT INTO public."RuleParameters" VALUES (43, 1, 9, 26);
INSERT INTO public."RuleParameters" VALUES (44, 0, 10, 26);
INSERT INTO public."RuleParameters" VALUES (45, 1, 10, 12);
INSERT INTO public."RuleParameters" VALUES (46, 0, 11, 25);
INSERT INTO public."RuleParameters" VALUES (47, 1, 11, 2);
INSERT INTO public."RuleParameters" VALUES (48, 0, 12, 25);
INSERT INTO public."RuleParameters" VALUES (49, 1, 12, 22);


--
-- TOC entry 2168 (class 0 OID 367412)
-- Dependencies: 190
-- Data for Name: Rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Rules" VALUES (1);
INSERT INTO public."Rules" VALUES (2);
INSERT INTO public."Rules" VALUES (3);
INSERT INTO public."Rules" VALUES (4);
INSERT INTO public."Rules" VALUES (5);
INSERT INTO public."Rules" VALUES (6);
INSERT INTO public."Rules" VALUES (7);
INSERT INTO public."Rules" VALUES (8);
INSERT INTO public."Rules" VALUES (9);
INSERT INTO public."Rules" VALUES (10);
INSERT INTO public."Rules" VALUES (11);
INSERT INTO public."Rules" VALUES (12);


--
-- TOC entry 2170 (class 0 OID 367420)
-- Dependencies: 192
-- Data for Name: Variables; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Variables" VALUES (1, 'Боль, характер', 'БХ', 2);
INSERT INTO public."Variables" VALUES (2, 'Боль, проявление', 'БП', 3);
INSERT INTO public."Variables" VALUES (3, 'Боль, уменьшается', 'БУМ', 2);
INSERT INTO public."Variables" VALUES (4, 'Боль, увеличение', 'БУВ', 2);
INSERT INTO public."Variables" VALUES (5, 'Сустав, девормация', 'СД', 2);
INSERT INTO public."Variables" VALUES (6, 'Локализация', 'Л', 3);
INSERT INTO public."Variables" VALUES (7, 'Хруст при движении', 'ХД', 1);
INSERT INTO public."Variables" VALUES (8, 'Тугоподвижность', 'ТП', 1);
INSERT INTO public."Variables" VALUES (9, 'Органичение подвижности', 'ОП', 1);
INSERT INTO public."Variables" VALUES (10, 'Быстрое утомление', 'БУ', 1);
INSERT INTO public."Variables" VALUES (11, 'Сердечные боли', 'СБ', 1);
INSERT INTO public."Variables" VALUES (12, 'Припухлость', 'П', 1);
INSERT INTO public."Variables" VALUES (13, 'Покраснение кожи', 'ПК', 1);
INSERT INTO public."Variables" VALUES (14, 'Диагноз', 'Д', 2);


--
-- TOC entry 2171 (class 0 OID 367444)
-- Dependencies: 193
-- Data for Name: __MigrationHistory; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."__MigrationHistory" VALUES ('201903161644112__init', 'DB.EFData.Migrations.Configuration', '\x1f8b0800000000000400ed5b5b4fe436147eafd4ff10e5b1a2132ed5aa8b667605035468778032b0ea1b32896788ea3869ecd041557f591ffa93fa177a9c7bec38f15c1860b55a69171c9f8b8f3f9f8b8ff7bf7ffe1d7e5c04c47ac431f3433ab2f706bbb685a91b7a3e9d8fec84cf7efcd9fef8e1fbef86a75eb0b0be14f30ec43ca0a46c643f701e1d3a0e731f7080d820f0dd3864e18c0fdc307090173afbbbbbef9dbd3d07030b1b7859d6f03aa1dc0f70fa0bfc3a0ea98b239e2032093d4c583e0e5fa62957eb02059845c8c523fbe478707a768238b2ad23e22350608ac9ccb610a521471cd43bbc6578cae390cea7110c2072f3146198374384e15cedc36abae90a76f7c50a9c8ab060e5268c87c1920cf70e72933832f94a86b54b9381d14ec1b8fc49ac3a35dcc8be4231fccb716c5bb2b4c33189c54c302c0cfb7490edc020e5e16336f844c33f09f6e6f818313c2839ed588df93b25360042e2cf8e354e084f623ca238e131223bd655724f7cf7137eba097fc774441342ea6a83e2f0ad310043577118e1983f5de359be9873cfb69c269d23139664359a6c95e7941fecdbd6050847f70497a8a85964cac318ff82298e11c7de15e2b05c2a78e0d4ae8a744996f8bb9006308483645b13b4f88ce99c3f8c6cf8d1b6cefc05f68a915c835beac3b903221e27b84fc8af096619589e59d009666eec475b91f505c5bed894fedd92f85ca0477f9e6e9ec4f13a21b8842cb3ad6b4cd269ecc18f32c75121fa4e9e7c1687c17548eae7479a73370d93d815bb1df64cbc41f11c7373c50b53f4a85c4d6b53b6f8daa16639a54dc1a15379924effd258ecda3ea6c1ed9b9fd1c94a4d248495e7b2c44636da776a247e5350abe4751c025010eda3115bb5f4696d5b87d8eacd1dfb1a16377de28b73d27be28b33b78cab6a55b7c1f72e9b5629aa7e554e7bcb94b54ffb460ef9b7b36d068b9e08b63440640c77606825805451694d90148cbe0145276b2bc9e618666e4510ecb712d0cab1f5a3415f0268984de943809270991d9f24906bb56cc5e7ec8ca07955b96ea27613f337749c606f3d1c9327c0421deecd5d99e0e01ec72590a8e767a5c417441218d955b6b141708d196852cede532d9ad9aec39e257e366ccd9cef8bd95264683ac3c87301e7d8adccb8df3d7b92709f35290e4c0c7fc458e8faa92ddb825716999a724fa967f5e7319533900a8d0998d68fc09870ac46f60fcab23ad997c7b8c95ee6ba67cb51e2929e807538b68edcecba678c988b3cd5238195bce60804161c0bcf8e089c050678f02957a3904f5d3f42a4577789d2308209cd4a19f297131c612a824fefbe98082fea035581528e64b23e0b0d9d1accbad1a7cdf37520e92ff3a53aab0d851bc58b816e1a182f714a5602659fadb600cd3e9398a8d0283e5f18a565fad18f01f566c700995dfeb12bcd69e4627982f4da7ca45effad0251de1713e1f55bcf2d20304b428186034519f3afe662042f784b5e74cb709e1ab13c8d975124384e316fc9b7ab945745a702c6261bb9f25558496ece809d8e4b2f71b1476d0caa432131a96d80665579b15e9ba82fe9656418a54ee53a54732a50334a9624860a9f06ec609a8145f4d771aa5dcc42fa7241bdb62443fbf4c66263b3af65ae0a785d866a8f2aa671651de32891a4c6ac76a08c2d5214d0a5efaadac44ed6272efac98ea6a13c9ca0288212abd660ce47ac69d65d1eff385dbeff1a643c1c97b5b4614b6d4b493c8cd11c4b5fb35b96333f665c34b7ef912851c75ea04cab3cb5c66515721467acee59e1c90a12f173def62bbaec3587ad06b39cee0cd6138870985e66b54146a54d7bfb88a0b8e5f26c1c9224a0fad0aca7ce6ea9eaf4d9883987aab15ae7528d9a736a744eebcc1a1fccf9d5f3843abbaefc419c056993940c43818192a035816504bb5ef76c02bd66805f1e7e3df4cf03c1dafd5d9d496dd89c57de92abf3c987cc791425789d89ae2cef5955512729ebd215502f0abf7550b722d8368db117b25e159c57b3a02ee530b0a29ef4b5068caa3952e7528d2ee5e055bf510e6e191d4ab2254f29a5974997945c0df344a7ff499f92f964536c0b0cf4e87b22ebb988e6ec0f528d4c10f5671093b32b74fbfde0dde09df420f0f53cce7318f3484b6a68f842ef1ceaffc5c8fecbfa7bfb1d4c9ff29f6cabb749b9e4938f7a3331bb74a818acf2fa6d151e2d0fdb5661a3be59cb4ca6df454177689dff765791ee58973180fad0da853dee36e6c6de647d7da8521e446562d67906759f36d9964344f34d94311a32b22590a03140f32595b1f81aed73a1f12b04e1269ec4bc751b6cdebdcbef4d56f4cb4bba02dd5eaeddd6de6e8b59738dbbf506a1a68c7ade06f5d7dc947ed1f6f3cb6269b9fddd3aa0de5413b98a44db6eecae8ce90da1a8e35ae2f95ac26fa705acf61f3497456ac9dad5e5cd6af9911da5afde60c3b300dcd113eb6c04f7f78135027b1a716d42b5dde20e11fd9cabb3d6d14ad648d0b7cdb6d96c561a9ce6dd647d6ff40d7592dbdba2afce081beb0fcb886dded41a3583cd4cb699e6af7aef08beb2f61f8ec159337f5eb110fffd9862b7e125cb39e77416160e5bd2a89822551e13cc91072ef428e6fe0cb91c3ebb98b1f4c57cfe9cf634b8c7de39bd4c78947058320eee49e3a9bf70fa5df2d30e7753e7e1657a97c536b10450d38725e04b7a9cf8c42bf53e6ba99a342c4434c94b4ab1975c9496f3a792d345480d19e5e62b83e00d0e2202ccd8259da247bc8a6eb70c7fc673e43e15b7c97a26fd1bd134fbf0c447734037cb7954f4f02b60d80b161ffe07fc0fd930773f0000', '6.2.0-61023');
INSERT INTO public."__MigrationHistory" VALUES ('201903161839504_updRuleParameter_2134', 'DB.EFData.Migrations.Configuration', '\x1f8b0800000000000400ed5bdb6edc36107d2fd07f10f458b82b5f8aa0317613f85a18c9daaed70efa66d0d2ec5a2845a922e5da28fa657de827f5173a5add4951e25ebcb68320406253e4cc7078e6c661fefbe7dfe1c7c7805a0f10733f64237b67b06d5bc0dcd0f3d96c642762fae3cff6c70fdf7f373cf18247eb4b316f2f9d872b191fd9f74244fb8ec3dd7b08081f04be1b873c9c8a811b060ef14267777bfbbdb3b3e30092b09196650daf1226fc00e6bfe0af472173211209a1e3d003caf371fc329953b5ce49003c222e8cece3c3c1c9e93111c4b60ea84f508009d0a96d11c64241048ab77fc36122e290cd26110e107afd1401ce9b12ca21177bbf9a6eba83eddd74074eb5b020e5265c84c1820477f6729538f2f2a5146b972a43a59da072c553baebb9e246f62589f15f01b16dc9dcf68f689cce44c5e2b0cf06d9090ce6347ce0834f2cfc9382378343c2615052dab21af3b74a6c2084d23f5bd651424512c38841226242b7accbe48efaee2778ba0e7f07366209a575b15170fcd618c0a1cb388c20164f5730cd3773e6d996d35ce7c80bcb65b535d92ecf98d8dbb5ad73644eee2894a8a8696422c2187e01063111e05d1281db65290d98eb55e12ef14aff2eb8210cd1906c6b4c1e3f039b89fb918d3fdad6a9ff085e31924b70c37cb43b5c24e204fa98fc9a00cfc0f2cc8c8e81bbb11f6d84d71712fbe9a1f49f9644e79c3cf8b3f9e14914af120a2564b96d5d019d4fe3f77e94398e0ad1b7f2e4d3380cae425ab71f69ceed244c62373dedb067e235896720cc052f54d1237235ad4dd8e26b8798e5943601874ee5493afd4b63b32bfb9806b56f7e46c76baea2945969972536b2d13eab69b195852daf4da6f4d8d667c2355caddb7a0bccf75a6f613f8bb89d56711b746fb36995a0ea57c5725ba6ac6cb96b31d86f766a068b9e68b43040640c776068298054116645901484be0145c76b2389e311cedc08233c6f25389563ab4783be64ce3033d28700257932339f2490ebae6cc767fc9492595585aea30e4be7afc99cf06c3d88e91362a10ef7e6a98c21b883b80412f3fcac2cf842688223dbca3136165c014749cad93baa4633dd75e8b3c4cf9ab599d37d315d1e8621d529469e8b3807b752e36ef7ec71227cde5cb167a2f803ce43d79febb22d786591a9c9f78479567f1e533903a96818a36afd0895896635b27f50b6d549be34e3267999ea8e2d47890b768cda11601db8d9d5cd11e12ef1548f845af29a231858204e3d3ba1680b1cf1e033a146219fb97e4468afecd24ac308964a56f290bf1c43042c0d3ebde762c2bca80f54014a3e92cafa3434746a30eb469f36cfd781a4bf64976aa63614ae152f06b26960bc80952c05ca3e5d6d009a7d2a3111a1517cbe304acbf4a31f03ea2d8d0132bbfc63579ad3c8c5f204e9b5f948bdfc1b05a27c2e26cceb37981b40609684e21a812bca987f394b47e051b4e445371cf2d488e769bc8ca294e204444bbe5da5bc2a3a153036c9c895af424a727306e474547a171767d446a0320a8948ed0034bbca8bf5da447d492f23c328752af7a1aa53819a51b2241154e8346087d30c34a2bf8e53f56216d2170beab52d19eaa737161bab7d257555c0eb52547b54318d2bab284789243562358332d648514097beab6af93a59cfb7e80d3b9ae6f0704ca2084bac5ab3381fb12659a7f8e8c7c9e2bdd420a3e1b8bca5a55a4a5b7212614c66207dcd6e594efd988bb4517d47d212f5c80b946995a7d6b8ac828fe28cd5332b3c59b124fd396fe1151df39ac3568359beee14f713a4e1707e99d5061975edbc4f4f28895b2ecf8e429a044c1f9af5abb35baafafa6cc49c42d524ad53a946cd2935baa075628d0fe6f4ea79429d5c57fe90da8274484a86a1c04049d09ac032825daf7b36815e33c02f0ebf9ef5cf03c1dafd5d9d486dd89c56513ed709e94aea1e898a1a47914957fcbc28745641cc924059373e5e487b55605d4e83ba74c1408bfaa5afd5d9578d8d3a956a7421e7acda7c39b861742889923ca5e45e264c526234cc9394fea7754ad6924db12d54d083efa519cb7934e37fd06a644c983fc5789a5d7fdbef07ef06efa48779afe7919cc3b9475bd23ac397726758bb3f8eecbfacbf37df7df499f8c9b67a1b8c0b3ed7a83702b30b838ac032afd096a1d1f2c06c1932eadbb14c65fa534cd7ed5b67bfdd564bb7ac8b1841bd6f6de319772b736d6fa3be3e54290f9332362b3d47323ecc6cd90207a991bff988c9987d6ded7381e92bc4d03a5ea3bc751dacdf3bcb4f3d9674ab0b5ab2ee2c57ee286fb6bbabb941dd786f4e53053d6f6ff86bee07bf68e7f765b1b4d8f96e1c506faa7f5b45a24df75497c6f49a50d471abf07cddd8b7d37d55affe35773d6ac5d9d560cd4af1911dcd1f9ce1816701b8a31dd5d983ed6fc16a18f6f4c0da986a1bb51d2cfa2957b6d6d1c5d570d077ac36d9e7557a8be68d5c7d5bf20d3571db3b92af4e096b6bcdca886d5eb41af561cd54b69ebeab7a6d88beb2f6ff76d159737f569148ff172f03b7e125cb39676c1a160e5b92a89822551e6310c443177a100b7f4a5c819f5de07cfe583d7fc97a12dc8177c62e12112502b70cc11d6dbcb24f9d7e17ff7973b929f3f0627e15c5d7b10514d3c72dc0053b4c7cea95729fb6544d1a126934c94bcaf42c455a5ace9e4a4ae721332494abaf0c82d710441489f10b36210fb08c6c371c3ec38cb84fc565b09e48ff4134d53e3cf6c90cd1cd731ad57afc1531ec058f1ffe072467776bbe3e0000', '6.2.0-61023');


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 185
-- Name: Parameters_Id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Parameters_Id_seq"', 33, true);


--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 187
-- Name: RuleParameters_Id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RuleParameters_Id_seq"', 49, true);


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 189
-- Name: Rules_Id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Rules_Id_seq"', 12, true);


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 191
-- Name: Variables_Id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Variables_Id_seq"', 14, true);


--
-- TOC entry 2031 (class 2606 OID 367398)
-- Name: Parameters PK_public.Parameters; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Parameters"
    ADD CONSTRAINT "PK_public.Parameters" PRIMARY KEY ("Id");


--
-- TOC entry 2034 (class 2606 OID 367407)
-- Name: RuleParameters PK_public.RuleParameters; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RuleParameters"
    ADD CONSTRAINT "PK_public.RuleParameters" PRIMARY KEY ("Id");


--
-- TOC entry 2038 (class 2606 OID 367417)
-- Name: Rules PK_public.Rules; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Rules"
    ADD CONSTRAINT "PK_public.Rules" PRIMARY KEY ("Id");


--
-- TOC entry 2040 (class 2606 OID 367428)
-- Name: Variables PK_public.Variables; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Variables"
    ADD CONSTRAINT "PK_public.Variables" PRIMARY KEY ("Id");


--
-- TOC entry 2042 (class 2606 OID 367451)
-- Name: __MigrationHistory PK_public.__MigrationHistory; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."__MigrationHistory"
    ADD CONSTRAINT "PK_public.__MigrationHistory" PRIMARY KEY ("MigrationId", "ContextKey");


--
-- TOC entry 2032 (class 1259 OID 367399)
-- Name: Parameters_IX_VariableId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "Parameters_IX_VariableId" ON public."Parameters" USING btree ("VariableId");


--
-- TOC entry 2035 (class 1259 OID 367409)
-- Name: RuleParameters_IX_ParameterId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RuleParameters_IX_ParameterId" ON public."RuleParameters" USING btree ("ParameterId");


--
-- TOC entry 2036 (class 1259 OID 367408)
-- Name: RuleParameters_IX_RuleId; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX "RuleParameters_IX_RuleId" ON public."RuleParameters" USING btree ("RuleId");


--
-- TOC entry 2043 (class 2606 OID 367429)
-- Name: Parameters FK_public.Parameters_public.Variables_VariableId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Parameters"
    ADD CONSTRAINT "FK_public.Parameters_public.Variables_VariableId" FOREIGN KEY ("VariableId") REFERENCES public."Variables"("Id") ON DELETE CASCADE;


--
-- TOC entry 2045 (class 2606 OID 367439)
-- Name: RuleParameters FK_public.RuleParameters_public.Parameters_ParameterId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RuleParameters"
    ADD CONSTRAINT "FK_public.RuleParameters_public.Parameters_ParameterId" FOREIGN KEY ("ParameterId") REFERENCES public."Parameters"("Id") ON DELETE CASCADE;


--
-- TOC entry 2044 (class 2606 OID 367434)
-- Name: RuleParameters FK_public.RuleParameters_public.Rules_RuleId; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RuleParameters"
    ADD CONSTRAINT "FK_public.RuleParameters_public.Rules_RuleId" FOREIGN KEY ("RuleId") REFERENCES public."Rules"("Id") ON DELETE CASCADE;


-- Completed on 2019-03-16 22:32:57

--
-- PostgreSQL database dump complete
--

