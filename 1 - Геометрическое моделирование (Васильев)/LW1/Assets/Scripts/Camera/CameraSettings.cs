﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettings : MonoBehaviour {

    [SerializeField]
    Vector3 startPosition;
    [SerializeField]
    Quaternion startRotation;

    private void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;

        GlobalEventSystem.Instance.AddEventListener(EventType.CameraProjectionChange, OnEvent);
    }

    private void OnEvent(EventType eventType, Component component, object param)
    {
        switch(eventType)
        {
            case EventType.CameraProjectionChange:
                Camera.main.orthographic = !Camera.main.orthographic;
                if(Camera.main.orthographic)
                {
                    transform.position = startPosition;
                    transform.rotation = startRotation;
                }
                break;
        }
    }
}
