﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Unity;
using WPF.Controls.FuzzyStatement;
using WPF.Helpers;
using WPF.Modules.Base;
using WPF.ViewModels.Base;
using WPF.ViewModels.FuzzyStatement;

namespace WPF.Modules.FuzzyStatement
{
    class FuzzyStatementModule : BaseModule, IDisposable
    {
        public FuzzyStatementModule(IUnityContainer container, SettingType type) : base(container, type) { }

        public override string Name => "Нечеткие высказывания";

        protected override UserControl View { get; set; }
        protected override BaseViewModel ViewModel { get; set; }

        protected override void CreateViewAndViewModel()
        {
            View = new FuzzyStatementPage();
            ViewModel = container.Resolve<FuzzyStatementViewModel>();
        }

        public void Dispose() { }
    }
}
