﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface ICinemaRoomList
    {
        CinemaRoom GetCinemaRoom(int sessionID);
        IEnumerable<CinemaRoom> CinemaRoomList();
    }
}
