﻿using DB.EFData;
using Domain.Models.Repositories.Interfaces;
using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DB.EF.Data.Repositories.Implementations
{
    class DefaultEntityRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly PgContext context;

        public DefaultEntityRepository(PgContext context)
        {
            this.context = context;
        }

        protected DbSet<TEntity> Set => context.Set<TEntity>();

        public async Task Remove(TEntity entity)
        {
            Set.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task Remove(int id)
        {
            var existing = await GetByIdAsync(id);
            if (existing == null) return;

            await Remove(existing);
        }

        public async Task RemoveRange(IEnumerable<int> ids)
        {
            var existings = await GetRaw().Where(x => ids.Any(id => id == x.Id)).ToListAsync();
            if (existings == null) return;

            await RemoveRange(existings);
        }

        public async Task RemoveRange(IEnumerable<TEntity> existings)
        {
            Set.RemoveRange(existings);
            await context.SaveChangesAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await Set.FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual IQueryable<TEntity> GetRaw()
        {
            return Set.AsQueryable();
        }

        public virtual async Task UpdateOrInsertAsync(TEntity entity)
        {
            if (entity.Id == 0)
                Set.Add(entity);
            else if (!Set.Local.Any(x => x.Id == entity.Id))
                Set.Attach(entity);

            await context.SaveChangesAsync();
        }

        public void SetDetached(Object some)
        {
            context.Entry(some).State = EntityState.Detached;
        }

        public void SetModified(Object some)
        {
            context.Entry(some).State = EntityState.Modified;
        }

        public async Task<IEnumerable<TEntity>> GetListAsync()
        {
            return await Set.ToListAsync();
        }

        public bool AnyCheck()
        {
            return Set.Any();
        }
    }
}
