﻿using Assets.Scripts.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BSpline : MethodItem
{
    protected override void Init()
    {
        qualityStep = 0.05f;
        step = 0.1f;

        GlobalEventSystem.Instance.AddEventListener(EventType.ChangeMethodType, OtherOnEvent);
    }

    private void OtherOnEvent(EventType eventType, Component component, object param)
    {
        switch (eventType)
        {
            case EventType.ChangeMethodType:
                MethodType = (MethodType)param;
                ((GuiManager)component).ShowMessage((MethodType == MethodType.type1) ? "B-сплайны: Квадратичные" : "B-сплайны: Кубические");
                break;
        }
    }

    protected override List<Vector3> CalcMethod(List<MainPoint> mainPoints)
    {
        var result = new List<Vector3>();
        var points = mainPoints.Select(x => x.transform.position);
        if (points.Count() <= 1) return result;
        
        var newPoints = new List<Vector3>();
        var additionalPointCount = (MethodType == MethodType.type1) ? 2 : 3;

        var firstPoint = points.First();
        var lastPoint = points.Last();

        for (var i = 0; i < additionalPointCount; i++) newPoints.Add(firstPoint);
        newPoints.AddRange(points);
        for (var i = 0; i < additionalPointCount; i++) newPoints.Add(lastPoint);

        for (var i = additionalPointCount; i < newPoints.Count(); i++)
        {
            var p0 = (MethodType == MethodType.type1) ? newPoints.ElementAt(i - 2) : newPoints.ElementAt(i - 3);
            var p1 = (MethodType == MethodType.type1) ? newPoints.ElementAt(i - 1) : newPoints.ElementAt(i - 2);
            var p2 = (MethodType == MethodType.type1) ? newPoints.ElementAt(i) : newPoints.ElementAt(i - 1);
            var p3 = (MethodType == MethodType.type1) ? new Vector3() : newPoints.ElementAt(i);

            for (var u = 1f; u >= 0f; u -= Step)
            {
                var x = 0f;
                var y = 0f;
                var z = 0f;
                if (MethodType == MethodType.type1)
                {
                    x = Quad(u, p0.x, p1.x, p2.x);
                    y = Quad(u, p0.y, p1.y, p2.y);
                    z = Quad(u, p0.z, p1.z, p2.z);
                }
                else
                {
                    x = Cubic(u, p0.x, p1.x, p2.x, p3.x);
                    y = Cubic(u, p0.y, p1.y, p2.y, p3.y);
                    z = Cubic(u, p0.z, p1.z, p2.z, p3.z);
                }
                result.Add(new Vector3(x, y, z));
            }
        }

        return result;
    }

    public float Quad(float u, float p0, float p1, float p2)
    {
        var result = ((0.5f * Math.Pow(u, 2)) * p0) +
                     ((0.75f - Math.Pow((u - 0.5f), 2)) * p1) +
                     ((0.5f * Math.Pow((u - 1f), 2)) * p2);
        return Convert.ToSingle(result);
    }

    public float Cubic(float u, float p0, float p1, float p2, float p3)
    {
        var pow3 = Math.Pow(u, 3);
        var result = ((0.166667f * pow3) * p0) +
                     ((0.666667f - (0.5f * Math.Pow((u - 1), 3)) - (Math.Pow((u - 1), 2))) * p1) +
                     ((0.666667f + (0.5f * pow3) - Math.Pow(u, 2)) * p2) +
                     ((0.166667f * Math.Pow((1 - u), 3)) * p3);
        return Convert.ToSingle(result);
    }
}