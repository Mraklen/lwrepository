﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Data;

namespace CinemaNet.Models.Repositories
{
    public class EFReserveList : IReserveList
    {
        private ApplicationDbContext context;
        public EFReserveList(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IEnumerable<Reserve> ReserveList => context.Reserves
            .Include(r => r.FilmSession).Include(r => r.ReserveSeats);

        public void AddReserve(Reserve reserve)
        {
            context.Reserves.Add(reserve);
            context.SaveChanges();
        }

        public IEnumerable<Reserve> GetReserveByEmail(string email) => context.Reserves
            .Where(x => x.Email == email)
            .Select(x => new Reserve()
            {
                Email = x.Email,
                FilmSession = x.FilmSession,
                FilmSessionID = x.FilmSessionID,
                ReserveID = x.ReserveID,
                ReserveSeats = x.ReserveSeats.Select(r => r).ToList()
            }).ToList();
    }
}
