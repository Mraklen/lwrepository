﻿using MimeKit;
using MailKit.Security;
using MimeKit.Text;
using MailKit.Net.Smtp;
using System;
using System.Configuration;
using System.Text;
using System.IO;

namespace SecondTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Write("Введите адрес получателя: ");
            var email = Console.ReadLine();

            var file = "F:\\Repositories\\LabWork\\2 - Сети и телекоммуникации (Галыгина)\\LW5\\SecondTask\\SecondTask\\bin\\Debug\\EmailContent\\Email.txt";
            using (var stream = File.OpenRead(file))
            {
                byte[] array = new byte[stream.Length];
                stream.Read(array, 0, array.Length);
                var message = Encoding.UTF8.GetString(array);

                MyEmailSendler.SendEmail(email, "Лабораторная работа№5. Задание 2", message);
                Console.Write("\nОтправка завершена! \n...нажмите любую кнопку");
            }
            
            Console.ReadKey();
        }
    }

    public class MyEmailSendler
    {
        public static void SendEmail(string email, string subject, string message)
        {
            var mailUserMail = ConfigurationManager.AppSettings["MailUserEmail"];
            var mailUserPass = ConfigurationManager.AppSettings["MailUserPass"];
            var mailServerHost = ConfigurationManager.AppSettings["MailServerHost"];
            var mailServerPort = ConfigurationManager.AppSettings["MailServerPort"];

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("", mailUserMail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(TextFormat.Text)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect(mailServerHost, Convert.ToInt32(mailServerPort), SecureSocketOptions.SslOnConnect);
                client.Authenticate(mailUserMail, mailUserPass);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }
    }
}
