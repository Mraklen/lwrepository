﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.Managers
{
    public class FileManager
    {
        public static string SaveFile(byte[] fileContent, string fileName)
        {
            var dir = $"{Application.StartupPath}\\Files\\";
            var fullName = $"{dir}{fileName}";
            Directory.CreateDirectory(dir);
            using (var stream = new FileStream(fullName, FileMode.OpenOrCreate))
            {
                stream.Write(fileContent, 0, fileContent.Length);
            }
            return fullName;
        }

        public static byte[] LoadFile()
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = $"{Application.StartupPath}\\Files";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            var bytes = new byte[myStream.Length];
                            myStream.Read(bytes, 0, bytes.Length);
                            return bytes;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            return new byte[0];
        }

        public static string GetFileName()
        {
            var result = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = $"{Application.StartupPath}\\Files";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                result = openFileDialog1.FileName;
            }

            return result;
        }
    }
}
