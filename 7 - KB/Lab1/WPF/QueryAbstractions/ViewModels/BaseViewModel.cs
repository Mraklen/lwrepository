﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryAbstractions.ViewModels
{
    public abstract class BaseViewModel<TID> where TID : struct
    {
        public TID? Id { get; set; }
    }
}
