﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class Reserve
    {
        public int ReserveID { get; set; }
        public string Email { get; set; }
        public string ReserveDate { get; set; }
        
        public int FilmSessionID { get; set; }
        public FilmSession FilmSession { get; set; }
        
        public List<ReserveSeat> ReserveSeats { get; set; }
    }
}
