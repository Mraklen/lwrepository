﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW3.Huffman.Tree
{
    public class TreeItem
    {
        public string Value { get; set; }
        public uint Weight { get; set; }
        public bool isRight { get; set; }
        public bool isRoot { get; set; }
        public TreeItem RightBranch { get; set; }
        public TreeItem LeftBranch { get; set; }

        public TreeItem Clone()
        {
            return new TreeItem()
            {
                Value = Value,
                Weight = Weight,
                LeftBranch = LeftBranch,
                RightBranch = RightBranch
            };
        }

        public string GetCode(char elem)
        {
            var itemCode = isRoot ? "" : (isRight ? "0" : "1");

            if (RightBranch == null && LeftBranch == null) return itemCode;

            var rightCheck = RightBranch.Value.Contains(elem);
            var leftCheck = LeftBranch.Value.Contains(elem);
            
            if (!rightCheck && !leftCheck) return null;
            
            return (rightCheck) ? $"{itemCode}{RightBranch.GetCode(elem)}" : ((leftCheck) ? $"{itemCode}{LeftBranch.GetCode(elem)}" : null); 
        }
    }
}
