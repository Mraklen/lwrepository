﻿namespace LW5.RSA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_p = new System.Windows.Forms.TextBox();
            this.txt_q = new System.Windows.Forms.TextBox();
            this.btEncrypt = new System.Windows.Forms.Button();
            this.txt_n = new System.Windows.Forms.TextBox();
            this.txt_d = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btDecrypt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "p = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(207, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "q = ";
            // 
            // txt_p
            // 
            this.txt_p.Location = new System.Drawing.Point(71, 55);
            this.txt_p.Name = "txt_p";
            this.txt_p.Size = new System.Drawing.Size(130, 29);
            this.txt_p.TabIndex = 3;
            this.txt_p.Text = "41";
            // 
            // txt_q
            // 
            this.txt_q.Location = new System.Drawing.Point(266, 55);
            this.txt_q.Name = "txt_q";
            this.txt_q.Size = new System.Drawing.Size(127, 29);
            this.txt_q.TabIndex = 4;
            this.txt_q.Text = "43";
            // 
            // btEncrypt
            // 
            this.btEncrypt.Location = new System.Drawing.Point(16, 195);
            this.btEncrypt.Name = "btEncrypt";
            this.btEncrypt.Size = new System.Drawing.Size(189, 62);
            this.btEncrypt.TabIndex = 5;
            this.btEncrypt.Text = "Зашифровать";
            this.btEncrypt.UseVisualStyleBackColor = true;
            this.btEncrypt.Click += new System.EventHandler(this.btEncrypt_Click);
            // 
            // txt_n
            // 
            this.txt_n.Location = new System.Drawing.Point(266, 139);
            this.txt_n.Name = "txt_n";
            this.txt_n.Size = new System.Drawing.Size(127, 29);
            this.txt_n.TabIndex = 10;
            // 
            // txt_d
            // 
            this.txt_d.Location = new System.Drawing.Point(71, 139);
            this.txt_d.Name = "txt_d";
            this.txt_d.Size = new System.Drawing.Size(130, 29);
            this.txt_d.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(207, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "n = ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 24);
            this.label5.TabIndex = 7;
            this.label5.Text = "d = ";
            // 
            // btDecrypt
            // 
            this.btDecrypt.Location = new System.Drawing.Point(211, 195);
            this.btDecrypt.Name = "btDecrypt";
            this.btDecrypt.Size = new System.Drawing.Size(182, 62);
            this.btDecrypt.TabIndex = 11;
            this.btDecrypt.Text = "Расшифровать";
            this.btDecrypt.UseVisualStyleBackColor = true;
            this.btDecrypt.Click += new System.EventHandler(this.btDecrypt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(133, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Открытые ключи";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(133, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 24);
            this.label6.TabIndex = 13;
            this.label6.Text = "Закрытые ключи";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 266);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btDecrypt);
            this.Controls.Add(this.txt_n);
            this.Controls.Add(this.txt_d);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btEncrypt);
            this.Controls.Add(this.txt_q);
            this.Controls.Add(this.txt_p);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "Form1";
            this.Text = "RSA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_p;
        private System.Windows.Forms.TextBox txt_q;
        private System.Windows.Forms.Button btEncrypt;
        private System.Windows.Forms.TextBox txt_n;
        private System.Windows.Forms.TextBox txt_d;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btDecrypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
    }
}

