﻿using LW1.Data.Repositories.Interfaces;
using LW1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories
{
    public interface IDomainRepositoryFactory
    {
        IBaseAggregateRepository<TAggregate> GetAggregateRepository<TAggregate>() where TAggregate : class, IAggregate;
        IBaseRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class, IEntity<int>;

        IProjectRepository GetProjectRepository();
    }
}
