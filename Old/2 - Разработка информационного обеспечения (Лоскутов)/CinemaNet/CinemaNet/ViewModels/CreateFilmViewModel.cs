﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.ViewModels
{
    public class CreateFilmViewModel
    {
        public string Name { get; set; }
        public string Production { get; set; }
        public string Premiere { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Duration { get; set; }
        public string Story { get; set; }
        public string Image { get; set; }
    }
}
