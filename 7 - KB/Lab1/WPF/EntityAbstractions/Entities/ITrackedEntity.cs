﻿using System;

namespace EntityAbstractions.Entities
{
    public interface ITrackedEntity
    {
        DateTime CreationDate { get; }
    }
}
