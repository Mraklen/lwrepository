﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Methods;
using System;

public class Interpolation : MethodItem
{
    protected override void Init()
    {
        qualityStep = 0.1f;
        step = 0.5f;
    }

    protected override List<Vector3> CalcMethod(List<MainPoint> mainPoints)
    {
        var points = mainPoints.Select(x => x.transform.position);
        if (CheckEqualsXCoordinates(points))
        {
            var result = new List<Vector3>();

            var firstX = points.Min(x => x.x);
            var lastX = points.Max(x => x.x);
            
            result.Add(points.SingleOrDefault(x => x.x == firstX));
            for (var currentX = firstX; currentX < lastX; currentX += Step)
            {
                var y = 0f;

                for (var i = 0; i < points.Count(); i++)
                {
                    var mult = 1f;

                    for (var j = 0; j < points.Count(); j++)
                    {
                        if (i != j) mult *= (currentX - points.ElementAt(j).x) / (points.ElementAt(i).x - points.ElementAt(j).x);
                    }

                    y += points.ElementAt(i).y * mult;
                }

                result.Add(new Vector3(currentX, y));
            }
            result.Add(points.SingleOrDefault(x => x.x == lastX));

            return result;
        }

        return new List<Vector3>();
    }

    private bool CheckEqualsXCoordinates(IEnumerable<Vector3> mainPoints)
    {
        var distinctPoints = mainPoints.Select(x => x.x).Distinct().ToList();
        return (distinctPoints.Count() == mainPoints.Count());
    }
}
