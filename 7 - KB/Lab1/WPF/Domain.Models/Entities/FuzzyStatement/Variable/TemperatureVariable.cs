﻿using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using System;

namespace Domain.Models.Entities.FuzzyStatement.Variable
{
    public class TemperatureVariable : BaseVariable
    {
        public override string CodeName => "T";

        public override string Name => "Температура процесса";

        public override VarType Type { get; set; }
        public override ModType Mod { get; set; }

        public override double CalcSmallTermSet(double value)
        {
            var result = (1.0 / 10000.0) * Math.Pow(value - 200, 2.0);
            return SetMode(result);
        }

        public override double CalcAverageTermSet(double value)
        {
            var result = 1 - (1.0 / 1000.0) * Math.Pow(125 - value, 2.0);
            return SetMode(result);
        }

        public override double CalcLargeTermSet(double value)
        {
            var result = (1.0 / 10000.0) * Math.Pow(value - 50, 2.0);
            return SetMode(result);
        }
    }
}
