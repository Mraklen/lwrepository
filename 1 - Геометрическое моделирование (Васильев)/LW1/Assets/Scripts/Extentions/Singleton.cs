﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

    private static T instance;
    public static T Instance { get { return instance; } }
    
    public abstract void AwakeInit();
    public abstract void StartInit();
    public abstract void UpdateInit();

    private void Awake()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<T>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(this);
        }

        AwakeInit();
    }
    private void Start() { StartInit(); }
    private void Update() { UpdateInit(); }
}
