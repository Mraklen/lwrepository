﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LW1.Methods
{
    public class CalcStaticMethods
    {
        static double Tp = 90;
        static double r = (2.26 * Math.Pow(10, 6));
        static double kt = 5000;
        static double F = 10;
        static double ct = 4187;

        public static IEnumerable<Point> CalcInputConcentration(double C0, double C1, double step, double const_m, double const_T)
        {
            List<Point> ponts = new List<Point>();

            for (var curr_C = C0; curr_C < C1; curr_C += step)
            {
                ponts.Add(new Point() { X = curr_C, Y = CalcOutputConcentration(const_T, const_m, curr_C) });
            }

            return ponts;
        }

        public static IEnumerable<Point> CalcInputMass(double m0, double m1, double step, double const_C, double const_T)
        {
            List<Point> ponts = new List<Point>();

            for (var curr_m = m0; curr_m < m1; curr_m += step)
            {
                ponts.Add(new Point() { X = curr_m, Y = CalcOutputConcentration(const_T, curr_m, const_C) });
            }

            return ponts;
        }

        public static IEnumerable<Point> CalcInputTemperature(double T0, double T1, double step, double const_C, double const_m)
        {
            List<Point> ponts = new List<Point>();

            for (var curr_T = T0; curr_T < T1; curr_T += step)
            {
                ponts.Add(new Point() { X = curr_T, Y = CalcOutputConcentration(curr_T, const_m, const_C) });
            }

            return ponts;
        }

        private static double CalcOutputConcentration(double tempereture, double mass, double concentration)
        {
            var tmp1 = mass * concentration;

            var tmp2 = kt * F * (tempereture - Tp);
            var tmp3 = r - (ct * Tp);

            var tmp4 = mass - (tmp2 / tmp3);

            return Math.Round(tmp1 / tmp4, 2);
        }

        public static double CalcMOut(double m_input, double C_input, double C_out)
        {
            return (m_input * C_input) / C_out;
        }

        public static double CalcMvt(double m_input, double m_out)
        {
            return m_input - m_out;
        }
    }
}
