﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingTarget : MonoBehaviour {

    [SerializeField] GameObject Traget;

    private void Update()
    {
        transform.LookAt(Traget.transform);
    }

}
