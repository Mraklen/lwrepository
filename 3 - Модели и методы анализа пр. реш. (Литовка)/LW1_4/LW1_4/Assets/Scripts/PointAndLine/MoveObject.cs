﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public static Point SelectedObject = null;
    
    [Space(10)]
    [SerializeField]
    Material DefaultPointMaterial;
    [SerializeField]
    Material DefaultVectorPointMaterial;

    [Space(10)]
    [SerializeField]
    Material SelectPointMaterial;
    [SerializeField]
    Material SelectVectorPointMaterial;

    [Space(10)]
    [SerializeField]
    Material MainPointsMaterial;

    [Space(10)]
    [SerializeField]
    GameObject Axis;

    Transform SelectedObjectTransform;

    Vector3 transfer;

    private void Start()
    {
        Axis = Instantiate(Resources.Load<GameObject>("Prefabs/AxisMove"));
        Axis.SetActive(false);
        
        GlobalEventSystem.Instance.AddEventListener(EventType.ChangeMethod, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.LastPointChange, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.AxisMoveShow, OnEvent);
    }

    private void OnEvent(EventType eventType, Component component, object param)
    {
        switch (eventType)
        {
            case EventType.LastPointChange:
                SelectNewObject((GameObject)param);
                break;
            case EventType.AxisMoveShow:
                if (SelectedObject && !Camera.main.orthographic)
                {
                    Axis.transform.position = SelectedObject.transform.position;
                    Axis.SetActive(!Axis.activeInHierarchy);
                }
                break;
            case EventType.ChangeMethod:
                SelectNewObject(null);
                break;
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit))
            {
                var go = hit.collider.gameObject;

                if (go.gameObject.layer == 5) { SelectNewObject(null); }

                if (go.tag.Equals("Point") || go.tag.Equals("VectorPoint"))
                {
                    SelectNewObject(go);
                }
            }
        }

        if(Input.GetMouseButtonUp(0) && SelectedObject != null)
        {
            SelectedObject.isMove = false;
        }

        if (Input.GetMouseButton(0) && SelectedObject != null && Camera.main.orthographic)
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                SelectedObject.isMove = true;
                SelectedObjectTransform.position = ScreenSize.GetMousePosition();
                CoordinateCheck(SelectedObjectTransform);
            }
        }

        if (Input.GetMouseButton(0) && SelectedObject != null && !Camera.main.orthographic)
        {
            SelectedObject.isMove = true;
            SelectedObject.transform.position = Axis.transform.position;
        }

        if (Camera.main.orthographic && Axis.activeInHierarchy)
        {
            Axis.SetActive(false);
        }
    }

    private void SelectNewObject(GameObject newGameObject)
    {
        if (SelectedObject != null)
        {
            var selectedPoint = SelectedObject.GetComponent<Point>();
            selectedPoint.CurrentMaterial = (SelectedObject.tag.Equals("Point")) ? DefaultPointMaterial : DefaultVectorPointMaterial;
            selectedPoint.isSelect = false;
            selectedPoint.isMove = false;
        }

        if (newGameObject)
        {
            var newGamePoint = newGameObject.GetComponent<Point>();
            newGamePoint.CurrentMaterial = (newGamePoint.tag.Equals("Point")) ? SelectPointMaterial : SelectVectorPointMaterial;
            newGamePoint.isSelect = true;

            Axis.transform.position = newGamePoint.transform.position;

            SelectedObject = newGamePoint;
            SelectedObjectTransform = newGamePoint.transform;
        }
        else
        {
            SelectedObject = null;
            SelectedObjectTransform = null;
        }
    }

    private void CoordinateCheck(Transform transform)
    {
        var currRot = transform.rotation;
        var currX = transform.position.x;
        var currY = transform.position.y;

        if (currX > ScreenSize.xMaxWorld) currX = ScreenSize.xMaxWorld;
        if (currX < ScreenSize.xMinWorld) currX = ScreenSize.xMinWorld;
        if (currY > ScreenSize.yMaxWorld) currY = ScreenSize.yMaxWorld;
        if (currY < ScreenSize.yMinWorld) currY = ScreenSize.yMinWorld;

        transform.SetPositionAndRotation(new Vector3(currX, currY), currRot);
    }
}
