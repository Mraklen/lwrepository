﻿using Newtonsoft.Json;
using SeatBattle.CSharp.EventManager;
using SeatBattle.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeatBattle.CSharp.Net
{
    public class Server
    {
        private CancellationTokenSource CancelTokenSource;
        private CancellationToken token;

        Socket sListener;

        public static int myPort = 0;
        public static int enemyPort = 0;

        public static bool isMainServer = false;

        public Server()
        {
            CancelTokenSource = new CancellationTokenSource();
            token = CancelTokenSource.Token;
        }

        public void Stop()
        {   
            CancelTokenSource.Cancel();
        }

        public void Start()
        {
            Task.Run(() =>
            {
                var ipHost = Dns.GetHostEntry("localhost");
                var ipAddress = ipHost.AddressList[0];
                var iPEndPoint = new IPEndPoint(ipAddress, myPort);

                sListener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    sListener.Bind(iPEndPoint);
                    sListener.Listen(10);

                    do
                    {
                        Socket handler = sListener.Accept();

                        string data = null;
                        byte[] bytes = new byte[10240];
                        int bytesRec = handler.Receive(bytes);

                        // действие на запрос
                        data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                        var postPack = JsonConvert.DeserializeObject<PostPackage>(data);

                        switch (postPack.Type)
                        {
                            case PackageType.ConnectToServer:
                                enemyPort = JsonConvert.DeserializeObject<int>(postPack.Content);
                                handler.Shutdown(SocketShutdown.Both);
                                handler.Close();
                                sListener.Dispose();
                                GlobalEventManager.Instance.PostEventMessage(EVENT_TYPE.StartGame);
                                return;
                            case PackageType.FirstPlayer:
                                var playerName = postPack.Content;
                                GlobalEventManager.Instance.PostEventMessage(EVENT_TYPE.SetFirstPlayer, playerName);
                                break;
                            case PackageType.ReadyPlay:
                                var ships = JsonConvert.DeserializeObject<List<Ship>>(postPack.Content);
                                GlobalEventManager.Instance.PostEventMessage(EVENT_TYPE.EnemyReadyPlay, ships);
                                break;
                            case PackageType.PlayerShot:
                                var point = JsonConvert.DeserializeObject<Point>(postPack.Content);
                                GlobalEventManager.Instance.PostEventMessage(EVENT_TYPE.EnemyPlayerShot, point);
                                break;
                        }

                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                    while (!token.IsCancellationRequested);
                }
                catch (Exception ex) { }
            });
        }

        private void SendAnsver(Socket handler)
        {
            string reply = JsonConvert.SerializeObject(new PostPackage()
            {
                Type = PackageType.ConnectToServerResult,
                Content = JsonConvert.SerializeObject(true)
            });
            byte[] msg = Encoding.UTF8.GetBytes(reply);
            handler.Send(msg);
        }
    }
}
