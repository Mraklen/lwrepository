﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.Models
{
    public class FunctionParam
    {
        public static double X1Value { get; private set; }
        public static double X2Value { get; private set; }
        public static double HValue { get; private set; }
        public static double SValue { get; private set; }

        const int width = 20;
        const int height = 20;
        const int x1Value = -10;
        const int x2Value = -10;

        public static IsolineType CurrenIsolineType { get; set; } = IsolineType.Ellipse;

        private Dictionary<IsolineType, IsolineData> IsolinesData = new Dictionary<IsolineType, IsolineData>()
        {
            { IsolineType.Ellipse, new IsolineData() },
            { IsolineType.Rosenbroke, new IsolineData() }
        };

        public FunctionParam(double x1, double x2, double h, double s)
        {
            X1Value = x1;
            X2Value = x2;
            HValue = h;
            SValue = s;
            CalcEllipseData();
            CalcRosenbrokeData();
        }


        public double CalcFunction(double x1, double x2)
        {
            switch (CurrenIsolineType)
            {
                case IsolineType.Ellipse: return CalcEllipseFuntion(x1, x2);
                case IsolineType.Rosenbroke: return CalcRosenbrokeFunction(x1, x2);
            }
            return 0;
        }

        public IsolineData GetIsolineData()
        {
            return IsolinesData[CurrenIsolineType];
        }

        private void CalcEllipseData()
        {
            var data = IsolinesData[IsolineType.Ellipse];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcEllipseFuntion(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);
        }

        private void CalcRosenbrokeData()
        {
            var data = IsolinesData[IsolineType.Rosenbroke];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcRosenbrokeFunction(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);
        }

        private double CalcEllipseFuntion(double x1, double x2)
        {
            return (Math.Pow(((x1 - (-2)) * Math.Cos(35) + (x2 - 4) * Math.Sin(35)), 2) / (Math.Pow(2, 2))) + (Math.Pow(((x2 - 4) * Math.Cos(35) + (x1 - (-2)) * Math.Sin(35)), 2) / (Math.Pow(5, 2)));
        }

        private double CalcRosenbrokeFunction(double x1, double x2)
        {
            return 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2) + Math.Pow((1 - x1), 2);
        }
    }
}
