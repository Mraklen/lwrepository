﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisManager : MonoBehaviour
{
    [SerializeField]
    AxisArrow axisArrow = null;
    [Space(10)]
    [SerializeField]
    Vector3 screenPoint;
    [SerializeField]
    Vector3 offset;

    private void Update()
    {
        if (!axisArrow && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                var go = hit.collider.gameObject;
                if (go.tag.Equals("AxisArrow"))
                {
                    screenPoint = Camera.main.WorldToScreenPoint(go.transform.position);
                    offset = go.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

                    axisArrow = go.GetComponent<AxisArrow>();
                    axisArrow.SetMaterial(true);
                }
            }
        }

        if (axisArrow && Input.GetMouseButtonUp(0))
        {
            axisArrow.SetMaterial(false);
            axisArrow = null;
        }

        if (axisArrow && Input.GetMouseButton(0))
        {
            var cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            var cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
            var shiftPos = cursorPosition - axisArrow.transform.position;
            transform.position += CalcShift(axisArrow.arrowType, shiftPos);
        }
    }

    private Vector3 CalcShift(AxisType selectedType, Vector3 shiftPos)
    {
        switch(selectedType)
        {
            case AxisType.X: return new Vector3(shiftPos.x, 0, 0);
            case AxisType.Y: return new Vector3(0, shiftPos.y, 0);
            case AxisType.Z: return new Vector3(0, 0, shiftPos.z);
            default: return Vector3.zero;
        }
    }
}
