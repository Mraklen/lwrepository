﻿using LW1.Models.Project;
using LW1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories.Interfaces
{
    public interface IProjectRepository : IBaseAggregateRepository<Project>
    {
        ICollection<ProjectViewModel> GetProjectsByUserId(string userId);
        ICollection<ProjectViewModel> GetActiveProject();
        ProjectViewModel GetProjectViewModelById(Guid id);
    }
}
