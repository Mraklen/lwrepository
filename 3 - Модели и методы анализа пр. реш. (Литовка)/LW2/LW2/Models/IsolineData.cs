﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.Models
{
    public enum IsolineType
    {
        Ellipse, Rosenbroke,
        MM
    }

    public class IsolineData
    {
        public double[,] Data { get; set; }
        public Point[,] Grid { get; set; }
    }
}
