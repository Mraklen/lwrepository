﻿using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Core
{
    public interface IVariableFactory
    {
        IVariable GetProcessTemperature(VarType type = VarType.None, ModType mod = ModType.None);
        IVariable GetPressureInMachine(VarType type = VarType.None, ModType mod = ModType.None);
    }
}
