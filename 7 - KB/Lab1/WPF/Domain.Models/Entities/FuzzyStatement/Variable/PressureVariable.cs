﻿using System;
using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;

namespace Domain.Models.Entities.FuzzyStatement.Variable
{
    public class PressureVariable : BaseVariable
    {
        public override string CodeName => "P";

        public override string Name => "Давление в аппарате";

        public override VarType Type { get; set; }
        public override ModType Mod { get; set; } 

        public override double CalcSmallTermSet(double value)
        {
            var div = (1.0 + 0.5 * value);
            if (div == 0) return 1;

            var result = Math.Abs(1.0 / (1.0 + 0.5 * value));

            if (result > 1) result = 1;

            return SetMode(result);
        }

        public override double CalcAverageTermSet(double value)
        {
            var div = (1.0 + 0.5 * (value - 25));
            if (div == 0) return 1;
            
            var result = Math.Abs(1.0 / (1.0 + 0.5 * (value - 25)));

            if (result > 1) result = 1;

            return SetMode(result);
        }

        public override double CalcLargeTermSet(double value)
        {
            var div = (1.0 + 0.5 * (value - 50));
            if (div == 0) return 1;

            var result = Math.Abs(1.0 / (1.0 + 0.5 * (value - 50)));

            if (result > 1) result = 1;

            return SetMode(result);
        }
    }
}
