﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueryAbstractions.Filter
{
    public static class FilterExtentions
    {
        public static IQueryable<TRead> ApplyFilter<TRead, TView>(this IQueryable<TRead> src, FilterModel<TRead, TView> filterModel)
            where TRead : class
            where TView : class
        {
            if (filterModel == null) return src;

            foreach (var filter in filterModel.Filters)
            {
                if (filter != null) src = src.Where(filter);
            }

            return src;
        }

        public static IQueryable<TRead> ApplySorting<TRead, TView>(this IQueryable<TRead> src, FilterModel<TRead, TView> filterModel)
            where TRead : class
            where TView : class
        {
            if (filterModel == null) return src;

            var sortExpression = filterModel.Sorter;
            if (sortExpression == null) return src;

            return filterModel.Descending.HasValue && filterModel.Descending.Value ?
                src.OrderByDescending(sortExpression) :
                src.OrderBy(sortExpression);
        }

        public static IQueryable<TRead> ApplyPaging<TRead, TView>(this IQueryable<TRead> src, FilterModel<TRead, TView> filterModel)
            where TRead : class
            where TView : class
        {
            var page = filterModel == null ? 1 : filterModel.Page.Value;
            var perPage = filterModel == null ? 10 : filterModel.RowsPerPage.Value;
            var getAll = filterModel == null ? false : filterModel.GetAll.Value;

            return !getAll ? src.Skip((page - 1) * perPage).Take(perPage) : src;
        }
    }
}
