﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace QueryAbstractions.Filter
{
    public abstract class FilterModel<TRead, TView>
        where TRead : class
        where TView : class
    {
        public string SortBy { get; set; }
        public bool? Descending { get; set; }

        public bool? GetAll { get; set; } = false;
        public int? Page { get; set; } = 1;
        public int? RowsPerPage { get; set; } = 10;

        public int? TotalItems { get; set; } = 0;

        public TView Filter { get; set; }

        protected abstract Dictionary<string, Expression<Func<TRead, object>>> FieldsMapping { get; }

        public virtual Expression<Func<TRead, object>> Sorter =>
            !string.IsNullOrEmpty(SortBy) && FieldsMapping.ContainsKey(SortBy.ToLower()) ? FieldsMapping[SortBy.ToLower()] : null;

        public abstract IEnumerable<Expression<Func<TRead, bool>>> Filters { get; }
    }
}
