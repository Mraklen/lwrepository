﻿using Domain.Models.Entities.KnowledgeBase;
using Domain.Models.Repositories.Interfaces;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using WPF.Events;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels.Main
{
    class MainPageViewModel : BaseViewModel, IDisposable
    {
        private readonly IDomainRepositoryFactory repositoryFactory;

        Visibility btnVis;
        public Visibility BtnVis { get => btnVis; set { btnVis = value; OnPropertyChanged("BtnVis"); } }

        public DelegateCommand CreateVariablesBtn { get; set; }
        public DelegateCommand CreateRulesBtn { get; set; }

        public DelegateCommand StartLab1Command { get; set; }
        public DelegateCommand StartLab2Command { get; set; }
        public DelegateCommand StartLab3Command { get; set; }

        public MainPageViewModel(AppStateStore stateStore, IUnityContainer container, IDomainRepositoryFactory repositoryFactory) : base(stateStore, container)
        {
            this.repositoryFactory = repositoryFactory;

            CreateVariablesBtn = new DelegateCommand(CreateVariablesEvent);
            CreateRulesBtn = new DelegateCommand(CreateRulesEvent);

            StartLab1Command = new DelegateCommand(action: StartLab1Event);
            StartLab2Command = new DelegateCommand(action: StartLab2Event);
            StartLab3Command = new DelegateCommand(action: StartLab3Event);

            BtnVis = Visibility.Hidden;
        }

        private void StartLab1Event()
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<ChangePageEvent>>().Publish(new ChangePageEvent { ModuleType = Modules.Base.ModuleType.ExpertSystem });
        }

        private void StartLab2Event()
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<ChangePageEvent>>().Publish(new ChangePageEvent { ModuleType = Modules.Base.ModuleType.FuzzyStatement });
        }

        private void StartLab3Event()
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<ChangePageEvent>>().Publish(new ChangePageEvent { ModuleType = Modules.Base.ModuleType.NeuronNet });
        }

        private async Task CreateVariablesEvent()
        {
            var varRepos = repositoryFactory.GetEntityRepository<Variable>();
            var paramRepos = repositoryFactory.GetEntityRepository<Parameter>();

            if (varRepos.GetRaw().Any()) return;

            var variables = new List<Variable>
            {
                Variable.CreateNew("БХ", "Боль, характер", VarType.Select, new List<Parameter> {
                        Parameter.CreteNew("сильная", "Больевые ощущения характерно сильные?"),
                        Parameter.CreteNew("умеренная", "Болевые ощущения умеренные?"),
                        Parameter.CreteNew("тупая", "Имеются тупые болевые ощущения?")
                    }),
                Variable.CreateNew( "БП", "Боль, проявление", VarType.Mutiselect, new List<Parameter> {
                        Parameter.CreteNew("периодическая", "Боль появляется периодически?"),
                        Parameter.CreteNew("постоянная", "Боль присутствует постоянно?"),
                        Parameter.CreteNew("спонтанная", "Боль возникает спонтанно, независимо от вермени?")
                    }),
                Variable.CreateNew("БУМ", "Боль, уменьшается", VarType.Select, new List<Parameter> {
                        Parameter.CreteNew("движение", "Уменьшается ли сила болезненных ощущений при движении?")
                    }),
                Variable.CreateNew("БУВ", "Боль, увеличение", VarType.Select, new List<Parameter> {
                        Parameter.CreteNew("движение", "Есть ли усиление боли в движении?"),
                        Parameter.CreteNew("сырая погода", "Боль усиливается во время сырой погоды?"),
                        Parameter.CreteNew("неудобная поза", "Боль усиливается при неудобных позах?")
                    }),
                Variable.CreateNew("СД", "Сустав, девормация", VarType.Select, new List<Parameter> {
                        Parameter.CreteNew("мягкие ткани", "Присутствует ли деформация мягких тканией?"),
                        Parameter.CreteNew("увеличение", "Увеличены ли суставы в размерах?"),
                        Parameter.CreteNew("деформация", "Заметны ли деформации в суставах?")
                    }),
                Variable.CreateNew("Л", "Локализация", VarType.Mutiselect, new List<Parameter> {
                        Parameter.CreteNew("коленные суставы", "Локализация проблем в коленных суставах?"),
                        Parameter.CreteNew("плечевые суставы", "Локализация проблем в плечевых суставах?"),
                        Parameter.CreteNew("локтевые суставы", "Локализация проблем в локтевых суставах?"),
                        Parameter.CreteNew("кистевые суставы", "Локализация проблем в кистевых суставах?"),
                        Parameter.CreteNew("позвоночник", "Локализация проблем в позвоночном отделе?"),
                        Parameter.CreteNew("суставы", "Локализация проблем в суставах?")
                    }),
                Variable.CreateNew("ХД", "Хруст при движении", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Есть ли хруст в суставах при движении?")
                    }),
                Variable.CreateNew("ТП", "Тугоподвижность", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Чувтвуется ли тугоподвижность суставов?")
                    }),
                Variable.CreateNew("ОП", "Органичение подвижности", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Характерно ли прогрессирующее ограничение подвижности?")
                    }),
                Variable.CreateNew("БУ", "Быстрое утомление", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Отмечается ли быстрое утомленеие во время активных движений?")
                    }),
                Variable.CreateNew("СБ", "Сердечные боли", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Возникали ли неприятные ощущения в области сердца?")
                    }),
                Variable.CreateNew("П", "Припухлость", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Обнаружено ли поражение суставов в виде припухлости?")
                    }),
                Variable.CreateNew("ПК", "Покраснение кожи", VarType.Bool, new List<Parameter> {
                        Parameter.CreteNew(null, "Замечено ли покраснение кожи в области суставов?")
                    }),
                Variable.CreateNew("Д", "Диагноз", VarType.Select, new List<Parameter> {
                        Parameter.CreteNew("артрит", null),
                        Parameter.CreteNew("острый артрит", null),
                        Parameter.CreteNew("хронический артрит", null),
                        Parameter.CreteNew("ревматоидный артрит", null),
                        Parameter.CreteNew("артроз", null),
                        Parameter.CreteNew("остеохандроз", null),
                        Parameter.CreteNew("ревматизм", null)
                    }, false)
            };

            foreach (var variable in variables)
            {
                try
                {
                    await varRepos.UpdateOrInsertAsync(variable);
                }
                catch (Exception ex)
                {
                    BtnVis = Visibility.Hidden;
                }
            }

            BtnVis = Visibility.Visible;
        }

        private async Task CreateRulesEvent()
        {
            var varRepos = repositoryFactory.GetEntityRepository<Variable>();
            var ruleRepos = repositoryFactory.GetEntityRepository<Rule>();
            var ruleParamRepos = repositoryFactory.GetEntityRepository<RuleParameter>();

            if (ruleRepos.GetRaw().Any() || !varRepos.GetRaw().Any()) return;

            var rules = new List<Rule>
            {
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 4),
                    RuleParameter.CreateNew(ParamType.Condition, 6),
                    RuleParameter.CreateNew(ParamType.Condition, 7),
                    RuleParameter.CreateNew(ParamType.Condition, 11),
                    RuleParameter.CreateNew(ParamType.Condition, 19),
                    RuleParameter.CreateNew(ParamType.Result, 27)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 1),
                    RuleParameter.CreateNew(ParamType.Condition, 5),
                    RuleParameter.CreateNew(ParamType.Condition, 26),
                    RuleParameter.CreateNew(ParamType.Condition, 12),
                    RuleParameter.CreateNew(ParamType.Condition, 19),
                    RuleParameter.CreateNew(ParamType.Result, 28)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 8),
                    RuleParameter.CreateNew(ParamType.Condition, 22),
                    RuleParameter.CreateNew(ParamType.Result, 29)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 2),
                    RuleParameter.CreateNew(ParamType.Condition, 8),
                    RuleParameter.CreateNew(ParamType.Condition, 17),
                    RuleParameter.CreateNew(ParamType.Condition, 16),
                    RuleParameter.CreateNew(ParamType.Result, 30)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 3),
                    RuleParameter.CreateNew(ParamType.Condition, 9),
                    RuleParameter.CreateNew(ParamType.Condition, 23),
                    RuleParameter.CreateNew(ParamType.Condition, 21),
                    RuleParameter.CreateNew(ParamType.Condition, 13),
                    RuleParameter.CreateNew(ParamType.Condition, 20),
                    RuleParameter.CreateNew(ParamType.Result, 31)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 5),
                    RuleParameter.CreateNew(ParamType.Condition, 10),
                    RuleParameter.CreateNew(ParamType.Condition, 24),
                    RuleParameter.CreateNew(ParamType.Condition, 21),
                    RuleParameter.CreateNew(ParamType.Condition, 18),
                    RuleParameter.CreateNew(ParamType.Result, 32)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 8),
                    RuleParameter.CreateNew(ParamType.Condition, 24),
                    RuleParameter.CreateNew(ParamType.Condition, 14),
                    RuleParameter.CreateNew(ParamType.Condition, 15),
                    RuleParameter.CreateNew(ParamType.Condition, 16),
                    RuleParameter.CreateNew(ParamType.Result, 33)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 22),
                    RuleParameter.CreateNew(ParamType.Result, 13)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 12),
                    RuleParameter.CreateNew(ParamType.Result, 26)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 26),
                    RuleParameter.CreateNew(ParamType.Result, 12)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 25),
                    RuleParameter.CreateNew(ParamType.Result, 2)
                }),
                Rule.CreateNew(new List<RuleParameter>
                {
                    RuleParameter.CreateNew(ParamType.Condition, 25),
                    RuleParameter.CreateNew(ParamType.Result, 22)
                })
            };

            foreach (var variable in rules)
            {
                try
                {
                    await ruleRepos.UpdateOrInsertAsync(variable);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void Dispose() { }
    }
}
