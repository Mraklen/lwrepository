﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueryAbstractions.ReadModels
{
    public abstract class BaseReadModel<TID> where TID : struct
    {
        public TID? Id { get; set; }
    }
}
