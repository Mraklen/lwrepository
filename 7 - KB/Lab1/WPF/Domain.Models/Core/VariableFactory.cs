﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models.Entities.FuzzyStatement.Variable;
using Domain.Models.Entities.FuzzyStatement.Variable.Interfaces;

namespace Domain.Models.Core
{
    public class VariableFactory : IVariableFactory
    {
        public IVariable GetPressureInMachine(VarType type = VarType.None, ModType mod = ModType.None) => new PressureVariable { Type = type, Mod = mod };
        public IVariable GetProcessTemperature(VarType type = VarType.None, ModType mod = ModType.None) => new TemperatureVariable { Type = type, Mod = mod };
    }
}
