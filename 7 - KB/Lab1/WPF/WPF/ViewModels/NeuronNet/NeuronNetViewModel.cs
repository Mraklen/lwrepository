﻿using Domain.Models.Entities.NeuronNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using WPF.Helpers;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels.NeuronNet
{
    class NeuronNetViewModel : BaseViewModel, IDisposable
    {
        int numHiddenNeuron = 0;

        public DelegateCommand NumHiddenNeuronCommand { get; set; }
        public DelegateCommand StartTrain { get; set; }

        public NeuronNetViewModel(AppStateStore stateStore, IUnityContainer container) : base(stateStore, container)
        {
            NumHiddenNeuronCommand = new DelegateCommand(action1: param => NumHiddenNeuronChange(param));
            StartTrain = new DelegateCommand(action: StartTrainEvent);
        }

        private void StartTrainEvent()
        {
            if (numHiddenNeuron == 0)
            {
                MessageBox.Show("Выберете количество нейронов!", "", MessageBoxButton.OK);
                return;
            }

            var shift = true;
            var nnet = NNet.CreateNew(numHiddenNeuron, 1, 3, shift);

            var trainResult = nnet.Train();
            var testResult = nnet.Test();

            FileManager.WriteJsonFile(PathBuilder.GetBasePathJson($"TrainResult_{numHiddenNeuron}{(shift ? "___" : "")}"), trainResult);
            FileManager.WriteJsonFile(PathBuilder.GetBasePathJson($"TestResult_{numHiddenNeuron}{(shift ? "___" : "")}"), testResult);

            MessageBox.Show("Обучение и тестирование завершено!", "", MessageBoxButton.OK);
        }

        private void NumHiddenNeuronChange(object param)
        {
            numHiddenNeuron = Convert.ToInt32(param);
        }

        public void Dispose() { }
    }
}
