﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Models.Interfaces;


namespace CinemaNet.Controllers
{
    public class FilmListController : Controller
    {
        private IFilmList filmList;
        public FilmListController(IFilmList list)
        {
            filmList = list;
        }

        // GET: /<controller>/
        public ViewResult List(int id)
        {
            ViewBag.cinemaID = id;
            return View(filmList.FilmList(id).ToList());
        }
    }
}
