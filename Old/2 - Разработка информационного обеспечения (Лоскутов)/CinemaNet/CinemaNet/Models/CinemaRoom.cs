﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class CinemaRoom
    {
        public int CinemaRoomID { get; set; }
        public string Name { get; set; } // Название зала
        public int Row { get; set; }
        public int SeatInRow { get; set; }

        public List<Seat> Seats { get; set; }
        public List<FilmSession> FilmSessions { get; set; }
    }
}
