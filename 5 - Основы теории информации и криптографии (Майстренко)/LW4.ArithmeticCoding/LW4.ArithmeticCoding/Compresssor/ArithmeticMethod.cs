﻿using LW4.ArithmeticCoding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW4.ArithmeticCoding.Compresssor
{
    public class ArithmeticMethod
    {
        public static Dictionary<char, Segment> DefineSegments(char[] letters, double[] probability)
        {
            var result = new Dictionary<char, Segment>();
            var l = 0.0;
            for (var i = 0; i < letters.Count(); i++)
            {
                var tmpLeft = l;
                var tmpRight = l + probability[i];
                l = tmpRight;
                result.Add(letters[i], new Segment()
                {
                    Left = tmpLeft,
                    Right = tmpRight
                });
            }
            return result;
        }

        public static double Coding(Dictionary<char, Segment> segments, string text)
        {
            var left = 0.0;
            var right = 1.0;
            for (var i = 0; i < text.Length; i++)
            {
                var symbol = text[i];
                var newRight = left + (right - left) * segments[text[i]].Right;
                var newLeft = left + (right - left) * segments[text[i]].Left;
                left = newLeft;
                right = newRight;
            }
            return (left + right) / 2;
        }

        public static string Decoding(Dictionary<char, Segment> segments, double code, int n, ProgressBar progress)
        {
            progress.Maximum = n;
            progress.Minimum = 0;
            progress.Step = 1;
            progress.Visible = true;

            var result = new string[(int)Math.Ceiling(n / 10000.0)];
            var tmp1 = "";
            for (int i = 0, count = 0, index = 0; i < n; i++, count++)
            {
                for (var j = 0; j < segments.Count(); j++)
                {
                    var segment = segments.ElementAt(j);
                    if (code >= segment.Value.Left && code < segment.Value.Right)
                    {
                        tmp1 += segment.Key;
                        code = (code - segment.Value.Left) / (segment.Value.Right - segment.Value.Left);

                        if (count >= 10000 || n == i + 1)
                        {
                            result[index++] = tmp1;
                            tmp1 = "";
                            count = 0;
                        }
                        break;
                    }
                }
                progress.PerformStep();
            }
            progress.Visible = false;
            var res = "";
            foreach (var str in result) res += str;
            return res;
        }

        public static (char[], double[]) CalcProbabilitySymbolsByText(string text)
        {
            var tmp = text.GroupBy(x => x).Select(x => (symbol: x.Key, probability: x.Count() / (text.Count() * 1.0)));
            return (tmp.Select(x => x.symbol).ToArray(), tmp.Select(x => x.probability).ToArray());
        }
    }
}
