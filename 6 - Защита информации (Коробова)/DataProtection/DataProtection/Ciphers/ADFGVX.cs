﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProtection.Ciphers
{
    public class ADFGVX
    {
        static List<char> adfgvx = new List<char> { 'A', 'D', 'F', 'G', 'V', 'X' };

        static List<List<char>> alphabet = new List<List<char>> {
            new List<char> { '1', 'J', 'R', '4', 'H', 'D' },
            new List<char> { 'E', '2', 'A', 'V', '9', 'M' },
            new List<char> { '8', 'P', 'I', 'N', 'K', 'Z' },
            new List<char> { 'B', 'Y', 'U', 'F', '6', 'T' },
            new List<char> { '5', 'G', 'X', 'S', '3', 'O' },
            new List<char> { 'W', 'L', 'Q', '7', 'C', '0' }
        };

        public static string Encrypt(string text, string key)
        {
            text = text.Replace(" ", "SPACE");
            text = CleanMessage(text);
            text = text.ToUpper();

            var resultCode = new List<char>();

            // first step
            for (int i = 0, j = 0; i < text.Length; i++, j += 2)
            {
                var code = GetCodeByChar(text[i]);
                resultCode.AddRange(code);
            }

            // second step
            var keyDictionary = key.ToDictionary(k => k, d => new List<char>());
            var counter = 0;
            foreach(var ch in text)
            {
                keyDictionary[key[counter]].Add(ch);
                counter++;
                if (counter >= key.Length) counter = 0;
            }

            var sb = new StringBuilder();
            var alphabeticalKey = key.ToList();
            alphabeticalKey.Sort();

            foreach (var c in alphabeticalKey)
            {
                foreach (var each in keyDictionary[c])
                {
                    sb.Append(each);
                }
                sb.Append(' ');
            }

            return sb.ToString();
        }

        public static string Decrypt(string text, string key)
        {
            var resultText = "";
            string[] splitCipher = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // first step
            var dictionaryKeys = key.ToList();
            dictionaryKeys.Sort();

            var keyDictionary = dictionaryKeys.ToDictionary(c => c, c => new List<char>());
            var counter = 0;
            foreach (var c in dictionaryKeys)
            {
                keyDictionary[c].AddRange(splitCipher[counter].ToList());
                counter++;
            }

            var readDictionary = new StringBuilder();
            int longestLength = GetLongestLength(splitCipher);

            // second step
            for (int i = 0; i < longestLength; i++)
            {
                foreach (var c in key)
                {
                    if (keyDictionary[c].Count != 0)
                    {
                        readDictionary.Append(keyDictionary[c].FirstOrDefault());
                        keyDictionary[c].RemoveAt(0);
                    }
                }
            }
            resultText = readDictionary.ToString();

            //// second step
            //var reverceFirstStep = new StringBuilder();
            //for (int i = 1; i < resultRead.Length; i += 2)
            //{
            //    reverceFirstStep.Append(GetCharByCode($"{resultRead[i - 1]}{resultRead[i]}"));
            //}
            //resultText = reverceFirstStep.ToString();

            return resultText.Replace("SPACE", " ");
        }

        private static char[] GetCodeByChar(char ch)
        {
            var rowIndex = alphabet.FindIndex(x => x.FindIndex(xx => xx.Equals(ch)) != -1);
            var colIndex = alphabet[rowIndex].FindIndex(x => x.Equals(ch));
            return new char[] { adfgvx[rowIndex], adfgvx[colIndex] };
        }

        private static char GetCharByCode(string code)
        {
            var rowIndex = adfgvx.FindIndex(x => x.Equals(code[0]));
            var colIndex = adfgvx.FindIndex(x => x.Equals(code[1]));
            return alphabet[rowIndex][colIndex];
        }

        private static int GetLongestLength(string[] splitCipher)
        {
            int largestLength = int.MinValue;
            foreach (var ch in splitCipher)
            {
                if (ch.Length > largestLength) largestLength = ch.Length;
            }
            return largestLength;
        }

        private static string CleanMessage(string message)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in message)
            {
                switch (c)
                {
                    case '\r':
                    case '\n':
                    case '\t':
                    case ' ':
                        continue;
                    default:
                        sb.Append(c);
                        break;

                }
            }
            return sb.ToString();
        }
    }
}
