import random

class DocContent(object):
    """класс 'генерирует' контент для документа"""

    def get_header(self, value):
        return 'Заголовок ' + str(value)

    def get_paragraph_run(self):
        return 'Текст для параграфа. Текст для параграфа.'

    def get_picture_path(self):
        return 'Images/' + str(self.random_value(1, 9)) + '.jpg'

    def get_table_data(self):
        rowCount = self.random_value(3, 10)
        colCount = self.random_value(3, 10)

        data = []

        for i in range(rowCount):
            data.append([])
            for j in range(colCount):
                data[i].append(self.random_value(0, 100))

        return data

    def get_table_style(self, i):
        styles = ['ColorfulShading', 'Table Grid', 'Light Grid Accent 3', 'Medium Shading 1 Accent 4']
        return styles[i]

    def random_value(self, start, end):
        return random.randint(start, end)