﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Charts;

namespace LW2.Models
{
    public class PlotterParam
    {
        public ChartPlotter Plotter { get; set; }
        public IsolineGraph IsolineGraph { get; set; }
        public IsolineTrackingGraph IsolineTracking { get; set; }
    }
}
