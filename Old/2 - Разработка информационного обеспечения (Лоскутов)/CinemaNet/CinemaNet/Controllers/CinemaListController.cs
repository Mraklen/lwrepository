﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CinemaNet.Models;
using CinemaNet.Models.Interfaces;


namespace CinemaNet.Controllers
{
    public class CinemaListController : Controller
    {
        private ICinemaList cinemaList;
        public CinemaListController(ICinemaList list)
        {
            cinemaList = list;
        }

        // GET: /<controller>/
        public ViewResult List(int id)
        {
            ViewBag.filmID = id;
            return View(cinemaList.CinemaList(id).ToList());
        }

        //public ViewResult List() => View(cinemaList.CinemaList().ToList());
    }
}
