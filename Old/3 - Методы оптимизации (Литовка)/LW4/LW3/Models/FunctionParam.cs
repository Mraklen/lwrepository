﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CW.Methods;

namespace LW3.Models
{
    public class FunctionParam
    {
        public static double X1Value { get; private set; }
        public static double X2Value { get; private set; }
        public static double EValue { get; private set; }

        const int width = 20;
        const int height = 20;
        const int x1Value = -10;
        const int x2Value = -10;
        
        const int width1 = 11;
        const int height1 = 201;
        const int x1Value1 = 600;
        const int x2Value1 = 30;

        public static double a = 2;
        public static double b = 1;
        public static double c = 2;
        public static double d = 3;
        public static double alpha = 60;
        public static double step = 1f;

        public static IsolineType CurrenIsolineType { get; set; } = IsolineType.Ellipse;
        public static FineType CurrenFineType { get; set; } = FineType.Internal;

        private Dictionary<IsolineType, IsolineData> IsolinesData = new Dictionary<IsolineType, IsolineData>()
        {
            { IsolineType.Ellipse, new IsolineData() },
            { IsolineType.Rosenbroke, new IsolineData() },
            { IsolineType.MM, new IsolineData() }
        };

        private Dictionary<FineType, FineData> FinesData = new Dictionary<FineType, FineData>()
        {
            { FineType.First, new FineData() },
            { FineType.Second, new FineData() }
        };

        public FunctionParam(double x1, double x2, double e)
        {
            X1Value = x1;
            X2Value = x2;
            EValue = e;
            //CalcEllipseData();
            //CalcRosenbrokeData();
            CalcMMData();
            //CalcFirstFineData();
            //CalcSecondFineData();
        }
        
        public double CalcFunction(double x1, double x2, double k, bool fine = false)
        {
            switch (CurrenIsolineType)
            {
                case IsolineType.Ellipse: return CalcEllipseFuntion(x1, x2) + Fine(x1, x2, k);
                case IsolineType.Rosenbroke: return CalcRosenbrokeFunction(x1, x2) + Fine(x1, x2, k);
                case IsolineType.MM: return CalcMM(x1, x2) * -1 + (fine ? Fine(x1, x2, k) : 0);
            }
            return 0;
        }

        public double CalcFineFunction(double x1, double x2, FineType fineType)
        {
            switch (fineType)
            {
                case FineType.First: return CalcFirstFineFunction(x1, x2);
                case FineType.Second: return CalcSecondFineFunction(x1, x2);
            }
            return 0;
        }

        private double Fine(double x1, double x2, double k)
        {
            double temp1, temp2, v1, v2, v3, v4, y;

            y = CalcMM(x1, x2);

            temp1 = x1;
            if (temp1 <= 800 && temp1 >= 600)
            {
                v1 = 0;
                v3 = 0;
            }
            else
            {
                //v1 = (1 / k) * y;//Math.Log(temp1, Math.E);
                v1 = 1000;
                v3 = k * Math.Pow(y, 2);
            }

            temp2 = x2;
            if (temp2 <= 40 && temp2 >= 30)
            {
                v2 = 0;
                v4 = 0;
            }
            else
            {
                //v2 = (1 / k) * (y);//Math.Log(temp2, Math.E);
                v2 = 1000;
                v4 = k * Math.Pow(y, 2);
            }

            switch (CurrenFineType)
            {
                case FineType.Internal: return v1 + v2;
                case FineType.External: return v1 + v2;//v3 + v4;
                default: return 0;
            }
        }
        //private double Fine(double x1, double x2, double k)
        //{
        //    double temp1, temp2, v1, v2, v3, v4;

        //    temp1 = -CalcFineFunction(x1, x2, FineType.First);
        //    if (temp1 <= 0)
        //    {
        //        v1 = 0;
        //        v3 = 0;
        //    }
        //    else
        //    {
        //        v1 = (1 / k) * (temp1);//Math.Log(temp1, Math.E);
        //        v3 = k * Math.Pow(temp1, 2);
        //    }

        //    temp2 = CalcFineFunction(x1, x2, FineType.Second);
        //    if (temp2 <= 0)
        //    {
        //        v2 = 0;
        //        v4 = 0;
        //    }
        //    else
        //    {
        //        v2 = (1 / k) * (temp2);//Math.Log(temp2, Math.E);
        //        v4 = k * Math.Pow(temp2, 2);
        //    }

        //    switch (CurrenFineType)
        //    {
        //        case FineType.Internal: return v1 + v2;
        //        case FineType.External: return v3 + v4;
        //        default: return 0;
        //    }
        //}

        public bool Condition(double x1, double x2)
        {
            double f1, f2;

            f1 = -CalcFineFunction(x1, x2, FineType.First);
            f2 = CalcFineFunction(x1, x2, FineType.Second);

            if (f1 <= 0 && f2 <= 0)
                return true;
            else
                return false;
        }

        public IsolineData GetIsolineData()
        {
            return IsolinesData[CurrenIsolineType];
        }

        public FineData GetFineData(FineType fineType)
        {
            return FinesData[fineType];
        }

        private void CalcEllipseData()
        {
            var data = IsolinesData[IsolineType.Ellipse];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcEllipseFuntion(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);

            data.ExtremPoint = new PointXY() { X1 = a, X2 = b };
        }

        private void CalcRosenbrokeData()
        {
            var data = IsolinesData[IsolineType.Rosenbroke];
            data.Data = new double[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Data[column, row] = CalcRosenbrokeFunction(x1, x2);

            data.Grid = new Point[width, height];
            for (int row = 0, x1 = x1Value; row < height; row++, x1++)
                for (int column = 0, x2 = x2Value; column < width; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);

            data.ExtremPoint = new PointXY() { X1 = 1, X2 = 1 };
        }

        private void CalcMMData()
        {
            var data = IsolinesData[IsolineType.MM];
            data.Data = new double[width1, height1];
            for (int row = 0, x1 = x1Value1; row < height1; row++, x1++)
                for (int column = 0, x2 = x2Value1; column < width1; column++, x2++)
                    data.Data[column, row] = CalcMM(x1, x2, 0.01) * (-1);

            data.Grid = new Point[width1, height1];
            for (int row = 0, x1 = x1Value1; row < height1; row++, x1++)
                for (int column = 0, x2 = x2Value1; column < width1; column++, x2++)
                    data.Grid[column, row] = new Point(x1, x2);
        }

        private void CalcFirstFineData()
        {
            var data = FinesData[FineType.First];
            data.Points = new List<PointXY>();
            for (double x1 = x1Value; x1 < -x1Value; x1 += step)
                for (double x2 = x2Value; x2 < -x2Value; x2 += step)
                {
                    var y = CalcFirstFineFunction(x1, x2);
                    if(y <= 0)
                    {
                        data.Points.Add(new PointXY() { X1 = x1, X2 = x2 });
                    }
                }
        }

        private void CalcSecondFineData()
        {
            var data = FinesData[FineType.Second];
            data.Points = new List<PointXY>();
            for (double x1 = x1Value; x1 < -x1Value; x1 += step)
                for (double x2 = x2Value; x2 < -x2Value; x2 += step)
                {
                    var y = CalcSecondFineFunction(x1, x2);
                    if (y >= 0)
                    {
                        data.Points.Add(new PointXY() { X1 = x1, X2 = x2 });
                    }
                }
        }

        private double CalcEllipseFuntion(double x1, double x2)
        {
            return (Math.Pow(((x1 - a) * Math.Cos(alpha) + (x2 - b) * Math.Sin(alpha)), 2) / (Math.Pow(c, 2))) + 
                   (Math.Pow(((x2 - b) * Math.Cos(alpha) + (x1 - a) * Math.Sin(alpha)), 2) / (Math.Pow(d, 2)));
        }

        private double CalcRosenbrokeFunction(double x1, double x2)
        {
            return 100 * Math.Pow((x2 - Math.Pow(x1, 2)), 2) + Math.Pow((1 - x1), 2);
        }

        private double CalcFirstFineFunction(double x1, double x2)
        {
            return 8 * Math.Pow(x1, 3) + Math.Pow(x2, 3) - 4;
        }

        private double CalcSecondFineFunction(double x1, double x2)
        {
            return x2;
        }

        private double CalcMM(double x1, double x2, double t = 0.01)
        {
            return new MathModel().StartCalc(x1, x2, t);
        }
    }
}
