﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SaveItem : MonoBehaviour
{
    [SerializeField] string FilePath;
    [SerializeField] Text Title;

    public void Init(string path)
    {
        FilePath = path;
        Title.text = new FileInfo(path).Name;
    }
    
    public void OpenFilePoints()
    {
        // получить точки из файла
        var points = FileManager.ReadJsonFile<JsonPoints>(FilePath);

        // заменить точки на точки из файла
        GetComponentInParent<SavePointPanel>().SetJsonPoints(points);
    }

    public void RemoveFilePoints()
    {
        // удалить файл
        File.Delete(FilePath);
        File.Delete($"{FilePath}.meta");

        // обновить панель
        GetComponentInParent<SavePointPanel>().LoadFiles();
    }
}
