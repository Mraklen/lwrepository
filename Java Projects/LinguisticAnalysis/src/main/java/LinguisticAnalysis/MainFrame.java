
package LinguisticAnalysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainFrame extends javax.swing.JFrame {

    // ������� ������
    Map<String, Integer> reservedNames = new HashMap<String, Integer>(){{
        put("if", 10);
        put("THEN", 11);
        put("begin", 12);
        put("end", 13);
    }};
    
    Map<String, Integer> operators = new HashMap<String, Integer>(){{
        put(":=", 1);
        put(">", 2);
        put("+", 3);
        put("-", 4);
        put("*", 5);
        put("/", 6);
        put("(", 7);
        put(")", 8);
        put(";", 9);
    }};
    
    int idValue = 14;
    int constValue = 15;
    
    // ������� ����������������
    Map<String, Integer> ids = new HashMap<String, Integer>() {{
        put("T", 1);
        put("EPS", 2);
        put("K", 3);
        put("SX", 4);
        put("S", 5);
    }};
    
    // ������� ��� ������ ����������� ���������������
    char[][] matrix = {
        //{ '�', '=', '>', '+', '-', '*', '/', '(', ')', ';', 'i', 'T', 'b', 'e', '�', '�', '�' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '<', '<', ' ', '<', '<', '<', ' ', '>' },
        { ' ', ' ', ' ', '<', '<', '<', '<', '<', ' ', '>', ' ', ' ', ' ', '>', '<', '<', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '>', ' ', ' ', '<', ' ', '>' },
        { ' ', ' ', ' ', '>', '>', '<', '<', '<', '>', '>', ' ', ' ', ' ', '>', '<', '<', '>' },
        { ' ', '<', ' ', '>', '>', '<', '<', '<', '>', '>', ' ', ' ', ' ', '>', '<', '<', '>' },
        { ' ', ' ', ' ', '>', '>', '>', '>', '<', '>', '>', ' ', ' ', ' ', '>', '<', '<', '>' },
        { ' ', ' ', ' ', '>', '>', '>', '>', '<', '>', '>', ' ', ' ', ' ', '>', '<', '<', '>' },
        { ' ', ' ', ' ', '<', '<', '<', '<', '<', '=', '>', ' ', ' ', ' ', ' ', '<', '<', '>' },
        { ' ', ' ', ' ', '>', '>', '>', '>', ' ', '>', '>', ' ', ' ', ' ', ' ', ' ', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '>', '>', '>', ' ', '>' },
        { ' ', ' ', '<', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '=', ' ', ' ', '<', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '<', '>', ' ', ' ', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '<', ' ', '<', ' ', '=', '>', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '>', ' ', ' ', ' ', '>', ' ', ' ', '>' },
        { ' ', '=', '>', '>', '>', '>', '>', ' ', '>', '>', ' ', '>', ' ', '>', ' ', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '>', '>', ' ', ' ', ' ', '>', ' ', ' ', '>' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' }
    };
    
    int lexID = 0;
    List lexAnalize = new ArrayList();
    
    public MainFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        codeText = new javax.swing.JTextArea();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        jScrollPane2 = new javax.swing.JScrollPane();
        lexResultText = new javax.swing.JTextArea();
        runAnalysisBtn = new javax.swing.JButton();
        label3 = new java.awt.Label();
        jScrollPane3 = new javax.swing.JScrollPane();
        sinResultText = new javax.swing.JTextArea();
        label4 = new java.awt.Label();
        jScrollPane4 = new javax.swing.JScrollPane();
        operatorPercResultText = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        codeText.setColumns(20);
        codeText.setFont(new java.awt.Font("Consolas", 0, 18)); // NOI18N
        codeText.setRows(5);
        codeText.setText("if T>EPS THEN begin\nK:=K+2;\nT:=-T*SX/(K*(K-1));\nS:=S+T\nend");
        jScrollPane1.setViewportView(codeText);
        codeText.getAccessibleContext().setAccessibleName("");

        label1.setText("�������� ���������");

        label2.setText("����������� ������");

        lexResultText.setColumns(20);
        lexResultText.setFont(new java.awt.Font("Consolas", 0, 18)); // NOI18N
        lexResultText.setRows(5);
        jScrollPane2.setViewportView(lexResultText);
        lexResultText.getAccessibleContext().setAccessibleName("resultTextArea");

        runAnalysisBtn.setText("��������� ������");
        runAnalysisBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runAnalysisBtnActionPerformed(evt);
            }
        });

        label3.setText("�������������� ������ (����������� �����)");

        sinResultText.setColumns(20);
        sinResultText.setRows(5);
        sinResultText.setEnabled(false);
        jScrollPane3.setViewportView(sinResultText);

        label4.setText("�������������� ������ (����������� ���������������)");

        operatorPercResultText.setColumns(20);
        operatorPercResultText.setRows(5);
        jScrollPane4.setViewportView(operatorPercResultText);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                                    .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jScrollPane2)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(321, 321, 321)
                                .addComponent(runAnalysisBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(runAnalysisBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        runAnalysisBtn.getAccessibleContext().setAccessibleName("runAnalysisBtn");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void runAnalysisBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runAnalysisBtnActionPerformed
        
        // ������� ����� � ������������
        lexResultText.setText("");
        operatorPercResultText.setText("");
        sinResultText.setText("");
        lexAnalize = new ArrayList();
        
        // ������ ����������� ������
        startLexAnalysis();
        
        // ������ �������������� ������ (����������� �����)
        startSinAnalysisRecursive();
        
        // ������ �������������� ������ (����������� ���������������)
        startSinAnalysisPrecedence();
    }//GEN-LAST:event_runAnalysisBtnActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(() -> {
            new MainFrame().setVisible(true);
        });
    }
    
    //////////////////////////////////////
    // ���� ������������ �������
    //////////////////////////////////////
    
    // ������� ����� ������������ �������
    private void startLexAnalysis() {
        // ������� �������� ����� �� text area
        String code = codeText.getText();
        
        String result = "";
        String temp = "";
        
        int codeLenght = code.length();
        char nextSymbol = ' ';
        
        // ���������� ������������ ������ ���������� ������
        for(int i = 0; i < codeLenght; i++)
        {   
            // �������� ������� ������
            char currentSymbol = code.charAt(i);    
            
            // ���� ������ �����������, ��������� �������� ������� �������� ������
            if (codeLenght - 1 == i)
                nextSymbol = '\n';
            else
                nextSymbol = code.charAt(i + 1);
            
            // �������� ��������� (���� ������� ������ - ��� �����)
            if (Character.isDigit(currentSymbol))
            {
                lexAnalize.add(constValue);
                lexAnalize.add(Integer.parseInt(temp + currentSymbol));
                
                result += constValue + " " + temp + currentSymbol + " ";
                temp = "";
            }
            else
            {
                // ���� ������� ������ �� ������
                if (currentSymbol != ' ' && currentSymbol != '\n') 
                {
                    temp += currentSymbol;
                }
                
                // ���� ������� ����� ��������� � ������ ����������������� ���� � ��������� ������ ������ ��� ������� ������
                if (reservedNames.containsKey(temp) && (nextSymbol == ' ' || nextSymbol == '\n'))
                {
                    lexAnalize.add(reservedNames.get(temp));
                    
                    result += reservedNames.get(temp) + " ";
                    temp = "";
                }
                // ���� ������� ������ ��� ������������� � ��������� ����� �������� ������ ��������� ��� ��� ������
                else if (ids.containsKey(temp) && (operatorCheck(Character.toString(nextSymbol)) || (nextSymbol == ' ' || nextSymbol == '\n')))
                {
                    lexAnalize.add(idValue);
                    lexAnalize.add(ids.get(temp));
                    
                    result += idValue + " " + ids.get(temp) + " ";
                    temp = "";
                }
                // ���� ������� ������ ��� ��������
                else if (operators.containsKey(temp))
                {
                    lexAnalize.add(operators.get(temp));
                    
                    result += operators.get(temp) + " ";
                    temp = "";
                }
                // ���� ������� ������ - ��� ������� ������
                else if (currentSymbol == '\n')
                {
                    result += currentSymbol;
                    temp = "";
                }
            }
        }
        
        lexResultText.setText(result);
        
        lexAnalize.add("16");
    }

    // �������� ������� �� ������������� ��� � ����������
    private boolean operatorCheck(String symbol2) {
        boolean result = false;
        
        for (Map.Entry<String, Integer> entry : operators.entrySet()) {
            if (entry.getKey().contains(symbol2)) result = true;
        }
                
        return result;  
    }
    
    //////////////////////////////////////
    
    
    //////////////////////////////////////
    // ���� ���������������� ������� (�����)
    //////////////////////////////////////
        
    // ������� ������ ��������������� ������� �����
    public void startSinAnalysisRecursive(){
        lexID = 0;
        // �������� ����������� ������ � ������� �������
        if(operator_ch()){
            sinResultText.setText(sinResultText.getText() + "<��������> | �������\n");
        } else {
            sinResultText.setText(sinResultText.getText() + "<��������> | ������\n");
        }
    }
    
    // <��������> -> <������������> | <�������>
    public boolean operator_ch(){ 
        boolean result = false; 
        
        // �������� �� if ��� �������: <�������> -> if <��������� �������> THEN <���� ����������>
        if(!result && (reservedNames.get("if") == (int)lexAnalize.get(lexID))){
            lexID++; 
            sinResultText.setText(sinResultText.getText() + "<�������> -> ��������: if | �������\n");
            
            if(if_ch()){
                sinResultText.setText(sinResultText.getText() + "<��������> -> <�������> | �������\n");
                result = true;
            } 
        }
        
        // �������� �� <�������������> ��� �������: <������������> -> <�������������> := <���������>
        if (!result && idValue == (int)lexAnalize.get(lexID)){
            lexID++; 
            
            sinResultText.setText(sinResultText.getText() + "<������������> -> <�������������> (" + lexAnalize.get(lexID) + ") | �������\n");
            
            lexID++;
            
            // �������� ��������� ������������: :=
            if(assignment()){
                sinResultText.setText(sinResultText.getText() + "<��������> -> <������������> | �������\n");
                result = true;
            }           
        }
        
        if (!result) {
            sinResultText.setText(sinResultText.getText() + "<������������> ��� <�������> | ������\n");
        }
        
        return result;
    } 
    
    // �������� ������������ := 
    public boolean assignment(){
        boolean result = false;
        
        // �������� �� :=
        if(operators.get(":=") == (int)lexAnalize.get(lexID)){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<������������> -> ��������: := | �������\n");
            
            // �������� �� <���������>
            if(expression()){
                sinResultText.setText(sinResultText.getText() + "<������������> -> <���������> | �������\n");
                result = true;
            } else {
                sinResultText.setText(sinResultText.getText() + "<������������> -> <���������> | ������\n");
            } 
        } else {
            sinResultText.setText(sinResultText.getText() + "<������������> -> ��������: := | ������\n");
        }
        
        return result;
    } 
    
    // <���������> -> <���� ������> | <������� ���������>
    public boolean expression(){
        boolean result = false, check = false;

        // �������� �� <������� ���������>
        if(simple_expression()){
            sinResultText.setText(sinResultText.getText() + "<���������> -> <������� ���������> | �������\n");
            result = true;
        } 
        
        // �������� �� <���� ������>
        if(!result && block_brackets()){
            sinResultText.setText(sinResultText.getText() + "<���������> -> <���� ������> | �������\n");
            result = true;
        }
        
        if (!result) {
            sinResultText.setText(sinResultText.getText() + "<���� ������> ��� <������� ���������> | ������\n");
        }
        
        return result;
    } 
    
    // <���� ������> -> ( <���������> )
    public boolean block_brackets(){
        boolean result = false;
        
        // �������� (
        if (operators.get("(") == (int)(lexAnalize.get(lexID))){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<���� ������> -> ��������: ( | �������\n");
            result = true;
        } else {
            result = false;
        }
        
        // �������� <���������>
        if (result && expression()) {
            sinResultText.setText(sinResultText.getText() + "<���� ������> -> <���������> | �������\n");
            result = true;
        } else {
            result = false;
        }
        
        // �������� )
        if (result && operators.get(")") == (int)(lexAnalize.get(lexID))){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<���� ������> -> ��������: ) | �������\n");
            result = true;
        } else {
            result = false;
        }
        
        if (!result) {
            sinResultText.setText(sinResultText.getText() + "<���� ������> | ������\n");
        }
        
        return result;
    }
    
    // <������� ���������> -> <���������> {+|- <���������>}
    public boolean simple_expression(){
        boolean result = false, check = false;
            
        while(!check){
            
            // �������� <���������>
            if(summand()){
                sinResultText.setText(sinResultText.getText() + "<������� ���������> -> <���������> | �������\n");
                result = true;
            } else {
                result = false;
            }                

            // �������� �� ���������: + ��� -
            if(result && operators.get("+") == (int)lexAnalize.get(lexID) || operators.get("-") == (int)lexAnalize.get(lexID)){
                lexID++;
                sinResultText.setText(sinResultText.getText() + "<������� ���������> -> ��������: + ��� - | �������\n");
            } else {
                check = true;
            }
        }
        
        return result;
    }
    
    // <���������> -> <���������> {*|/ <���������> | <���������>}
    public boolean summand(){
        boolean result = false, check = false, firstCheck = true;
        
        while(!check){
            
            // �������� �� <���������>
            if(multiplier()){
                sinResultText.setText(sinResultText.getText() + "<���������> -> <���������> | �������\n");
                result = true;
            } 
            // �������� �� <���������>
            else if (firstCheck && expression()){
                sinResultText.setText(sinResultText.getText() + "<���������> -> <���������> | �������\n");
                result = true;
            } else {
                result = false;
            }
            
            // �������� �� ���������: * ��� /
            if(result && operators.get("*") == (int)lexAnalize.get(lexID) || operators.get("/") == (int)lexAnalize.get(lexID)){
                lexID++;
                sinResultText.setText(sinResultText.getText() + "<���������> -> ��������: * ��� / | �������\n");
            } else {
                check = true;
            }
            
            firstCheck = false;
        }
        return result;
    } 
    
    // <������ ����������> -> <��������> {; <������ ����������>}
    public boolean arr_operator_ch(){
        boolean result = false, check = false;
        
        while(!check){
            
            // �������� <��������>
            if(operator_ch()){
                sinResultText.setText(sinResultText.getText() + "<������ ���������� >-> <��������> | �������\n");
                result = true;
            } else {
                result = false;
            }
            
            // �������� �� ;
            if(result && operators.get(";") == (int)lexAnalize.get(lexID)){
                lexID++;
                sinResultText.setText(sinResultText.getText() + "<������ ����������> -> ��������: ; | �������\n");
            } else {
                check = true;
            }
        }
        return result;
    } 
    
    // <���� ����������> -> begin <������ ����������> end
    public boolean block_operator_ch() {
        boolean result = false;
        
        // �������� begin
        if (reservedNames.get("begin") == (int)(lexAnalize.get(lexID))){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> begin | �������\n");
            result = true;
        } else {
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> begin | ������\n");
            result = false;
        }
        
        // �������� <������ ����������>
        if (result && arr_operator_ch()) {
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> <������ ����������> | �������\n");
            result = true;
        } else {
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> <������ ����������> | ������\n");
            result = false;
        }
        
        // �������� end
        if (result && reservedNames.get("end") == (int)(lexAnalize.get(lexID))){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> end | �������\n");
            result = true;
        } else {
            sinResultText.setText(sinResultText.getText() + "<���� ����������> -> end | ������\n");
            result = false;
        }
        
        return result;
    }
    
    // <�������> -> if <��������� �������> THEN <���� ����������>
    public boolean if_ch(){
        boolean result = false; 
        
        // �������� <��������� �������>
        if(exp_if()){
            sinResultText.setText(sinResultText.getText() + "<�������> -> <��������� �������> | �������\n");
            result = true;
        } else {
            sinResultText.setText(sinResultText.getText() + "<�������> -> <��������� �������> | ������\n");
            result = false;
        }   
        
        // �������� THEN
        if (result && reservedNames.get("THEN") == (int)(lexAnalize.get(lexID))){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<�������> -> THEN | �������\n");
            result = true;
        } else {
            sinResultText.setText(sinResultText.getText() + "<�������> -> THEN | ������\n");
            result = false;
        } 
        
        // �������� <���� ����������>
        if(result && block_operator_ch()){
            sinResultText.setText(sinResultText.getText() + "<�������> -> <���� ����������> | �������\n");
            result = true;               
        } else {
            sinResultText.setText(sinResultText.getText() + "<�������> -> <���� ����������> | ������\n");
            result = false;
        }
        
        return result;
    } 
    
    // <��������� �������> -> <������� ���������> {<|> <������� ���������>}
    public boolean exp_if(){
        boolean result = false, check = false; 
        
        while(!check){
            
            // �������� �� <������� ���������>
            if(simple_multiplier()){
                sinResultText.setText(sinResultText.getText() + "<��������� �������> -> <������� ���������> | �������\n");
                result = true; 
            } else {
                result = false;    
            }
            
            // �������� ��������� >
            if(result && operators.get(">") == (int)lexAnalize.get(lexID)){
                lexID++;
                sinResultText.setText(sinResultText.getText() + "<��������� �������> -> ��������: > | �������\n");
            } else {
                check = true;
            }         
        }
        return result;
    } 
    
    // <���������> -> <������� ���������> | <������� �������� � ���������>
    public boolean multiplier(){
        boolean result = false;
        
        // �������� <������� ���������>
        if (simple_multiplier()) {
            sinResultText.setText(sinResultText.getText() + "<���������> -> <������� ���������> | �������\n");
            result = true;
        }
        
        // �������� <������� �������� � ���������>
        if (!result && unary_multiplier()) {
            sinResultText.setText(sinResultText.getText() + "<���������> -> <������� �������� � ���������> | �������\n");
            result = true;
        }
        
        return result;
    }
    
    // <������� ���������> -> <�������������> | <���������>
    public boolean simple_multiplier(){
        boolean result = false; 
        
        // �������� �� <�������������> ��� <���������>
        if(idValue == (int)lexAnalize.get(lexID) || constValue == (int)lexAnalize.get(lexID)){
            result = true; 
            
            String text = sinResultText.getText() + "<������� ���������> -> ";
            
            // ����������� ���� <������� ���������>
            if (idValue == (int)lexAnalize.get(lexID)) {
                text += "<�������������> (";
            } else {
                text += "<���������> (";
            }
            
            lexID++;
            
            sinResultText.setText(text + lexAnalize.get(lexID) + ")\n");
            
            lexID++;
        }        
        
        return result;    
    }

    // <������� �������� � ���������> -> +|- <������� ���������>
    public boolean unary_multiplier(){
        boolean result = false;
        
        // �������� �� ���������: + ��� -
        if(operators.get("+") == (int)lexAnalize.get(lexID) || operators.get("-") == (int)lexAnalize.get(lexID)){
            lexID++;
            sinResultText.setText(sinResultText.getText() + "<������� �������� � ���������> -> ��������: + ��� - | �������\n");
            result = true;
        }
        
        // �������� <������� ���������>
        if (result && simple_multiplier()) {
            sinResultText.setText(sinResultText.getText() + "<������� �������� � ���������> -> <������� ���������> | �������\n");
            result = true;
        } else {
            //sinResultText.setText(sinResultText.getText() + "<������� ���������> | ������\n");
            result = false;
        }
        
        return result;
    }
        
    //////////////////////////////////////
    
    //////////////////////////////////////
    // ���� ��������������� ������� (����������� ���������������)
    //////////////////////////////////////
    
    public void startSinAnalysisPrecedence(){
        ArrayList<String> tmpAnalize = new ArrayList<>();
        int i = 0, j = 0;
        String n1, n2;
        char tmpCh = ' ', value = ' ';

        tmpAnalize.add("0");
        tmpAnalize.add(lexAnalize.get(i).toString());

        do {
            do {
                i++;
                n1 = lexAnalize.get(i - 1).toString();
                if("14".equals(n1) || "15".equals(n1)){
                    i++;
                }
                n2 = lexAnalize.get(i).toString(); 

                // ������� ������� �� ��������� ������
                tmpAnalize.add(lexAnalize.get(i).toString());

                value = matrix[Integer.valueOf(n1)][Integer.valueOf(n2)];
                if(value == ' '){
                    operatorPercResultText.setText(operatorPercResultText.getText() + "������\n");
                    break;
                }
                
                value = matrix[Integer.valueOf(n1)][Integer.valueOf(n2)];
            } while (value != '>');

            value = matrix[Integer.valueOf(n1)][Integer.valueOf(n2)];
            if(value == ' '){
                break;
            } 

            do {
                if("16".equals(tmpAnalize.get(1))){
                    break;
                }
                
                j = tmpAnalize.size() - 2;

                var ii = tmpAnalize.get(j-1);
                var jj = tmpAnalize.get(j);
                
                value = matrix[Integer.valueOf(ii)][Integer.valueOf(jj)];
                if(value != '='){
                    operatorPercResultText.setText(operatorPercResultText.getText() + tmpAnalize.get(j) + "\n");
                } else {
                    operatorPercResultText.setText(operatorPercResultText.getText() + tmpAnalize.get(j) + " = ");
                } 
                
                tmpAnalize.remove(j);

                n1 = tmpAnalize.get(j - 1);
                n2 = tmpAnalize.get(j);

                value = matrix[Integer.valueOf(n1)][Integer.valueOf(n2)];
                
                if((tmpCh == '<' || tmpCh == '>') && '=' == value){
                    break;
                }
                
                tmpCh = matrix[Integer.valueOf(n1)][Integer.valueOf(n2)];

            } while (tmpCh != '<'); 
        } while(!"16".equals(tmpAnalize.get(1)));
    }

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea codeText;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private java.awt.Label label1;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label4;
    private javax.swing.JTextArea lexResultText;
    private javax.swing.JTextArea operatorPercResultText;
    private javax.swing.JButton runAnalysisBtn;
    private javax.swing.JTextArea sinResultText;
    // End of variables declaration//GEN-END:variables

}
