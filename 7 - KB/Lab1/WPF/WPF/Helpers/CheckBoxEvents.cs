﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WPF.Helpers
{
    public class CheckBoxBehaviour
    {
        #region checked
        public static readonly DependencyProperty CheckedCommandProperty =
               DependencyProperty.RegisterAttached("CheckedCommand", 
               typeof(ICommand), typeof(CheckBoxBehaviour), 
               new FrameworkPropertyMetadata(new PropertyChangedCallback(CheckedCommandChanged)));
        
        public static void SetCheckedCommand(UIElement element, ICommand value)
        {
            element.SetValue(CheckedCommandProperty, value);
        }

        public static ICommand GetCheckedCommand(UIElement element)
        {
            return (ICommand)element.GetValue(CheckedCommandProperty);
        }

        private static void CheckedCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckBox element = (CheckBox)d;

            element.Checked += new RoutedEventHandler(element_Checked); 
        }

        private static void element_Checked(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;

            ICommand command = GetCheckedCommand(element);

            command.Execute(e);
        }
        #endregion

        #region unchecked
        public static readonly DependencyProperty UncheckedCommandProperty =
           DependencyProperty.RegisterAttached("UncheckedCommand", typeof(ICommand),
           typeof(CheckBoxBehaviour), new FrameworkPropertyMetadata(
           new PropertyChangedCallback(UncheckedCommandChanged)));

        private static void UncheckedCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckBox element = (CheckBox)d;

            element.Unchecked += new RoutedEventHandler(element_Unchecked);
        }

        private static void element_Unchecked(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;

            ICommand command = GetUncheckedCommand(element);

            command.Execute(e);
        }

        public static void SetUncheckedCommand(UIElement element, ICommand value)
        {
            element.SetValue(UncheckedCommandProperty, value);
        }

        public static ICommand GetUncheckedCommand(UIElement element)
        {
            return (ICommand)element.GetValue(UncheckedCommandProperty);
        }
        #endregion
    }
}
