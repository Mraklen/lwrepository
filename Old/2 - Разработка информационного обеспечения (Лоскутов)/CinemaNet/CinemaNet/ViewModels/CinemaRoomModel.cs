﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaNet.Models;

namespace CinemaNet.ViewModels
{
    public class CinemaRoomModel
    {
        public int SessionID { get; set; }
        public Film Film { get; set; }
        public CinemaRoom CinemaRoom { get; set; }
        public List<Reserve> Reserves { get; set; }
    }
}
