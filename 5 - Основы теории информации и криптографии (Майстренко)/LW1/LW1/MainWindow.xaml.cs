﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LW1.Servises;
using LW1.Model;

namespace LW1
{
    public partial class MainWindow : Window
    {
        AlphabetManager alphabetManager { get; set; }

        public MainWindow()
        {
            alphabetManager = new AlphabetManager();
            InitializeComponent();
            UpdateMsgPanelUI();
            UpdateAlphabetPanelUI();
        }

        private void UpdateMsgPanelUI()
        {
            // Обновление информации в окне сообщейний
            CreateComboBoxItems(CurrentAlphabetInMsg);
        }
        private void UpdateAlphabetPanelUI()
        {
            // Обновление информации в окне алфавита
            CreateComboBoxItems(SelectAlphabet);
            UpdateAlphabetInfo();
        }
        private void UpdateMsgInfo()
        {
            var msgCharList = ConvertStringToListChar(Message.Text);

            AmountSymbolInMsg.Content = msgCharList.Count + " символа(ов)"; // Символов в сообщении

            if (CurrentAlphabetInMsg.SelectedIndex != -1)
            {
                var selectAlpabet = (ComboBoxItem) CurrentAlphabetInMsg.SelectedItem;
                var selectAlpabetName = selectAlpabet.Content.ToString();
                var alphabet = alphabetManager.GetAlphabet(selectAlpabetName);

                var checkAmountSymbols = CheckMessageSymbolInAlphabet(alphabet.Symbols, msgCharList);
                AmountSymbolMsgInAlphabet.Content = $"{checkAmountSymbols} символа(ов)";

                var bitInMsg = alphabetManager.CalcBitInMsg(selectAlpabetName, checkAmountSymbols);
                AmountBitInMsg.Content = $"{bitInMsg} бит";
            }
        }
        private void UpdateAlphabetInfo()
        {
            if (SelectAlphabet.SelectedIndex != -1)
            {
                var selectAlpabet = (ComboBoxItem) SelectAlphabet.SelectedItem;
                var alphabet = alphabetManager.GetAlphabet(selectAlpabet.Content.ToString());

                ViewAlphabetSymbols.Text = alphabet.Symbols;

                AmountSimbolInAlphabet.Content = alphabet.Symbols.Length;

                AmountBitInSymbol.Content = $"{alphabet.DimensionSymbol} бит";
            }
        }
        private void UpdateAlphabetEditorInfo()
        {
            var amountSymbol = Text_AlphabetContentInEditor.Text.Count();

            Label_AmountSimbolInEditor.Content = $"{amountSymbol} символа(ов)";

            Label_AmountBitInEditor.Content = $"{AlphabetItem.CalcDimensionSymbol(amountSymbol)} бит";
        }
        private void CreateComboBoxItems(ComboBox comboBox)
        {
            comboBox.Items.Clear();

            if (alphabetManager.Alphabets == null) return;

            foreach(var alphabet in alphabetManager.Alphabets)
            {
                comboBox.Items.Add(new ComboBoxItem { Content = alphabet.Name });
            }

            if (comboBox.SelectedIndex == -1)
                comboBox.SelectedIndex = 0;
        }
        private void InvisiblePanel(Panel elem)
        {
            elem.Visibility = Visibility.Collapsed;
        }
        private void VisiblePanel(Panel elem)
        {
            elem.Visibility = Visibility.Visible;
        }

        private void ClearEditorPanel()
        {
            Text_AlphabetNameInEditor.Text = "";
            Text_AlphabetContentInEditor.Text = "";
            Label_AmountBitInEditor.Content = "";
            Label_AmountSimbolInEditor.Content = "";
        }        

        private int CheckMessageSymbolInAlphabet(string alphabet, List<char> message)
        {
            return message.Where(c => { return alphabet.Contains(c); }).Count();
        }
        private List<char> ConvertStringToListChar(string msg)
        {
            return msg.ToCharArray().ToList();
        }
        
        private void Message_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateMsgInfo();
        }

        private void CurrentAlphabetInMsg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateMsgInfo();
        }

        private void SelectAlphabet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateAlphabetInfo();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            InvisiblePanel(Canvas_ViewAlphabet);
            VisiblePanel(Canvas_EditAlphabet);

            var alphabetName = ((ComboBoxItem) SelectAlphabet.SelectedItem).Content.ToString();

            Text_AlphabetNameInEditor.Text = alphabetName;
            Text_AlphabetContentInEditor.Text = alphabetManager.GetAlphabet(alphabetName).Symbols;

            UpdateAlphabetEditorInfo();
        }

        private void BtnNewAlphabet_Click(object sender, RoutedEventArgs e)
        {
            InvisiblePanel(Canvas_ViewAlphabet);
            VisiblePanel(Canvas_EditAlphabet);
        }

        private void Text_AlphabetContentInEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateAlphabetEditorInfo();
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            ClearEditorPanel();
            InvisiblePanel(Canvas_EditAlphabet);
            VisiblePanel(Canvas_ViewAlphabet);
        }

        private void Btn_Save_Click(object sender, RoutedEventArgs e)
        {
            var alphabetName = Text_AlphabetNameInEditor.Text;
            var alphabetSymbols = Text_AlphabetContentInEditor.Text;
            alphabetManager.AddAlphabet(new AlphabetItem(alphabetName, alphabetSymbols));

            alphabetManager.SaveAlpabetsToXml();

            InvisiblePanel(Canvas_EditAlphabet);
            VisiblePanel(Canvas_ViewAlphabet);

            UpdateAlphabetPanelUI();
            UpdateMsgPanelUI();
        }
    }
}
