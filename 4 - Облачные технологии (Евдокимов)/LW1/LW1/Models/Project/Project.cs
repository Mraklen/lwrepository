﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LW1.EditModels;

namespace LW1.Models.Project
{
    public class Project : IAggregate
    {
        public Guid Id { get; private set; }

        public string ApplicationUserId { get; private set; }
        public ApplicationUser ApplicationUser { get; private set; }

        public string Name { get; private set; }
        public string MainUrl { get; private set; }
        public DateTime? LastSelectionDate { get; private set; }

        HashSet<KeyWord> keyWords = new HashSet<KeyWord>();
        public IEnumerable<KeyWord> KeyWords
        {
            get => keyWords;
            private set
            {
                keyWords = new HashSet<KeyWord>(value);
            }
        }

        HashSet<Selection> selections = new HashSet<Selection>();
        public IEnumerable<Selection> Selections
        {
            get => selections;
            private set
            {
                selections = new HashSet<Selection>(value);
            }
        }

        public void AddNewSelection(IEnumerable<KeyWordResultEditModel> keyWordResult)
        {
            var prevSelectionKeyWords = selections?.OrderBy(x => x.DateSelection).LastOrDefault();

            var selectionUrls = keyWordResult.Select(x => SelectionUrl.CreateNew(x.UrlValue, x.Order, x.ServerKeywordId)).ToList();
            var s = selectionUrls.Where(x => x.Value.Contains(MainUrl)).ToList();

            var selectionKeyWords = selectionUrls.GroupBy(x => x.KeyWordId).Select(x =>
            {
                var mainUrls = x.Where(url => url.Value.Contains(MainUrl)).ToList();
                mainUrls.ForEach(su => su.SetMarked(su.Value.Contains(MainUrl)));

                int? bestPos = mainUrls?.Min(url => url.Order);
                int? offset = null;
                bool? isUp = null;
                if (prevSelectionKeyWords != null)
                {
                    var prevKeyWord = prevSelectionKeyWords.SelectionKeyWords.FirstOrDefault(kw => kw.KeyWordId == x.Key);
                    offset = prevKeyWord.Offset - bestPos;
                    if (offset != 0)
                    {
                        isUp = offset > 0;
                        offset = Math.Abs(offset.Value);
                    }
                    else
                    {
                        offset = null;
                    }
                }

                return SelectionKeyWord.CreateNew(x.Key, bestPos, offset, isUp, x);
            }).ToList();

            LastSelectionDate = DateTime.Now;
            selections.Add(Selection.CreateNew(selectionKeyWords));
        }

        public static Project CreateProject(string userId, string name, string mainUrl, string keyWords)
        {
            return new Project()
            {
                Id = Guid.NewGuid(),
                Name = name,
                KeyWords = keyWords.Split('\n').Select(x => KeyWord.CreateNew(x)).ToList(),
                MainUrl = mainUrl,
                ApplicationUserId = userId
            };
        }
    }
}
