﻿using LW4.ArithmeticCoding.Compresssor;
using LW4.ArithmeticCoding.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW4.ArithmeticCoding
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PrintTable();
        }

        private void PrintTable(char[] letters = null, double[] probability = null)
        {
            tableAlphabet.Controls.Clear();

            var controls = new List<Control>
            {
                new Label() { Text = "#" },
                new Label() { Text = "Символ" },
                new Label() { Text = "Частота появления" }
            };

            if (letters != null || probability != null)
                for (var i = 0; i < letters.Count(); i++)
                {
                    controls.Add(new Label() { Text = (i + 1).ToString() });
                    controls.Add(new Label() { Text = letters[i].ToString() });
                    controls.Add(new Label() { Text = probability[i].ToString() });
                }

            tableAlphabet.Controls.AddRange(controls.ToArray());
        }

        private void btnCreateArchive_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.SelectFile();                                        // выбрать и читать файл
            var file = FileManager.ReadFile(fileName);
            var text = Encoding.UTF8.GetString(file);                                    // конвертация в текст

            var (letters, probability) = ArithmeticMethod.CalcProbabilitySymbolsByText(text);
            var segments = ArithmeticMethod.DefineSegments(letters, probability);
            var code = ArithmeticMethod.Coding(segments, text);
            
            var initialText = ArithmeticMethod.Decoding(segments, code, text.Length, pbUnarchive);
            var initialTextBytes = Encoding.UTF8.GetBytes(initialText);
            FileManager.SaveFile(initialTextBytes, "UncompressedFiles", Path.GetFileNameWithoutExtension(fileName), "txt");

            var separatorBytes = new byte[] { 0, 255, 255, 0 };
            var codeBytes = BitConverter.GetBytes(code);
            var textLengthBytes = BitConverter.GetBytes(text.Length);
            var lettersBytes = Encoding.UTF8.GetBytes(letters);
            var probabilityBytes = Services.ByteConverter.DoubleArrayToBytes(probability);
            var archiveFile = codeBytes.Concat(separatorBytes)
                .Concat(textLengthBytes).Concat(separatorBytes)
                .Concat(lettersBytes).Concat(separatorBytes)
                .Concat(probabilityBytes).ToArray();
            
            lbMainFileSize.Text = $"{file.Count()} кб";
            lbArchiveFileSize.Text = $"{archiveFile.Count()} кб";
            lbSymbolCount.Text = $"{text.Count()}";
            lbAlphabetCount.Text = $"{letters.Count()}";
            lbPercentCompression.Text = $"{Math.Round(100 - ((archiveFile.Count() / (file.Count() * 1.0)) * 100), 2)} %";
            Task.Delay(100);
            PrintTable(letters, probability);

            FileManager.SaveFile(archiveFile, "CompressedFiles", Path.GetFileNameWithoutExtension(fileName), "ath");  // сохраняем
        }

        private void btnOpenArchive_Click(object sender, EventArgs e)
        {
            var fileName = FileManager.SelectFile(1);                                       // выбрать и читать файл
            var archiveFile = FileManager.ReadFile(fileName);

            var (code, textLength, letters, probability) = Services.ByteConverter.ConvertBytesToFileContent(archiveFile);
            
            var segments = ArithmeticMethod.DefineSegments(letters, probability);
            var initialText = ArithmeticMethod.Decoding(segments, code, textLength, pbUnarchive);

            var initialTextBytes = Encoding.UTF8.GetBytes(initialText);

            //lbMainFileSize.Text = $"{initialTextBytes.Count()} кб";
            //lbArchiveFileSize.Text = $"{archiveFile.Count()} кб";
            //lbSymbolCount.Text = $"{initialText.Count()}";
            //lbAlphabetCount.Text = $"{letters.Count()}";
            //lbPercentCompression.Text = $"{Math.Round(100 - ((archiveFile.Count() / (initialTextBytes.Count() * 1.0)) * 100), 2)} %";
            //Task.Delay(100);
            PrintTable(letters, probability);

            FileManager.SaveFile(initialTextBytes, "UncompressedFiles", Path.GetFileNameWithoutExtension(fileName), "txt");
        }
    }
}
