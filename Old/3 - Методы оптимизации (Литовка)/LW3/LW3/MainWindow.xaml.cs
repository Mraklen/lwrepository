﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LW3.Models;
using LW3.Services;

namespace LW3
{
    public partial class MainWindow : Window
    {
        Methods CalcFuncMethods { get; set; }
        
        public MainWindow()
        {
            InitializeComponent();
            CalcFuncMethods = new Methods();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var x1Value = Convert.ToSingle(Text_x1Value.Text);
            var x2Value = Convert.ToSingle(Text_x2Value.Text);
            var eValue = Convert.ToSingle(Text_eValue.Text);

            CalcFuncMethods.SetFunction(x1Value, x2Value, eValue);

            //CalcFuncMethods.CalcDescentMethod(new PlotterParam()
            //{
            //    Plotter = DescentPlotter,
            //    IsolineGraph = DescentIsoline,
            //    IsolineTracking = DescentTracking
            //}, lbDescentSteps, lbDescentExtrem);
            CalcFuncMethods.CalcGradientMethod(new PlotterParam()
            {
                Plotter = GredientPlotter,
                IsolineGraph = GredientIsoline,
                IsolineTracking = GredientTracking
            }, lbGredientSteps, lbGredientExtrem);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cbox_CurrenFunction.SelectedIndex)
            {
                case 0:
                    FunctionParam.CurrenIsolineType =
                        IsolineType.Ellipse;
                    break;
                case 1:
                    FunctionParam.CurrenIsolineType =
                        IsolineType.Rosenbroke;
                    break;
                case 2:
                    FunctionParam.CurrenIsolineType =
                        IsolineType.MM;
                    break;
            }
        }
    }
}
