﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MethodType
{
    type1,
    type2,
    type3
}

public enum EntityType
{
    line,
    surface
}

public class Method : MonoBehaviour {
    
    protected MethodType MethodType = MethodType.type1;
    protected EntityType EntityType = EntityType.line;
    
    public EntityType GetEntityType()
    {
        return EntityType;
    }
}
