﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Web.Logging
{
    public class FileLogger : ILogger
    {
        private string filePath;
        private object _lock = new object();
        public FileLogger(string path)
        {
            filePath = path;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            //return logLevel == LogLevel.Trace;
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter != null)
            {
                lock (_lock)
                {
                    var message = exception != null ? exception.ToString() : formatter(state, exception);
                    var path = Path.Combine(filePath, $"{DateTime.Now.ToString("yyyyMMdd")}_{logLevel.ToString()}_log.txt");
                    //File.AppendAllText(path, $"{DateTime.Now}: {message}" + Environment.NewLine);
                    var fullMessage = $"{DateTime.Now}: {message}" + Environment.NewLine;
                    using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.Write))
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.Write(fullMessage);
                    }
                }
            }
        }
    }
}
