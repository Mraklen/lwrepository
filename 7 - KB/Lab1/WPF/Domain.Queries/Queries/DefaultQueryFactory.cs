﻿using Domain.Queries.Queries.Implementations;
using Domain.Queries.Queries.Interfaces;
using QueryAbstractions.Queries;
using QueryAbstractions.Queries.Implementations;
using QueryAbstractions.ReadModels;
using QueryAbstractions.ViewModels;
using System;

namespace Data.Queries.Queries
{
    public class DefaultQueryFactory : IQueryFactory
    {
        private readonly IQueryDatabase database;

        public DefaultQueryFactory(IQueryDatabase database)
        {
            this.database = database;
        }

        public IBaseQueries<TEntity, TView, TID> GetBaseQueries<TEntity, TView, TID>()
            where TEntity : BaseReadModel<TID>
            where TView : BaseViewModel<TID>
            where TID : struct, IEquatable<TID> => new BaseQueries<TEntity, TView, TID>(database);

        public IRuleQueries GetRuleQueries() => new RuleQueries(database);
        public IVariableQueries GetVariableQueries() => new VariableQueries(database);
    }
}
