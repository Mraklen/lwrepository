﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Extentions;
using System;

public class LineRendering : EntityRender
{
    [SerializeField]
    Points MainPoints;

    [Space(10)]
    [SerializeField]
    LineRenderer LineRenderer;
    [SerializeField]
    LineRenderer MainLineRenderer;

    bool showVectors = false;

    protected override void Init()
    {
        GlobalEventSystem.Instance.AddEventListener(EventType.AddEntity, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.RemoveEntity, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.ChangeMethod, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.SupportLineShowChange, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.ShowOnlyCalcLine, OnEvent);
    }

    float time = 0;
    private void Update()
    {
        time += Time.deltaTime;
        if (time > 0.04f)
        {
            if (showMainLine && !showOnlyCalcLine)
            {
                MainLineRenderer.LineRenderByPoints(MainPoints.GetPositions().Select(x => x.transform.position));
            }
            else
            {
                MainLineRenderer.Clear();
            }
            time = 0;
        }
    }

    public override List<MainPoint> GetPositionMainPoints()
    {
        return MainPoints.GetPositions();
    }

    public List<Vector3> GetPositionMainPointsByVector3()
    {
        return MainPoints.GetPositions().Select(x => x.transform.position).ToList();
    }

    public override void LineRenderByPoint(Vector3 point, int index)
    {
        LineRenderer.RenderPoint(point, index);
    }

    public override void LineRenderByPoints(List<Vector3> points)
    {
        LineRenderer.LineRenderByPoints(points);
    }

    public override IEnumerator CoroutineLineRenderByPoints(List<Vector3> points)
    {
        for (var i = 0; i < points.Count(); i++)
        {
            LineRenderer.RenderPoint(points[i], i);
            yield return new WaitForSeconds(0.02f);
        }
    }

    private void ResetPointPosition(GameObject point)
    {
        point.transform.SetPositionAndRotation(new Vector3(200, 200), Quaternion.Euler(new Vector3(-90, 0, 0)));
    }

    public override void ClearLine()
    {
        LineRenderer.Clear();
    }

    public override int CountEntity()
    {
        return MainPoints.Count();
    }

    public void OnEvent(EventType eventType, Component component, object Param = null)
    {
        if (gameObject.activeInHierarchy)
            switch (eventType)
            {
                case EventType.AddEntity:
                    MainPoints.AddNewPoint(showVectors);
                    ResetPointPosition(MainPoints.GetLastPointGameObject());

                    GlobalEventSystem.Instance.PostEventMessage(EventType.PointCountChange, this, MainPoints.Count());
                    GlobalEventSystem.Instance.PostEventMessage(EventType.LastPointChange, this, MainPoints.GetLastPointGameObject());
                    break;
                case EventType.RemoveEntity:
                    if (MainPoints.Count() > minEntityCount)
                    {
                        MainPoints.RemoveLastPoint();
                        GlobalEventSystem.Instance.PostEventMessage(EventType.PointCountChange, this, MainPoints.Count());
                        GlobalEventSystem.Instance.PostEventMessage(EventType.LastPointChange, this, MainPoints.GetLastPointGameObject());
                    }
                    break;
                case EventType.SupportLineShowChange:
                    showMainLine = !showMainLine;
                    break;
                case EventType.ChangeMethod:
                    showVectors = (Param.GetType() == typeof(ParametricLines));
                    MainPoints.VectorShow(showVectors);
                    break;
                case EventType.ShowOnlyCalcLine:
                    showOnlyCalcLine = !showOnlyCalcLine;
                    MainPoints.Invisible(showOnlyCalcLine);
                    if (showVectors) { MainPoints.VectorShow(!showOnlyCalcLine); }
                    break;
            }
    }

    public void SetJsonPoints(List<JsonPoint> jsonPoints)
    {
        MainPoints.ClearPoints();
        ClearLine();

        MainPoints.SetJsonPoints(jsonPoints);
    }

    public void SetJsonMainPoints(List<JsonMainPoint> jsonPoints)
    {
        MainPoints.ClearPoints();
        ClearLine();

        MainPoints.SetJsonMainPoints(jsonPoints);
    }


    internal List<JsonPoint> GetJsonPoints()
    {
        return MainPoints.GetJsonPoints();
    }

    internal List<JsonMainPoint> GetJsonMainPoints()
    {
        return MainPoints.GetJsonMainPoints();
    }
}
