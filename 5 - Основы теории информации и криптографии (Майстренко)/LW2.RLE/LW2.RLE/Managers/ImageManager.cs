﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.RLE.Managers
{
    public class ImageManager
    {
        public static List<int> CreateImageDataByBitmap(Bitmap bmp, int width, int height)
        {
            var imageData = new List<int>();
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var color = bmp.GetPixel(x, y);
                    if (color.ToArgb() == Color.Black.ToArgb())
                    {
                        imageData.Add(0);
                    }
                    else
                    {
                        imageData.Add(1);
                    }
                }
            }
            return imageData;
        }

        public static Bitmap CreateBitmapByImageData(List<int> imageData, int width, int height)
        {
            var bitmap = new Bitmap(width, height);

            using (Graphics graph = Graphics.FromImage(bitmap))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.White, ImageSize);
            }

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var index = y * width + x;
                    if (imageData.ElementAt(index) == 0)
                    {
                        bitmap.SetPixel(x, y, Color.Black);
                    }
                }
            }

            return bitmap;
        }
    }
}
