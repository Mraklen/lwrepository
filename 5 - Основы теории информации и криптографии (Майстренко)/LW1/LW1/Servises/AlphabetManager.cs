﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Servises
{
    public class AlphabetManager
    {
        public List<AlphabetItem> Alphabets { get; private set; }
        string fileAlphabetName { get; set; }

        public AlphabetManager()
        {
            fileAlphabetName = $"{Directory.GetCurrentDirectory()}/Alphabets.xml";
            LoadAlphabetFromXml();
        }

        public void AddAlphabet(AlphabetItem alphabetItem)
        {
            int alphabetId = Alphabets.IndexOf(Alphabets.Where(a => a.Name == alphabetItem.Name).SingleOrDefault());
            if (alphabetId == -1)
            {
                Alphabets.Add(alphabetItem);
            }
            else
            {
                Alphabets[alphabetId] = alphabetItem;
            }
        }

        public AlphabetItem GetAlphabet(string name)
        {
            return Alphabets.Where(a => a.Name == name).FirstOrDefault();
        }

        public int CalcBitInMsg(string alphabetName, int amountSymbols)
        {
            return amountSymbols * GetAlphabet(alphabetName).DimensionSymbol;
        }

        public void SaveAlpabetsToXml()
        {
            SaveListToXML(Alphabets, fileAlphabetName);
        }
        public void LoadAlphabetFromXml()
        {
            if (File.Exists(fileAlphabetName))
                Alphabets = StartParsingXMLToRegion(fileAlphabetName);
        }

        private List<AlphabetItem> StartParsingXMLToRegion(string fileName)
        {
            using (var xmlReader = new StreamReader(fileName))
            {
                var alphabets = new List<AlphabetItem>();
                var doc = XDocument.Load(xmlReader);

                foreach (XElement alphabet in doc.Element("ArrayOfAlphabetItem").Elements("AlphabetItem"))
                {
                    string name = alphabet.Element("Name").Value;
                    string symbols = alphabet.Element("Symbols").Value;

                    alphabets.Add(new AlphabetItem(name, symbols));
                }

                return alphabets;
            }
        }
        private void SaveListToXML(List<AlphabetItem> list, string fileName)
        {
            XmlSerializer xmlFormat = new XmlSerializer(typeof(List<AlphabetItem>));
            using (Stream fStream =
                new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlFormat.Serialize(fStream, list);
            }
        }
    }
}
