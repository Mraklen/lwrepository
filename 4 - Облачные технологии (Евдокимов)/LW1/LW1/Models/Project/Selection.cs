﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Models.Project
{
    public class Selection : IEntity<int>
    {
        public int Id { get; private set; }

        public int ProjectId { get; private set; }
        public Project Project { get; private set; }

        public DateTime DateSelection { get; private set; }

        HashSet<SelectionKeyWord> selectionKeyWords = new HashSet<SelectionKeyWord>();
        public IEnumerable<SelectionKeyWord> SelectionKeyWords
        {
            get => selectionKeyWords;
            private set
            {
                selectionKeyWords = new HashSet<SelectionKeyWord>(value);
            }
        }

        public static Selection CreateNew(IEnumerable<SelectionKeyWord> selectionKeyWords)
        {
            return new Selection()
            {
                DateSelection = DateTime.Now,
                SelectionKeyWords = selectionKeyWords
            };
        }
    }
}
