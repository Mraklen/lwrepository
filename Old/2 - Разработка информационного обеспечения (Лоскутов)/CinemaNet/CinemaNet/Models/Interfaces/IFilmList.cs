﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface IFilmList
    {
        IEnumerable<Film> FilmList();
        IEnumerable<Film> FilmList(int cinemaID);
        void RemoveFilmById(int id);
        void AddFilm(Film film);
    }
}
