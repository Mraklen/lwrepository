﻿using System.Collections.Generic;

namespace QueryAbstractions.TableModel
{
    public class TableAnswer<TView> where TView : class
    {
        public IEnumerable<TView> Data { get; set; }
        public FilterModel FilterModel { get; set; }
    }

    public class FilterModel
    {
        public int? TotalItems { get; set; }
    }
}
