﻿using Domain.Queries.Queries.Interfaces;
using QueryAbstractions.Queries;
using QueryAbstractions.ReadModels;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries.Queries
{
    public interface IQueryFactory
    {
        IBaseQueries<TEntity, TView, TID> GetBaseQueries<TEntity, TView, TID>()
            where TEntity : BaseReadModel<TID>
            where TView : BaseViewModel<TID>
            where TID : struct, IEquatable<TID>;

        IVariableQueries GetVariableQueries();
        IRuleQueries GetRuleQueries();
    }
}
