﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW2.Models
{
    public class PointXY : IComparable<PointXY>
    {
        public double X1 { get; set; }
        public double X2 { get; set; }
        public double Y { get; set; }

        public void SetValues(double x1, double x2, double y = 0)
        {
            X1 = x1;
            X2 = x2;
            Y = y;
        }

        public string PrintPointXY()
        {
            return $"({Math.Round(X1, 2)} ; {Math.Round(X2, 2)})";
        }

        public int CompareTo(PointXY other)
        {
            if (Y > other.Y)
                return 1;
            if (Y < other.Y)
                return -1;
            else
                return 0;
        }

        public PointXY Clone()
        {
            return new PointXY { X1 = X1, X2 = X2, Y = Y };
        }
    }
}
