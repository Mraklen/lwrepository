﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LW1.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LW1.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {
        private readonly IDomainRepositoryFactory repositoryFactory;

        public ProjectsController(IDomainRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        [HttpGet]
        [Route("GetActiveProjects")]
        public IActionResult GetProjects()
        {
            try
            {
                var repos = repositoryFactory.GetProjectRepository();
                var projects = repos.GetActiveProject().ToList();

                var res2 = projects.Select(x => new
                {
                    ProjectId = x.Id,
                    x.LastSelection,
                    KeyWords = x.KeyWords.Select(e => new
                    {
                        KeyWordId = e.Id,
                        e.Value
                    }).ToList(),
                    Settings = new List<object>(new[] {
                new
                {
                    SearchEngine = 0,
                    SearchDepth = 50,
                    RegionsId = new[] { 481 }
                }
                }).ToList()
                }).ToList();

                return Ok(res2);
            }
            catch (Exception ex)
            {
                return Ok();
            }


        }
    }
}
