﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Models.Entities.KnowledgeBase;
using QueryAbstractions.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Queries.ViewModels.KnowledgeBase
{
    public class RuleParameterViewModel : BaseViewModel<int>
    {
        public bool? State { get; set; }

        public ParamType ParamType { get; set; }
        
        public ParameterViewModel Parameter { get; set; }
    }
}
