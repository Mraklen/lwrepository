﻿using Assets.Scripts.Methods;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GuiManager : Singleton<GuiManager>
{
    [SerializeField] Text TextInfo;
    [SerializeField] Text CollisionInfo;
    [SerializeField] Text PointCount;
    [SerializeField] Text SelectPointPosition;
    [Space(10)]
    [SerializeField] GameObject ModelPanel;
    [SerializeField] Button RenderBtn;
    [SerializeField] Slider RotateAngleSlider;
    [Space(10)]
    [SerializeField] GraphicRaycaster GraphicRaycaster;
    [SerializeField] EventSystem EventSystem;
    [Space(10)]
    [SerializeField] GameObject HelpPanel;
    [Space(10)]
    [SerializeField] GameObject SavePanel;
    [Space(10)]
    [SerializeField] GameObject InputDialog;

    public override void StartInit()
    {
        GlobalEventSystem.Instance.AddEventListener(EventType.PointCountChange, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.ModelModChange, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.QualityChanged, OnEvent);
        GlobalEventSystem.Instance.AddEventListener(EventType.CollisionChange, OnEvent);
    }

    public override void UpdateInit()
    {
        if (InputDialog.activeInHierarchy) return;

        // добавить точку
        if (Input.GetKeyDown(KeyCode.KeypadPlus)) AddPointEvent();
        // удалить последнюю добавленную точку
        if (Input.GetKeyDown(KeyCode.KeypadMinus)) RemovePointEvent();

        // включить/выключить сглаживание 
        if (Input.GetKeyDown(KeyCode.Q)) ChangeQualityMod();

        // это для лабораторной работы Литовки (связано с обнаружением коллизий) / (Collision.Instance - выключено)
        // if (Input.GetKeyDown(KeyCode.X)) CollisionMod();

        // включить модуль рендера моделей
        if (Input.GetKeyDown(KeyCode.C)) CreateModelMod();

        // 3d => 2d && 2d => 3d
        if (Input.GetKeyDown(KeyCode.Tab)) ProgectionChange();

        // 2d
        if (Input.GetKeyDown(KeyCode.Keypad1)) InterpolationPlay();
        if (Input.GetKeyDown(KeyCode.Keypad2)) BeziersPlay();
        if (Input.GetKeyDown(KeyCode.Keypad3)) ParametricLinesPlay();
        if (Input.GetKeyDown(KeyCode.Keypad4)) BSplinePlay();

        // 3d
        if (Input.GetKeyDown(KeyCode.Keypad5)) BeziersSurfacePlay();

        // sub methods
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad1)) ChangeMethodType(MethodType.type1);
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad2)) ChangeMethodType(MethodType.type2);

        // линия соединяющая точки
        if (Input.GetKeyDown(KeyCode.L)) ShowSupportLine();

        // показывать только основную линию
        if (Input.GetKeyDown(KeyCode.M)) ShowOnlyMainLine();

        // замедленная отрисовка линии
        if (Input.GetKeyDown(KeyCode.RightControl)) SlowMotionRender();

        // показать элементы управления точкой в 3д пространстве
        if (Input.GetKeyDown(KeyCode.X)) ShowAxis();

        // Панелька помощи
        if (Input.GetKeyDown(KeyCode.F1)) ShowHelp();

        // Панелька сохранения точек
        if (Input.GetKeyDown(KeyCode.P)) ShowSavePanel();

        if (MoveObject.SelectedObject) SelectPointPosition.text = CreateTextPointPos(MoveObject.SelectedObject.transform.position);
    }

    private void ShowSavePanel()
    {
        SavePanel.SetActive(!SavePanel.activeInHierarchy);
    }

    private void ShowHelp()
    {
        HelpPanel.SetActive(!HelpPanel.activeInHierarchy);
    }

    private void CollisionMod()
    {
        CollisionInfo.gameObject.SetActive(!CollisionInfo.gameObject.activeInHierarchy);
        //LW1.Collision.Collision.Instance.gameObject.SetActive(!LW1.Collision.Collision.Instance.gameObject.activeInHierarchy);

        if (CollisionInfo.gameObject.activeInHierarchy)
        {
            CollisionInfo.text = "";
        }
    }

    public bool GetRaycastLayer(int layer)
    {
        var pointerData = new PointerEventData(EventSystem);
        pointerData.position = Input.mousePosition;
        var result = new List<RaycastResult>();
        GraphicRaycaster.Raycast(pointerData, result);
        return result.Any(x => x.gameObject.layer == layer);
    }

    private void ChangeQualityMod()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeQuality, this);
    }

    private void CreateModelMod()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.CreateModelMod, this);
    }

    public void ModelModChange(bool? value = null)
    {
        ModelPanel.SetActive(value ?? !ModelPanel.activeInHierarchy);
        ShowMessage(ModelPanel.activeInHierarchy ? "Создание модели: вкл" : "Создание модели: выкл");
    }

    private string CreateTextPointPos(Vector3 position)
    {
        var x = Math.Round(position.x - 200, 2);
        var y = Math.Round(position.y - 200, 2);
        var z = Math.Round(position.z, 2);
        return $"P({x}; {y}; {z})";
    }

    private void ShowAxis()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.AxisMoveShow, this);
    }

    private void ProgectionChange()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.CameraProjectionChange, this);
    }

    private void SlowMotionRender()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.SlowMotionRender, this);
    }

    private void ChangeMethodType(MethodType type)
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethodType, this, type);
    }

    private void ShowOnlyMainLine()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ShowOnlyCalcLine, this);
    }
    
    private void ShowSupportLine()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.SupportLineShowChange, this);
    }

    private void BeziersPlay()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethod, this, new Beziers());
        ShowMessage("Метод Безье");
    }

    private void InterpolationPlay()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethod, this, new Interpolation());
        ShowMessage("Интерполяция Лагранжа");
    }

    private void ParametricLinesPlay()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethod, this, new ParametricLines());
        ShowMessage("Параметрические кубические кривые");
    }

    private void BSplinePlay()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethod, this, new BSpline());
        ShowMessage("B-сплайны");
    }

    private void BeziersSurfacePlay()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.ChangeMethod, this, new BeziersSurface());
        ShowMessage("Поверхность: Безье");
    }

    public void AddPointEvent()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.AddEntity, this);
    }

    public void RemovePointEvent()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.RemoveEntity, this);
    }

    public void RenderEvent()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.RenderEvent, this);
        ShowMessage($"Рендеринг...");
    }

    public void RotateAngleChange()
    {
        GlobalEventSystem.Instance.PostEventMessage(EventType.RotateAngleChange, this, RotateAngleSlider.value);
        ShowMessage($"Угол поворота: {RotateAngleSlider.value}");
    }

    public void ShowMessage(string message)
    {
        StopAllCoroutines();
        StartCoroutine(SendInfoMessage(message));
    }

    private IEnumerator SendInfoMessage(string text)
    {
        TextInfo.text = text;
        yield return new WaitForSeconds(3);
        TextInfo.text = "";
    }

    private void ShowCollisionInfo(string message)
    {
        CollisionInfo.text = message;
    }

    public void OnEvent(EventType eventType, Component component, object Param = null)
    {
        switch (eventType)
        {
            case EventType.PointCountChange:
                PointCount.text = Param.ToString();
                break;
            case EventType.ModelModChange:
                ModelModChange((bool?) Param);
                break;
            case EventType.QualityChanged:
                ShowMessage($"Сглаживание: {((bool)Param ? "вкл" : "выкл")}");
                break;
            case EventType.CollisionChange:
                ShowCollisionInfo(Param.ToString());
                break;
        }
    }

    public override void AwakeInit() { }
}
