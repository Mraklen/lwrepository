// <auto-generated />
namespace DB.EFData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class updRuleParameter_2134 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updRuleParameter_2134));
        
        string IMigrationMetadata.Id
        {
            get { return "201903161839504_updRuleParameter_2134"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
