﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models.Interfaces
{
    public interface ICinemaList
    {
        IEnumerable<Cinema> CinemaList();
        IEnumerable<Cinema> CinemaList(int filmID);
    }
}
