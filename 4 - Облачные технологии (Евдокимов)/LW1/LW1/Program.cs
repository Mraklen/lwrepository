﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LW1.Web.Logging;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace LW1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddFilter<FileLoggerProvider>((category, level) =>
                    (level == LogLevel.Critical || level == LogLevel.Error)
                    || category.StartsWith("Likors.Web.Controllers")
                    || category.StartsWith("Likors.Core.Sagas"));
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
                logging.AddDebug();
                logging.AddFile(hostingContext.HostingEnvironment.ContentRootPath);
            })
                .UseStartup<Startup>()
                .Build();
    }
}
