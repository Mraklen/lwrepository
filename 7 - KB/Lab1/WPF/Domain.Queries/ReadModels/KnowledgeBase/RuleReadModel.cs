﻿using QueryAbstractions.ReadModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Queries.ReadModels.KnowledgeBase
{
    public class RuleReadModel : BaseReadModel<int>
    {
        public ICollection<RuleParameterReadModel> RuleParameters { get; set; }
    }
}
