﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Model
{
    public class SegmentAB
    {
        public Dictionary<string, PointXY> Points { get; set; }
    }
}
