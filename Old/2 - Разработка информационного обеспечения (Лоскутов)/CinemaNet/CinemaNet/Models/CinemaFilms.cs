﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.Models
{
    public class CinemaFilms
    {
        public int FilmID { get; set; }
        public Film Film { get; set; }

        public int CinemaID { get; set; }
        public Cinema Cinema { get; set; }
    }
}
