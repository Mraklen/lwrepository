﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.NeuronNet
{
    public enum NeuronType
    {
        Hidden,
        Output
    }

    public class Neuron
    {
        /// <summary> Тип нейрона </summary>
        public NeuronType Type { get; private set; }

        /// <summary> Входы нейрона </summary>
        public List<double> Inputs { get; private set; }
        /// <summary> Весовые коэффициенты нейрона </summary>
        public List<double> Weights { get; private set; }

        /// <summary> Выход нейрона </summary>
        public double Output => Activator(Inputs, Weights);

        public Neuron(List<double> inputs, List<double> weights, NeuronType type)
        {
            Type = type;
            Inputs = inputs;
            Weights = weights;
        }

        internal void UpdateInputs(List<double> inputs)
        {
            Inputs = inputs;
        }

        /// <summary> Функция активации нейрона </summary>
        /// <param name="inputs">Входы нейрона</param>
        /// <param name="weights">Веса входов нерона (размер всегда на 1 больше чем входов)</param>
        /// <returns>Выход нейрона</returns>
        private double Activator(IEnumerable<double> inputs, IEnumerable<double> weights)
        {
            var sum = 0.0;

            // линейные преобразования
            for (var i = 0; i < inputs.Count(); i++) sum += inputs.ElementAt(i) * weights.ElementAt(i);

            //// добавляем в рассчеты так же нейрон смещения
            //sum += weights.Last();

            // нелинейные преобразования
            return Math.Pow(1 + Math.Exp(-sum), -1);
        }

        /// <summary> Производная для текущей функции активации </summary>
        /// <param name="outSignal">Исходящий сигнал нейрона</param>
        public double OutputDerivative(double outSignal) => outSignal * (1 - outSignal);

        /// <summary> Функция градиента </summary>
        /// <param name="error"></param>
        /// <param name="dif"></param>
        /// <param name="nextGradientSum"> Сумма градиентов следующего слоя </param>
        public double Gradientor(double error, double dif, double nextGradientSum) => Type == NeuronType.Output ? error * dif : nextGradientSum * dif;
    }
}
