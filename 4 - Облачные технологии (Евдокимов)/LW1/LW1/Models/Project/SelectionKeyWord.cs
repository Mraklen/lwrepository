﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Models.Project
{
    public class SelectionKeyWord : IEntity<int>
    {
        public int Id { get; private set; }

        public int SelectionId { get; private set; }
        public Selection Selection { get; private set; }

        public int KeyWordId { get; private set; }
        public KeyWord KeyWord { get; private set; }

        public int? BestPosition { get; private set; }
        public int? Offset { get; private set; }
        public bool? IsUp { get; private set; }

        HashSet<SelectionUrl> selectionUrls = new HashSet<SelectionUrl>();
        public IEnumerable<SelectionUrl> SelectionUrls
        {
            get => selectionUrls;
            private set
            {
                selectionUrls = new HashSet<SelectionUrl>(value);
            }
        }

        public static SelectionKeyWord CreateNew(int keyWordId, int? bestPos, int? offset, bool? isUp, IEnumerable<SelectionUrl> selectionUrls)
        {
            return new SelectionKeyWord() {
                KeyWordId = keyWordId,
                BestPosition = bestPos,
                Offset = offset,
                IsUp = isUp,
                SelectionUrls = selectionUrls
            };
        }
    }
}
