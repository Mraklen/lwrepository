﻿using SeatBattle.CSharp.EventManager;
using System;
using System.Windows.Forms;

namespace SeatBattle.CSharp
{
    static class Program
    {
        private delegate DialogResult ShowSaveFileDialogInvoker();
        private static StartForm StartForm;
        private static MainForm GameForm;

        [STAThread]
        static void Main()
        {
            GlobalEventManager.Instance.AddEventListener(EVENT_TYPE.StartGame, OvEvent);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);
            Application.Run(StartForm = new StartForm());
        }

        private static void OvEvent(EVENT_TYPE Event_Type, object Param)
        {
            switch(Event_Type)
            {
                case EVENT_TYPE.StartGame:
                    GameForm = new MainForm();
                    ShowSaveFileDialogInvoker invoker = new ShowSaveFileDialogInvoker(GameForm.ShowDialog);
                    StartForm.NetworkStop();
                    StartForm.Invoke(invoker);
                    break;
            }
        }
    }
}
