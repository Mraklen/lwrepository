﻿using Domain.Models.Entities.NeuronNet.Layers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.NeuronNet
{
    public class NNet
    {
        public int NumHiddenNeuron { get; private set; } = 4;
        public int NumOutput { get; private set; } = 1;
        
        bool AddShift { get; set; }

        public InputLayer InputLayer { get; private set; }
        public HiddenLayer HiddenLayer { get; private set; }
        public OutputLayer OutputLayer { get; private set; }

        public List<double> Fact { get; set; }

        public static NNet CreateNew(int numHiddenNeuron, int numOutput, int numInput, bool addShift = false)
        {
            return new NNet
            {
                NumHiddenNeuron = numHiddenNeuron,
                NumOutput = numOutput,
                AddShift = addShift,
                Fact = new List<double>(),
                InputLayer = new InputLayer(),
                HiddenLayer = new HiddenLayer(numHiddenNeuron, numInput, NeuronType.Hidden, $"{nameof(HiddenLayer)}_{numHiddenNeuron}{(addShift ? "___" : "")}", addShift),
                OutputLayer = new OutputLayer(numOutput, numHiddenNeuron, NeuronType.Output, $"{nameof(OutputLayer)}_{numHiddenNeuron}{(addShift ? "___" : "")}", addShift)
            };
        }

        /// <summary> Ошибка одной итерации </summary>
        double GetError(List<double> errors)
        {
            return 0.5 * errors.Sum(x => Math.Pow(x, 2));
        }

        /// <summary> Ошибка одной эпохи </summary>
        double GetCost(List<double> values)
        {
            return values.Sum(x => x) / values.Count;
        }

        /// <summary> Обучение сети </summary>
        public List<string> Train()
        {
            var result = new List<string>();

            // порог ошибки
            const double e = 0.001;
            // текущая ошибка эпохи
            var tmpCost = e;

            for (var n = 1; tmpCost >= e; n++)
            {
                // хранение ошибок
                var tmpErrors = new List<double>();

                for (int i = 0; i < InputLayer.TrainSet.Count; i++)
                {
                    // прямой проход
                    HiddenLayer.Data = InputLayer.TrainSet[i].Inputs;
                    HiddenLayer.Recognize(null, OutputLayer);
                    OutputLayer.Recognize(this, null);

                    // ошибка по итерации
                    var errors = new List<double>();

                    for (var x = 0; x < InputLayer.TrainSet[i].Outputs.Count; x++)
                    {
                        errors.Add(InputLayer.TrainSet[i].Outputs[x] - Fact[x]);
                    }

                    tmpErrors.Add(GetError(errors));

                    // обратный проход и корректировка весовых коэффициентов 
                    var tmpGradientSums = OutputLayer.BackwardPass(errors);
                    HiddenLayer.BackwardPass(tmpGradientSums);
                }

                // ошибка эпохи
                tmpCost = GetCost(tmpErrors);

                result.Add($"Результат {n} эпохи: {tmpCost}");
            }
            
            // сохраняем весовые коэффициенты
            HiddenLayer.WeightInit(MemoryMode.SET, $"{nameof(HiddenLayer)}_{NumHiddenNeuron}{(AddShift ? "___" : "")}");
            OutputLayer.WeightInit(MemoryMode.SET, $"{nameof(OutputLayer)}_{NumHiddenNeuron}{(AddShift ? "___" : "")}");

            return result;
        }

        /// <summary> Тестирование сети </summary>
        public List<string> Test()
        {
            var result = new List<string>();

            for (int i = 0; i < InputLayer.TrainSet.Count; i++)
            {
                HiddenLayer.Data = InputLayer.TrainSet[i].Inputs;
                HiddenLayer.Recognize(null, OutputLayer);
                OutputLayer.Recognize(this, null);

                result.Add($"{i + 1} - сет. Результат теста: {string.Join(", ", Fact)} --- {string.Join(", ", Fact.Select(x => Math.Round(x)))}");
            }

            return result;
        }
    }
}
