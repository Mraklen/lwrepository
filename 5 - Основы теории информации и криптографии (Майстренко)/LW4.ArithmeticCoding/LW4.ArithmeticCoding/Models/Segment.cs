﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW4.ArithmeticCoding.Models
{
    public class Segment
    {
        public double Left { get; set; }
        public double Right { get; set; }
    }
}
