﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiveTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine($"Список сообщений почтового ящика: {ConfigurationManager.AppSettings["MailUserEmail"]}");

            var result = EmailManager.GetAllMail();
            Console.WriteLine($"\nКоличество сообщений в ящике: {result.TotalCount}\n");
            for (var i = 0; i < result.Messages.Count(); i++)
                Console.WriteLine($"{i + 1}: {result.Messages[i]}");

            Console.WriteLine("\nДля продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }
    }

    public class EmailManager
    {
        public static EmailFolder GetAllMail()
        {
            var mailUserMail = ConfigurationManager.AppSettings["MailUserEmail"];
            var mailUserPass = ConfigurationManager.AppSettings["MailUserPass"];
            var mailServerHost = ConfigurationManager.AppSettings["MailServerHost"];
            var mailServerPort = ConfigurationManager.AppSettings["MailServerPort"];

            using (var client = new ImapClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect(mailServerHost, Convert.ToInt32(mailServerPort), SecureSocketOptions.SslOnConnect);
                client.Authenticate(mailUserMail, mailUserPass);

                var inbox = client.Inbox;
                inbox.Open(FolderAccess.ReadOnly);

                var result = new EmailFolder()
                {
                    TotalCount = inbox.Count,
                    Messages = inbox.Select(x => $"От кого: {x.From} || Тема: {x.Subject}").ToList()
                };

                client.Disconnect(true);
                return result;
            }
        }
    }

    public class EmailFolder
    {
        public int TotalCount { get; set; }
        public List<string> Messages { get; set; }
    }
}
