﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Entities.NeuronNet.Layers
{
    public class HiddenLayer : Layer
    {
        public HiddenLayer(int numNeuron, int numPrevNeuron, NeuronType type, string memoryName, bool addShift = false) : base(numNeuron, numPrevNeuron, type, memoryName, addShift) { }

        public override void Recognize(NNet net, Layer nextLayer)
        {
            nextLayer.Data = Neurons.Select(x => x.Output).ToList();
        }

        public override List<double> BackwardPass(List<double> gradientSum)
        {
            for (var i = 0; i < NumNeuron; i++)
            {
                for (var n = 0; n < NumPrevNeuron; n++)
                {
                    var inputValue = AddShift && n == NumPrevNeuron - 1 ? 1 : Neurons[i].Inputs[n];

                    // коррекция весовых коэффициентов
                    Neurons[i].Weights[n] += LearningRate * inputValue * Neurons[i].Gradientor(0, Neurons[i].OutputDerivative(Neurons[i].Output), gradientSum[i]);
                }
            }
            return null;
        }
    }
}
