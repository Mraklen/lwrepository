﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Web.Logging
{
    public static class FileLoggerFactoryExtensions
    {
        public static ILoggingBuilder AddFile(this ILoggingBuilder builder, string logPath)
        {
            builder.AddProvider(new FileLoggerProvider(logPath));
            return builder;
        }
    }
}
