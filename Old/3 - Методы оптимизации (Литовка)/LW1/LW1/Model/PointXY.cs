﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Model
{
    public class PointXY : IComparable<PointXY>
    {
        public double X { get; set; }
        public double Y { get; set; }

        public void SetValues(double x, double y)
        {
            X = x;
            Y = y;
        }

        public string PrintPointXY()
        {
            return $"({Math.Round(X, 3)} ; {Math.Round(Y, 3)})";
        }

        public int CompareTo(PointXY other)
        {
            if (Y > other.Y)
                return 1;
            if (Y < other.Y)
                return -1;
            else
                return 0;
        }

        public PointXY Clone()
        {
            return new PointXY { X = X, Y = Y };
        }
    }
}
