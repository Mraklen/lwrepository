﻿using CinemaNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaNet.ViewModels
{
    public class CinemaViewModel
    {
        public Cinema Cinema { get; set; }
        public List<Film> Films { get; set; }
    }
}
