﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Models
{
    public interface IAggregate
    {
        Guid Id { get; }
    }
}
