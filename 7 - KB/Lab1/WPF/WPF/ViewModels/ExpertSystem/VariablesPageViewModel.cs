﻿using Domain.Models.Entities.KnowledgeBase;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Unity;
using WPF.Events;
using WPF.State;
using WPF.ViewModels.Base;

namespace WPF.ViewModels.ExpertSystem
{
    class VariablesPageViewModel : BaseViewModel, IDisposable
    {
        Visibility stateCheckVis;
        public Visibility StateCheckVis { get => stateCheckVis; set { stateCheckVis = value; OnPropertyChanged("StateCheckVis"); } }
        
        Visibility varViw;
        public Visibility VarVis { get => varViw; set { varViw = value; OnPropertyChanged("VarVis"); } }
        
        public DelegateCommand AcceptComplaintsCommand { get; }
        public DelegateCommand StateChangeCommand { get; }
        
        public List<Variable> Variables { get => stateStore.State.Variables?.Where(x => x.IsVisible).ToList(); }

        public VariablesPageViewModel(AppStateStore stateStore, IUnityContainer container) : base(stateStore, container)
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<UpdateDataEvent>>().Subscribe(UpdateModel);

            AcceptComplaintsCommand = new DelegateCommand(action: AcceptComplaintsEvent);
            StateChangeCommand = new DelegateCommand(action: StateChangeEvent);

            StateCheckVis = Visibility.Hidden;
            VarVis = Visibility.Visible;
        }

        private void StateChangeEvent()
        {
            StateCheckVis = Variables.Any(x => x.Parameters.Any(p => p.State.HasValue && p.State.Value)) ? Visibility.Visible : Visibility.Hidden;
        }

        private void UpdateModel(UpdateDataEvent obj)
        {
            OnPropertyChanged("Variables");
        }

        private void AcceptComplaintsEvent()
        {
            stateStore.EventAggregator.GetEvent<PubSubEvent<AcceptComplaintsEvent>>().Publish(new AcceptComplaintsEvent());
            VarVis = Visibility.Hidden;
        }
        
        public void Dispose() { }
    }
}
