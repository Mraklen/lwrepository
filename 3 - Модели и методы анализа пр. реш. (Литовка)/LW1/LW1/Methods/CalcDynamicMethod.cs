﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LW1.Methods
{
    public class CalcDynamicMethod
    {
        public static double dt = 0.2;
        static double tmax = 1200;

        static double ro = 0.12;
        static double S = 0.75;
        static double P0 = 7900;
        static double P1 = 7600;

        static double Tp = 90;
        static double r = (2.26 * Math.Pow(10, 6));
        static double kt = 5000;
        static double F = 10;
        static double ct = 4187;

        public static double deltaCinput = 2.0;
        public static double deltaMass_input = 2.5;
        public static double deltaT = 9.0;

        public static List<Point> CalcDisturbance(double value, double delta)
        {
            var result = new List<Point>
            {
                new Point() { X = 0, Y = value }
            };
            var newValue = value + delta;
            for (var t = 0.0; t <= tmax; t += dt)
            {
                result.Add(new Point() { X = t, Y = newValue });
            }
            return result;
        }

        public static List<Point> CalcDynamicConcentration(double m_input, double m_out, double m_vt, double C_input, double C_out, double Tп, bool isEuler)
        {
            var result = new List<Point>() { };
            var prevY = new Point() { X = 0, Y = C_out };
            var newPoint = new Point();
            var newCinput = C_input + deltaCinput;
            result.Add(prevY);
            for (var t = 0.0; t <= tmax; t += dt)
            {
                var YbyT = dCout_dT(m_input, m_out, m_vt, newCinput, prevY.Y, Tп);
                if (isEuler)
                {
                    newPoint = new Point() { X = t, Y = prevY.Y + YbyT * dt };
                }
                else
                {
                    var deltaY1 = YbyT * dt;
                    var deltaY2 = (YbyT + (deltaY1 / 2)) * (dt + dt / 2);
                    var deltaY3 = (YbyT + (deltaY2 / 2)) * (dt + dt / 2);
                    var deltaY4 = (YbyT + deltaY3) * 2 * dt;
                    newPoint = new Point() { X = t, Y = prevY.Y + ((deltaY1 + 2 * deltaY2 + 2 * deltaY3 + deltaY4) * (dt / 6)) };
                }
                prevY = newPoint;
                result.Add(newPoint);
            }
            return result;
        }

        public static List<Point> CalcDynamicMass(double m_input, double m_out, double m_vt, double C_input, double C_out, double Tп, bool isEuler)
        {
            var result = new List<Point>();
            var prevY = new Point() { X = 0, Y = C_out };
            var newPoint = new Point();
            var newMassInput = m_input + deltaMass_input;
            var newMassVt = CalcStaticMethods.CalcMvt(newMassInput, m_out);
            result.Add(prevY);
            for (var t = 0.0; t <= tmax; t += dt)
            {
                var YbyT = dCout_dT(newMassInput, m_out, newMassVt, C_input, prevY.Y, Tп);
                if (isEuler)
                {
                    newPoint = new Point() { X = t, Y = prevY.Y + dt * YbyT };
                }
                else
                {
                    var deltaY1 = YbyT * dt;
                    var deltaY2 = (YbyT + (deltaY1 / 2)) * (dt + dt / 2);
                    var deltaY3 = (YbyT + (deltaY2 / 2)) * (dt + dt / 2);
                    var deltaY4 = (YbyT + deltaY3) * 2 * dt;
                    newPoint = new Point() { X = t, Y = prevY.Y + ((deltaY1 + 2 * deltaY2 + 2 * deltaY3 + deltaY4) * (dt / 6)) };
                }
                prevY = newPoint;
                result.Add(newPoint);
            }
            return result;
        }

        public static List<Point> CalcDynamicTemperature(double m_input, double m_out, double m_vt, double C_input, double C_out, double Tп, bool isEuler)
        {
            var result = new List<Point>();
            var prevY = new Point() { X = 0, Y = C_out };
            var newPoint = new Point();
            var newTп = Tп + deltaT;
            result.Add(prevY);
            for (var t = 0.0; t <= tmax; t += dt)
            {
                var YbyT = dCout_dT(m_input, m_out, m_vt, C_input, prevY.Y, newTп);
                if (isEuler)
                {
                    newPoint = new Point() { X = t, Y = prevY.Y + dt * YbyT };
                }
                else
                {
                    var deltaY1 = YbyT * dt;
                    var deltaY2 = (YbyT + (deltaY1 / 2)) * (dt + dt / 2);
                    var deltaY3 = (YbyT + (deltaY2 / 2)) * (dt + dt / 2);
                    var deltaY4 = (YbyT + deltaY3) * 2 * dt;
                    newPoint = new Point() { X = t, Y = prevY.Y + ((deltaY1 + 2 * deltaY2 + 2 * deltaY3 + deltaY4) * (dt / 6)) };
                }
                prevY = newPoint;
                result.Add(newPoint);
            }
            return result;
        }

        public static double dCout_dT(double m_input, double m_out, double m_vt, double C_input, double C_out, double Tп)
        {
            var M_value = M(m_input, m_vt);
            return (((m_input * C_input) - (m_out * C_out) - (C_out * dM_dT(m_input, m_out, Tп, M_value))) / (M_value));
        }

        public static double dM_dT(double m_input, double m_out, double Tп, double M)
        {
            return (((kt * F * (Tп - Tp)) + ((m_input - m_out) * ct * Tp) - (m_input * r) + (ro * r * Math.Sqrt(P0 + M / S - P1))) / (ct * Tp - r));
        }

        public static double M(double m_input, double m_vt)
        {
            return (S * (Math.Pow(((m_input - m_vt) / ro), 2) + P1 - P0));
        }
    }
}
