﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Modules.Base;

namespace WPF.Events
{
    public class ChangePageEvent: EventArgs
    {
        public ModuleType ModuleType { get; set; }
    }
}
