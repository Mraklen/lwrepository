﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Windows.Controls;
using System.Threading.Tasks;
using Microsoft.Research.DynamicDataDisplay.DataSources.MultiDimensional;
using Microsoft.Research.DynamicDataDisplay.Common.Auxiliary;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using System.Reflection;
using System.Windows.Media;
using LW2.Models;
using System.IO;
using LW1.Modules;
using CW.Methods;
using LW1.PrintMethods;

namespace LW2.Services
{
    public class Methods
    {
        public FunctionParam Function { get; set; }
        int maxSleepValue = 500;
        int minSleepValue = 1;
        int count = 0;
        public void SetFunction(double x1Value, double x2Value, double hValue, double sValue)
        {
            //Function = new FunctionParam(x1Value, x2Value, hValue, sValue);
        }
        
        public async void Calc(ChartPlotter plotterParamZ, ChartPlotter plotterParamResult, Label label_stepValue, Label label_extremum)
        {
            label_extremum.Content = "Ведется расчет оптимальных значений...";

            await Task.Delay(1);

            Function = new FunctionParam(0, 0, 0, 0.01);

            Simplex.x10[0] = Convert.ToSingle(640);
            Simplex.x20[0] = Convert.ToSingle(37);
            Simplex.x10[1] = Convert.ToSingle(670);
            Simplex.x20[1] = Convert.ToSingle(39);
            Simplex.x10[2] = Convert.ToSingle(660);
            Simplex.x20[2] = Convert.ToSingle(38);
            
            double t, x1t, x2t, dx1, dx2;
            int i = 3, j, k;
            Simplex.f1[0] = Function.CalcFunction(Simplex.x10[0], Simplex.x20[0]);
            Simplex.f1[1] = Function.CalcFunction(Simplex.x10[1], Simplex.x20[1]);
            Simplex.f1[2] = Function.CalcFunction(Simplex.x10[2], Simplex.x20[2]);

            do
            {
                for (j = 1; j <= 2; j++)
                {
                    k = j;
                    while (Simplex.f1.ElementAt(k) > Simplex.f1.ElementAt(k - 1) && k >= 1)
                    {
                        t = Simplex.f1[k - 1];
                        Simplex.f1[k - 1] = Simplex.f1[k];
                        Simplex.f1[k] = t;
                        t = Simplex.x10[k - 1];
                        Simplex.x10[k - 1] = Simplex.x10[k];
                        Simplex.x10[k] = t;
                        t = Simplex.x20[k - 1];
                        Simplex.x20[k - 1] = Simplex.x20[k];
                        Simplex.x20[k] = t;
                        k--;

                        if (!(k >= 1)) break;
                    }
                }

                x1t = Simplex.x10[2] - (Simplex.x10[2] - Simplex.x10[1]) / 2;
                x2t = Simplex.x20[2] - (Simplex.x20[2] - Simplex.x20[1]) / 2;
                dx1 = x1t - Simplex.x10[0];
                dx2 = x2t - Simplex.x20[0];

                if (Function.CalcFunction(x1t + dx1, x2t + dx2) < Simplex.f1[0])
                {
                    if (Function.CalcFunction(x1t + 2 * dx1, x2t + 2 * dx2) < Simplex.f1[0])
                    {
                        Simplex.x10[0] = x1t + 2 * dx1;
                        Simplex.x20[0] = x2t + 2 * dx2;
                    }
                    else
                    {
                        Simplex.x10[0] = x1t + dx1;
                        Simplex.x20[0] = x2t + dx2;
                    }
                }
                else
                {
                    Simplex.x10[0] = x1t - dx1 / 2;
                    Simplex.x20[0] = x2t - dx2 / 2;
                }
                Simplex.f1[0] = Function.CalcFunction(Simplex.x10[0], Simplex.x20[0]);
                i++;

                await Task.Delay(1);

                //label_extremum.Content = "Начало рассчета... " + count++;

            } while (Condition(Simplex.x10[0], Simplex.x20[0], Simplex.x10[1], Simplex.x20[1], Simplex.x10[2], Simplex.x20[2], FunctionParam.SValue));
            
            label_extremum.Content = Simplex.PrintMinValue();

            // рассчеты для имитационного моделирования
            var arr_x = RandomProcessGenerator.Random(6010);
            var M_x = RandomProcessGenerator.M(arr_x);
            var disp_x = RandomProcessGenerator.Dispersion(arr_x, M_x);
            var arr_z = RandomProcessGenerator.Filter(arr_x, disp_x);

            // сделать рассчет имитационного моделирования при оптимальных T и V 
            Function.UpdateArrZValues(arr_z);
            var opt = Simplex.GetMinValue();
            Function.CalcFunction(opt.X1, opt.X2);

            var tmpResult = MathModel.Z_Result;

            var z_result = new List<double>();
            var result = new List<Point>();
            var tmpY = 0.0;
            var tmpZ = 0.0;

            for (var index = 0; index < tmpResult.Count(); index++)
            {
                if (index % 25 == 0)
                {
                    z_result.Add(tmpZ / 25.0);
                    tmpZ = 0.0;
                } 
                else
                {
                    tmpZ += arr_z[index];
                }

                if (index % 100 == 0)
                {
                    result.Add(new Point { X = tmpResult[index].X, Y = tmpY / 100.0 });
                    tmpY = 0.0;
                } 
                else
                {
                    tmpY += tmpResult[index].Y;
                }
            }

            // ВЫВОД РЕЗУЛЬТАТА НА ГРАФИК !!!
            PrintGrapth.ClearPlotter(plotterParamZ);
            PrintGrapth.ClearPlotter(plotterParamResult);
            PrintGrapth.PrintLineGrapth(PrintGrapth.ConvertToPoints(z_result.ToArray()), plotterParamZ, Brushes.Blue);
            PrintGrapth.PrintLineGrapth(result, plotterParamResult, Brushes.Blue);
        }

        bool Condition(double x10, double x20, double x11, double x21, double x12, double x22, double e)
        {
            if (Math.Sqrt((x10 - x11) * (x10 - x11) + (x20 - x21) * (x20 - x21)) +
               Math.Sqrt((x11 - x12) * (x11 - x12) + (x21 - x22) * (x21 - x22)) +
               Math.Sqrt((x10 - x12) * (x10 - x12) + (x20 - x22) * (x20 - x22)) > e)
                return true;
            else
                return false;
        }
    }
}
