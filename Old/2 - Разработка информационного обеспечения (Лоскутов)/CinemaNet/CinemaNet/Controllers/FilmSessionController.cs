﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CinemaNet.Models;
using CinemaNet.Models.Interfaces;

namespace CinemaNet.Controllers
{
    public class FilmSessionController : Controller
    {
        public IFilmSessionList filmSessions;
        public FilmSessionController(IFilmSessionList list)
        {
            filmSessions = list;
        }

        // GET: /<controller>/
        public IActionResult SessionList(int cID, int fID) {
            return View(filmSessions.GetFilmSession(cID, fID));
        }
    }
}
