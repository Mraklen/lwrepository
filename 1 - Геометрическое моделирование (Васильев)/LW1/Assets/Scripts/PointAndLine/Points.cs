﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Points : MonoBehaviour
{
    [SerializeField] List<MainPoint> PointList;

    GameObject PointPrefab;

    private void Start()
    {
        PointPrefab = Resources.Load<GameObject>("Prefabs/Point");
    }

    public void GeneratePoints(int rows, int columns, Vector3 step)
    {
        if(!PointPrefab) PointPrefab = Resources.Load<GameObject>("Prefabs/Point");
        var firstPointPos = transform.position;
        PointList = new List<MainPoint>();
        for (var i = 0; i < rows; i++)
        {
            var firstPointInRow = new Vector3(firstPointPos.x, firstPointPos.y, firstPointPos.z + (step.z * i));
            for (var j = 0; j < columns; j++)
            {
                var tmpPos = new Vector3(firstPointInRow.x + (j * step.x), firstPointInRow.y, firstPointInRow.z);
                var newPoint = Instantiate(PointPrefab);
                newPoint.transform.position = tmpPos;
                newPoint.transform.parent = transform;
                PointList.Add(newPoint.GetComponent<MainPoint>());
            }
        }
    }

    public void AddNewPoint(bool showVector = false, Vector3? pos = null)
    {
        if (!PointPrefab) PointPrefab = Resources.Load<GameObject>("Prefabs/Point");
        var newPoint = Instantiate(PointPrefab);
        newPoint.transform.position = pos ?? transform.position;
        newPoint.transform.parent = transform;
        var mp = newPoint.GetComponent<MainPoint>();
        mp.VectorShow(showVector);
        PointList.Add(mp);
    }

    public void AddNewPoint(bool showVector = false, Vector3? pos = null, Vector3? fvPos = null, Vector3? svPos = null)
    {
        if (!PointPrefab) PointPrefab = Resources.Load<GameObject>("Prefabs/Point");
        var newPoint = Instantiate(PointPrefab);
        newPoint.transform.position = pos ?? transform.position;
        newPoint.transform.parent = transform;
        var mp = newPoint.GetComponent<MainPoint>();
        mp.VectorShow(showVector);
        mp.VectorUpdate(fvPos, svPos);
        PointList.Add(mp);
    }
    public void RemoveLastPoint()
    {
        var lastIndex = Count() - 1;
        var point = PointList.ElementAt(lastIndex);
        PointList.RemoveAt(lastIndex);
        Destroy(point.gameObject);
    }

    public int Count()
    {
        return PointList.Count();
    }

    public List<MainPoint> GetPositions()
    {
        return PointList;
    }

    public void VectorShow(bool isShow)
    {
        PointList.ForEach(x => x.VectorShow(isShow));
    }

    public void Invisible(bool showOnlyCalcLine)
    {
        PointList.ForEach(x => x.Invisible(showOnlyCalcLine));
    }

    public GameObject GetLastPointGameObject()
    {
        return PointList.Last().gameObject;
    }

    public List<JsonMainPoint> GetJsonMainPoints()
    {
        return PointList.Select(x => x.GetJsonMainPoint()).ToList();
    }

    public List<JsonPoint> GetJsonPoints()
    {
        return PointList.Select(x => x.GetJsonPoint()).ToList();
    }

    public void ClearPoints()
    {
        PointList.ForEach(x => Destroy(x.gameObject));
        PointList.Clear();
    }

    public void SetJsonPoints(List<JsonPoint> jsonPoints)
    {
        if (!jsonPoints.Any()) return;

        foreach (var point in jsonPoints)
        {
            AddNewPoint(false, point.Position.GetVector3());
        }
    }

    public void SetJsonMainPoints(List<JsonMainPoint> jsonPoints)
    {
        if (!jsonPoints.Any()) return;

        foreach (var point in jsonPoints)
        {
            AddNewPoint(false, point.Main.Position.GetVector3(), point.FirstVector.Position.GetVector3(), point.SecondVector.Position.GetVector3());
        }
    }
}
