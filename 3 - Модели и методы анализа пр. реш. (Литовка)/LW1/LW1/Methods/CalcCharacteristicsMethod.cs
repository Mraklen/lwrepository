﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW1.Methods
{
    public class CalcCharacteristicsMethod
    {
        static double L = 1;
        static double tmax = 10;

        static double kt = 6500;
        static double ct = 4190;
        static double p = 1000;
        static double D = 0.05;
        static double Tt = 80;
        static double u = 0.2;

        static double ts => L / u;

        static double dAlpha = 0.3;
        static double dBeta = 0.2;

        static double alphaBegin = 0;
        static double alphaEnd = 0;
        static double T = 0;

        public static List<List<(double x, double y, double z)>> CalcMethod()
        {
            var result = new List<(double x, double y, double z)>();

            var result2 = new List<List<(double x, double y, double z)>>();
            for (int j = 1; j <= 13; j++) result2.Add(new List<(double x, double y, double z)>());

            var i = 0;
            var count = 0;
            var maxCount = 0;

            for (var beta = (-ts / 2.0); beta <= (tmax / 2.0); beta += dBeta)
            {
                if (beta >= (-ts / 2.0) && beta <= 0)
                {
                    alphaBegin = -beta;
                    alphaEnd = (ts + beta);
                    T = To(-2.0 * u * beta);
                    
                    i = 1;
                    count++;
                    maxCount++;
                    for (var alpha = alphaBegin; alpha <= alphaEnd && i <= count; alpha += dAlpha, i++)
                    {
                        T += B(T) * dAlpha;

                        var res = (x: Math.Round(fl(alpha, beta), 2), y: Math.Round(ft(alpha, beta), 2), z: T);
                        result.Add(res);
                        result2[count - i].Add(res);
                    }
                }
                if (beta >= 0 && beta <= ((tmax - ts) / 2.0))
                {
                    alphaBegin = beta;
                    alphaEnd = (ts + beta);
                    T = Tin(2.0 * beta);

                    i = 1;
                    for (var alpha = alphaBegin; alpha <= alphaEnd && i <= count; alpha += dAlpha, i++)
                    {
                        T += B(T) * dAlpha;

                        var res = (x: Math.Round(fl(alpha, beta), 2), y: Math.Round(ft(alpha, beta), 2), z: T);
                        result.Add(res);
                        result2[count - i].Add(res);
                    }
                }
                if (beta >= ((tmax - ts) / 2.0) && beta <= (tmax / 2.0))
                {
                    alphaBegin = beta;
                    alphaEnd = (tmax - beta);
                    T = Tin(2.0 * beta);
                    
                    i = count;
                    count--;
                    var newIndex = maxCount - 1;
                    for (var alpha = alphaBegin; alpha <= alphaEnd && i >= 1; alpha += dAlpha, i--)
                    {
                        T += B(T) * dAlpha;

                        var res = (x: Math.Round(fl(alpha, beta), 2), y: Math.Round(ft(alpha, beta), 2), z: T);
                        result.Add(res);
                        result2[newIndex--].Add(res);
                    }
                }
            }
            result2[0].RemoveAt(result2[0].Count() - 1);
            return result2;
        }

        private static double A()
        {
            return (4 * kt) / (ct * p * D);
        }

        private static double B(double T)
        {
            return A() * (Tt - T);
        }

        private static double To(double l)
        {
            return 34 - Math.Pow((l + 2), 2);
        }

        private static double Tin(double t)
        {
            return 20 + (20 / (Math.Exp(t) + 1));
        }

        private static double ft(double alpha, double beta)
        {
            return alpha + beta;
        }

        private static double fl(double alpha, double beta)
        {
            return u * (alpha - beta);
        }
    }
}
