﻿using LW1.Models;
using LW1.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Services
{
    public class UserProvider : IUserProvider
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IHttpContextAccessor httpContext;

        public UserProvider(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContext)
        {
            this.userManager = userManager;
            this.httpContext = httpContext;
        }

        public string CurrentUserId
        {
            get
            {
                var currUser = httpContext.HttpContext.User;
                if (currUser == null) return null;

                var userAccount = userManager.GetUserAsync(currUser).Result;
                if (userAccount == null) return null;

                return userAccount.Id;
            }
        }

        public ApplicationUser CurrentUserAccount
        {
            get
            {
                var currUser = httpContext.HttpContext.User;
                if (currUser == null) return null;

                var userAccount = userManager.GetUserAsync(currUser).Result;
                if (userAccount == null) return null;
                userAccount = userManager.Users.Where(x => x.Id == userAccount.Id).FirstOrDefault();

                return userAccount;
            }
        }
    }
}
