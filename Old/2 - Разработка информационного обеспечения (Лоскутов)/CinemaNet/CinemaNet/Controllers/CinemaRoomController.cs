﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CinemaNet.Models.Interfaces;
using CinemaNet.ViewModels;


namespace CinemaNet.Controllers
{
    public class CinemaRoomController : Controller
    {
        public ICinemaRoomList roomList;
        public IReserveList reserveList;
        public IFilmList filmList;
        public CinemaRoomController(ICinemaRoomList roomList, IReserveList reserveList, IFilmList filmList)
        {
            this.roomList = roomList;
            this.reserveList = reserveList;
            this.filmList = filmList;
        }

        // GET: /<controller>/
        public IActionResult Room(int sessionID) => View(new CinemaRoomModel
        {
            SessionID = sessionID,
            Film = filmList.FilmList()
                .Where(f => f.FilmSessions.Any(fs => fs.FilmSessionID == sessionID))
                .ToList().ElementAt(0),
            CinemaRoom = roomList.GetCinemaRoom(sessionID),
            Reserves = reserveList.ReserveList
                .Where(rl => rl.FilmSessionID == sessionID).ToList()
        });
    }
}
