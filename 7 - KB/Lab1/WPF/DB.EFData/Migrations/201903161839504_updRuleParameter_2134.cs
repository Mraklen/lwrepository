namespace DB.EFData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updRuleParameter_2134 : DbMigration
    {
        public override void Up()
        {
            DropColumn("public.RuleParameters", "State");
        }
        
        public override void Down()
        {
            AddColumn("public.RuleParameters", "State", c => c.Boolean());
        }
    }
}
