﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace LW1.Methods
{
    public class CalcStaticDistCoordMethod
    {
        static double R = 8.31;
        static double E1 = 251000.0;
        static double E2 = 297000.0;
        static Int64 A1 = Convert.ToInt64(2 * Math.Pow(10, 11));
        static Int64 A2 = Convert.ToInt64(8 * Math.Pow(10, 12));
        static double po = 1.4;
        static double D = 0.1;
        static double dl = 0.5;
        static double dC = 5.0;
        static double muEthylene = 0.02805;
        static double muAcetylene = 0.02604;

        public static (List<List<Point>> C1, List<List<Point>> C2) CalcStatic(double C1, double C2, double T, double m, double l)
        {
            var allResC1 = new List<List<Point>>();
            var allResC2 = new List<List<Point>>();

            var V = _V(D, po);
            var k1 = K(A1, E1, R, T);
            var k2 = K(A2, E2, R, T);
            
            for (var currC = C1; currC <= C2; currC += dC)
            {
                var tmpResC1 = new List<Point>();
                var tmpResC2 = new List<Point>();

                var pointC1 = new Point() { X = 0.0, Y = currC };
                var pointC2 = new Point() { X = 0.0, Y = 0.0 };

                tmpResC1.Add(pointC1);
                tmpResC2.Add(pointC2);

                for (var currL = dl; currL <= l; currL += dl)
                {
                    var c1mole = CinToMole(pointC1.Y, po, muEthylene);
                    var c2mole = CinToMole(pointC2.Y, po, muAcetylene);
                    var A = _AB(k1, c1mole, V);
                    var B = _AB(k2, c2mole, V);
                    var C1_YbyL = dC1_dl(A, m);
                    var C2_YbyL = dC2_dl(A, B, m);
                    
                    pointC1 = new Point() { X = currL, Y = MoleToCin(c1mole + C1_YbyL * dl, po, muEthylene) };
                    pointC2 = new Point() { X = currL, Y = MoleToCin(c2mole + C2_YbyL * dl, po, muAcetylene) };
                    
                    tmpResC1.Add(pointC1);
                    tmpResC2.Add(pointC2);
                }

                allResC1.Add(tmpResC1);
                allResC2.Add(tmpResC2);
            }

            return (allResC1, allResC2);
        }

        public static double dC1_dl(double _A, double m)
        {
            return -(_A) / m;
        }

        public static double dC2_dl(double A, double B, double m)
        {
            return (A - B) / m;
        }

        public static double _V(double D, double po)
        {
            return (3.14 * Math.Pow(D, 2.0) * po) / 4.0;
        }

        public static double _AB(double k, double C, double _V)
        {
            return k * C * _V;
        }

        public static double K(double A, double E, double R, double T)
        {
            return (A * Math.Exp((-E) / (R * T)));
        }

        public static double CinToMole(double Cin, double po, double mu)
        {
            return (Cin * po) / (100.0 * mu);
        }

        public static double MoleToCin(double mole, double po, double mu)
        {
            return (mole * 100.0 * mu) / (po);
        }
    }
}