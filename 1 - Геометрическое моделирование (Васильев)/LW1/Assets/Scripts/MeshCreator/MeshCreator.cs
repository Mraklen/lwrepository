﻿using System.Collections.Generic;
using UnityEngine;

public class MeshCreator : MonoBehaviour
{
    Mesh Mesh { get; set; }

    List<Vector3> Verticles { get; set; } = new List<Vector3>();
    List<int> Triangles { get; set; } = new List<int>();
    List<Vector2> Uvs { get; set; } = new List<Vector2>();

    private void Start()
    {
        var meshFilter = GetComponent<MeshFilter>();
        Mesh = meshFilter.mesh;
    }

    public void Render(Vector3[,] matrix)
    {
        //LW1.Collision.Collision.Instance.ClearCollisions();
        Verticles.Clear();
        Triangles.Clear();
        Uvs.Clear();

        for (var i = matrix.GetLength(1) - 1; i > 0; i--)
        {
            for (var j = 0; j < matrix.GetLength(0) - 1; j++)
            {
                AddPolygon(matrix[j, i], matrix[j, i - 1], matrix[j + 1, i], matrix[j + 1, i - 1]);
            }
        }

        // заделка шва
        var lastI = matrix.GetLength(1) - 1;
        for (var j = 0; j < matrix.GetLength(0) - 1; j++)
        {
            AddPolygon(matrix[j, 0], matrix[j, lastI], matrix[j + 1, 0], matrix[j + 1, lastI]);
        }

        //LW1.Collision.Collision.Instance.CalcIntersectionCount();

        UpdateMesh();
    }

    private void AddPolygon(Vector3 point_0, Vector3 point_1, Vector3 point_2, Vector3 point_3)
    {
        Verticles.AddRange(new List<Vector3>()
        {
            point_0, point_1, point_2, point_3
        });
        
        var zero = Verticles.Count - 4;
        Triangles.AddRange(new List<int>()
        {
            zero + 1, zero + 3, zero + 2,
            zero + 2, zero, zero + 1
        });

        //var collision = LW1.Collision.Collision.Instance;
        //collision.TriangleIntersection(collision.transform.position, collision.Targeting.GetVector(), point_1, point_3, point_2);
        //collision.TriangleIntersection(collision.transform.position, collision.Targeting.GetVector(), point_2, point_0, point_1);

        Uvs.AddRange(new List<Vector2>()
        {
            new Vector2(0, 1),
            new Vector2(0, 0),
            new Vector2(1, 1),
            new Vector2(1, 0)
        });
    }

    public Vector3 NormalCalc(Vector3 a, Vector3 b, Vector3 c)
    {
        var side1 = b - a;
        var side2 = c - a;
        var perp = Vector3.Cross(side1, side2);
        var len = perp.magnitude;
        return perp / len;
    }

    public void UpdateMesh()
    {
        Mesh.Clear();

        Mesh.vertices = Verticles.ToArray();
        Mesh.triangles = Triangles.ToArray();
        Mesh.uv = Uvs.ToArray();
        
        //UnityEditor.MeshUtility.Optimize(Mesh);
        Mesh.RecalculateNormals();
    }

    public void ClearModel()
    {
        Mesh?.Clear();
    }
}
