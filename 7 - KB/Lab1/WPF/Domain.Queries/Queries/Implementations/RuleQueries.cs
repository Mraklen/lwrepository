﻿using Data.Queries.ReadModels.KnowledgeBase;
using Domain.Queries.Queries.Interfaces;
using Domain.Queries.ViewModels.KnowledgeBase;
using QueryAbstractions.Queries;
using QueryAbstractions.Queries.Implementations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Queries.Queries.Implementations
{
    public class RuleQueries : AbstractQueries<RuleReadModel, RuleViewModel, int>, IRuleQueries
    {
        public RuleQueries(IQueryDatabase db) : base(db) { }

        protected override Expression<Func<RuleReadModel, RuleViewModel>> Projection => x => new RuleViewModel
        {
            Id = x.Id,
            RuleParameters = x.RuleParameters.Select(rp => new RuleParameterViewModel
            {
                Id = rp.Id,
                ParamType = rp.ParamType,
                Parameter = new ParameterViewModel
                {
                    Id = rp.Parameter.Id,
                    Name = rp.Parameter.Name,
                    Description = rp.Parameter.Description,
                    Question = rp.Parameter.Question
                }
            }).ToList()
        };

        public async Task<IEnumerable<RuleViewModel>> GetListAsync()
        {
            return await Queryable.Select(Projection).ToListAsync();
        }
    }
}
