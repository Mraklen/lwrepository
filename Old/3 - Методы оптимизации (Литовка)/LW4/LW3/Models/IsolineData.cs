﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW3.Models
{
    public enum IsolineType
    {
        Ellipse, Rosenbroke, MM
    }
    
    public enum FineType
    {
        External, Internal, First, Second 
    }

    public class IsolineData
    {
        public double[,] Data { get; set; }
        public Point[,] Grid { get; set; }
        public PointXY ExtremPoint { get; set; } 
    }

    public class FineData
    {
        public List<PointXY> Points { get; set; }
    }
}
