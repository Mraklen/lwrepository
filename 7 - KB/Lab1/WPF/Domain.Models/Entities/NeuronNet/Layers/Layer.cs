﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF.Helpers;

namespace Domain.Models.Entities.NeuronNet.Layers
{
    public enum MemoryMode
    {
        GET,
        SET
    }

    public abstract class Layer
    {
        /// <summary> Скорость обчения </summary>
        protected const double LearningRate = 0.1;

        /// <summary> Количество нейронов в слое </summary>
        protected int NumNeuron;
        /// <summary> Количество нейронов в предыдущем слое </summary>
        protected int NumPrevNeuron;

        /// <summary> Нейроны текущего слоя </summary>
        public List<Neuron> Neurons { get; protected set; }

        /// <summary> Передача данных на вход с предыдущего слоя </summary>
        public List<double> Data
        {
            set { for (var i = 0; i < Neurons.Count; i++) Neurons[i].UpdateInputs(value); }
        }

        protected bool AddShift { get; private set; }

        /// <summary> Прямой проход </summary>
        /// <param name="net">Текущая сеть</param>
        /// <param name="nextLayer">Следующий слой</param>
        abstract public void Recognize(NNet net, Layer nextLayer);

        /// <summary> Обратный проход </summary>
        /// <param name="values"></param>
        abstract public List<double> BackwardPass(List<double> values);

        /// <summary> Базовый класс слоя </summary>
        /// <param name="numNeuron">Количество нейронов этого слоя</param>
        /// <param name="numPrevNeuron">Количество нейронов предыдущего слоя</param>
        /// <param name="type">Тип слоя</param>
        /// <param name="memoryName">Имя слоя в памяти (для сохранения значений весов)</param>
        protected Layer(int numNeuron, int numPrevNeuron, NeuronType type, string memoryName, bool addShift = false)
        {
            NumNeuron = numNeuron;
            NumPrevNeuron = numPrevNeuron + (addShift ? 1 : 0);
            AddShift = addShift;

            Neurons = new List<Neuron>();

            var Weights = WeightInit(MemoryMode.GET, memoryName);

            for (var i = 0; i < NumNeuron; i++)
            {
                var tmpWeights = new List<double>();

                for (var j = 0; j < NumPrevNeuron; j++) tmpWeights.Add(Weights[i, j]);

                Neurons.Add(new Neuron(null, tmpWeights, type));
            }
        }

        /// <summary> Инициализация весовых коэффициентов слоя </summary>
        /// <param name="mode">Тип инициализации</param>
        /// <param name="memoryName">Имя файла в памяти</param>
        /// <returns>Массив весовых коэффициентов всех выходов</returns>
        public double[,] WeightInit(MemoryMode mode, string memoryName)
        {
            var weights = new double[NumNeuron, NumPrevNeuron];

            // если файла нет, то создаем
            if (!FileManager.Exist(PathBuilder.GetBasePathJson(memoryName)))
            {
                for (var i = 0; i < NumNeuron; i++)
                {
                    for (var j = 0; j < NumPrevNeuron; j++)
                    {
                        var ticks = (int)DateTime.UtcNow.Ticks;
                        var rnd = new Random(ticks);
                        weights[i, j] = rnd.NextDouble();
                    }
                }
                
                FileManager.WriteJsonFile(PathBuilder.GetBasePathJson(memoryName), weights);
            }

            switch (mode)
            {
                case MemoryMode.GET:
                    weights = FileManager.ReadJsonFile<double[,]>(PathBuilder.GetBasePathJson(memoryName));
                    break;
                case MemoryMode.SET:
                    FileManager.WriteJsonFile(PathBuilder.GetBasePathJson(memoryName), Neurons.Select(x => x.Weights.ToArray()).ToArray());
                    break;
            }

            return weights;
        }
    }
}
