﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProtection.Ciphers
{
    class Transposition
    {
        static private int[] key = null;

        public static void SetKey(int[] key)
        {
            Transposition.key = new int[key.Length];
            for (var i = 0; i < key.Length; i++) Transposition.key[i] = key[i];
        }

        public static void SetKey(string[] key)
        {
            Transposition.key = new int[key.Length];
            for (int i = 0; i < key.Length; i++) Transposition.key[i] = Convert.ToInt32(key[i]);
        }

        public static void SetKey(string key)
        {
            SetKey(key.Split(' '));
        }

        public static string Encrypt(string input)
        {
            for (int i = 0; i < input.Length % key.Length; i++) input += ' ';

            var result = "";

            for (var i = 0; i < input.Length; i += key.Length)
            {
                var transposition = new char[key.Length];

                for (var j = 0; j < key.Length; j++) transposition[key[j] - 1] = input[i + j];
                for (var j = 0; j < key.Length; j++) result += transposition[j];
            }

            return result;
        }

        public static string Decrypt(string input)
        {
            var result = "";

            for (var i = 0; i < input.Length; i += key.Length)
            {
                var transposition = new char[key.Length];

                for (var j = 0; j < key.Length; j++) transposition[j] = input[i + key[j] - 1];
                for (var j = 0; j < key.Length; j++) result += transposition[j];
            }

            return result;
        }
    }
}
