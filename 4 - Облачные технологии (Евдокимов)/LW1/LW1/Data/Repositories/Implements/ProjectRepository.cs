﻿using LW1.Data.Repositories.Interfaces;
using LW1.Models.Project;
using LW1.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LW1.Data.Repositories.Implements
{
    class ProjectRepository : DefaultAggregateRepository<Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ICollection<ProjectViewModel> GetActiveProject()
        {
            var res = context.Set<Project>().
                Where(x => LastDateOnCheck(x.LastSelectionDate)).
                Select(x => new ProjectViewModel()
                {
                    Id = x.Id,
                    ApplicationUserId = x.ApplicationUserId,
                    Name = x.Name,
                    MainUrl = x.MainUrl,
                    LastSelection = x.LastSelectionDate,
                    KeyWords = x.KeyWords.Select(kw => new KeyWordViewModel()
                    {
                        Id = kw.Id,
                        Value = kw.Value
                    }).ToList(),
                    Selections = x.Selections.Select(s => new SelectionViewModel()
                    {
                        Id = s.Id,
                        DateSelection = s.DateSelection,
                        SelectionKeyWords = s.SelectionKeyWords.Select(su => new SelectionKeyWordViewModel()
                        {
                            Id = su.Id,
                            KeyWordId = su.KeyWordId,
                            KeyWord = new KeyWordViewModel()
                            {
                                Id = su.KeyWord.Id,
                                Value = su.KeyWord.Value
                            },
                            Offset = su.Offset,
                            IsUp = su.IsUp,
                            BestPosition = su.BestPosition,
                            SelectionUrls = su.SelectionUrls.Select(selu => new SelectionUrlViewModel()
                            {
                                Id = selu.Id,
                                Value = selu.Value,
                                KeyWordId = selu.KeyWordId,
                                Order = selu.Order,
                                Offset = selu.Offset,
                                IsUp = selu.IsUp,
                                Marked = selu.Marked
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();
            return res;
        }

        private bool LastDateOnCheck(DateTime? lastSelectionDate)
        {
            if (lastSelectionDate == null) return true;
            var thersholdDate = DateTime.Now.AddHours(-12);
            return lastSelectionDate < thersholdDate;
        }

        public ICollection<ProjectViewModel> GetProjectsByUserId(string userId)
        {
            return context.Set<Project>().Where(x => x.ApplicationUserId == userId).
                Select(x => new ProjectViewModel()
                {
                    Id = x.Id,
                    ApplicationUserId = x.ApplicationUserId,
                    Name = x.Name,
                    MainUrl = x.MainUrl,
                    LastSelection = x.LastSelectionDate,
                    KeyWords = x.KeyWords.Select(kw => new KeyWordViewModel()
                    {
                        Id = kw.Id,
                        Value = kw.Value
                    }).ToList(),
                    Selections = x.Selections.Select(s => new SelectionViewModel()
                    {
                        Id = s.Id,
                        DateSelection = s.DateSelection,
                        SelectionKeyWords = s.SelectionKeyWords.Select(su => new SelectionKeyWordViewModel()
                        {
                            Id = su.Id,
                            KeyWordId = su.KeyWordId,
                            KeyWord = new KeyWordViewModel()
                            {
                                Id = su.KeyWord.Id,
                                Value = su.KeyWord.Value
                            },
                            Offset = su.Offset,
                            IsUp = su.IsUp,
                            BestPosition = su.BestPosition,
                            SelectionUrls = su.SelectionUrls.Select(selu => new SelectionUrlViewModel()
                            {
                                Id = selu.Id,
                                Value = selu.Value,
                                KeyWordId = selu.KeyWordId,
                                Order = selu.Order,
                                Offset = selu.Offset,
                                IsUp = selu.IsUp,
                                Marked = selu.Marked
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();
        }

        public ProjectViewModel GetProjectViewModelById(Guid id)
        {
            return context.Set<Project>().Where(x => x.Id == id).
                Select(x => new ProjectViewModel()
                {
                    Id = x.Id,
                    ApplicationUserId = x.ApplicationUserId,
                    Name = x.Name,
                    MainUrl = x.MainUrl,
                    LastSelection = x.LastSelectionDate,
                    KeyWords = x.KeyWords.Select(kw => new KeyWordViewModel()
                    {
                        Id = kw.Id,
                        Value = kw.Value
                    }).ToList(),
                    Selections = x.Selections.Select(s => new SelectionViewModel()
                    {
                        Id = s.Id,
                        DateSelection = s.DateSelection,
                        SelectionKeyWords = s.SelectionKeyWords.Select(su => new SelectionKeyWordViewModel()
                        {
                            Id = su.Id,
                            KeyWordId = su.KeyWordId,
                            KeyWord = new KeyWordViewModel()
                            {
                                Id = su.KeyWord.Id,
                                Value = su.KeyWord.Value
                            },
                            Offset = su.Offset,
                            IsUp = su.IsUp,
                            BestPosition = su.BestPosition,
                            SelectionUrls = su.SelectionUrls.Select(selu => new SelectionUrlViewModel()
                            {
                                Id = selu.Id,
                                Value = selu.Value,
                                KeyWordId = selu.KeyWordId,
                                Order = selu.Order,
                                Offset = selu.Offset,
                                IsUp = selu.IsUp,
                                Marked = selu.Marked
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).SingleOrDefault();
        }
    }
}
