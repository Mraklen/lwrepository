﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF.Helpers
{
    public class FileManager
    {
        public static string WriteJsonFile(string path, object obj)
        {
            try
            {
                using (FileStream fstream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    var str = JsonConvert.SerializeObject(obj);
                    
                    byte[] array = Encoding.UTF8.GetBytes(str);

                    fstream.SetLength(0);

                    fstream.Write(array, 0, array.Length);

                    Console.WriteLine($"Файл записан: {path}");
                }
                return path;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка записи файла {e}");
                return null;
            }
        }

        public static string WriteTextFile(string path, string value, bool addData = false)
        {
            try
            {
                using (FileStream fstream = new FileStream(path, FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.UTF8.GetBytes(value);

                    if (!addData) fstream.SetLength(0);
                    fstream.Write(array, 0, array.Length);
                    
                    Console.WriteLine($"Файл записан: {path}");
                }
                return path;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка записи файла {e}");
                return null;
            }
        }

        public static T ReadJsonFile<T>(string path)
        {
            var text = ReadTextFile(path);
            if (string.IsNullOrEmpty(text)) return default(T);
            return JsonConvert.DeserializeObject<T>(text);
        }

        public static string ReadTextFile(string path)
        {
            try
            {
                using (FileStream fstream = File.OpenRead(path))
                {
                    byte[] array = new byte[fstream.Length];
                    fstream.Read(array, 0, array.Length);

                    var value = Encoding.UTF8.GetString(array);

                    Console.WriteLine($"Файл прочитан: {path}");
                    return value;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка чтения файла {e}");
                return null;
            }
        }
        
        public static bool Exist(string path)
        {
            return new FileInfo(path).Exists;
        }

        public static void Remove(string filePath)
        {
            File.Delete(filePath);
        }

        public static void RemoveDir(string dirPath)
        {
            Directory.Delete(dirPath, true);
        }
    }
}
