﻿using EntityAbstractions.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Domain.Models.Entities.KnowledgeBase
{
    public enum VarType
    {
        Bool = 1,
        Select,
        Mutiselect
    }

    [Serializable]
    public class Variable : BaseEntity
    {
        public bool IsVisible { get; private set; } = true;

        public string Name { get; private set; }
        public string CodeName { get; private set; }

        public VarType VarType { get; private set; }
        
        public virtual ICollection<Parameter> Parameters { get; private set; }

        public static Variable CreateNew(string code, string name, VarType type, ICollection<Parameter> parameters = null, bool isVisible = true)
        {
            return new Variable
            {
                Name = name,
                CodeName = code,
                VarType = type,
                Parameters = parameters,
                IsVisible = isVisible
            };
        }

        public static Variable CreateNew(string code, string name, VarType type, Parameter parameter = null, bool isVisible = true)
        {
            return new Variable
            {
                Name = name,
                CodeName = code,
                VarType = type,
                Parameters = new List<Parameter> { parameter },
                IsVisible = isVisible
            };
        }
    }
}