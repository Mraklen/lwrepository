﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LW3.Models
{
    public class Simplex
    {
        public static double[] x10 = new double[3];
        public static double[] x20 = new double[3];
        public static double[] f1 = new double[3];

        public static int IndexMinValue()
        {
            return Array.FindIndex(f1, val => val == f1.Min());
        }

        public static List<PointXY> PrintPointXY()
        {
            return new List<PointXY>()
            {
                new PointXY() { X1 = x10[0], X2 = x20[0] },
                new PointXY() { X1 = x10[1], X2 = x20[1] },
                new PointXY() { X1 = x10[2], X2 = x20[2] },
                new PointXY() { X1 = x10[0], X2 = x20[0] }
            };
        }

        public static string PrintMinValue()
        {
            var index = IndexMinValue();

            return $"({Math.Round(x10[index], 2)} ; {Math.Round(x20[index], 2)})";
        }
    }
}
