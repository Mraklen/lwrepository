﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonVector
{
    public float x;
    public float y;
    public float z;

    public Vector3 GetVector3()
    {
        return new Vector3 { x = x, y = y, z = z };
    }
}

public class JsonMainPoint
{
    public JsonPoint Main;
    public JsonPoint FirstVector;
    public JsonPoint SecondVector;
}

public class JsonPoint
{
    public JsonVector Position;
}

public class JsonPoints
{
    public List<JsonMainPoint> MainPoints;
    public List<JsonPoint> SurfacePoints;
}

public class Point : MonoBehaviour {
    
    [Space(10)]
    [SerializeField] protected GameObject ModelPoint;

    [Space(10)]
    [SerializeField] protected Material currentMaterial;
    [SerializeField] protected Material lastMaterial;
    
    [Space(10)]
    [SerializeField] public bool isSelect = false;
    [SerializeField] public bool isMove = false;

    protected MeshRenderer ThisMesh;
    
    public Material CurrentMaterial
    {
        get { return currentMaterial; }
        set
        {
            lastMaterial = currentMaterial;
            currentMaterial = value;
            if (ThisMesh == null) ThisMesh = ModelPoint.GetComponent<MeshRenderer>();
            ThisMesh.material = value;
        }
    }

    private void Start()
    {
        if (!ModelPoint) ModelPoint = gameObject; 
        ThisMesh = ModelPoint.GetComponent<MeshRenderer>();
    }

    public JsonPoint GetJsonPoint()
    {
        return new JsonPoint
        {
            Position = new JsonVector { x = transform.position.x, y = transform.position.y, z = transform.position.z }
        };
    }
}
