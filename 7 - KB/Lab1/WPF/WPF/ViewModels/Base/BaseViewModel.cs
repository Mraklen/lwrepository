﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using WPF.State;

namespace WPF.ViewModels.Base
{
    class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected readonly AppStateStore stateStore;
        protected readonly IUnityContainer container;

        public BaseViewModel(AppStateStore stateStore, IUnityContainer container)
        {
            this.stateStore = stateStore;
            this.container = container;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
