﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LW2.RLE.Managers
{
    public class FileManager
    {
        public static void SaveFile(byte[] fileContent, string fileName)
        {
            using (var stream = new FileStream($"{Application.StartupPath}\\CompressedFiles\\{fileName}.rle", FileMode.OpenOrCreate))
            {
                stream.Write(fileContent, 0, fileContent.Length);
            }
        }

        public static byte[] LoadFile()
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = $"{Application.StartupPath}\\CompressedFiles";
            openFileDialog1.Filter = "rle files (*.rle)|*.rle|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            var bytes = new byte[myStream.Length];
                            myStream.Read(bytes, 0, bytes.Length);
                            return bytes;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            return new byte[0];
        }
    }
}
